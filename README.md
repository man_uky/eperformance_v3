ePerformance2
======

1. install nodejs, kemudian [konfigurasi npm](https://docs.npmjs.com/getting-started/fixing-npm-permissions)
2. install gassetic *npm install -g gassetic*
3. copy dan rename * *.html.twig.dist* di *app/Resources/views/cssJs* jadi * *.html.twig* 
4. di root directory, jalankan command *npm install*
5. Kalau mau menambahkan css atau javascript, edit file *gassetic.yml*, kemudian jalankan command *gassetic* (untuk production, jalankan *gassetic build --env=prod*)
<?php

namespace Eperformance\UnitMasterBundle\Controller;

use CoreBundle\Util\SessionFilter;
use CoreBundle\Model\Eperformance\UnitMasterQuery;
use Eperformance\UnitMasterBundle\Form\Type\FilterType;
use CoreBundle\Util\QueryFilterBuilder;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Symfony\Component\HttpFoundation\JsonResponse;
//use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UnitMasterController extends Controller
{
    /**
     * @Route("/unitmaster-index", name="unitmaster_index")
     */
    public function indexAction(Request $request)
    {
        $sessionFilter = new SessionFilter($this->get('session'), 'filters');
        if ($request->query->has('_reset')) {
            $sessionFilter->set([]);
        }

        $formFilter = $this->createForm(FilterType::class, $sessionFilter->get([]));
        $formFilter->handleRequest($request);  
        if ($formFilter->isSubmitted() && $formFilter->isValid()) {
            $sessionFilter->set($formFilter->getData());
        }

        return $this->render('EperformanceUnitMasterBundle::index.html.twig', [
            'formFilter' => $formFilter->createView()
        ]);
    }
    
    /**
     * @Route("/unitmaster-ajax", name="unitmaster_ajax")
     * @Method("GET")
     */
    public function listAjaxAction(Request $request)
    {
        $aaData = [];  
        
        $pager = $this->paginate($request);

        foreach ($pager->getResults() as $key => $v) {
            /*@var $v \CoreBundle\Model\Eperformance\UnitMaster*/
            $aaData[] = [
                ($key + 1) + ($pager->getMaxPerPage() * ($pager->getPage()-1)),
                $v->getKode(),
                $v->getNama(),
                $this->getStatusBooleanString($v->getIsSekretariatDaerah()),
                $this->getStatusBooleanString($v->getIsDinas()),
                $this->getStatusBooleanString($v->getIsBadan()),
                $this->getStatusBooleanString($v->getIsRumahSakit()),
                $this->getStatusBooleanString($v->getIsBagian()),
                $this->getStatusBooleanString($v->getIsKecamatan()),
                $this->getStatusBooleanString($v->getIsKelurahan()),
                $v->getParentId() > 0 ? $v->getUnitParent()->getNama() : '',
                $v->getUpdatedAt('Y-m-d H:i:s'),
                $this->buildActionsLink($v),
            ];
        }

        return new JsonResponse([
                "aaData" => $aaData,
                "iTotalRecords" => $pager->getNbResults(),
                "iTotalDisplayRecords" => $pager->getNbResults()
        ]);
    }
    
    private function paginate(Request $request)
    {   
        $query = UnitMasterQuery::create()
            ->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        
        $this->doSearching($request, $query);
        $this->doSorting($query, $request);

        $itemPerPage = $request->query->getInt('length', 10);
        $page = ($request->query->getInt('start', 0) / $itemPerPage) + 1;
     
        return $query->paginate($page, $itemPerPage);
    }
    
    private function doSearching(Request $request, UnitMasterQuery $query, $filters = null)
    {
        if (null === $filters) {
            $sessionFilter = new SessionFilter($request->getSession(), 'filters');
            $filters = $sessionFilter->get([]);
        }

        $filterBuilder = new QueryFilterBuilder();
        $filterBuilder->build($query, $filters);
    }
    
    private function doSorting(UnitMasterQuery $query, Request $request)
    {
        $orderColumns = [
            1 => 'Kode',
            2 => 'Nama',
            3 => 'IsSekretariatDaerah',
            4 => 'IsDinas',
            5 => 'IsBadan',
            6 => 'IsRumahSakit',
            7 => 'IsBagian',
            8 => 'IsKecamatan',
            9 => 'IsKelurahan',
            10 => 'UpdatedAt',
        ];
        
        $orders = $request->query->get('order');
  
        $column = 'Nama';
        $order = 'desc';
        if (count($orders) > 0 && isset($orderColumns[$orders[0]['column']])) {
            $column = $orderColumns[$orders[0]['column']];
            $order = $orders[0]['dir'];
        }
        
        $query->orderBy($column, $order);
    }
    
    private function getStatusBooleanString($status)
    {
        $icon = $status ? 'fa fa-check-circle' : 'fa fa-circle-thin';
        $retval = sprintf('<i class="%s"></i>', $icon);
        
        return $retval;
    }
    
    private function buildActionsLink($unitKerja)
    {
//        $link = new LinkUtil();
//        
//        $urlEdit = $link->linkEdit('@unit_kerja_master_edit?id='.$unitKerja->getId());
//        $urlDelete = $link->linkDelete('@unit_kerja_master_delete?id='.$unitKerja->getId(), 'Hapus', sprintf('Hapus data Unit Kerja %s?', $unitKerja->getNama()));
//        
//        return sprintf('%s &nbsp; %s', $urlEdit, $urlDelete);
        
        $urlEdit = $this->generateUrl('unitmaster_index', array('slug' => 'my-blog-post'));
        
        return sprintf("<a href=%s><i class='fa fa-edit'> Edit</i></a>", $urlEdit);
    }
}

<?php

namespace Eperformance\UnitMasterBundle\Form\Type;

use Propel\Bundle\PropelBundle\Form\Type\ModelType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class FilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) 
    {
        $builder
            ->add('kode', TextType::class, [
                'label' => 'Kode',
                'required' => false,
                'attr' => [
                    'autofocus' => true
                ]
            ])
            ->add('nama', TextType::class, [
                'label' => 'Nama',
                'required' => false
            ])
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver) 
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }
}
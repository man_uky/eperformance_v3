<?php

namespace Eperformance\UnitMasterBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UnitMasterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('kode');
        $builder->add('nama');
        $builder->add('isSekretariatDaerah');
        $builder->add('isDinas');
        $builder->add('isBadan');
        $builder->add('isRumahSakit');
        $builder->add('isBagian');
        $builder->add('isKecamatan');
        $builder->add('isKelurahan');
        $builder->add('unitMaster');
        $builder->add('createdAt');
        $builder->add('updatedAt');
        $builder->add('deletedAt');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'CoreBundle\Model\Eperformance\UnitMaster',
            'name'       => 'unitmaster',
        ]);
    }
}

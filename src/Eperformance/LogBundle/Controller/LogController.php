<?php

namespace Eperformance\LogBundle\Controller;

use CoreBundle\Model\Eperformance\UserLogQuery;
use CoreBundle\Util\QueryFilterBuilder;
use CoreBundle\Util\SessionFilter;

use Symfony\Component\HttpFoundation\Request;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class LogController extends Controller
{   
    /**
     * @Route("/userlog-ajax", name="userlog_ajax")
     * @Method("GET")
     */
    public function listAjaxAction(Request $request)
    {
        $aaData = [];  
        
        $pager = $this->paginate($request);

        foreach ($pager->getResults() as $key => $v) {
            /*@var $v \CoreBundle\Model\Eperformance\UserLog*/
            $aaData[] = [
                ($key + 1) + ($pager->getMaxPerPage() * ($pager->getPage()-1)),
                $v->getId(),
                $v->getParentId(),
                $v->getUsername(),
                $v->getAksi(),
                $v->getIpAddress(),
                $v->getCredential()->getNama(),
                $v->getCreatedAt('Y-m-d H:i:s'),
                "<a class='fa fa-chevron-down' href='javascript:;'></a>",
                'details' => [
                    'parameter' => $v->getParameter(),
                    'keterangan' => $v->getKeterangan(),
                ]
            ];
        }
        
        return new JsonResponse([
                "aaData" => $aaData,
                "iTotalRecords" => $pager->getNbResults(),
                "iTotalDisplayRecords" => $pager->getNbResults()
        ]);
    }
    
    private function paginate(Request $request)
    {   
        $loginId = null;
        
        if (!$this->getUser()->hasRoles('ROLE_SUPERADMIN')) {
            $loginId = $request->getSession()->get('loginId');
        }

        $query = UserLogQuery::create()
            ->setFormatter(ModelCriteria::FORMAT_ON_DEMAND)
            ->getListQuery($loginId);
        
        $this->doSearching($request, $query);
        $this->doSorting($query, $request);

        $itemPerPage = $request->query->getInt('length', 10);
        $page = ($request->query->getInt('start', 0) / $itemPerPage) + 1;
     
        return $query->paginate($page, $itemPerPage);
    }
    
    private function doSearching(Request $request, UserLogQuery $query, $filters = null)
    {
        if (null === $filters) {
            $sessionFilter = new SessionFilter($request->getSession(), 'filters');
            $filters = $sessionFilter->get([]);
        }

        $filterBuilder = new QueryFilterBuilder();
        $filterBuilder->build($query, $filters);
    }
    
    private function doSorting(UserLogQuery $query, Request $request)
    {
        $orderColumns = [
            1 => 'Id',
            2 => 'ParentId',
            3 => 'Username',
            4 => 'Aksi',
            5 => 'IpAddress',
            6 => 'CredentialId',
            7 => 'CreatedAt',
        ];
        
        $orders = $request->query->get('order');
  
        $column = 'Id';
        $order = 'desc';
        if (count($orders) > 0 && isset($orderColumns[$orders[0]['column']])) {
            $column = $orderColumns[$orders[0]['column']];
            $order = $orders[0]['dir'];
        }
        
        $query->orderBy($column, $order);
    }
}

<?php

namespace Eperformance\LogBundle\Form\Type;

use CoreBundle\Model\Eperformance\CredentialQuery;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CredentialType extends AbstractType
{   
    public function configureOptions(OptionsResolver $resolver)
    {
        $credentials = CredentialQuery::create()->orderById()->find();
        foreach ($credentials as $c) {
            $options[$c->getNama()] = $c->getId();
        }
        
        $resolver->setDefaults(array(
            'choices' => $options,
        ));
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}
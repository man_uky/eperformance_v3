<?php

namespace Eperformance\LogBundle\Form\Type;

use CoreBundle\Model\Eperformance\CredentialQuery;

use Propel\Bundle\PropelBundle\Form\Type\ModelType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class FilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) 
    {
        $builder
            ->add('username', TextType::class, [
                'label' => 'Username',
                'required' => false,
                'attr' => [
                    'autofocus' => true
                ]
            ])
            ->add('parent_id', TextType::class, [
                'label' => 'Parent ID',
                'required' => false
            ])
            ->add('credential_id', CredentialType::class, [
                'label' => 'Credential',
                'placeholder' => 'Choose an option',
                'required' => false,
                'choice_translation_domain' => false,
            ])
            /*
            ->add('credential_id', ModelType::class, [
                'class' => 'CoreBundle\Model\Eperformance\Credential',
                'query' => CredentialQuery::create()->orderById(),
                'label' => 'Credential',
                'required' => false,
                'placeholder' => 'Choose an option',
                'choice_translation_domain' => false,
                'choices_as_values' => true,
            ]) 
             */
            ->add('aksi', TextType::class, [
                'label' => 'Action',
                'required' => false
            ])
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver) 
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }
}
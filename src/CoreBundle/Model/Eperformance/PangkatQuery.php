<?php

namespace CoreBundle\Model\Eperformance;

use CoreBundle\Model\Eperformance\Base\PangkatQuery as BasePangkatQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'eperformance.pangkat' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PangkatQuery extends BasePangkatQuery
{

}

<?php

namespace CoreBundle\Model\Eperformance;

use CoreBundle\Model\Eperformance\Base\Pegawai as BasePegawai;

/**
 * Skeleton subclass for representing a row from the 'eperformance.pegawai' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Pegawai extends BasePegawai
{
    public function __toString()
    {
        return $this->getUsername();
    }
    
    public function usernameVerify($username)
    {
        if (!preg_match('/\s/', $username)) {
            $exist = PegawaiQuery::create()
                ->filterByUsername($username)
                ->count()
            ;

            if ($exist == 0) { return true; }
        }
        
        return false;
    }
    
    public function getSalt()
    {
        return '';
    }
    
    public function isEnabled()
    {
        return $this->isAktif();
    }
    
    public function getRoles()
    {
        return array('ROLE_USER', $this->getCredential()->getNama());
    }
    
    public function resetPassword()
    {
        return $this->setPassword($this->getUsername());
    }
    
    public function softDelete(PropelPDO $con = null)
    {
        if (null === $con) {
            $con = Propel::getConnection();
        }
        
        try {
            $con->beginTransaction();
            
            $this->setDeletedAt(time());
            $this->setIsAktif(false);
            $this->save($con);
            
            $con->commit();
        } catch (Exception $ex) {
            $con->rollBack();
            throw $ex;
        }
    }
}

<?php

namespace CoreBundle\Model\Eperformance\Base;

use \Exception;
use \PDO;
use CoreBundle\Model\Eperformance\Credential as ChildCredential;
use CoreBundle\Model\Eperformance\CredentialQuery as ChildCredentialQuery;
use CoreBundle\Model\Eperformance\Pegawai as ChildPegawai;
use CoreBundle\Model\Eperformance\PegawaiQuery as ChildPegawaiQuery;
use CoreBundle\Model\Eperformance\PenggunaMaster as ChildPenggunaMaster;
use CoreBundle\Model\Eperformance\PenggunaMasterQuery as ChildPenggunaMasterQuery;
use CoreBundle\Model\Eperformance\UserLog as ChildUserLog;
use CoreBundle\Model\Eperformance\UserLogQuery as ChildUserLogQuery;
use CoreBundle\Model\Eperformance\Map\CredentialTableMap;
use CoreBundle\Model\Eperformance\Map\PegawaiTableMap;
use CoreBundle\Model\Eperformance\Map\PenggunaMasterTableMap;
use CoreBundle\Model\Eperformance\Map\UserLogTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'eperformance.credential' table.
 *
 *
 *
 * @package    propel.generator.src.CoreBundle.Model.Eperformance.Base
 */
abstract class Credential implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\CoreBundle\\Model\\Eperformance\\Map\\CredentialTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the nama field.
     *
     * @var        string
     */
    protected $nama;

    /**
     * The value for the is_aktif field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_aktif;

    /**
     * @var        ObjectCollection|ChildUserLog[] Collection to store aggregation of ChildUserLog objects.
     */
    protected $collUserLogs;
    protected $collUserLogsPartial;

    /**
     * @var        ObjectCollection|ChildPenggunaMaster[] Collection to store aggregation of ChildPenggunaMaster objects.
     */
    protected $collPenggunaMasters;
    protected $collPenggunaMastersPartial;

    /**
     * @var        ObjectCollection|ChildPegawai[] Collection to store aggregation of ChildPegawai objects.
     */
    protected $collPegawais;
    protected $collPegawaisPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUserLog[]
     */
    protected $userLogsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPenggunaMaster[]
     */
    protected $penggunaMastersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPegawai[]
     */
    protected $pegawaisScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->is_aktif = false;
    }

    /**
     * Initializes internal state of CoreBundle\Model\Eperformance\Base\Credential object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Credential</code> instance.  If
     * <code>obj</code> is an instance of <code>Credential</code>, delegates to
     * <code>equals(Credential)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Credential The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [nama] column value.
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [is_aktif] column value.
     *
     * @return boolean
     */
    public function getIsAktif()
    {
        return $this->is_aktif;
    }

    /**
     * Get the [is_aktif] column value.
     *
     * @return boolean
     */
    public function isAktif()
    {
        return $this->getIsAktif();
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\Eperformance\Credential The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[CredentialTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [nama] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\Eperformance\Credential The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[CredentialTableMap::COL_NAMA] = true;
        }

        return $this;
    } // setNama()

    /**
     * Sets the value of the [is_aktif] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\Eperformance\Credential The current object (for fluent API support)
     */
    public function setIsAktif($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_aktif !== $v) {
            $this->is_aktif = $v;
            $this->modifiedColumns[CredentialTableMap::COL_IS_AKTIF] = true;
        }

        return $this;
    } // setIsAktif()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->is_aktif !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CredentialTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CredentialTableMap::translateFieldName('Nama', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nama = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CredentialTableMap::translateFieldName('IsAktif', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_aktif = (null !== $col) ? (boolean) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 3; // 3 = CredentialTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\CoreBundle\\Model\\Eperformance\\Credential'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CredentialTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCredentialQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collUserLogs = null;

            $this->collPenggunaMasters = null;

            $this->collPegawais = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Credential::setDeleted()
     * @see Credential::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CredentialTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCredentialQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CredentialTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CredentialTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->userLogsScheduledForDeletion !== null) {
                if (!$this->userLogsScheduledForDeletion->isEmpty()) {
                    \CoreBundle\Model\Eperformance\UserLogQuery::create()
                        ->filterByPrimaryKeys($this->userLogsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->userLogsScheduledForDeletion = null;
                }
            }

            if ($this->collUserLogs !== null) {
                foreach ($this->collUserLogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->penggunaMastersScheduledForDeletion !== null) {
                if (!$this->penggunaMastersScheduledForDeletion->isEmpty()) {
                    \CoreBundle\Model\Eperformance\PenggunaMasterQuery::create()
                        ->filterByPrimaryKeys($this->penggunaMastersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->penggunaMastersScheduledForDeletion = null;
                }
            }

            if ($this->collPenggunaMasters !== null) {
                foreach ($this->collPenggunaMasters as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pegawaisScheduledForDeletion !== null) {
                if (!$this->pegawaisScheduledForDeletion->isEmpty()) {
                    \CoreBundle\Model\Eperformance\PegawaiQuery::create()
                        ->filterByPrimaryKeys($this->pegawaisScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pegawaisScheduledForDeletion = null;
                }
            }

            if ($this->collPegawais !== null) {
                foreach ($this->collPegawais as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[CredentialTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CredentialTableMap::COL_ID . ')');
        }
        if (null === $this->id) {
            try {
                $dataFetcher = $con->query("SELECT nextval('eperformance.credential_id_seq')");
                $this->id = (int) $dataFetcher->fetchColumn();
            } catch (Exception $e) {
                throw new PropelException('Unable to get sequence id.', 0, $e);
            }
        }


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CredentialTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(CredentialTableMap::COL_NAMA)) {
            $modifiedColumns[':p' . $index++]  = 'nama';
        }
        if ($this->isColumnModified(CredentialTableMap::COL_IS_AKTIF)) {
            $modifiedColumns[':p' . $index++]  = 'is_aktif';
        }

        $sql = sprintf(
            'INSERT INTO eperformance.credential (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'nama':
                        $stmt->bindValue($identifier, $this->nama, PDO::PARAM_STR);
                        break;
                    case 'is_aktif':
                        $stmt->bindValue($identifier, $this->is_aktif, PDO::PARAM_BOOL);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CredentialTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getNama();
                break;
            case 2:
                return $this->getIsAktif();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Credential'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Credential'][$this->hashCode()] = true;
        $keys = CredentialTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getNama(),
            $keys[2] => $this->getIsAktif(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collUserLogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'userLogs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.user_logs';
                        break;
                    default:
                        $key = 'UserLogs';
                }

                $result[$key] = $this->collUserLogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPenggunaMasters) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'penggunaMasters';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.pengguna_masters';
                        break;
                    default:
                        $key = 'PenggunaMasters';
                }

                $result[$key] = $this->collPenggunaMasters->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPegawais) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pegawais';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.pegawais';
                        break;
                    default:
                        $key = 'Pegawais';
                }

                $result[$key] = $this->collPegawais->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\CoreBundle\Model\Eperformance\Credential
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CredentialTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\CoreBundle\Model\Eperformance\Credential
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setNama($value);
                break;
            case 2:
                $this->setIsAktif($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = CredentialTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setNama($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setIsAktif($arr[$keys[2]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\CoreBundle\Model\Eperformance\Credential The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CredentialTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CredentialTableMap::COL_ID)) {
            $criteria->add(CredentialTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(CredentialTableMap::COL_NAMA)) {
            $criteria->add(CredentialTableMap::COL_NAMA, $this->nama);
        }
        if ($this->isColumnModified(CredentialTableMap::COL_IS_AKTIF)) {
            $criteria->add(CredentialTableMap::COL_IS_AKTIF, $this->is_aktif);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCredentialQuery::create();
        $criteria->add(CredentialTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \CoreBundle\Model\Eperformance\Credential (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNama($this->getNama());
        $copyObj->setIsAktif($this->getIsAktif());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getUserLogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUserLog($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPenggunaMasters() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPenggunaMaster($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPegawais() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPegawai($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \CoreBundle\Model\Eperformance\Credential Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('UserLog' == $relationName) {
            return $this->initUserLogs();
        }
        if ('PenggunaMaster' == $relationName) {
            return $this->initPenggunaMasters();
        }
        if ('Pegawai' == $relationName) {
            return $this->initPegawais();
        }
    }

    /**
     * Clears out the collUserLogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUserLogs()
     */
    public function clearUserLogs()
    {
        $this->collUserLogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUserLogs collection loaded partially.
     */
    public function resetPartialUserLogs($v = true)
    {
        $this->collUserLogsPartial = $v;
    }

    /**
     * Initializes the collUserLogs collection.
     *
     * By default this just sets the collUserLogs collection to an empty array (like clearcollUserLogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUserLogs($overrideExisting = true)
    {
        if (null !== $this->collUserLogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = UserLogTableMap::getTableMap()->getCollectionClassName();

        $this->collUserLogs = new $collectionClassName;
        $this->collUserLogs->setModel('\CoreBundle\Model\Eperformance\UserLog');
    }

    /**
     * Gets an array of ChildUserLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCredential is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUserLog[] List of ChildUserLog objects
     * @throws PropelException
     */
    public function getUserLogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUserLogsPartial && !$this->isNew();
        if (null === $this->collUserLogs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUserLogs) {
                // return empty collection
                $this->initUserLogs();
            } else {
                $collUserLogs = ChildUserLogQuery::create(null, $criteria)
                    ->filterByCredential($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUserLogsPartial && count($collUserLogs)) {
                        $this->initUserLogs(false);

                        foreach ($collUserLogs as $obj) {
                            if (false == $this->collUserLogs->contains($obj)) {
                                $this->collUserLogs->append($obj);
                            }
                        }

                        $this->collUserLogsPartial = true;
                    }

                    return $collUserLogs;
                }

                if ($partial && $this->collUserLogs) {
                    foreach ($this->collUserLogs as $obj) {
                        if ($obj->isNew()) {
                            $collUserLogs[] = $obj;
                        }
                    }
                }

                $this->collUserLogs = $collUserLogs;
                $this->collUserLogsPartial = false;
            }
        }

        return $this->collUserLogs;
    }

    /**
     * Sets a collection of ChildUserLog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $userLogs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCredential The current object (for fluent API support)
     */
    public function setUserLogs(Collection $userLogs, ConnectionInterface $con = null)
    {
        /** @var ChildUserLog[] $userLogsToDelete */
        $userLogsToDelete = $this->getUserLogs(new Criteria(), $con)->diff($userLogs);


        $this->userLogsScheduledForDeletion = $userLogsToDelete;

        foreach ($userLogsToDelete as $userLogRemoved) {
            $userLogRemoved->setCredential(null);
        }

        $this->collUserLogs = null;
        foreach ($userLogs as $userLog) {
            $this->addUserLog($userLog);
        }

        $this->collUserLogs = $userLogs;
        $this->collUserLogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UserLog objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UserLog objects.
     * @throws PropelException
     */
    public function countUserLogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUserLogsPartial && !$this->isNew();
        if (null === $this->collUserLogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUserLogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUserLogs());
            }

            $query = ChildUserLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCredential($this)
                ->count($con);
        }

        return count($this->collUserLogs);
    }

    /**
     * Method called to associate a ChildUserLog object to this object
     * through the ChildUserLog foreign key attribute.
     *
     * @param  ChildUserLog $l ChildUserLog
     * @return $this|\CoreBundle\Model\Eperformance\Credential The current object (for fluent API support)
     */
    public function addUserLog(ChildUserLog $l)
    {
        if ($this->collUserLogs === null) {
            $this->initUserLogs();
            $this->collUserLogsPartial = true;
        }

        if (!$this->collUserLogs->contains($l)) {
            $this->doAddUserLog($l);

            if ($this->userLogsScheduledForDeletion and $this->userLogsScheduledForDeletion->contains($l)) {
                $this->userLogsScheduledForDeletion->remove($this->userLogsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUserLog $userLog The ChildUserLog object to add.
     */
    protected function doAddUserLog(ChildUserLog $userLog)
    {
        $this->collUserLogs[]= $userLog;
        $userLog->setCredential($this);
    }

    /**
     * @param  ChildUserLog $userLog The ChildUserLog object to remove.
     * @return $this|ChildCredential The current object (for fluent API support)
     */
    public function removeUserLog(ChildUserLog $userLog)
    {
        if ($this->getUserLogs()->contains($userLog)) {
            $pos = $this->collUserLogs->search($userLog);
            $this->collUserLogs->remove($pos);
            if (null === $this->userLogsScheduledForDeletion) {
                $this->userLogsScheduledForDeletion = clone $this->collUserLogs;
                $this->userLogsScheduledForDeletion->clear();
            }
            $this->userLogsScheduledForDeletion[]= clone $userLog;
            $userLog->setCredential(null);
        }

        return $this;
    }

    /**
     * Clears out the collPenggunaMasters collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPenggunaMasters()
     */
    public function clearPenggunaMasters()
    {
        $this->collPenggunaMasters = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPenggunaMasters collection loaded partially.
     */
    public function resetPartialPenggunaMasters($v = true)
    {
        $this->collPenggunaMastersPartial = $v;
    }

    /**
     * Initializes the collPenggunaMasters collection.
     *
     * By default this just sets the collPenggunaMasters collection to an empty array (like clearcollPenggunaMasters());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPenggunaMasters($overrideExisting = true)
    {
        if (null !== $this->collPenggunaMasters && !$overrideExisting) {
            return;
        }

        $collectionClassName = PenggunaMasterTableMap::getTableMap()->getCollectionClassName();

        $this->collPenggunaMasters = new $collectionClassName;
        $this->collPenggunaMasters->setModel('\CoreBundle\Model\Eperformance\PenggunaMaster');
    }

    /**
     * Gets an array of ChildPenggunaMaster objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCredential is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPenggunaMaster[] List of ChildPenggunaMaster objects
     * @throws PropelException
     */
    public function getPenggunaMasters(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPenggunaMastersPartial && !$this->isNew();
        if (null === $this->collPenggunaMasters || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPenggunaMasters) {
                // return empty collection
                $this->initPenggunaMasters();
            } else {
                $collPenggunaMasters = ChildPenggunaMasterQuery::create(null, $criteria)
                    ->filterByCredential($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPenggunaMastersPartial && count($collPenggunaMasters)) {
                        $this->initPenggunaMasters(false);

                        foreach ($collPenggunaMasters as $obj) {
                            if (false == $this->collPenggunaMasters->contains($obj)) {
                                $this->collPenggunaMasters->append($obj);
                            }
                        }

                        $this->collPenggunaMastersPartial = true;
                    }

                    return $collPenggunaMasters;
                }

                if ($partial && $this->collPenggunaMasters) {
                    foreach ($this->collPenggunaMasters as $obj) {
                        if ($obj->isNew()) {
                            $collPenggunaMasters[] = $obj;
                        }
                    }
                }

                $this->collPenggunaMasters = $collPenggunaMasters;
                $this->collPenggunaMastersPartial = false;
            }
        }

        return $this->collPenggunaMasters;
    }

    /**
     * Sets a collection of ChildPenggunaMaster objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $penggunaMasters A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCredential The current object (for fluent API support)
     */
    public function setPenggunaMasters(Collection $penggunaMasters, ConnectionInterface $con = null)
    {
        /** @var ChildPenggunaMaster[] $penggunaMastersToDelete */
        $penggunaMastersToDelete = $this->getPenggunaMasters(new Criteria(), $con)->diff($penggunaMasters);


        $this->penggunaMastersScheduledForDeletion = $penggunaMastersToDelete;

        foreach ($penggunaMastersToDelete as $penggunaMasterRemoved) {
            $penggunaMasterRemoved->setCredential(null);
        }

        $this->collPenggunaMasters = null;
        foreach ($penggunaMasters as $penggunaMaster) {
            $this->addPenggunaMaster($penggunaMaster);
        }

        $this->collPenggunaMasters = $penggunaMasters;
        $this->collPenggunaMastersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PenggunaMaster objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PenggunaMaster objects.
     * @throws PropelException
     */
    public function countPenggunaMasters(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPenggunaMastersPartial && !$this->isNew();
        if (null === $this->collPenggunaMasters || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPenggunaMasters) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPenggunaMasters());
            }

            $query = ChildPenggunaMasterQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCredential($this)
                ->count($con);
        }

        return count($this->collPenggunaMasters);
    }

    /**
     * Method called to associate a ChildPenggunaMaster object to this object
     * through the ChildPenggunaMaster foreign key attribute.
     *
     * @param  ChildPenggunaMaster $l ChildPenggunaMaster
     * @return $this|\CoreBundle\Model\Eperformance\Credential The current object (for fluent API support)
     */
    public function addPenggunaMaster(ChildPenggunaMaster $l)
    {
        if ($this->collPenggunaMasters === null) {
            $this->initPenggunaMasters();
            $this->collPenggunaMastersPartial = true;
        }

        if (!$this->collPenggunaMasters->contains($l)) {
            $this->doAddPenggunaMaster($l);

            if ($this->penggunaMastersScheduledForDeletion and $this->penggunaMastersScheduledForDeletion->contains($l)) {
                $this->penggunaMastersScheduledForDeletion->remove($this->penggunaMastersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPenggunaMaster $penggunaMaster The ChildPenggunaMaster object to add.
     */
    protected function doAddPenggunaMaster(ChildPenggunaMaster $penggunaMaster)
    {
        $this->collPenggunaMasters[]= $penggunaMaster;
        $penggunaMaster->setCredential($this);
    }

    /**
     * @param  ChildPenggunaMaster $penggunaMaster The ChildPenggunaMaster object to remove.
     * @return $this|ChildCredential The current object (for fluent API support)
     */
    public function removePenggunaMaster(ChildPenggunaMaster $penggunaMaster)
    {
        if ($this->getPenggunaMasters()->contains($penggunaMaster)) {
            $pos = $this->collPenggunaMasters->search($penggunaMaster);
            $this->collPenggunaMasters->remove($pos);
            if (null === $this->penggunaMastersScheduledForDeletion) {
                $this->penggunaMastersScheduledForDeletion = clone $this->collPenggunaMasters;
                $this->penggunaMastersScheduledForDeletion->clear();
            }
            $this->penggunaMastersScheduledForDeletion[]= clone $penggunaMaster;
            $penggunaMaster->setCredential(null);
        }

        return $this;
    }

    /**
     * Clears out the collPegawais collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPegawais()
     */
    public function clearPegawais()
    {
        $this->collPegawais = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPegawais collection loaded partially.
     */
    public function resetPartialPegawais($v = true)
    {
        $this->collPegawaisPartial = $v;
    }

    /**
     * Initializes the collPegawais collection.
     *
     * By default this just sets the collPegawais collection to an empty array (like clearcollPegawais());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPegawais($overrideExisting = true)
    {
        if (null !== $this->collPegawais && !$overrideExisting) {
            return;
        }

        $collectionClassName = PegawaiTableMap::getTableMap()->getCollectionClassName();

        $this->collPegawais = new $collectionClassName;
        $this->collPegawais->setModel('\CoreBundle\Model\Eperformance\Pegawai');
    }

    /**
     * Gets an array of ChildPegawai objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCredential is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     * @throws PropelException
     */
    public function getPegawais(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPegawaisPartial && !$this->isNew();
        if (null === $this->collPegawais || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPegawais) {
                // return empty collection
                $this->initPegawais();
            } else {
                $collPegawais = ChildPegawaiQuery::create(null, $criteria)
                    ->filterByCredential($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPegawaisPartial && count($collPegawais)) {
                        $this->initPegawais(false);

                        foreach ($collPegawais as $obj) {
                            if (false == $this->collPegawais->contains($obj)) {
                                $this->collPegawais->append($obj);
                            }
                        }

                        $this->collPegawaisPartial = true;
                    }

                    return $collPegawais;
                }

                if ($partial && $this->collPegawais) {
                    foreach ($this->collPegawais as $obj) {
                        if ($obj->isNew()) {
                            $collPegawais[] = $obj;
                        }
                    }
                }

                $this->collPegawais = $collPegawais;
                $this->collPegawaisPartial = false;
            }
        }

        return $this->collPegawais;
    }

    /**
     * Sets a collection of ChildPegawai objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $pegawais A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCredential The current object (for fluent API support)
     */
    public function setPegawais(Collection $pegawais, ConnectionInterface $con = null)
    {
        /** @var ChildPegawai[] $pegawaisToDelete */
        $pegawaisToDelete = $this->getPegawais(new Criteria(), $con)->diff($pegawais);


        $this->pegawaisScheduledForDeletion = $pegawaisToDelete;

        foreach ($pegawaisToDelete as $pegawaiRemoved) {
            $pegawaiRemoved->setCredential(null);
        }

        $this->collPegawais = null;
        foreach ($pegawais as $pegawai) {
            $this->addPegawai($pegawai);
        }

        $this->collPegawais = $pegawais;
        $this->collPegawaisPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Pegawai objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Pegawai objects.
     * @throws PropelException
     */
    public function countPegawais(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPegawaisPartial && !$this->isNew();
        if (null === $this->collPegawais || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPegawais) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPegawais());
            }

            $query = ChildPegawaiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCredential($this)
                ->count($con);
        }

        return count($this->collPegawais);
    }

    /**
     * Method called to associate a ChildPegawai object to this object
     * through the ChildPegawai foreign key attribute.
     *
     * @param  ChildPegawai $l ChildPegawai
     * @return $this|\CoreBundle\Model\Eperformance\Credential The current object (for fluent API support)
     */
    public function addPegawai(ChildPegawai $l)
    {
        if ($this->collPegawais === null) {
            $this->initPegawais();
            $this->collPegawaisPartial = true;
        }

        if (!$this->collPegawais->contains($l)) {
            $this->doAddPegawai($l);

            if ($this->pegawaisScheduledForDeletion and $this->pegawaisScheduledForDeletion->contains($l)) {
                $this->pegawaisScheduledForDeletion->remove($this->pegawaisScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPegawai $pegawai The ChildPegawai object to add.
     */
    protected function doAddPegawai(ChildPegawai $pegawai)
    {
        $this->collPegawais[]= $pegawai;
        $pegawai->setCredential($this);
    }

    /**
     * @param  ChildPegawai $pegawai The ChildPegawai object to remove.
     * @return $this|ChildCredential The current object (for fluent API support)
     */
    public function removePegawai(ChildPegawai $pegawai)
    {
        if ($this->getPegawais()->contains($pegawai)) {
            $pos = $this->collPegawais->search($pegawai);
            $this->collPegawais->remove($pos);
            if (null === $this->pegawaisScheduledForDeletion) {
                $this->pegawaisScheduledForDeletion = clone $this->collPegawais;
                $this->pegawaisScheduledForDeletion->clear();
            }
            $this->pegawaisScheduledForDeletion[]= clone $pegawai;
            $pegawai->setCredential(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Credential is new, it will return
     * an empty collection; or if this Credential has previously
     * been saved, it will retrieve related Pegawais from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Credential.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     */
    public function getPegawaisJoinPegawaiMaster(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPegawaiQuery::create(null, $criteria);
        $query->joinWith('PegawaiMaster', $joinBehavior);

        return $this->getPegawais($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Credential is new, it will return
     * an empty collection; or if this Credential has previously
     * been saved, it will retrieve related Pegawais from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Credential.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     */
    public function getPegawaisJoinUnitMaster(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPegawaiQuery::create(null, $criteria);
        $query->joinWith('UnitMaster', $joinBehavior);

        return $this->getPegawais($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Credential is new, it will return
     * an empty collection; or if this Credential has previously
     * been saved, it will retrieve related Pegawais from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Credential.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     */
    public function getPegawaisJoinPegawaiAtasan(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPegawaiQuery::create(null, $criteria);
        $query->joinWith('PegawaiAtasan', $joinBehavior);

        return $this->getPegawais($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Credential is new, it will return
     * an empty collection; or if this Credential has previously
     * been saved, it will retrieve related Pegawais from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Credential.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     */
    public function getPegawaisJoinPangkat(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPegawaiQuery::create(null, $criteria);
        $query->joinWith('Pangkat', $joinBehavior);

        return $this->getPegawais($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Credential is new, it will return
     * an empty collection; or if this Credential has previously
     * been saved, it will retrieve related Pegawais from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Credential.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     */
    public function getPegawaisJoinJabatan(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPegawaiQuery::create(null, $criteria);
        $query->joinWith('Jabatan', $joinBehavior);

        return $this->getPegawais($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Credential is new, it will return
     * an empty collection; or if this Credential has previously
     * been saved, it will retrieve related Pegawais from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Credential.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     */
    public function getPegawaisJoinPegawaiStatus(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPegawaiQuery::create(null, $criteria);
        $query->joinWith('PegawaiStatus', $joinBehavior);

        return $this->getPegawais($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->nama = null;
        $this->is_aktif = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collUserLogs) {
                foreach ($this->collUserLogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPenggunaMasters) {
                foreach ($this->collPenggunaMasters as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPegawais) {
                foreach ($this->collPegawais as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collUserLogs = null;
        $this->collPenggunaMasters = null;
        $this->collPegawais = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CredentialTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}

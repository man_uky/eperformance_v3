<?php

namespace CoreBundle\Model\Eperformance\Base;

use \Exception;
use \PDO;
use CoreBundle\Model\Eperformance\Jabatan as ChildJabatan;
use CoreBundle\Model\Eperformance\JabatanQuery as ChildJabatanQuery;
use CoreBundle\Model\Eperformance\Map\JabatanTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'eperformance.jabatan' table.
 *
 *
 *
 * @method     ChildJabatanQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildJabatanQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method     ChildJabatanQuery orderByUnitMasterId($order = Criteria::ASC) Order by the unit_master_id column
 * @method     ChildJabatanQuery orderByUrut($order = Criteria::ASC) Order by the urut column
 * @method     ChildJabatanQuery orderByIsStaf($order = Criteria::ASC) Order by the is_staf column
 * @method     ChildJabatanQuery orderByIsJfu($order = Criteria::ASC) Order by the is_jfu column
 * @method     ChildJabatanQuery orderByIsJft($order = Criteria::ASC) Order by the is_jft column
 * @method     ChildJabatanQuery orderByIsStafAhli($order = Criteria::ASC) Order by the is_staf_ahli column
 * @method     ChildJabatanQuery orderByIsAsisten($order = Criteria::ASC) Order by the is_asisten column
 * @method     ChildJabatanQuery orderByIsSekda($order = Criteria::ASC) Order by the is_sekda column
 * @method     ChildJabatanQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildJabatanQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildJabatanQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 *
 * @method     ChildJabatanQuery groupById() Group by the id column
 * @method     ChildJabatanQuery groupByNama() Group by the nama column
 * @method     ChildJabatanQuery groupByUnitMasterId() Group by the unit_master_id column
 * @method     ChildJabatanQuery groupByUrut() Group by the urut column
 * @method     ChildJabatanQuery groupByIsStaf() Group by the is_staf column
 * @method     ChildJabatanQuery groupByIsJfu() Group by the is_jfu column
 * @method     ChildJabatanQuery groupByIsJft() Group by the is_jft column
 * @method     ChildJabatanQuery groupByIsStafAhli() Group by the is_staf_ahli column
 * @method     ChildJabatanQuery groupByIsAsisten() Group by the is_asisten column
 * @method     ChildJabatanQuery groupByIsSekda() Group by the is_sekda column
 * @method     ChildJabatanQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildJabatanQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildJabatanQuery groupByDeletedAt() Group by the deleted_at column
 *
 * @method     ChildJabatanQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildJabatanQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildJabatanQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildJabatanQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildJabatanQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildJabatanQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildJabatanQuery leftJoinUnitMaster($relationAlias = null) Adds a LEFT JOIN clause to the query using the UnitMaster relation
 * @method     ChildJabatanQuery rightJoinUnitMaster($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UnitMaster relation
 * @method     ChildJabatanQuery innerJoinUnitMaster($relationAlias = null) Adds a INNER JOIN clause to the query using the UnitMaster relation
 *
 * @method     ChildJabatanQuery joinWithUnitMaster($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UnitMaster relation
 *
 * @method     ChildJabatanQuery leftJoinWithUnitMaster() Adds a LEFT JOIN clause and with to the query using the UnitMaster relation
 * @method     ChildJabatanQuery rightJoinWithUnitMaster() Adds a RIGHT JOIN clause and with to the query using the UnitMaster relation
 * @method     ChildJabatanQuery innerJoinWithUnitMaster() Adds a INNER JOIN clause and with to the query using the UnitMaster relation
 *
 * @method     ChildJabatanQuery leftJoinPegawai($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pegawai relation
 * @method     ChildJabatanQuery rightJoinPegawai($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pegawai relation
 * @method     ChildJabatanQuery innerJoinPegawai($relationAlias = null) Adds a INNER JOIN clause to the query using the Pegawai relation
 *
 * @method     ChildJabatanQuery joinWithPegawai($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Pegawai relation
 *
 * @method     ChildJabatanQuery leftJoinWithPegawai() Adds a LEFT JOIN clause and with to the query using the Pegawai relation
 * @method     ChildJabatanQuery rightJoinWithPegawai() Adds a RIGHT JOIN clause and with to the query using the Pegawai relation
 * @method     ChildJabatanQuery innerJoinWithPegawai() Adds a INNER JOIN clause and with to the query using the Pegawai relation
 *
 * @method     \CoreBundle\Model\Eperformance\UnitMasterQuery|\CoreBundle\Model\Eperformance\PegawaiQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildJabatan findOne(ConnectionInterface $con = null) Return the first ChildJabatan matching the query
 * @method     ChildJabatan findOneOrCreate(ConnectionInterface $con = null) Return the first ChildJabatan matching the query, or a new ChildJabatan object populated from the query conditions when no match is found
 *
 * @method     ChildJabatan findOneById(int $id) Return the first ChildJabatan filtered by the id column
 * @method     ChildJabatan findOneByNama(string $nama) Return the first ChildJabatan filtered by the nama column
 * @method     ChildJabatan findOneByUnitMasterId(int $unit_master_id) Return the first ChildJabatan filtered by the unit_master_id column
 * @method     ChildJabatan findOneByUrut(int $urut) Return the first ChildJabatan filtered by the urut column
 * @method     ChildJabatan findOneByIsStaf(boolean $is_staf) Return the first ChildJabatan filtered by the is_staf column
 * @method     ChildJabatan findOneByIsJfu(boolean $is_jfu) Return the first ChildJabatan filtered by the is_jfu column
 * @method     ChildJabatan findOneByIsJft(boolean $is_jft) Return the first ChildJabatan filtered by the is_jft column
 * @method     ChildJabatan findOneByIsStafAhli(boolean $is_staf_ahli) Return the first ChildJabatan filtered by the is_staf_ahli column
 * @method     ChildJabatan findOneByIsAsisten(boolean $is_asisten) Return the first ChildJabatan filtered by the is_asisten column
 * @method     ChildJabatan findOneByIsSekda(boolean $is_sekda) Return the first ChildJabatan filtered by the is_sekda column
 * @method     ChildJabatan findOneByCreatedAt(string $created_at) Return the first ChildJabatan filtered by the created_at column
 * @method     ChildJabatan findOneByUpdatedAt(string $updated_at) Return the first ChildJabatan filtered by the updated_at column
 * @method     ChildJabatan findOneByDeletedAt(string $deleted_at) Return the first ChildJabatan filtered by the deleted_at column *

 * @method     ChildJabatan requirePk($key, ConnectionInterface $con = null) Return the ChildJabatan by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJabatan requireOne(ConnectionInterface $con = null) Return the first ChildJabatan matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildJabatan requireOneById(int $id) Return the first ChildJabatan filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJabatan requireOneByNama(string $nama) Return the first ChildJabatan filtered by the nama column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJabatan requireOneByUnitMasterId(int $unit_master_id) Return the first ChildJabatan filtered by the unit_master_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJabatan requireOneByUrut(int $urut) Return the first ChildJabatan filtered by the urut column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJabatan requireOneByIsStaf(boolean $is_staf) Return the first ChildJabatan filtered by the is_staf column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJabatan requireOneByIsJfu(boolean $is_jfu) Return the first ChildJabatan filtered by the is_jfu column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJabatan requireOneByIsJft(boolean $is_jft) Return the first ChildJabatan filtered by the is_jft column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJabatan requireOneByIsStafAhli(boolean $is_staf_ahli) Return the first ChildJabatan filtered by the is_staf_ahli column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJabatan requireOneByIsAsisten(boolean $is_asisten) Return the first ChildJabatan filtered by the is_asisten column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJabatan requireOneByIsSekda(boolean $is_sekda) Return the first ChildJabatan filtered by the is_sekda column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJabatan requireOneByCreatedAt(string $created_at) Return the first ChildJabatan filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJabatan requireOneByUpdatedAt(string $updated_at) Return the first ChildJabatan filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJabatan requireOneByDeletedAt(string $deleted_at) Return the first ChildJabatan filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildJabatan[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildJabatan objects based on current ModelCriteria
 * @method     ChildJabatan[]|ObjectCollection findById(int $id) Return ChildJabatan objects filtered by the id column
 * @method     ChildJabatan[]|ObjectCollection findByNama(string $nama) Return ChildJabatan objects filtered by the nama column
 * @method     ChildJabatan[]|ObjectCollection findByUnitMasterId(int $unit_master_id) Return ChildJabatan objects filtered by the unit_master_id column
 * @method     ChildJabatan[]|ObjectCollection findByUrut(int $urut) Return ChildJabatan objects filtered by the urut column
 * @method     ChildJabatan[]|ObjectCollection findByIsStaf(boolean $is_staf) Return ChildJabatan objects filtered by the is_staf column
 * @method     ChildJabatan[]|ObjectCollection findByIsJfu(boolean $is_jfu) Return ChildJabatan objects filtered by the is_jfu column
 * @method     ChildJabatan[]|ObjectCollection findByIsJft(boolean $is_jft) Return ChildJabatan objects filtered by the is_jft column
 * @method     ChildJabatan[]|ObjectCollection findByIsStafAhli(boolean $is_staf_ahli) Return ChildJabatan objects filtered by the is_staf_ahli column
 * @method     ChildJabatan[]|ObjectCollection findByIsAsisten(boolean $is_asisten) Return ChildJabatan objects filtered by the is_asisten column
 * @method     ChildJabatan[]|ObjectCollection findByIsSekda(boolean $is_sekda) Return ChildJabatan objects filtered by the is_sekda column
 * @method     ChildJabatan[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildJabatan objects filtered by the created_at column
 * @method     ChildJabatan[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildJabatan objects filtered by the updated_at column
 * @method     ChildJabatan[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildJabatan objects filtered by the deleted_at column
 * @method     ChildJabatan[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class JabatanQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \CoreBundle\Model\Eperformance\Base\JabatanQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\CoreBundle\\Model\\Eperformance\\Jabatan', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildJabatanQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildJabatanQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildJabatanQuery) {
            return $criteria;
        }
        $query = new ChildJabatanQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildJabatan|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(JabatanTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = JabatanTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildJabatan A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, nama, unit_master_id, urut, is_staf, is_jfu, is_jft, is_staf_ahli, is_asisten, is_sekda, created_at, updated_at, deleted_at FROM eperformance.jabatan WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildJabatan $obj */
            $obj = new ChildJabatan();
            $obj->hydrate($row);
            JabatanTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildJabatan|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(JabatanTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(JabatanTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(JabatanTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(JabatanTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JabatanTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%', Criteria::LIKE); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JabatanTableMap::COL_NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the unit_master_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitMasterId(1234); // WHERE unit_master_id = 1234
     * $query->filterByUnitMasterId(array(12, 34)); // WHERE unit_master_id IN (12, 34)
     * $query->filterByUnitMasterId(array('min' => 12)); // WHERE unit_master_id > 12
     * </code>
     *
     * @see       filterByUnitMaster()
     *
     * @param     mixed $unitMasterId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function filterByUnitMasterId($unitMasterId = null, $comparison = null)
    {
        if (is_array($unitMasterId)) {
            $useMinMax = false;
            if (isset($unitMasterId['min'])) {
                $this->addUsingAlias(JabatanTableMap::COL_UNIT_MASTER_ID, $unitMasterId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($unitMasterId['max'])) {
                $this->addUsingAlias(JabatanTableMap::COL_UNIT_MASTER_ID, $unitMasterId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JabatanTableMap::COL_UNIT_MASTER_ID, $unitMasterId, $comparison);
    }

    /**
     * Filter the query on the urut column
     *
     * Example usage:
     * <code>
     * $query->filterByUrut(1234); // WHERE urut = 1234
     * $query->filterByUrut(array(12, 34)); // WHERE urut IN (12, 34)
     * $query->filterByUrut(array('min' => 12)); // WHERE urut > 12
     * </code>
     *
     * @param     mixed $urut The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function filterByUrut($urut = null, $comparison = null)
    {
        if (is_array($urut)) {
            $useMinMax = false;
            if (isset($urut['min'])) {
                $this->addUsingAlias(JabatanTableMap::COL_URUT, $urut['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($urut['max'])) {
                $this->addUsingAlias(JabatanTableMap::COL_URUT, $urut['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JabatanTableMap::COL_URUT, $urut, $comparison);
    }

    /**
     * Filter the query on the is_staf column
     *
     * Example usage:
     * <code>
     * $query->filterByIsStaf(true); // WHERE is_staf = true
     * $query->filterByIsStaf('yes'); // WHERE is_staf = true
     * </code>
     *
     * @param     boolean|string $isStaf The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function filterByIsStaf($isStaf = null, $comparison = null)
    {
        if (is_string($isStaf)) {
            $isStaf = in_array(strtolower($isStaf), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(JabatanTableMap::COL_IS_STAF, $isStaf, $comparison);
    }

    /**
     * Filter the query on the is_jfu column
     *
     * Example usage:
     * <code>
     * $query->filterByIsJfu(true); // WHERE is_jfu = true
     * $query->filterByIsJfu('yes'); // WHERE is_jfu = true
     * </code>
     *
     * @param     boolean|string $isJfu The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function filterByIsJfu($isJfu = null, $comparison = null)
    {
        if (is_string($isJfu)) {
            $isJfu = in_array(strtolower($isJfu), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(JabatanTableMap::COL_IS_JFU, $isJfu, $comparison);
    }

    /**
     * Filter the query on the is_jft column
     *
     * Example usage:
     * <code>
     * $query->filterByIsJft(true); // WHERE is_jft = true
     * $query->filterByIsJft('yes'); // WHERE is_jft = true
     * </code>
     *
     * @param     boolean|string $isJft The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function filterByIsJft($isJft = null, $comparison = null)
    {
        if (is_string($isJft)) {
            $isJft = in_array(strtolower($isJft), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(JabatanTableMap::COL_IS_JFT, $isJft, $comparison);
    }

    /**
     * Filter the query on the is_staf_ahli column
     *
     * Example usage:
     * <code>
     * $query->filterByIsStafAhli(true); // WHERE is_staf_ahli = true
     * $query->filterByIsStafAhli('yes'); // WHERE is_staf_ahli = true
     * </code>
     *
     * @param     boolean|string $isStafAhli The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function filterByIsStafAhli($isStafAhli = null, $comparison = null)
    {
        if (is_string($isStafAhli)) {
            $isStafAhli = in_array(strtolower($isStafAhli), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(JabatanTableMap::COL_IS_STAF_AHLI, $isStafAhli, $comparison);
    }

    /**
     * Filter the query on the is_asisten column
     *
     * Example usage:
     * <code>
     * $query->filterByIsAsisten(true); // WHERE is_asisten = true
     * $query->filterByIsAsisten('yes'); // WHERE is_asisten = true
     * </code>
     *
     * @param     boolean|string $isAsisten The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function filterByIsAsisten($isAsisten = null, $comparison = null)
    {
        if (is_string($isAsisten)) {
            $isAsisten = in_array(strtolower($isAsisten), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(JabatanTableMap::COL_IS_ASISTEN, $isAsisten, $comparison);
    }

    /**
     * Filter the query on the is_sekda column
     *
     * Example usage:
     * <code>
     * $query->filterByIsSekda(true); // WHERE is_sekda = true
     * $query->filterByIsSekda('yes'); // WHERE is_sekda = true
     * </code>
     *
     * @param     boolean|string $isSekda The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function filterByIsSekda($isSekda = null, $comparison = null)
    {
        if (is_string($isSekda)) {
            $isSekda = in_array(strtolower($isSekda), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(JabatanTableMap::COL_IS_SEKDA, $isSekda, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(JabatanTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(JabatanTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JabatanTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(JabatanTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(JabatanTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JabatanTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(JabatanTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(JabatanTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JabatanTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\UnitMaster object
     *
     * @param \CoreBundle\Model\Eperformance\UnitMaster|ObjectCollection $unitMaster The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildJabatanQuery The current query, for fluid interface
     */
    public function filterByUnitMaster($unitMaster, $comparison = null)
    {
        if ($unitMaster instanceof \CoreBundle\Model\Eperformance\UnitMaster) {
            return $this
                ->addUsingAlias(JabatanTableMap::COL_UNIT_MASTER_ID, $unitMaster->getId(), $comparison);
        } elseif ($unitMaster instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(JabatanTableMap::COL_UNIT_MASTER_ID, $unitMaster->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUnitMaster() only accepts arguments of type \CoreBundle\Model\Eperformance\UnitMaster or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UnitMaster relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function joinUnitMaster($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UnitMaster');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UnitMaster');
        }

        return $this;
    }

    /**
     * Use the UnitMaster relation UnitMaster object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\UnitMasterQuery A secondary query class using the current class as primary query
     */
    public function useUnitMasterQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUnitMaster($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UnitMaster', '\CoreBundle\Model\Eperformance\UnitMasterQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\Pegawai object
     *
     * @param \CoreBundle\Model\Eperformance\Pegawai|ObjectCollection $pegawai the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildJabatanQuery The current query, for fluid interface
     */
    public function filterByPegawai($pegawai, $comparison = null)
    {
        if ($pegawai instanceof \CoreBundle\Model\Eperformance\Pegawai) {
            return $this
                ->addUsingAlias(JabatanTableMap::COL_ID, $pegawai->getJabatanId(), $comparison);
        } elseif ($pegawai instanceof ObjectCollection) {
            return $this
                ->usePegawaiQuery()
                ->filterByPrimaryKeys($pegawai->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPegawai() only accepts arguments of type \CoreBundle\Model\Eperformance\Pegawai or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pegawai relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function joinPegawai($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pegawai');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pegawai');
        }

        return $this;
    }

    /**
     * Use the Pegawai relation Pegawai object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\PegawaiQuery A secondary query class using the current class as primary query
     */
    public function usePegawaiQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPegawai($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pegawai', '\CoreBundle\Model\Eperformance\PegawaiQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildJabatan $jabatan Object to remove from the list of results
     *
     * @return $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function prune($jabatan = null)
    {
        if ($jabatan) {
            $this->addUsingAlias(JabatanTableMap::COL_ID, $jabatan->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the eperformance.jabatan table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(JabatanTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            JabatanTableMap::clearInstancePool();
            JabatanTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(JabatanTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(JabatanTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            JabatanTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            JabatanTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 7200)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(JabatanTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(JabatanTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }


        return $con->getDataFetcher($stmt);
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(JabatanTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(JabatanTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(JabatanTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(JabatanTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(JabatanTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildJabatanQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(JabatanTableMap::COL_CREATED_AT);
    }

} // JabatanQuery

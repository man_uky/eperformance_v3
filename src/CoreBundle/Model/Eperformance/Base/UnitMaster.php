<?php

namespace CoreBundle\Model\Eperformance\Base;

use \DateTime;
use \Exception;
use \PDO;
use CoreBundle\Model\Eperformance\Jabatan as ChildJabatan;
use CoreBundle\Model\Eperformance\JabatanQuery as ChildJabatanQuery;
use CoreBundle\Model\Eperformance\Pegawai as ChildPegawai;
use CoreBundle\Model\Eperformance\PegawaiMaster as ChildPegawaiMaster;
use CoreBundle\Model\Eperformance\PegawaiMasterQuery as ChildPegawaiMasterQuery;
use CoreBundle\Model\Eperformance\PegawaiQuery as ChildPegawaiQuery;
use CoreBundle\Model\Eperformance\UnitMaster as ChildUnitMaster;
use CoreBundle\Model\Eperformance\UnitMasterQuery as ChildUnitMasterQuery;
use CoreBundle\Model\Eperformance\Map\JabatanTableMap;
use CoreBundle\Model\Eperformance\Map\PegawaiMasterTableMap;
use CoreBundle\Model\Eperformance\Map\PegawaiTableMap;
use CoreBundle\Model\Eperformance\Map\UnitMasterTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'eperformance.unit_master' table.
 *
 *
 *
 * @package    propel.generator.src.CoreBundle.Model.Eperformance.Base
 */
abstract class UnitMaster implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\CoreBundle\\Model\\Eperformance\\Map\\UnitMasterTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the kode field.
     *
     * @var        string
     */
    protected $kode;

    /**
     * The value for the nama field.
     *
     * @var        string
     */
    protected $nama;

    /**
     * The value for the is_sekretariat_daerah field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_sekretariat_daerah;

    /**
     * The value for the is_dinas field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_dinas;

    /**
     * The value for the is_badan field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_badan;

    /**
     * The value for the is_rumah_sakit field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_rumah_sakit;

    /**
     * The value for the is_bagian field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_bagian;

    /**
     * The value for the is_kecamatan field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_kecamatan;

    /**
     * The value for the is_kelurahan field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_kelurahan;

    /**
     * The value for the parent_id field.
     *
     * @var        int
     */
    protected $parent_id;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * The value for the deleted_at field.
     *
     * @var        DateTime
     */
    protected $deleted_at;

    /**
     * @var        ChildUnitMaster
     */
    protected $aUnitParent;

    /**
     * @var        ObjectCollection|ChildJabatan[] Collection to store aggregation of ChildJabatan objects.
     */
    protected $collJabatans;
    protected $collJabatansPartial;

    /**
     * @var        ObjectCollection|ChildPegawai[] Collection to store aggregation of ChildPegawai objects.
     */
    protected $collPegawais;
    protected $collPegawaisPartial;

    /**
     * @var        ObjectCollection|ChildPegawaiMaster[] Collection to store aggregation of ChildPegawaiMaster objects.
     */
    protected $collPegawaiMasters;
    protected $collPegawaiMastersPartial;

    /**
     * @var        ObjectCollection|ChildUnitMaster[] Collection to store aggregation of ChildUnitMaster objects.
     */
    protected $collUnitMastersRelatedById;
    protected $collUnitMastersRelatedByIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildJabatan[]
     */
    protected $jabatansScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPegawai[]
     */
    protected $pegawaisScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPegawaiMaster[]
     */
    protected $pegawaiMastersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUnitMaster[]
     */
    protected $unitMastersRelatedByIdScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->is_sekretariat_daerah = false;
        $this->is_dinas = false;
        $this->is_badan = false;
        $this->is_rumah_sakit = false;
        $this->is_bagian = false;
        $this->is_kecamatan = false;
        $this->is_kelurahan = false;
    }

    /**
     * Initializes internal state of CoreBundle\Model\Eperformance\Base\UnitMaster object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>UnitMaster</code> instance.  If
     * <code>obj</code> is an instance of <code>UnitMaster</code>, delegates to
     * <code>equals(UnitMaster)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|UnitMaster The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [kode] column value.
     *
     * @return string
     */
    public function getKode()
    {
        return $this->kode;
    }

    /**
     * Get the [nama] column value.
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [is_sekretariat_daerah] column value.
     *
     * @return boolean
     */
    public function getIsSekretariatDaerah()
    {
        return $this->is_sekretariat_daerah;
    }

    /**
     * Get the [is_sekretariat_daerah] column value.
     *
     * @return boolean
     */
    public function isSekretariatDaerah()
    {
        return $this->getIsSekretariatDaerah();
    }

    /**
     * Get the [is_dinas] column value.
     *
     * @return boolean
     */
    public function getIsDinas()
    {
        return $this->is_dinas;
    }

    /**
     * Get the [is_dinas] column value.
     *
     * @return boolean
     */
    public function isDinas()
    {
        return $this->getIsDinas();
    }

    /**
     * Get the [is_badan] column value.
     *
     * @return boolean
     */
    public function getIsBadan()
    {
        return $this->is_badan;
    }

    /**
     * Get the [is_badan] column value.
     *
     * @return boolean
     */
    public function isBadan()
    {
        return $this->getIsBadan();
    }

    /**
     * Get the [is_rumah_sakit] column value.
     *
     * @return boolean
     */
    public function getIsRumahSakit()
    {
        return $this->is_rumah_sakit;
    }

    /**
     * Get the [is_rumah_sakit] column value.
     *
     * @return boolean
     */
    public function isRumahSakit()
    {
        return $this->getIsRumahSakit();
    }

    /**
     * Get the [is_bagian] column value.
     *
     * @return boolean
     */
    public function getIsBagian()
    {
        return $this->is_bagian;
    }

    /**
     * Get the [is_bagian] column value.
     *
     * @return boolean
     */
    public function isBagian()
    {
        return $this->getIsBagian();
    }

    /**
     * Get the [is_kecamatan] column value.
     *
     * @return boolean
     */
    public function getIsKecamatan()
    {
        return $this->is_kecamatan;
    }

    /**
     * Get the [is_kecamatan] column value.
     *
     * @return boolean
     */
    public function isKecamatan()
    {
        return $this->getIsKecamatan();
    }

    /**
     * Get the [is_kelurahan] column value.
     *
     * @return boolean
     */
    public function getIsKelurahan()
    {
        return $this->is_kelurahan;
    }

    /**
     * Get the [is_kelurahan] column value.
     *
     * @return boolean
     */
    public function isKelurahan()
    {
        return $this->getIsKelurahan();
    }

    /**
     * Get the [parent_id] column value.
     *
     * @return int
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [deleted_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDeletedAt($format = NULL)
    {
        if ($format === null) {
            return $this->deleted_at;
        } else {
            return $this->deleted_at instanceof \DateTimeInterface ? $this->deleted_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[UnitMasterTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [kode] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster The current object (for fluent API support)
     */
    public function setKode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->kode !== $v) {
            $this->kode = $v;
            $this->modifiedColumns[UnitMasterTableMap::COL_KODE] = true;
        }

        return $this;
    } // setKode()

    /**
     * Set the value of [nama] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[UnitMasterTableMap::COL_NAMA] = true;
        }

        return $this;
    } // setNama()

    /**
     * Sets the value of the [is_sekretariat_daerah] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster The current object (for fluent API support)
     */
    public function setIsSekretariatDaerah($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_sekretariat_daerah !== $v) {
            $this->is_sekretariat_daerah = $v;
            $this->modifiedColumns[UnitMasterTableMap::COL_IS_SEKRETARIAT_DAERAH] = true;
        }

        return $this;
    } // setIsSekretariatDaerah()

    /**
     * Sets the value of the [is_dinas] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster The current object (for fluent API support)
     */
    public function setIsDinas($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_dinas !== $v) {
            $this->is_dinas = $v;
            $this->modifiedColumns[UnitMasterTableMap::COL_IS_DINAS] = true;
        }

        return $this;
    } // setIsDinas()

    /**
     * Sets the value of the [is_badan] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster The current object (for fluent API support)
     */
    public function setIsBadan($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_badan !== $v) {
            $this->is_badan = $v;
            $this->modifiedColumns[UnitMasterTableMap::COL_IS_BADAN] = true;
        }

        return $this;
    } // setIsBadan()

    /**
     * Sets the value of the [is_rumah_sakit] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster The current object (for fluent API support)
     */
    public function setIsRumahSakit($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_rumah_sakit !== $v) {
            $this->is_rumah_sakit = $v;
            $this->modifiedColumns[UnitMasterTableMap::COL_IS_RUMAH_SAKIT] = true;
        }

        return $this;
    } // setIsRumahSakit()

    /**
     * Sets the value of the [is_bagian] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster The current object (for fluent API support)
     */
    public function setIsBagian($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_bagian !== $v) {
            $this->is_bagian = $v;
            $this->modifiedColumns[UnitMasterTableMap::COL_IS_BAGIAN] = true;
        }

        return $this;
    } // setIsBagian()

    /**
     * Sets the value of the [is_kecamatan] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster The current object (for fluent API support)
     */
    public function setIsKecamatan($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_kecamatan !== $v) {
            $this->is_kecamatan = $v;
            $this->modifiedColumns[UnitMasterTableMap::COL_IS_KECAMATAN] = true;
        }

        return $this;
    } // setIsKecamatan()

    /**
     * Sets the value of the [is_kelurahan] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster The current object (for fluent API support)
     */
    public function setIsKelurahan($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_kelurahan !== $v) {
            $this->is_kelurahan = $v;
            $this->modifiedColumns[UnitMasterTableMap::COL_IS_KELURAHAN] = true;
        }

        return $this;
    } // setIsKelurahan()

    /**
     * Set the value of [parent_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster The current object (for fluent API support)
     */
    public function setParentId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->parent_id !== $v) {
            $this->parent_id = $v;
            $this->modifiedColumns[UnitMasterTableMap::COL_PARENT_ID] = true;
        }

        if ($this->aUnitParent !== null && $this->aUnitParent->getId() !== $v) {
            $this->aUnitParent = null;
        }

        return $this;
    } // setParentId()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UnitMasterTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UnitMasterTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Sets the value of [deleted_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster The current object (for fluent API support)
     */
    public function setDeletedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->deleted_at !== null || $dt !== null) {
            if ($this->deleted_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->deleted_at->format("Y-m-d H:i:s.u")) {
                $this->deleted_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UnitMasterTableMap::COL_DELETED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setDeletedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->is_sekretariat_daerah !== false) {
                return false;
            }

            if ($this->is_dinas !== false) {
                return false;
            }

            if ($this->is_badan !== false) {
                return false;
            }

            if ($this->is_rumah_sakit !== false) {
                return false;
            }

            if ($this->is_bagian !== false) {
                return false;
            }

            if ($this->is_kecamatan !== false) {
                return false;
            }

            if ($this->is_kelurahan !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : UnitMasterTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : UnitMasterTableMap::translateFieldName('Kode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->kode = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : UnitMasterTableMap::translateFieldName('Nama', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nama = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : UnitMasterTableMap::translateFieldName('IsSekretariatDaerah', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_sekretariat_daerah = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : UnitMasterTableMap::translateFieldName('IsDinas', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_dinas = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : UnitMasterTableMap::translateFieldName('IsBadan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_badan = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : UnitMasterTableMap::translateFieldName('IsRumahSakit', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_rumah_sakit = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : UnitMasterTableMap::translateFieldName('IsBagian', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_bagian = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : UnitMasterTableMap::translateFieldName('IsKecamatan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_kecamatan = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : UnitMasterTableMap::translateFieldName('IsKelurahan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_kelurahan = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : UnitMasterTableMap::translateFieldName('ParentId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->parent_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : UnitMasterTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : UnitMasterTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : UnitMasterTableMap::translateFieldName('DeletedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->deleted_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 14; // 14 = UnitMasterTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\CoreBundle\\Model\\Eperformance\\UnitMaster'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aUnitParent !== null && $this->parent_id !== $this->aUnitParent->getId()) {
            $this->aUnitParent = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UnitMasterTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildUnitMasterQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aUnitParent = null;
            $this->collJabatans = null;

            $this->collPegawais = null;

            $this->collPegawaiMasters = null;

            $this->collUnitMastersRelatedById = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see UnitMaster::setDeleted()
     * @see UnitMaster::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UnitMasterTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildUnitMasterQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UnitMasterTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(UnitMasterTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(UnitMasterTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(UnitMasterTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UnitMasterTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aUnitParent !== null) {
                if ($this->aUnitParent->isModified() || $this->aUnitParent->isNew()) {
                    $affectedRows += $this->aUnitParent->save($con);
                }
                $this->setUnitParent($this->aUnitParent);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->jabatansScheduledForDeletion !== null) {
                if (!$this->jabatansScheduledForDeletion->isEmpty()) {
                    \CoreBundle\Model\Eperformance\JabatanQuery::create()
                        ->filterByPrimaryKeys($this->jabatansScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->jabatansScheduledForDeletion = null;
                }
            }

            if ($this->collJabatans !== null) {
                foreach ($this->collJabatans as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pegawaisScheduledForDeletion !== null) {
                if (!$this->pegawaisScheduledForDeletion->isEmpty()) {
                    \CoreBundle\Model\Eperformance\PegawaiQuery::create()
                        ->filterByPrimaryKeys($this->pegawaisScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pegawaisScheduledForDeletion = null;
                }
            }

            if ($this->collPegawais !== null) {
                foreach ($this->collPegawais as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pegawaiMastersScheduledForDeletion !== null) {
                if (!$this->pegawaiMastersScheduledForDeletion->isEmpty()) {
                    \CoreBundle\Model\Eperformance\PegawaiMasterQuery::create()
                        ->filterByPrimaryKeys($this->pegawaiMastersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pegawaiMastersScheduledForDeletion = null;
                }
            }

            if ($this->collPegawaiMasters !== null) {
                foreach ($this->collPegawaiMasters as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->unitMastersRelatedByIdScheduledForDeletion !== null) {
                if (!$this->unitMastersRelatedByIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->unitMastersRelatedByIdScheduledForDeletion as $unitMasterRelatedById) {
                        // need to save related object because we set the relation to null
                        $unitMasterRelatedById->save($con);
                    }
                    $this->unitMastersRelatedByIdScheduledForDeletion = null;
                }
            }

            if ($this->collUnitMastersRelatedById !== null) {
                foreach ($this->collUnitMastersRelatedById as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[UnitMasterTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UnitMasterTableMap::COL_ID . ')');
        }
        if (null === $this->id) {
            try {
                $dataFetcher = $con->query("SELECT nextval('eperformance.unit_master_id_seq')");
                $this->id = (int) $dataFetcher->fetchColumn();
            } catch (Exception $e) {
                throw new PropelException('Unable to get sequence id.', 0, $e);
            }
        }


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UnitMasterTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_KODE)) {
            $modifiedColumns[':p' . $index++]  = 'kode';
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_NAMA)) {
            $modifiedColumns[':p' . $index++]  = 'nama';
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_IS_SEKRETARIAT_DAERAH)) {
            $modifiedColumns[':p' . $index++]  = 'is_sekretariat_daerah';
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_IS_DINAS)) {
            $modifiedColumns[':p' . $index++]  = 'is_dinas';
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_IS_BADAN)) {
            $modifiedColumns[':p' . $index++]  = 'is_badan';
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_IS_RUMAH_SAKIT)) {
            $modifiedColumns[':p' . $index++]  = 'is_rumah_sakit';
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_IS_BAGIAN)) {
            $modifiedColumns[':p' . $index++]  = 'is_bagian';
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_IS_KECAMATAN)) {
            $modifiedColumns[':p' . $index++]  = 'is_kecamatan';
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_IS_KELURAHAN)) {
            $modifiedColumns[':p' . $index++]  = 'is_kelurahan';
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_PARENT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'parent_id';
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_DELETED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'deleted_at';
        }

        $sql = sprintf(
            'INSERT INTO eperformance.unit_master (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'kode':
                        $stmt->bindValue($identifier, $this->kode, PDO::PARAM_STR);
                        break;
                    case 'nama':
                        $stmt->bindValue($identifier, $this->nama, PDO::PARAM_STR);
                        break;
                    case 'is_sekretariat_daerah':
                        $stmt->bindValue($identifier, $this->is_sekretariat_daerah, PDO::PARAM_BOOL);
                        break;
                    case 'is_dinas':
                        $stmt->bindValue($identifier, $this->is_dinas, PDO::PARAM_BOOL);
                        break;
                    case 'is_badan':
                        $stmt->bindValue($identifier, $this->is_badan, PDO::PARAM_BOOL);
                        break;
                    case 'is_rumah_sakit':
                        $stmt->bindValue($identifier, $this->is_rumah_sakit, PDO::PARAM_BOOL);
                        break;
                    case 'is_bagian':
                        $stmt->bindValue($identifier, $this->is_bagian, PDO::PARAM_BOOL);
                        break;
                    case 'is_kecamatan':
                        $stmt->bindValue($identifier, $this->is_kecamatan, PDO::PARAM_BOOL);
                        break;
                    case 'is_kelurahan':
                        $stmt->bindValue($identifier, $this->is_kelurahan, PDO::PARAM_BOOL);
                        break;
                    case 'parent_id':
                        $stmt->bindValue($identifier, $this->parent_id, PDO::PARAM_INT);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'deleted_at':
                        $stmt->bindValue($identifier, $this->deleted_at ? $this->deleted_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UnitMasterTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getKode();
                break;
            case 2:
                return $this->getNama();
                break;
            case 3:
                return $this->getIsSekretariatDaerah();
                break;
            case 4:
                return $this->getIsDinas();
                break;
            case 5:
                return $this->getIsBadan();
                break;
            case 6:
                return $this->getIsRumahSakit();
                break;
            case 7:
                return $this->getIsBagian();
                break;
            case 8:
                return $this->getIsKecamatan();
                break;
            case 9:
                return $this->getIsKelurahan();
                break;
            case 10:
                return $this->getParentId();
                break;
            case 11:
                return $this->getCreatedAt();
                break;
            case 12:
                return $this->getUpdatedAt();
                break;
            case 13:
                return $this->getDeletedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['UnitMaster'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['UnitMaster'][$this->hashCode()] = true;
        $keys = UnitMasterTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getKode(),
            $keys[2] => $this->getNama(),
            $keys[3] => $this->getIsSekretariatDaerah(),
            $keys[4] => $this->getIsDinas(),
            $keys[5] => $this->getIsBadan(),
            $keys[6] => $this->getIsRumahSakit(),
            $keys[7] => $this->getIsBagian(),
            $keys[8] => $this->getIsKecamatan(),
            $keys[9] => $this->getIsKelurahan(),
            $keys[10] => $this->getParentId(),
            $keys[11] => $this->getCreatedAt(),
            $keys[12] => $this->getUpdatedAt(),
            $keys[13] => $this->getDeletedAt(),
        );
        if ($result[$keys[11]] instanceof \DateTime) {
            $result[$keys[11]] = $result[$keys[11]]->format('c');
        }

        if ($result[$keys[12]] instanceof \DateTime) {
            $result[$keys[12]] = $result[$keys[12]]->format('c');
        }

        if ($result[$keys[13]] instanceof \DateTime) {
            $result[$keys[13]] = $result[$keys[13]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aUnitParent) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'unitMaster';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.unit_master';
                        break;
                    default:
                        $key = 'UnitParent';
                }

                $result[$key] = $this->aUnitParent->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collJabatans) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'jabatans';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.jabatans';
                        break;
                    default:
                        $key = 'Jabatans';
                }

                $result[$key] = $this->collJabatans->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPegawais) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pegawais';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.pegawais';
                        break;
                    default:
                        $key = 'Pegawais';
                }

                $result[$key] = $this->collPegawais->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPegawaiMasters) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pegawaiMasters';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.pegawai_masters';
                        break;
                    default:
                        $key = 'PegawaiMasters';
                }

                $result[$key] = $this->collPegawaiMasters->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUnitMastersRelatedById) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'unitMasters';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.unit_masters';
                        break;
                    default:
                        $key = 'UnitMasters';
                }

                $result[$key] = $this->collUnitMastersRelatedById->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UnitMasterTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setKode($value);
                break;
            case 2:
                $this->setNama($value);
                break;
            case 3:
                $this->setIsSekretariatDaerah($value);
                break;
            case 4:
                $this->setIsDinas($value);
                break;
            case 5:
                $this->setIsBadan($value);
                break;
            case 6:
                $this->setIsRumahSakit($value);
                break;
            case 7:
                $this->setIsBagian($value);
                break;
            case 8:
                $this->setIsKecamatan($value);
                break;
            case 9:
                $this->setIsKelurahan($value);
                break;
            case 10:
                $this->setParentId($value);
                break;
            case 11:
                $this->setCreatedAt($value);
                break;
            case 12:
                $this->setUpdatedAt($value);
                break;
            case 13:
                $this->setDeletedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = UnitMasterTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setKode($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setNama($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setIsSekretariatDaerah($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setIsDinas($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setIsBadan($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setIsRumahSakit($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setIsBagian($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setIsKecamatan($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setIsKelurahan($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setParentId($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setCreatedAt($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setUpdatedAt($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setDeletedAt($arr[$keys[13]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(UnitMasterTableMap::DATABASE_NAME);

        if ($this->isColumnModified(UnitMasterTableMap::COL_ID)) {
            $criteria->add(UnitMasterTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_KODE)) {
            $criteria->add(UnitMasterTableMap::COL_KODE, $this->kode);
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_NAMA)) {
            $criteria->add(UnitMasterTableMap::COL_NAMA, $this->nama);
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_IS_SEKRETARIAT_DAERAH)) {
            $criteria->add(UnitMasterTableMap::COL_IS_SEKRETARIAT_DAERAH, $this->is_sekretariat_daerah);
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_IS_DINAS)) {
            $criteria->add(UnitMasterTableMap::COL_IS_DINAS, $this->is_dinas);
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_IS_BADAN)) {
            $criteria->add(UnitMasterTableMap::COL_IS_BADAN, $this->is_badan);
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_IS_RUMAH_SAKIT)) {
            $criteria->add(UnitMasterTableMap::COL_IS_RUMAH_SAKIT, $this->is_rumah_sakit);
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_IS_BAGIAN)) {
            $criteria->add(UnitMasterTableMap::COL_IS_BAGIAN, $this->is_bagian);
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_IS_KECAMATAN)) {
            $criteria->add(UnitMasterTableMap::COL_IS_KECAMATAN, $this->is_kecamatan);
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_IS_KELURAHAN)) {
            $criteria->add(UnitMasterTableMap::COL_IS_KELURAHAN, $this->is_kelurahan);
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_PARENT_ID)) {
            $criteria->add(UnitMasterTableMap::COL_PARENT_ID, $this->parent_id);
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_CREATED_AT)) {
            $criteria->add(UnitMasterTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_UPDATED_AT)) {
            $criteria->add(UnitMasterTableMap::COL_UPDATED_AT, $this->updated_at);
        }
        if ($this->isColumnModified(UnitMasterTableMap::COL_DELETED_AT)) {
            $criteria->add(UnitMasterTableMap::COL_DELETED_AT, $this->deleted_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildUnitMasterQuery::create();
        $criteria->add(UnitMasterTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \CoreBundle\Model\Eperformance\UnitMaster (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setKode($this->getKode());
        $copyObj->setNama($this->getNama());
        $copyObj->setIsSekretariatDaerah($this->getIsSekretariatDaerah());
        $copyObj->setIsDinas($this->getIsDinas());
        $copyObj->setIsBadan($this->getIsBadan());
        $copyObj->setIsRumahSakit($this->getIsRumahSakit());
        $copyObj->setIsBagian($this->getIsBagian());
        $copyObj->setIsKecamatan($this->getIsKecamatan());
        $copyObj->setIsKelurahan($this->getIsKelurahan());
        $copyObj->setParentId($this->getParentId());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        $copyObj->setDeletedAt($this->getDeletedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getJabatans() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addJabatan($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPegawais() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPegawai($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPegawaiMasters() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPegawaiMaster($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUnitMastersRelatedById() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUnitMasterRelatedById($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \CoreBundle\Model\Eperformance\UnitMaster Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildUnitMaster object.
     *
     * @param  ChildUnitMaster $v
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUnitParent(ChildUnitMaster $v = null)
    {
        if ($v === null) {
            $this->setParentId(NULL);
        } else {
            $this->setParentId($v->getId());
        }

        $this->aUnitParent = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUnitMaster object, it will not be re-added.
        if ($v !== null) {
            $v->addUnitMasterRelatedById($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUnitMaster object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUnitMaster The associated ChildUnitMaster object.
     * @throws PropelException
     */
    public function getUnitParent(ConnectionInterface $con = null)
    {
        if ($this->aUnitParent === null && ($this->parent_id !== null)) {
            $this->aUnitParent = ChildUnitMasterQuery::create()->findPk($this->parent_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUnitParent->addUnitMastersRelatedById($this);
             */
        }

        return $this->aUnitParent;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Jabatan' == $relationName) {
            return $this->initJabatans();
        }
        if ('Pegawai' == $relationName) {
            return $this->initPegawais();
        }
        if ('PegawaiMaster' == $relationName) {
            return $this->initPegawaiMasters();
        }
        if ('UnitMasterRelatedById' == $relationName) {
            return $this->initUnitMastersRelatedById();
        }
    }

    /**
     * Clears out the collJabatans collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addJabatans()
     */
    public function clearJabatans()
    {
        $this->collJabatans = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collJabatans collection loaded partially.
     */
    public function resetPartialJabatans($v = true)
    {
        $this->collJabatansPartial = $v;
    }

    /**
     * Initializes the collJabatans collection.
     *
     * By default this just sets the collJabatans collection to an empty array (like clearcollJabatans());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initJabatans($overrideExisting = true)
    {
        if (null !== $this->collJabatans && !$overrideExisting) {
            return;
        }

        $collectionClassName = JabatanTableMap::getTableMap()->getCollectionClassName();

        $this->collJabatans = new $collectionClassName;
        $this->collJabatans->setModel('\CoreBundle\Model\Eperformance\Jabatan');
    }

    /**
     * Gets an array of ChildJabatan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUnitMaster is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildJabatan[] List of ChildJabatan objects
     * @throws PropelException
     */
    public function getJabatans(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collJabatansPartial && !$this->isNew();
        if (null === $this->collJabatans || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collJabatans) {
                // return empty collection
                $this->initJabatans();
            } else {
                $collJabatans = ChildJabatanQuery::create(null, $criteria)
                    ->filterByUnitMaster($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collJabatansPartial && count($collJabatans)) {
                        $this->initJabatans(false);

                        foreach ($collJabatans as $obj) {
                            if (false == $this->collJabatans->contains($obj)) {
                                $this->collJabatans->append($obj);
                            }
                        }

                        $this->collJabatansPartial = true;
                    }

                    return $collJabatans;
                }

                if ($partial && $this->collJabatans) {
                    foreach ($this->collJabatans as $obj) {
                        if ($obj->isNew()) {
                            $collJabatans[] = $obj;
                        }
                    }
                }

                $this->collJabatans = $collJabatans;
                $this->collJabatansPartial = false;
            }
        }

        return $this->collJabatans;
    }

    /**
     * Sets a collection of ChildJabatan objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $jabatans A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUnitMaster The current object (for fluent API support)
     */
    public function setJabatans(Collection $jabatans, ConnectionInterface $con = null)
    {
        /** @var ChildJabatan[] $jabatansToDelete */
        $jabatansToDelete = $this->getJabatans(new Criteria(), $con)->diff($jabatans);


        $this->jabatansScheduledForDeletion = $jabatansToDelete;

        foreach ($jabatansToDelete as $jabatanRemoved) {
            $jabatanRemoved->setUnitMaster(null);
        }

        $this->collJabatans = null;
        foreach ($jabatans as $jabatan) {
            $this->addJabatan($jabatan);
        }

        $this->collJabatans = $jabatans;
        $this->collJabatansPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Jabatan objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Jabatan objects.
     * @throws PropelException
     */
    public function countJabatans(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collJabatansPartial && !$this->isNew();
        if (null === $this->collJabatans || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collJabatans) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getJabatans());
            }

            $query = ChildJabatanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUnitMaster($this)
                ->count($con);
        }

        return count($this->collJabatans);
    }

    /**
     * Method called to associate a ChildJabatan object to this object
     * through the ChildJabatan foreign key attribute.
     *
     * @param  ChildJabatan $l ChildJabatan
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster The current object (for fluent API support)
     */
    public function addJabatan(ChildJabatan $l)
    {
        if ($this->collJabatans === null) {
            $this->initJabatans();
            $this->collJabatansPartial = true;
        }

        if (!$this->collJabatans->contains($l)) {
            $this->doAddJabatan($l);

            if ($this->jabatansScheduledForDeletion and $this->jabatansScheduledForDeletion->contains($l)) {
                $this->jabatansScheduledForDeletion->remove($this->jabatansScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildJabatan $jabatan The ChildJabatan object to add.
     */
    protected function doAddJabatan(ChildJabatan $jabatan)
    {
        $this->collJabatans[]= $jabatan;
        $jabatan->setUnitMaster($this);
    }

    /**
     * @param  ChildJabatan $jabatan The ChildJabatan object to remove.
     * @return $this|ChildUnitMaster The current object (for fluent API support)
     */
    public function removeJabatan(ChildJabatan $jabatan)
    {
        if ($this->getJabatans()->contains($jabatan)) {
            $pos = $this->collJabatans->search($jabatan);
            $this->collJabatans->remove($pos);
            if (null === $this->jabatansScheduledForDeletion) {
                $this->jabatansScheduledForDeletion = clone $this->collJabatans;
                $this->jabatansScheduledForDeletion->clear();
            }
            $this->jabatansScheduledForDeletion[]= clone $jabatan;
            $jabatan->setUnitMaster(null);
        }

        return $this;
    }

    /**
     * Clears out the collPegawais collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPegawais()
     */
    public function clearPegawais()
    {
        $this->collPegawais = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPegawais collection loaded partially.
     */
    public function resetPartialPegawais($v = true)
    {
        $this->collPegawaisPartial = $v;
    }

    /**
     * Initializes the collPegawais collection.
     *
     * By default this just sets the collPegawais collection to an empty array (like clearcollPegawais());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPegawais($overrideExisting = true)
    {
        if (null !== $this->collPegawais && !$overrideExisting) {
            return;
        }

        $collectionClassName = PegawaiTableMap::getTableMap()->getCollectionClassName();

        $this->collPegawais = new $collectionClassName;
        $this->collPegawais->setModel('\CoreBundle\Model\Eperformance\Pegawai');
    }

    /**
     * Gets an array of ChildPegawai objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUnitMaster is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     * @throws PropelException
     */
    public function getPegawais(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPegawaisPartial && !$this->isNew();
        if (null === $this->collPegawais || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPegawais) {
                // return empty collection
                $this->initPegawais();
            } else {
                $collPegawais = ChildPegawaiQuery::create(null, $criteria)
                    ->filterByUnitMaster($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPegawaisPartial && count($collPegawais)) {
                        $this->initPegawais(false);

                        foreach ($collPegawais as $obj) {
                            if (false == $this->collPegawais->contains($obj)) {
                                $this->collPegawais->append($obj);
                            }
                        }

                        $this->collPegawaisPartial = true;
                    }

                    return $collPegawais;
                }

                if ($partial && $this->collPegawais) {
                    foreach ($this->collPegawais as $obj) {
                        if ($obj->isNew()) {
                            $collPegawais[] = $obj;
                        }
                    }
                }

                $this->collPegawais = $collPegawais;
                $this->collPegawaisPartial = false;
            }
        }

        return $this->collPegawais;
    }

    /**
     * Sets a collection of ChildPegawai objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $pegawais A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUnitMaster The current object (for fluent API support)
     */
    public function setPegawais(Collection $pegawais, ConnectionInterface $con = null)
    {
        /** @var ChildPegawai[] $pegawaisToDelete */
        $pegawaisToDelete = $this->getPegawais(new Criteria(), $con)->diff($pegawais);


        $this->pegawaisScheduledForDeletion = $pegawaisToDelete;

        foreach ($pegawaisToDelete as $pegawaiRemoved) {
            $pegawaiRemoved->setUnitMaster(null);
        }

        $this->collPegawais = null;
        foreach ($pegawais as $pegawai) {
            $this->addPegawai($pegawai);
        }

        $this->collPegawais = $pegawais;
        $this->collPegawaisPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Pegawai objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Pegawai objects.
     * @throws PropelException
     */
    public function countPegawais(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPegawaisPartial && !$this->isNew();
        if (null === $this->collPegawais || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPegawais) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPegawais());
            }

            $query = ChildPegawaiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUnitMaster($this)
                ->count($con);
        }

        return count($this->collPegawais);
    }

    /**
     * Method called to associate a ChildPegawai object to this object
     * through the ChildPegawai foreign key attribute.
     *
     * @param  ChildPegawai $l ChildPegawai
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster The current object (for fluent API support)
     */
    public function addPegawai(ChildPegawai $l)
    {
        if ($this->collPegawais === null) {
            $this->initPegawais();
            $this->collPegawaisPartial = true;
        }

        if (!$this->collPegawais->contains($l)) {
            $this->doAddPegawai($l);

            if ($this->pegawaisScheduledForDeletion and $this->pegawaisScheduledForDeletion->contains($l)) {
                $this->pegawaisScheduledForDeletion->remove($this->pegawaisScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPegawai $pegawai The ChildPegawai object to add.
     */
    protected function doAddPegawai(ChildPegawai $pegawai)
    {
        $this->collPegawais[]= $pegawai;
        $pegawai->setUnitMaster($this);
    }

    /**
     * @param  ChildPegawai $pegawai The ChildPegawai object to remove.
     * @return $this|ChildUnitMaster The current object (for fluent API support)
     */
    public function removePegawai(ChildPegawai $pegawai)
    {
        if ($this->getPegawais()->contains($pegawai)) {
            $pos = $this->collPegawais->search($pegawai);
            $this->collPegawais->remove($pos);
            if (null === $this->pegawaisScheduledForDeletion) {
                $this->pegawaisScheduledForDeletion = clone $this->collPegawais;
                $this->pegawaisScheduledForDeletion->clear();
            }
            $this->pegawaisScheduledForDeletion[]= clone $pegawai;
            $pegawai->setUnitMaster(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UnitMaster is new, it will return
     * an empty collection; or if this UnitMaster has previously
     * been saved, it will retrieve related Pegawais from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UnitMaster.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     */
    public function getPegawaisJoinCredential(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPegawaiQuery::create(null, $criteria);
        $query->joinWith('Credential', $joinBehavior);

        return $this->getPegawais($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UnitMaster is new, it will return
     * an empty collection; or if this UnitMaster has previously
     * been saved, it will retrieve related Pegawais from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UnitMaster.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     */
    public function getPegawaisJoinPegawaiMaster(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPegawaiQuery::create(null, $criteria);
        $query->joinWith('PegawaiMaster', $joinBehavior);

        return $this->getPegawais($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UnitMaster is new, it will return
     * an empty collection; or if this UnitMaster has previously
     * been saved, it will retrieve related Pegawais from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UnitMaster.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     */
    public function getPegawaisJoinPegawaiAtasan(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPegawaiQuery::create(null, $criteria);
        $query->joinWith('PegawaiAtasan', $joinBehavior);

        return $this->getPegawais($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UnitMaster is new, it will return
     * an empty collection; or if this UnitMaster has previously
     * been saved, it will retrieve related Pegawais from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UnitMaster.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     */
    public function getPegawaisJoinPangkat(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPegawaiQuery::create(null, $criteria);
        $query->joinWith('Pangkat', $joinBehavior);

        return $this->getPegawais($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UnitMaster is new, it will return
     * an empty collection; or if this UnitMaster has previously
     * been saved, it will retrieve related Pegawais from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UnitMaster.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     */
    public function getPegawaisJoinJabatan(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPegawaiQuery::create(null, $criteria);
        $query->joinWith('Jabatan', $joinBehavior);

        return $this->getPegawais($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UnitMaster is new, it will return
     * an empty collection; or if this UnitMaster has previously
     * been saved, it will retrieve related Pegawais from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UnitMaster.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     */
    public function getPegawaisJoinPegawaiStatus(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPegawaiQuery::create(null, $criteria);
        $query->joinWith('PegawaiStatus', $joinBehavior);

        return $this->getPegawais($query, $con);
    }

    /**
     * Clears out the collPegawaiMasters collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPegawaiMasters()
     */
    public function clearPegawaiMasters()
    {
        $this->collPegawaiMasters = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPegawaiMasters collection loaded partially.
     */
    public function resetPartialPegawaiMasters($v = true)
    {
        $this->collPegawaiMastersPartial = $v;
    }

    /**
     * Initializes the collPegawaiMasters collection.
     *
     * By default this just sets the collPegawaiMasters collection to an empty array (like clearcollPegawaiMasters());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPegawaiMasters($overrideExisting = true)
    {
        if (null !== $this->collPegawaiMasters && !$overrideExisting) {
            return;
        }

        $collectionClassName = PegawaiMasterTableMap::getTableMap()->getCollectionClassName();

        $this->collPegawaiMasters = new $collectionClassName;
        $this->collPegawaiMasters->setModel('\CoreBundle\Model\Eperformance\PegawaiMaster');
    }

    /**
     * Gets an array of ChildPegawaiMaster objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUnitMaster is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPegawaiMaster[] List of ChildPegawaiMaster objects
     * @throws PropelException
     */
    public function getPegawaiMasters(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPegawaiMastersPartial && !$this->isNew();
        if (null === $this->collPegawaiMasters || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPegawaiMasters) {
                // return empty collection
                $this->initPegawaiMasters();
            } else {
                $collPegawaiMasters = ChildPegawaiMasterQuery::create(null, $criteria)
                    ->filterByUnitMaster($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPegawaiMastersPartial && count($collPegawaiMasters)) {
                        $this->initPegawaiMasters(false);

                        foreach ($collPegawaiMasters as $obj) {
                            if (false == $this->collPegawaiMasters->contains($obj)) {
                                $this->collPegawaiMasters->append($obj);
                            }
                        }

                        $this->collPegawaiMastersPartial = true;
                    }

                    return $collPegawaiMasters;
                }

                if ($partial && $this->collPegawaiMasters) {
                    foreach ($this->collPegawaiMasters as $obj) {
                        if ($obj->isNew()) {
                            $collPegawaiMasters[] = $obj;
                        }
                    }
                }

                $this->collPegawaiMasters = $collPegawaiMasters;
                $this->collPegawaiMastersPartial = false;
            }
        }

        return $this->collPegawaiMasters;
    }

    /**
     * Sets a collection of ChildPegawaiMaster objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $pegawaiMasters A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUnitMaster The current object (for fluent API support)
     */
    public function setPegawaiMasters(Collection $pegawaiMasters, ConnectionInterface $con = null)
    {
        /** @var ChildPegawaiMaster[] $pegawaiMastersToDelete */
        $pegawaiMastersToDelete = $this->getPegawaiMasters(new Criteria(), $con)->diff($pegawaiMasters);


        $this->pegawaiMastersScheduledForDeletion = $pegawaiMastersToDelete;

        foreach ($pegawaiMastersToDelete as $pegawaiMasterRemoved) {
            $pegawaiMasterRemoved->setUnitMaster(null);
        }

        $this->collPegawaiMasters = null;
        foreach ($pegawaiMasters as $pegawaiMaster) {
            $this->addPegawaiMaster($pegawaiMaster);
        }

        $this->collPegawaiMasters = $pegawaiMasters;
        $this->collPegawaiMastersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PegawaiMaster objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PegawaiMaster objects.
     * @throws PropelException
     */
    public function countPegawaiMasters(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPegawaiMastersPartial && !$this->isNew();
        if (null === $this->collPegawaiMasters || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPegawaiMasters) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPegawaiMasters());
            }

            $query = ChildPegawaiMasterQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUnitMaster($this)
                ->count($con);
        }

        return count($this->collPegawaiMasters);
    }

    /**
     * Method called to associate a ChildPegawaiMaster object to this object
     * through the ChildPegawaiMaster foreign key attribute.
     *
     * @param  ChildPegawaiMaster $l ChildPegawaiMaster
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster The current object (for fluent API support)
     */
    public function addPegawaiMaster(ChildPegawaiMaster $l)
    {
        if ($this->collPegawaiMasters === null) {
            $this->initPegawaiMasters();
            $this->collPegawaiMastersPartial = true;
        }

        if (!$this->collPegawaiMasters->contains($l)) {
            $this->doAddPegawaiMaster($l);

            if ($this->pegawaiMastersScheduledForDeletion and $this->pegawaiMastersScheduledForDeletion->contains($l)) {
                $this->pegawaiMastersScheduledForDeletion->remove($this->pegawaiMastersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPegawaiMaster $pegawaiMaster The ChildPegawaiMaster object to add.
     */
    protected function doAddPegawaiMaster(ChildPegawaiMaster $pegawaiMaster)
    {
        $this->collPegawaiMasters[]= $pegawaiMaster;
        $pegawaiMaster->setUnitMaster($this);
    }

    /**
     * @param  ChildPegawaiMaster $pegawaiMaster The ChildPegawaiMaster object to remove.
     * @return $this|ChildUnitMaster The current object (for fluent API support)
     */
    public function removePegawaiMaster(ChildPegawaiMaster $pegawaiMaster)
    {
        if ($this->getPegawaiMasters()->contains($pegawaiMaster)) {
            $pos = $this->collPegawaiMasters->search($pegawaiMaster);
            $this->collPegawaiMasters->remove($pos);
            if (null === $this->pegawaiMastersScheduledForDeletion) {
                $this->pegawaiMastersScheduledForDeletion = clone $this->collPegawaiMasters;
                $this->pegawaiMastersScheduledForDeletion->clear();
            }
            $this->pegawaiMastersScheduledForDeletion[]= clone $pegawaiMaster;
            $pegawaiMaster->setUnitMaster(null);
        }

        return $this;
    }

    /**
     * Clears out the collUnitMastersRelatedById collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUnitMastersRelatedById()
     */
    public function clearUnitMastersRelatedById()
    {
        $this->collUnitMastersRelatedById = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUnitMastersRelatedById collection loaded partially.
     */
    public function resetPartialUnitMastersRelatedById($v = true)
    {
        $this->collUnitMastersRelatedByIdPartial = $v;
    }

    /**
     * Initializes the collUnitMastersRelatedById collection.
     *
     * By default this just sets the collUnitMastersRelatedById collection to an empty array (like clearcollUnitMastersRelatedById());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUnitMastersRelatedById($overrideExisting = true)
    {
        if (null !== $this->collUnitMastersRelatedById && !$overrideExisting) {
            return;
        }

        $collectionClassName = UnitMasterTableMap::getTableMap()->getCollectionClassName();

        $this->collUnitMastersRelatedById = new $collectionClassName;
        $this->collUnitMastersRelatedById->setModel('\CoreBundle\Model\Eperformance\UnitMaster');
    }

    /**
     * Gets an array of ChildUnitMaster objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUnitMaster is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUnitMaster[] List of ChildUnitMaster objects
     * @throws PropelException
     */
    public function getUnitMastersRelatedById(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUnitMastersRelatedByIdPartial && !$this->isNew();
        if (null === $this->collUnitMastersRelatedById || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUnitMastersRelatedById) {
                // return empty collection
                $this->initUnitMastersRelatedById();
            } else {
                $collUnitMastersRelatedById = ChildUnitMasterQuery::create(null, $criteria)
                    ->filterByUnitParent($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUnitMastersRelatedByIdPartial && count($collUnitMastersRelatedById)) {
                        $this->initUnitMastersRelatedById(false);

                        foreach ($collUnitMastersRelatedById as $obj) {
                            if (false == $this->collUnitMastersRelatedById->contains($obj)) {
                                $this->collUnitMastersRelatedById->append($obj);
                            }
                        }

                        $this->collUnitMastersRelatedByIdPartial = true;
                    }

                    return $collUnitMastersRelatedById;
                }

                if ($partial && $this->collUnitMastersRelatedById) {
                    foreach ($this->collUnitMastersRelatedById as $obj) {
                        if ($obj->isNew()) {
                            $collUnitMastersRelatedById[] = $obj;
                        }
                    }
                }

                $this->collUnitMastersRelatedById = $collUnitMastersRelatedById;
                $this->collUnitMastersRelatedByIdPartial = false;
            }
        }

        return $this->collUnitMastersRelatedById;
    }

    /**
     * Sets a collection of ChildUnitMaster objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $unitMastersRelatedById A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUnitMaster The current object (for fluent API support)
     */
    public function setUnitMastersRelatedById(Collection $unitMastersRelatedById, ConnectionInterface $con = null)
    {
        /** @var ChildUnitMaster[] $unitMastersRelatedByIdToDelete */
        $unitMastersRelatedByIdToDelete = $this->getUnitMastersRelatedById(new Criteria(), $con)->diff($unitMastersRelatedById);


        $this->unitMastersRelatedByIdScheduledForDeletion = $unitMastersRelatedByIdToDelete;

        foreach ($unitMastersRelatedByIdToDelete as $unitMasterRelatedByIdRemoved) {
            $unitMasterRelatedByIdRemoved->setUnitParent(null);
        }

        $this->collUnitMastersRelatedById = null;
        foreach ($unitMastersRelatedById as $unitMasterRelatedById) {
            $this->addUnitMasterRelatedById($unitMasterRelatedById);
        }

        $this->collUnitMastersRelatedById = $unitMastersRelatedById;
        $this->collUnitMastersRelatedByIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UnitMaster objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UnitMaster objects.
     * @throws PropelException
     */
    public function countUnitMastersRelatedById(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUnitMastersRelatedByIdPartial && !$this->isNew();
        if (null === $this->collUnitMastersRelatedById || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUnitMastersRelatedById) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUnitMastersRelatedById());
            }

            $query = ChildUnitMasterQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUnitParent($this)
                ->count($con);
        }

        return count($this->collUnitMastersRelatedById);
    }

    /**
     * Method called to associate a ChildUnitMaster object to this object
     * through the ChildUnitMaster foreign key attribute.
     *
     * @param  ChildUnitMaster $l ChildUnitMaster
     * @return $this|\CoreBundle\Model\Eperformance\UnitMaster The current object (for fluent API support)
     */
    public function addUnitMasterRelatedById(ChildUnitMaster $l)
    {
        if ($this->collUnitMastersRelatedById === null) {
            $this->initUnitMastersRelatedById();
            $this->collUnitMastersRelatedByIdPartial = true;
        }

        if (!$this->collUnitMastersRelatedById->contains($l)) {
            $this->doAddUnitMasterRelatedById($l);

            if ($this->unitMastersRelatedByIdScheduledForDeletion and $this->unitMastersRelatedByIdScheduledForDeletion->contains($l)) {
                $this->unitMastersRelatedByIdScheduledForDeletion->remove($this->unitMastersRelatedByIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUnitMaster $unitMasterRelatedById The ChildUnitMaster object to add.
     */
    protected function doAddUnitMasterRelatedById(ChildUnitMaster $unitMasterRelatedById)
    {
        $this->collUnitMastersRelatedById[]= $unitMasterRelatedById;
        $unitMasterRelatedById->setUnitParent($this);
    }

    /**
     * @param  ChildUnitMaster $unitMasterRelatedById The ChildUnitMaster object to remove.
     * @return $this|ChildUnitMaster The current object (for fluent API support)
     */
    public function removeUnitMasterRelatedById(ChildUnitMaster $unitMasterRelatedById)
    {
        if ($this->getUnitMastersRelatedById()->contains($unitMasterRelatedById)) {
            $pos = $this->collUnitMastersRelatedById->search($unitMasterRelatedById);
            $this->collUnitMastersRelatedById->remove($pos);
            if (null === $this->unitMastersRelatedByIdScheduledForDeletion) {
                $this->unitMastersRelatedByIdScheduledForDeletion = clone $this->collUnitMastersRelatedById;
                $this->unitMastersRelatedByIdScheduledForDeletion->clear();
            }
            $this->unitMastersRelatedByIdScheduledForDeletion[]= $unitMasterRelatedById;
            $unitMasterRelatedById->setUnitParent(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aUnitParent) {
            $this->aUnitParent->removeUnitMasterRelatedById($this);
        }
        $this->id = null;
        $this->kode = null;
        $this->nama = null;
        $this->is_sekretariat_daerah = null;
        $this->is_dinas = null;
        $this->is_badan = null;
        $this->is_rumah_sakit = null;
        $this->is_bagian = null;
        $this->is_kecamatan = null;
        $this->is_kelurahan = null;
        $this->parent_id = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->deleted_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collJabatans) {
                foreach ($this->collJabatans as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPegawais) {
                foreach ($this->collPegawais as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPegawaiMasters) {
                foreach ($this->collPegawaiMasters as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUnitMastersRelatedById) {
                foreach ($this->collUnitMastersRelatedById as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collJabatans = null;
        $this->collPegawais = null;
        $this->collPegawaiMasters = null;
        $this->collUnitMastersRelatedById = null;
        $this->aUnitParent = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UnitMasterTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildUnitMaster The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[UnitMasterTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}

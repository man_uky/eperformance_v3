<?php

namespace CoreBundle\Model\Eperformance\Base;

use \Exception;
use \PDO;
use CoreBundle\Model\Eperformance\PenggunaMaster as ChildPenggunaMaster;
use CoreBundle\Model\Eperformance\PenggunaMasterQuery as ChildPenggunaMasterQuery;
use CoreBundle\Model\Eperformance\Map\PenggunaMasterTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'eperformance.pengguna_master' table.
 *
 *
 *
 * @method     ChildPenggunaMasterQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPenggunaMasterQuery orderByUsername($order = Criteria::ASC) Order by the username column
 * @method     ChildPenggunaMasterQuery orderByPassword($order = Criteria::ASC) Order by the password column
 * @method     ChildPenggunaMasterQuery orderByNip($order = Criteria::ASC) Order by the nip column
 * @method     ChildPenggunaMasterQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method     ChildPenggunaMasterQuery orderByIsAktif($order = Criteria::ASC) Order by the is_aktif column
 * @method     ChildPenggunaMasterQuery orderByCredentialId($order = Criteria::ASC) Order by the credential_id column
 * @method     ChildPenggunaMasterQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPenggunaMasterQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildPenggunaMasterQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 *
 * @method     ChildPenggunaMasterQuery groupById() Group by the id column
 * @method     ChildPenggunaMasterQuery groupByUsername() Group by the username column
 * @method     ChildPenggunaMasterQuery groupByPassword() Group by the password column
 * @method     ChildPenggunaMasterQuery groupByNip() Group by the nip column
 * @method     ChildPenggunaMasterQuery groupByNama() Group by the nama column
 * @method     ChildPenggunaMasterQuery groupByIsAktif() Group by the is_aktif column
 * @method     ChildPenggunaMasterQuery groupByCredentialId() Group by the credential_id column
 * @method     ChildPenggunaMasterQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPenggunaMasterQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildPenggunaMasterQuery groupByDeletedAt() Group by the deleted_at column
 *
 * @method     ChildPenggunaMasterQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPenggunaMasterQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPenggunaMasterQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPenggunaMasterQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPenggunaMasterQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPenggunaMasterQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPenggunaMasterQuery leftJoinCredential($relationAlias = null) Adds a LEFT JOIN clause to the query using the Credential relation
 * @method     ChildPenggunaMasterQuery rightJoinCredential($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Credential relation
 * @method     ChildPenggunaMasterQuery innerJoinCredential($relationAlias = null) Adds a INNER JOIN clause to the query using the Credential relation
 *
 * @method     ChildPenggunaMasterQuery joinWithCredential($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Credential relation
 *
 * @method     ChildPenggunaMasterQuery leftJoinWithCredential() Adds a LEFT JOIN clause and with to the query using the Credential relation
 * @method     ChildPenggunaMasterQuery rightJoinWithCredential() Adds a RIGHT JOIN clause and with to the query using the Credential relation
 * @method     ChildPenggunaMasterQuery innerJoinWithCredential() Adds a INNER JOIN clause and with to the query using the Credential relation
 *
 * @method     \CoreBundle\Model\Eperformance\CredentialQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPenggunaMaster findOne(ConnectionInterface $con = null) Return the first ChildPenggunaMaster matching the query
 * @method     ChildPenggunaMaster findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPenggunaMaster matching the query, or a new ChildPenggunaMaster object populated from the query conditions when no match is found
 *
 * @method     ChildPenggunaMaster findOneById(int $id) Return the first ChildPenggunaMaster filtered by the id column
 * @method     ChildPenggunaMaster findOneByUsername(string $username) Return the first ChildPenggunaMaster filtered by the username column
 * @method     ChildPenggunaMaster findOneByPassword(string $password) Return the first ChildPenggunaMaster filtered by the password column
 * @method     ChildPenggunaMaster findOneByNip(string $nip) Return the first ChildPenggunaMaster filtered by the nip column
 * @method     ChildPenggunaMaster findOneByNama(string $nama) Return the first ChildPenggunaMaster filtered by the nama column
 * @method     ChildPenggunaMaster findOneByIsAktif(boolean $is_aktif) Return the first ChildPenggunaMaster filtered by the is_aktif column
 * @method     ChildPenggunaMaster findOneByCredentialId(int $credential_id) Return the first ChildPenggunaMaster filtered by the credential_id column
 * @method     ChildPenggunaMaster findOneByCreatedAt(string $created_at) Return the first ChildPenggunaMaster filtered by the created_at column
 * @method     ChildPenggunaMaster findOneByUpdatedAt(string $updated_at) Return the first ChildPenggunaMaster filtered by the updated_at column
 * @method     ChildPenggunaMaster findOneByDeletedAt(string $deleted_at) Return the first ChildPenggunaMaster filtered by the deleted_at column *

 * @method     ChildPenggunaMaster requirePk($key, ConnectionInterface $con = null) Return the ChildPenggunaMaster by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPenggunaMaster requireOne(ConnectionInterface $con = null) Return the first ChildPenggunaMaster matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPenggunaMaster requireOneById(int $id) Return the first ChildPenggunaMaster filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPenggunaMaster requireOneByUsername(string $username) Return the first ChildPenggunaMaster filtered by the username column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPenggunaMaster requireOneByPassword(string $password) Return the first ChildPenggunaMaster filtered by the password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPenggunaMaster requireOneByNip(string $nip) Return the first ChildPenggunaMaster filtered by the nip column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPenggunaMaster requireOneByNama(string $nama) Return the first ChildPenggunaMaster filtered by the nama column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPenggunaMaster requireOneByIsAktif(boolean $is_aktif) Return the first ChildPenggunaMaster filtered by the is_aktif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPenggunaMaster requireOneByCredentialId(int $credential_id) Return the first ChildPenggunaMaster filtered by the credential_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPenggunaMaster requireOneByCreatedAt(string $created_at) Return the first ChildPenggunaMaster filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPenggunaMaster requireOneByUpdatedAt(string $updated_at) Return the first ChildPenggunaMaster filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPenggunaMaster requireOneByDeletedAt(string $deleted_at) Return the first ChildPenggunaMaster filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPenggunaMaster[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPenggunaMaster objects based on current ModelCriteria
 * @method     ChildPenggunaMaster[]|ObjectCollection findById(int $id) Return ChildPenggunaMaster objects filtered by the id column
 * @method     ChildPenggunaMaster[]|ObjectCollection findByUsername(string $username) Return ChildPenggunaMaster objects filtered by the username column
 * @method     ChildPenggunaMaster[]|ObjectCollection findByPassword(string $password) Return ChildPenggunaMaster objects filtered by the password column
 * @method     ChildPenggunaMaster[]|ObjectCollection findByNip(string $nip) Return ChildPenggunaMaster objects filtered by the nip column
 * @method     ChildPenggunaMaster[]|ObjectCollection findByNama(string $nama) Return ChildPenggunaMaster objects filtered by the nama column
 * @method     ChildPenggunaMaster[]|ObjectCollection findByIsAktif(boolean $is_aktif) Return ChildPenggunaMaster objects filtered by the is_aktif column
 * @method     ChildPenggunaMaster[]|ObjectCollection findByCredentialId(int $credential_id) Return ChildPenggunaMaster objects filtered by the credential_id column
 * @method     ChildPenggunaMaster[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildPenggunaMaster objects filtered by the created_at column
 * @method     ChildPenggunaMaster[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildPenggunaMaster objects filtered by the updated_at column
 * @method     ChildPenggunaMaster[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildPenggunaMaster objects filtered by the deleted_at column
 * @method     ChildPenggunaMaster[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PenggunaMasterQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \CoreBundle\Model\Eperformance\Base\PenggunaMasterQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\CoreBundle\\Model\\Eperformance\\PenggunaMaster', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPenggunaMasterQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPenggunaMasterQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPenggunaMasterQuery) {
            return $criteria;
        }
        $query = new ChildPenggunaMasterQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPenggunaMaster|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PenggunaMasterTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PenggunaMasterTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPenggunaMaster A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, username, password, nip, nama, is_aktif, credential_id, created_at, updated_at, deleted_at FROM eperformance.pengguna_master WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPenggunaMaster $obj */
            $obj = new ChildPenggunaMaster();
            $obj->hydrate($row);
            PenggunaMasterTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPenggunaMaster|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PenggunaMasterTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PenggunaMasterTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PenggunaMasterTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PenggunaMasterTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PenggunaMasterTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the username column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE username = 'fooValue'
     * $query->filterByUsername('%fooValue%', Criteria::LIKE); // WHERE username LIKE '%fooValue%'
     * </code>
     *
     * @param     string $username The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function filterByUsername($username = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PenggunaMasterTableMap::COL_USERNAME, $username, $comparison);
    }

    /**
     * Filter the query on the password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE password = 'fooValue'
     * $query->filterByPassword('%fooValue%', Criteria::LIKE); // WHERE password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PenggunaMasterTableMap::COL_PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the nip column
     *
     * Example usage:
     * <code>
     * $query->filterByNip('fooValue');   // WHERE nip = 'fooValue'
     * $query->filterByNip('%fooValue%', Criteria::LIKE); // WHERE nip LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nip The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function filterByNip($nip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nip)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PenggunaMasterTableMap::COL_NIP, $nip, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%', Criteria::LIKE); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PenggunaMasterTableMap::COL_NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the is_aktif column
     *
     * Example usage:
     * <code>
     * $query->filterByIsAktif(true); // WHERE is_aktif = true
     * $query->filterByIsAktif('yes'); // WHERE is_aktif = true
     * </code>
     *
     * @param     boolean|string $isAktif The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function filterByIsAktif($isAktif = null, $comparison = null)
    {
        if (is_string($isAktif)) {
            $isAktif = in_array(strtolower($isAktif), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(PenggunaMasterTableMap::COL_IS_AKTIF, $isAktif, $comparison);
    }

    /**
     * Filter the query on the credential_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCredentialId(1234); // WHERE credential_id = 1234
     * $query->filterByCredentialId(array(12, 34)); // WHERE credential_id IN (12, 34)
     * $query->filterByCredentialId(array('min' => 12)); // WHERE credential_id > 12
     * </code>
     *
     * @see       filterByCredential()
     *
     * @param     mixed $credentialId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function filterByCredentialId($credentialId = null, $comparison = null)
    {
        if (is_array($credentialId)) {
            $useMinMax = false;
            if (isset($credentialId['min'])) {
                $this->addUsingAlias(PenggunaMasterTableMap::COL_CREDENTIAL_ID, $credentialId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($credentialId['max'])) {
                $this->addUsingAlias(PenggunaMasterTableMap::COL_CREDENTIAL_ID, $credentialId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PenggunaMasterTableMap::COL_CREDENTIAL_ID, $credentialId, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PenggunaMasterTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PenggunaMasterTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PenggunaMasterTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PenggunaMasterTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PenggunaMasterTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PenggunaMasterTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(PenggunaMasterTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(PenggunaMasterTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PenggunaMasterTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\Credential object
     *
     * @param \CoreBundle\Model\Eperformance\Credential|ObjectCollection $credential The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function filterByCredential($credential, $comparison = null)
    {
        if ($credential instanceof \CoreBundle\Model\Eperformance\Credential) {
            return $this
                ->addUsingAlias(PenggunaMasterTableMap::COL_CREDENTIAL_ID, $credential->getId(), $comparison);
        } elseif ($credential instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PenggunaMasterTableMap::COL_CREDENTIAL_ID, $credential->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCredential() only accepts arguments of type \CoreBundle\Model\Eperformance\Credential or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Credential relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function joinCredential($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Credential');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Credential');
        }

        return $this;
    }

    /**
     * Use the Credential relation Credential object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\CredentialQuery A secondary query class using the current class as primary query
     */
    public function useCredentialQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCredential($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Credential', '\CoreBundle\Model\Eperformance\CredentialQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPenggunaMaster $penggunaMaster Object to remove from the list of results
     *
     * @return $this|ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function prune($penggunaMaster = null)
    {
        if ($penggunaMaster) {
            $this->addUsingAlias(PenggunaMasterTableMap::COL_ID, $penggunaMaster->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the eperformance.pengguna_master table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PenggunaMasterTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PenggunaMasterTableMap::clearInstancePool();
            PenggunaMasterTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PenggunaMasterTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PenggunaMasterTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PenggunaMasterTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PenggunaMasterTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 7200)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PenggunaMasterTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(PenggunaMasterTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }


        return $con->getDataFetcher($stmt);
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(PenggunaMasterTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(PenggunaMasterTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(PenggunaMasterTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(PenggunaMasterTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(PenggunaMasterTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildPenggunaMasterQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(PenggunaMasterTableMap::COL_CREATED_AT);
    }

} // PenggunaMasterQuery

<?php

namespace CoreBundle\Model\Eperformance\Base;

use \DateTime;
use \Exception;
use \PDO;
use CoreBundle\Model\Eperformance\Credential as ChildCredential;
use CoreBundle\Model\Eperformance\CredentialQuery as ChildCredentialQuery;
use CoreBundle\Model\Eperformance\Jabatan as ChildJabatan;
use CoreBundle\Model\Eperformance\JabatanQuery as ChildJabatanQuery;
use CoreBundle\Model\Eperformance\Pangkat as ChildPangkat;
use CoreBundle\Model\Eperformance\PangkatQuery as ChildPangkatQuery;
use CoreBundle\Model\Eperformance\Pegawai as ChildPegawai;
use CoreBundle\Model\Eperformance\PegawaiMaster as ChildPegawaiMaster;
use CoreBundle\Model\Eperformance\PegawaiMasterQuery as ChildPegawaiMasterQuery;
use CoreBundle\Model\Eperformance\PegawaiQuery as ChildPegawaiQuery;
use CoreBundle\Model\Eperformance\PegawaiStatus as ChildPegawaiStatus;
use CoreBundle\Model\Eperformance\PegawaiStatusQuery as ChildPegawaiStatusQuery;
use CoreBundle\Model\Eperformance\UnitMaster as ChildUnitMaster;
use CoreBundle\Model\Eperformance\UnitMasterQuery as ChildUnitMasterQuery;
use CoreBundle\Model\Eperformance\Map\PegawaiTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'eperformance.pegawai' table.
 *
 *
 *
 * @package    propel.generator.src.CoreBundle.Model.Eperformance.Base
 */
abstract class Pegawai implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\CoreBundle\\Model\\Eperformance\\Map\\PegawaiTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the username field.
     *
     * @var        string
     */
    protected $username;

    /**
     * The value for the password field.
     *
     * @var        string
     */
    protected $password;

    /**
     * The value for the kode_rapor field.
     *
     * @var        string
     */
    protected $kode_rapor;

    /**
     * The value for the credential_id field.
     *
     * @var        int
     */
    protected $credential_id;

    /**
     * The value for the pegawai_master_id field.
     *
     * @var        int
     */
    protected $pegawai_master_id;

    /**
     * The value for the unit_master_id field.
     *
     * @var        int
     */
    protected $unit_master_id;

    /**
     * The value for the atasan_id field.
     *
     * @var        int
     */
    protected $atasan_id;

    /**
     * The value for the pangkat_id field.
     *
     * @var        int
     */
    protected $pangkat_id;

    /**
     * The value for the jabatan_id field.
     *
     * @var        int
     */
    protected $jabatan_id;

    /**
     * The value for the pegawai_status_id field.
     *
     * @var        int
     */
    protected $pegawai_status_id;

    /**
     * The value for the eselon field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $eselon;

    /**
     * The value for the level field.
     *
     * @var        int
     */
    protected $level;

    /**
     * The value for the is_aktif field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_aktif;

    /**
     * The value for the is_trantib field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_trantib;

    /**
     * The value for the is_lapangan field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_lapangan;

    /**
     * The value for the tanggal_aktif field.
     *
     * @var        DateTime
     */
    protected $tanggal_aktif;

    /**
     * The value for the tanggal_nonaktif field.
     *
     * @var        DateTime
     */
    protected $tanggal_nonaktif;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * The value for the deleted_at field.
     *
     * @var        DateTime
     */
    protected $deleted_at;

    /**
     * @var        ChildCredential
     */
    protected $aCredential;

    /**
     * @var        ChildPegawaiMaster
     */
    protected $aPegawaiMaster;

    /**
     * @var        ChildUnitMaster
     */
    protected $aUnitMaster;

    /**
     * @var        ChildPegawai
     */
    protected $aPegawaiAtasan;

    /**
     * @var        ChildPangkat
     */
    protected $aPangkat;

    /**
     * @var        ChildJabatan
     */
    protected $aJabatan;

    /**
     * @var        ChildPegawaiStatus
     */
    protected $aPegawaiStatus;

    /**
     * @var        ObjectCollection|ChildPegawai[] Collection to store aggregation of ChildPegawai objects.
     */
    protected $collPegawaisRelatedById;
    protected $collPegawaisRelatedByIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPegawai[]
     */
    protected $pegawaisRelatedByIdScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->eselon = 0;
        $this->is_aktif = false;
        $this->is_trantib = false;
        $this->is_lapangan = false;
    }

    /**
     * Initializes internal state of CoreBundle\Model\Eperformance\Base\Pegawai object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Pegawai</code> instance.  If
     * <code>obj</code> is an instance of <code>Pegawai</code>, delegates to
     * <code>equals(Pegawai)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Pegawai The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [username] column value.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Get the [password] column value.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get the [kode_rapor] column value.
     *
     * @return string
     */
    public function getKodeRapor()
    {
        return $this->kode_rapor;
    }

    /**
     * Get the [credential_id] column value.
     *
     * @return int
     */
    public function getCredentialId()
    {
        return $this->credential_id;
    }

    /**
     * Get the [pegawai_master_id] column value.
     *
     * @return int
     */
    public function getPegawaiMasterId()
    {
        return $this->pegawai_master_id;
    }

    /**
     * Get the [unit_master_id] column value.
     *
     * @return int
     */
    public function getUnitMasterId()
    {
        return $this->unit_master_id;
    }

    /**
     * Get the [atasan_id] column value.
     *
     * @return int
     */
    public function getAtasanId()
    {
        return $this->atasan_id;
    }

    /**
     * Get the [pangkat_id] column value.
     *
     * @return int
     */
    public function getPangkatId()
    {
        return $this->pangkat_id;
    }

    /**
     * Get the [jabatan_id] column value.
     *
     * @return int
     */
    public function getJabatanId()
    {
        return $this->jabatan_id;
    }

    /**
     * Get the [pegawai_status_id] column value.
     *
     * @return int
     */
    public function getPegawaiStatusId()
    {
        return $this->pegawai_status_id;
    }

    /**
     * Get the [eselon] column value.
     *
     * @return int
     */
    public function getEselon()
    {
        return $this->eselon;
    }

    /**
     * Get the [level] column value.
     *
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Get the [is_aktif] column value.
     *
     * @return boolean
     */
    public function getIsAktif()
    {
        return $this->is_aktif;
    }

    /**
     * Get the [is_aktif] column value.
     *
     * @return boolean
     */
    public function isAktif()
    {
        return $this->getIsAktif();
    }

    /**
     * Get the [is_trantib] column value.
     *
     * @return boolean
     */
    public function getIsTrantib()
    {
        return $this->is_trantib;
    }

    /**
     * Get the [is_trantib] column value.
     *
     * @return boolean
     */
    public function isTrantib()
    {
        return $this->getIsTrantib();
    }

    /**
     * Get the [is_lapangan] column value.
     *
     * @return boolean
     */
    public function getIsLapangan()
    {
        return $this->is_lapangan;
    }

    /**
     * Get the [is_lapangan] column value.
     *
     * @return boolean
     */
    public function isLapangan()
    {
        return $this->getIsLapangan();
    }

    /**
     * Get the [optionally formatted] temporal [tanggal_aktif] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getTanggalAktif($format = NULL)
    {
        if ($format === null) {
            return $this->tanggal_aktif;
        } else {
            return $this->tanggal_aktif instanceof \DateTimeInterface ? $this->tanggal_aktif->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [tanggal_nonaktif] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getTanggalNonaktif($format = NULL)
    {
        if ($format === null) {
            return $this->tanggal_nonaktif;
        } else {
            return $this->tanggal_nonaktif instanceof \DateTimeInterface ? $this->tanggal_nonaktif->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [deleted_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDeletedAt($format = NULL)
    {
        if ($format === null) {
            return $this->deleted_at;
        } else {
            return $this->deleted_at instanceof \DateTimeInterface ? $this->deleted_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[PegawaiTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [username] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setUsername($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->username !== $v) {
            $this->username = $v;
            $this->modifiedColumns[PegawaiTableMap::COL_USERNAME] = true;
        }

        return $this;
    } // setUsername()

    /**
     * Set the value of [password] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[PegawaiTableMap::COL_PASSWORD] = true;
        }

        return $this;
    } // setPassword()

    /**
     * Set the value of [kode_rapor] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setKodeRapor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->kode_rapor !== $v) {
            $this->kode_rapor = $v;
            $this->modifiedColumns[PegawaiTableMap::COL_KODE_RAPOR] = true;
        }

        return $this;
    } // setKodeRapor()

    /**
     * Set the value of [credential_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setCredentialId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->credential_id !== $v) {
            $this->credential_id = $v;
            $this->modifiedColumns[PegawaiTableMap::COL_CREDENTIAL_ID] = true;
        }

        if ($this->aCredential !== null && $this->aCredential->getId() !== $v) {
            $this->aCredential = null;
        }

        return $this;
    } // setCredentialId()

    /**
     * Set the value of [pegawai_master_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setPegawaiMasterId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->pegawai_master_id !== $v) {
            $this->pegawai_master_id = $v;
            $this->modifiedColumns[PegawaiTableMap::COL_PEGAWAI_MASTER_ID] = true;
        }

        if ($this->aPegawaiMaster !== null && $this->aPegawaiMaster->getId() !== $v) {
            $this->aPegawaiMaster = null;
        }

        return $this;
    } // setPegawaiMasterId()

    /**
     * Set the value of [unit_master_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setUnitMasterId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->unit_master_id !== $v) {
            $this->unit_master_id = $v;
            $this->modifiedColumns[PegawaiTableMap::COL_UNIT_MASTER_ID] = true;
        }

        if ($this->aUnitMaster !== null && $this->aUnitMaster->getId() !== $v) {
            $this->aUnitMaster = null;
        }

        return $this;
    } // setUnitMasterId()

    /**
     * Set the value of [atasan_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setAtasanId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->atasan_id !== $v) {
            $this->atasan_id = $v;
            $this->modifiedColumns[PegawaiTableMap::COL_ATASAN_ID] = true;
        }

        if ($this->aPegawaiAtasan !== null && $this->aPegawaiAtasan->getId() !== $v) {
            $this->aPegawaiAtasan = null;
        }

        return $this;
    } // setAtasanId()

    /**
     * Set the value of [pangkat_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setPangkatId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->pangkat_id !== $v) {
            $this->pangkat_id = $v;
            $this->modifiedColumns[PegawaiTableMap::COL_PANGKAT_ID] = true;
        }

        if ($this->aPangkat !== null && $this->aPangkat->getId() !== $v) {
            $this->aPangkat = null;
        }

        return $this;
    } // setPangkatId()

    /**
     * Set the value of [jabatan_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setJabatanId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->jabatan_id !== $v) {
            $this->jabatan_id = $v;
            $this->modifiedColumns[PegawaiTableMap::COL_JABATAN_ID] = true;
        }

        if ($this->aJabatan !== null && $this->aJabatan->getId() !== $v) {
            $this->aJabatan = null;
        }

        return $this;
    } // setJabatanId()

    /**
     * Set the value of [pegawai_status_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setPegawaiStatusId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->pegawai_status_id !== $v) {
            $this->pegawai_status_id = $v;
            $this->modifiedColumns[PegawaiTableMap::COL_PEGAWAI_STATUS_ID] = true;
        }

        if ($this->aPegawaiStatus !== null && $this->aPegawaiStatus->getId() !== $v) {
            $this->aPegawaiStatus = null;
        }

        return $this;
    } // setPegawaiStatusId()

    /**
     * Set the value of [eselon] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setEselon($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->eselon !== $v) {
            $this->eselon = $v;
            $this->modifiedColumns[PegawaiTableMap::COL_ESELON] = true;
        }

        return $this;
    } // setEselon()

    /**
     * Set the value of [level] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setLevel($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->level !== $v) {
            $this->level = $v;
            $this->modifiedColumns[PegawaiTableMap::COL_LEVEL] = true;
        }

        return $this;
    } // setLevel()

    /**
     * Sets the value of the [is_aktif] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setIsAktif($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_aktif !== $v) {
            $this->is_aktif = $v;
            $this->modifiedColumns[PegawaiTableMap::COL_IS_AKTIF] = true;
        }

        return $this;
    } // setIsAktif()

    /**
     * Sets the value of the [is_trantib] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setIsTrantib($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_trantib !== $v) {
            $this->is_trantib = $v;
            $this->modifiedColumns[PegawaiTableMap::COL_IS_TRANTIB] = true;
        }

        return $this;
    } // setIsTrantib()

    /**
     * Sets the value of the [is_lapangan] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setIsLapangan($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_lapangan !== $v) {
            $this->is_lapangan = $v;
            $this->modifiedColumns[PegawaiTableMap::COL_IS_LAPANGAN] = true;
        }

        return $this;
    } // setIsLapangan()

    /**
     * Sets the value of [tanggal_aktif] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setTanggalAktif($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->tanggal_aktif !== null || $dt !== null) {
            if ($this->tanggal_aktif === null || $dt === null || $dt->format("Y-m-d") !== $this->tanggal_aktif->format("Y-m-d")) {
                $this->tanggal_aktif = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PegawaiTableMap::COL_TANGGAL_AKTIF] = true;
            }
        } // if either are not null

        return $this;
    } // setTanggalAktif()

    /**
     * Sets the value of [tanggal_nonaktif] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setTanggalNonaktif($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->tanggal_nonaktif !== null || $dt !== null) {
            if ($this->tanggal_nonaktif === null || $dt === null || $dt->format("Y-m-d") !== $this->tanggal_nonaktif->format("Y-m-d")) {
                $this->tanggal_nonaktif = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PegawaiTableMap::COL_TANGGAL_NONAKTIF] = true;
            }
        } // if either are not null

        return $this;
    } // setTanggalNonaktif()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PegawaiTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PegawaiTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Sets the value of [deleted_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function setDeletedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->deleted_at !== null || $dt !== null) {
            if ($this->deleted_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->deleted_at->format("Y-m-d H:i:s.u")) {
                $this->deleted_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PegawaiTableMap::COL_DELETED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setDeletedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->eselon !== 0) {
                return false;
            }

            if ($this->is_aktif !== false) {
                return false;
            }

            if ($this->is_trantib !== false) {
                return false;
            }

            if ($this->is_lapangan !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : PegawaiTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : PegawaiTableMap::translateFieldName('Username', TableMap::TYPE_PHPNAME, $indexType)];
            $this->username = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : PegawaiTableMap::translateFieldName('Password', TableMap::TYPE_PHPNAME, $indexType)];
            $this->password = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : PegawaiTableMap::translateFieldName('KodeRapor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->kode_rapor = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : PegawaiTableMap::translateFieldName('CredentialId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->credential_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : PegawaiTableMap::translateFieldName('PegawaiMasterId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pegawai_master_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : PegawaiTableMap::translateFieldName('UnitMasterId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->unit_master_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : PegawaiTableMap::translateFieldName('AtasanId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->atasan_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : PegawaiTableMap::translateFieldName('PangkatId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pangkat_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : PegawaiTableMap::translateFieldName('JabatanId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->jabatan_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : PegawaiTableMap::translateFieldName('PegawaiStatusId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pegawai_status_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : PegawaiTableMap::translateFieldName('Eselon', TableMap::TYPE_PHPNAME, $indexType)];
            $this->eselon = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : PegawaiTableMap::translateFieldName('Level', TableMap::TYPE_PHPNAME, $indexType)];
            $this->level = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : PegawaiTableMap::translateFieldName('IsAktif', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_aktif = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : PegawaiTableMap::translateFieldName('IsTrantib', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_trantib = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : PegawaiTableMap::translateFieldName('IsLapangan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_lapangan = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : PegawaiTableMap::translateFieldName('TanggalAktif', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tanggal_aktif = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : PegawaiTableMap::translateFieldName('TanggalNonaktif', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tanggal_nonaktif = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : PegawaiTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : PegawaiTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : PegawaiTableMap::translateFieldName('DeletedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->deleted_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 21; // 21 = PegawaiTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\CoreBundle\\Model\\Eperformance\\Pegawai'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aCredential !== null && $this->credential_id !== $this->aCredential->getId()) {
            $this->aCredential = null;
        }
        if ($this->aPegawaiMaster !== null && $this->pegawai_master_id !== $this->aPegawaiMaster->getId()) {
            $this->aPegawaiMaster = null;
        }
        if ($this->aUnitMaster !== null && $this->unit_master_id !== $this->aUnitMaster->getId()) {
            $this->aUnitMaster = null;
        }
        if ($this->aPegawaiAtasan !== null && $this->atasan_id !== $this->aPegawaiAtasan->getId()) {
            $this->aPegawaiAtasan = null;
        }
        if ($this->aPangkat !== null && $this->pangkat_id !== $this->aPangkat->getId()) {
            $this->aPangkat = null;
        }
        if ($this->aJabatan !== null && $this->jabatan_id !== $this->aJabatan->getId()) {
            $this->aJabatan = null;
        }
        if ($this->aPegawaiStatus !== null && $this->pegawai_status_id !== $this->aPegawaiStatus->getId()) {
            $this->aPegawaiStatus = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PegawaiTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildPegawaiQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCredential = null;
            $this->aPegawaiMaster = null;
            $this->aUnitMaster = null;
            $this->aPegawaiAtasan = null;
            $this->aPangkat = null;
            $this->aJabatan = null;
            $this->aPegawaiStatus = null;
            $this->collPegawaisRelatedById = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Pegawai::setDeleted()
     * @see Pegawai::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PegawaiTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildPegawaiQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PegawaiTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(PegawaiTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(PegawaiTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(PegawaiTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PegawaiTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCredential !== null) {
                if ($this->aCredential->isModified() || $this->aCredential->isNew()) {
                    $affectedRows += $this->aCredential->save($con);
                }
                $this->setCredential($this->aCredential);
            }

            if ($this->aPegawaiMaster !== null) {
                if ($this->aPegawaiMaster->isModified() || $this->aPegawaiMaster->isNew()) {
                    $affectedRows += $this->aPegawaiMaster->save($con);
                }
                $this->setPegawaiMaster($this->aPegawaiMaster);
            }

            if ($this->aUnitMaster !== null) {
                if ($this->aUnitMaster->isModified() || $this->aUnitMaster->isNew()) {
                    $affectedRows += $this->aUnitMaster->save($con);
                }
                $this->setUnitMaster($this->aUnitMaster);
            }

            if ($this->aPegawaiAtasan !== null) {
                if ($this->aPegawaiAtasan->isModified() || $this->aPegawaiAtasan->isNew()) {
                    $affectedRows += $this->aPegawaiAtasan->save($con);
                }
                $this->setPegawaiAtasan($this->aPegawaiAtasan);
            }

            if ($this->aPangkat !== null) {
                if ($this->aPangkat->isModified() || $this->aPangkat->isNew()) {
                    $affectedRows += $this->aPangkat->save($con);
                }
                $this->setPangkat($this->aPangkat);
            }

            if ($this->aJabatan !== null) {
                if ($this->aJabatan->isModified() || $this->aJabatan->isNew()) {
                    $affectedRows += $this->aJabatan->save($con);
                }
                $this->setJabatan($this->aJabatan);
            }

            if ($this->aPegawaiStatus !== null) {
                if ($this->aPegawaiStatus->isModified() || $this->aPegawaiStatus->isNew()) {
                    $affectedRows += $this->aPegawaiStatus->save($con);
                }
                $this->setPegawaiStatus($this->aPegawaiStatus);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->pegawaisRelatedByIdScheduledForDeletion !== null) {
                if (!$this->pegawaisRelatedByIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->pegawaisRelatedByIdScheduledForDeletion as $pegawaiRelatedById) {
                        // need to save related object because we set the relation to null
                        $pegawaiRelatedById->save($con);
                    }
                    $this->pegawaisRelatedByIdScheduledForDeletion = null;
                }
            }

            if ($this->collPegawaisRelatedById !== null) {
                foreach ($this->collPegawaisRelatedById as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[PegawaiTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . PegawaiTableMap::COL_ID . ')');
        }
        if (null === $this->id) {
            try {
                $dataFetcher = $con->query("SELECT nextval('eperformance.pegawai_id_seq')");
                $this->id = (int) $dataFetcher->fetchColumn();
            } catch (Exception $e) {
                throw new PropelException('Unable to get sequence id.', 0, $e);
            }
        }


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(PegawaiTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_USERNAME)) {
            $modifiedColumns[':p' . $index++]  = 'username';
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = 'password';
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_KODE_RAPOR)) {
            $modifiedColumns[':p' . $index++]  = 'kode_rapor';
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_CREDENTIAL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'credential_id';
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_PEGAWAI_MASTER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'pegawai_master_id';
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_UNIT_MASTER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'unit_master_id';
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_ATASAN_ID)) {
            $modifiedColumns[':p' . $index++]  = 'atasan_id';
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_PANGKAT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'pangkat_id';
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_JABATAN_ID)) {
            $modifiedColumns[':p' . $index++]  = 'jabatan_id';
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_PEGAWAI_STATUS_ID)) {
            $modifiedColumns[':p' . $index++]  = 'pegawai_status_id';
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_ESELON)) {
            $modifiedColumns[':p' . $index++]  = 'eselon';
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_LEVEL)) {
            $modifiedColumns[':p' . $index++]  = 'level';
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_IS_AKTIF)) {
            $modifiedColumns[':p' . $index++]  = 'is_aktif';
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_IS_TRANTIB)) {
            $modifiedColumns[':p' . $index++]  = 'is_trantib';
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_IS_LAPANGAN)) {
            $modifiedColumns[':p' . $index++]  = 'is_lapangan';
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_TANGGAL_AKTIF)) {
            $modifiedColumns[':p' . $index++]  = 'tanggal_aktif';
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_TANGGAL_NONAKTIF)) {
            $modifiedColumns[':p' . $index++]  = 'tanggal_nonaktif';
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_DELETED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'deleted_at';
        }

        $sql = sprintf(
            'INSERT INTO eperformance.pegawai (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'username':
                        $stmt->bindValue($identifier, $this->username, PDO::PARAM_STR);
                        break;
                    case 'password':
                        $stmt->bindValue($identifier, $this->password, PDO::PARAM_STR);
                        break;
                    case 'kode_rapor':
                        $stmt->bindValue($identifier, $this->kode_rapor, PDO::PARAM_STR);
                        break;
                    case 'credential_id':
                        $stmt->bindValue($identifier, $this->credential_id, PDO::PARAM_INT);
                        break;
                    case 'pegawai_master_id':
                        $stmt->bindValue($identifier, $this->pegawai_master_id, PDO::PARAM_INT);
                        break;
                    case 'unit_master_id':
                        $stmt->bindValue($identifier, $this->unit_master_id, PDO::PARAM_INT);
                        break;
                    case 'atasan_id':
                        $stmt->bindValue($identifier, $this->atasan_id, PDO::PARAM_INT);
                        break;
                    case 'pangkat_id':
                        $stmt->bindValue($identifier, $this->pangkat_id, PDO::PARAM_INT);
                        break;
                    case 'jabatan_id':
                        $stmt->bindValue($identifier, $this->jabatan_id, PDO::PARAM_INT);
                        break;
                    case 'pegawai_status_id':
                        $stmt->bindValue($identifier, $this->pegawai_status_id, PDO::PARAM_INT);
                        break;
                    case 'eselon':
                        $stmt->bindValue($identifier, $this->eselon, PDO::PARAM_INT);
                        break;
                    case 'level':
                        $stmt->bindValue($identifier, $this->level, PDO::PARAM_INT);
                        break;
                    case 'is_aktif':
                        $stmt->bindValue($identifier, $this->is_aktif, PDO::PARAM_BOOL);
                        break;
                    case 'is_trantib':
                        $stmt->bindValue($identifier, $this->is_trantib, PDO::PARAM_BOOL);
                        break;
                    case 'is_lapangan':
                        $stmt->bindValue($identifier, $this->is_lapangan, PDO::PARAM_BOOL);
                        break;
                    case 'tanggal_aktif':
                        $stmt->bindValue($identifier, $this->tanggal_aktif ? $this->tanggal_aktif->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'tanggal_nonaktif':
                        $stmt->bindValue($identifier, $this->tanggal_nonaktif ? $this->tanggal_nonaktif->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'deleted_at':
                        $stmt->bindValue($identifier, $this->deleted_at ? $this->deleted_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PegawaiTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getUsername();
                break;
            case 2:
                return $this->getPassword();
                break;
            case 3:
                return $this->getKodeRapor();
                break;
            case 4:
                return $this->getCredentialId();
                break;
            case 5:
                return $this->getPegawaiMasterId();
                break;
            case 6:
                return $this->getUnitMasterId();
                break;
            case 7:
                return $this->getAtasanId();
                break;
            case 8:
                return $this->getPangkatId();
                break;
            case 9:
                return $this->getJabatanId();
                break;
            case 10:
                return $this->getPegawaiStatusId();
                break;
            case 11:
                return $this->getEselon();
                break;
            case 12:
                return $this->getLevel();
                break;
            case 13:
                return $this->getIsAktif();
                break;
            case 14:
                return $this->getIsTrantib();
                break;
            case 15:
                return $this->getIsLapangan();
                break;
            case 16:
                return $this->getTanggalAktif();
                break;
            case 17:
                return $this->getTanggalNonaktif();
                break;
            case 18:
                return $this->getCreatedAt();
                break;
            case 19:
                return $this->getUpdatedAt();
                break;
            case 20:
                return $this->getDeletedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Pegawai'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Pegawai'][$this->hashCode()] = true;
        $keys = PegawaiTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getUsername(),
            $keys[2] => $this->getPassword(),
            $keys[3] => $this->getKodeRapor(),
            $keys[4] => $this->getCredentialId(),
            $keys[5] => $this->getPegawaiMasterId(),
            $keys[6] => $this->getUnitMasterId(),
            $keys[7] => $this->getAtasanId(),
            $keys[8] => $this->getPangkatId(),
            $keys[9] => $this->getJabatanId(),
            $keys[10] => $this->getPegawaiStatusId(),
            $keys[11] => $this->getEselon(),
            $keys[12] => $this->getLevel(),
            $keys[13] => $this->getIsAktif(),
            $keys[14] => $this->getIsTrantib(),
            $keys[15] => $this->getIsLapangan(),
            $keys[16] => $this->getTanggalAktif(),
            $keys[17] => $this->getTanggalNonaktif(),
            $keys[18] => $this->getCreatedAt(),
            $keys[19] => $this->getUpdatedAt(),
            $keys[20] => $this->getDeletedAt(),
        );
        if ($result[$keys[16]] instanceof \DateTime) {
            $result[$keys[16]] = $result[$keys[16]]->format('c');
        }

        if ($result[$keys[17]] instanceof \DateTime) {
            $result[$keys[17]] = $result[$keys[17]]->format('c');
        }

        if ($result[$keys[18]] instanceof \DateTime) {
            $result[$keys[18]] = $result[$keys[18]]->format('c');
        }

        if ($result[$keys[19]] instanceof \DateTime) {
            $result[$keys[19]] = $result[$keys[19]]->format('c');
        }

        if ($result[$keys[20]] instanceof \DateTime) {
            $result[$keys[20]] = $result[$keys[20]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aCredential) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'credential';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.credential';
                        break;
                    default:
                        $key = 'Credential';
                }

                $result[$key] = $this->aCredential->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPegawaiMaster) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pegawaiMaster';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.pegawai_master';
                        break;
                    default:
                        $key = 'PegawaiMaster';
                }

                $result[$key] = $this->aPegawaiMaster->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUnitMaster) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'unitMaster';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.unit_master';
                        break;
                    default:
                        $key = 'UnitMaster';
                }

                $result[$key] = $this->aUnitMaster->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPegawaiAtasan) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pegawai';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.pegawai';
                        break;
                    default:
                        $key = 'PegawaiAtasan';
                }

                $result[$key] = $this->aPegawaiAtasan->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPangkat) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pangkat';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.pangkat';
                        break;
                    default:
                        $key = 'Pangkat';
                }

                $result[$key] = $this->aPangkat->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJabatan) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'jabatan';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.jabatan';
                        break;
                    default:
                        $key = 'Jabatan';
                }

                $result[$key] = $this->aJabatan->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPegawaiStatus) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pegawaiStatus';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.pegawai_status';
                        break;
                    default:
                        $key = 'PegawaiStatus';
                }

                $result[$key] = $this->aPegawaiStatus->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collPegawaisRelatedById) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pegawais';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.pegawais';
                        break;
                    default:
                        $key = 'Pegawais';
                }

                $result[$key] = $this->collPegawaisRelatedById->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PegawaiTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setUsername($value);
                break;
            case 2:
                $this->setPassword($value);
                break;
            case 3:
                $this->setKodeRapor($value);
                break;
            case 4:
                $this->setCredentialId($value);
                break;
            case 5:
                $this->setPegawaiMasterId($value);
                break;
            case 6:
                $this->setUnitMasterId($value);
                break;
            case 7:
                $this->setAtasanId($value);
                break;
            case 8:
                $this->setPangkatId($value);
                break;
            case 9:
                $this->setJabatanId($value);
                break;
            case 10:
                $this->setPegawaiStatusId($value);
                break;
            case 11:
                $this->setEselon($value);
                break;
            case 12:
                $this->setLevel($value);
                break;
            case 13:
                $this->setIsAktif($value);
                break;
            case 14:
                $this->setIsTrantib($value);
                break;
            case 15:
                $this->setIsLapangan($value);
                break;
            case 16:
                $this->setTanggalAktif($value);
                break;
            case 17:
                $this->setTanggalNonaktif($value);
                break;
            case 18:
                $this->setCreatedAt($value);
                break;
            case 19:
                $this->setUpdatedAt($value);
                break;
            case 20:
                $this->setDeletedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = PegawaiTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setUsername($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setPassword($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setKodeRapor($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setCredentialId($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setPegawaiMasterId($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setUnitMasterId($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setAtasanId($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setPangkatId($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setJabatanId($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setPegawaiStatusId($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setEselon($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setLevel($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setIsAktif($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setIsTrantib($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setIsLapangan($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setTanggalAktif($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setTanggalNonaktif($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setCreatedAt($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setUpdatedAt($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setDeletedAt($arr[$keys[20]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PegawaiTableMap::DATABASE_NAME);

        if ($this->isColumnModified(PegawaiTableMap::COL_ID)) {
            $criteria->add(PegawaiTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_USERNAME)) {
            $criteria->add(PegawaiTableMap::COL_USERNAME, $this->username);
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_PASSWORD)) {
            $criteria->add(PegawaiTableMap::COL_PASSWORD, $this->password);
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_KODE_RAPOR)) {
            $criteria->add(PegawaiTableMap::COL_KODE_RAPOR, $this->kode_rapor);
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_CREDENTIAL_ID)) {
            $criteria->add(PegawaiTableMap::COL_CREDENTIAL_ID, $this->credential_id);
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_PEGAWAI_MASTER_ID)) {
            $criteria->add(PegawaiTableMap::COL_PEGAWAI_MASTER_ID, $this->pegawai_master_id);
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_UNIT_MASTER_ID)) {
            $criteria->add(PegawaiTableMap::COL_UNIT_MASTER_ID, $this->unit_master_id);
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_ATASAN_ID)) {
            $criteria->add(PegawaiTableMap::COL_ATASAN_ID, $this->atasan_id);
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_PANGKAT_ID)) {
            $criteria->add(PegawaiTableMap::COL_PANGKAT_ID, $this->pangkat_id);
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_JABATAN_ID)) {
            $criteria->add(PegawaiTableMap::COL_JABATAN_ID, $this->jabatan_id);
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_PEGAWAI_STATUS_ID)) {
            $criteria->add(PegawaiTableMap::COL_PEGAWAI_STATUS_ID, $this->pegawai_status_id);
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_ESELON)) {
            $criteria->add(PegawaiTableMap::COL_ESELON, $this->eselon);
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_LEVEL)) {
            $criteria->add(PegawaiTableMap::COL_LEVEL, $this->level);
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_IS_AKTIF)) {
            $criteria->add(PegawaiTableMap::COL_IS_AKTIF, $this->is_aktif);
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_IS_TRANTIB)) {
            $criteria->add(PegawaiTableMap::COL_IS_TRANTIB, $this->is_trantib);
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_IS_LAPANGAN)) {
            $criteria->add(PegawaiTableMap::COL_IS_LAPANGAN, $this->is_lapangan);
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_TANGGAL_AKTIF)) {
            $criteria->add(PegawaiTableMap::COL_TANGGAL_AKTIF, $this->tanggal_aktif);
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_TANGGAL_NONAKTIF)) {
            $criteria->add(PegawaiTableMap::COL_TANGGAL_NONAKTIF, $this->tanggal_nonaktif);
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_CREATED_AT)) {
            $criteria->add(PegawaiTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_UPDATED_AT)) {
            $criteria->add(PegawaiTableMap::COL_UPDATED_AT, $this->updated_at);
        }
        if ($this->isColumnModified(PegawaiTableMap::COL_DELETED_AT)) {
            $criteria->add(PegawaiTableMap::COL_DELETED_AT, $this->deleted_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildPegawaiQuery::create();
        $criteria->add(PegawaiTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \CoreBundle\Model\Eperformance\Pegawai (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUsername($this->getUsername());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setKodeRapor($this->getKodeRapor());
        $copyObj->setCredentialId($this->getCredentialId());
        $copyObj->setPegawaiMasterId($this->getPegawaiMasterId());
        $copyObj->setUnitMasterId($this->getUnitMasterId());
        $copyObj->setAtasanId($this->getAtasanId());
        $copyObj->setPangkatId($this->getPangkatId());
        $copyObj->setJabatanId($this->getJabatanId());
        $copyObj->setPegawaiStatusId($this->getPegawaiStatusId());
        $copyObj->setEselon($this->getEselon());
        $copyObj->setLevel($this->getLevel());
        $copyObj->setIsAktif($this->getIsAktif());
        $copyObj->setIsTrantib($this->getIsTrantib());
        $copyObj->setIsLapangan($this->getIsLapangan());
        $copyObj->setTanggalAktif($this->getTanggalAktif());
        $copyObj->setTanggalNonaktif($this->getTanggalNonaktif());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        $copyObj->setDeletedAt($this->getDeletedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getPegawaisRelatedById() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPegawaiRelatedById($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \CoreBundle\Model\Eperformance\Pegawai Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildCredential object.
     *
     * @param  ChildCredential $v
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCredential(ChildCredential $v = null)
    {
        if ($v === null) {
            $this->setCredentialId(NULL);
        } else {
            $this->setCredentialId($v->getId());
        }

        $this->aCredential = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCredential object, it will not be re-added.
        if ($v !== null) {
            $v->addPegawai($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCredential object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCredential The associated ChildCredential object.
     * @throws PropelException
     */
    public function getCredential(ConnectionInterface $con = null)
    {
        if ($this->aCredential === null && ($this->credential_id !== null)) {
            $this->aCredential = ChildCredentialQuery::create()->findPk($this->credential_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCredential->addPegawais($this);
             */
        }

        return $this->aCredential;
    }

    /**
     * Declares an association between this object and a ChildPegawaiMaster object.
     *
     * @param  ChildPegawaiMaster $v
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPegawaiMaster(ChildPegawaiMaster $v = null)
    {
        if ($v === null) {
            $this->setPegawaiMasterId(NULL);
        } else {
            $this->setPegawaiMasterId($v->getId());
        }

        $this->aPegawaiMaster = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPegawaiMaster object, it will not be re-added.
        if ($v !== null) {
            $v->addPegawai($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPegawaiMaster object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildPegawaiMaster The associated ChildPegawaiMaster object.
     * @throws PropelException
     */
    public function getPegawaiMaster(ConnectionInterface $con = null)
    {
        if ($this->aPegawaiMaster === null && ($this->pegawai_master_id !== null)) {
            $this->aPegawaiMaster = ChildPegawaiMasterQuery::create()->findPk($this->pegawai_master_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPegawaiMaster->addPegawais($this);
             */
        }

        return $this->aPegawaiMaster;
    }

    /**
     * Declares an association between this object and a ChildUnitMaster object.
     *
     * @param  ChildUnitMaster $v
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUnitMaster(ChildUnitMaster $v = null)
    {
        if ($v === null) {
            $this->setUnitMasterId(NULL);
        } else {
            $this->setUnitMasterId($v->getId());
        }

        $this->aUnitMaster = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUnitMaster object, it will not be re-added.
        if ($v !== null) {
            $v->addPegawai($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUnitMaster object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUnitMaster The associated ChildUnitMaster object.
     * @throws PropelException
     */
    public function getUnitMaster(ConnectionInterface $con = null)
    {
        if ($this->aUnitMaster === null && ($this->unit_master_id !== null)) {
            $this->aUnitMaster = ChildUnitMasterQuery::create()->findPk($this->unit_master_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUnitMaster->addPegawais($this);
             */
        }

        return $this->aUnitMaster;
    }

    /**
     * Declares an association between this object and a ChildPegawai object.
     *
     * @param  ChildPegawai $v
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPegawaiAtasan(ChildPegawai $v = null)
    {
        if ($v === null) {
            $this->setAtasanId(NULL);
        } else {
            $this->setAtasanId($v->getId());
        }

        $this->aPegawaiAtasan = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPegawai object, it will not be re-added.
        if ($v !== null) {
            $v->addPegawaiRelatedById($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPegawai object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildPegawai The associated ChildPegawai object.
     * @throws PropelException
     */
    public function getPegawaiAtasan(ConnectionInterface $con = null)
    {
        if ($this->aPegawaiAtasan === null && ($this->atasan_id !== null)) {
            $this->aPegawaiAtasan = ChildPegawaiQuery::create()->findPk($this->atasan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPegawaiAtasan->addPegawaisRelatedById($this);
             */
        }

        return $this->aPegawaiAtasan;
    }

    /**
     * Declares an association between this object and a ChildPangkat object.
     *
     * @param  ChildPangkat $v
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPangkat(ChildPangkat $v = null)
    {
        if ($v === null) {
            $this->setPangkatId(NULL);
        } else {
            $this->setPangkatId($v->getId());
        }

        $this->aPangkat = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPangkat object, it will not be re-added.
        if ($v !== null) {
            $v->addPegawai($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPangkat object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildPangkat The associated ChildPangkat object.
     * @throws PropelException
     */
    public function getPangkat(ConnectionInterface $con = null)
    {
        if ($this->aPangkat === null && ($this->pangkat_id !== null)) {
            $this->aPangkat = ChildPangkatQuery::create()->findPk($this->pangkat_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPangkat->addPegawais($this);
             */
        }

        return $this->aPangkat;
    }

    /**
     * Declares an association between this object and a ChildJabatan object.
     *
     * @param  ChildJabatan $v
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJabatan(ChildJabatan $v = null)
    {
        if ($v === null) {
            $this->setJabatanId(NULL);
        } else {
            $this->setJabatanId($v->getId());
        }

        $this->aJabatan = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildJabatan object, it will not be re-added.
        if ($v !== null) {
            $v->addPegawai($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildJabatan object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildJabatan The associated ChildJabatan object.
     * @throws PropelException
     */
    public function getJabatan(ConnectionInterface $con = null)
    {
        if ($this->aJabatan === null && ($this->jabatan_id !== null)) {
            $this->aJabatan = ChildJabatanQuery::create()->findPk($this->jabatan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJabatan->addPegawais($this);
             */
        }

        return $this->aJabatan;
    }

    /**
     * Declares an association between this object and a ChildPegawaiStatus object.
     *
     * @param  ChildPegawaiStatus $v
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPegawaiStatus(ChildPegawaiStatus $v = null)
    {
        if ($v === null) {
            $this->setPegawaiStatusId(NULL);
        } else {
            $this->setPegawaiStatusId($v->getId());
        }

        $this->aPegawaiStatus = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPegawaiStatus object, it will not be re-added.
        if ($v !== null) {
            $v->addPegawai($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPegawaiStatus object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildPegawaiStatus The associated ChildPegawaiStatus object.
     * @throws PropelException
     */
    public function getPegawaiStatus(ConnectionInterface $con = null)
    {
        if ($this->aPegawaiStatus === null && ($this->pegawai_status_id !== null)) {
            $this->aPegawaiStatus = ChildPegawaiStatusQuery::create()->findPk($this->pegawai_status_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPegawaiStatus->addPegawais($this);
             */
        }

        return $this->aPegawaiStatus;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('PegawaiRelatedById' == $relationName) {
            return $this->initPegawaisRelatedById();
        }
    }

    /**
     * Clears out the collPegawaisRelatedById collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPegawaisRelatedById()
     */
    public function clearPegawaisRelatedById()
    {
        $this->collPegawaisRelatedById = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPegawaisRelatedById collection loaded partially.
     */
    public function resetPartialPegawaisRelatedById($v = true)
    {
        $this->collPegawaisRelatedByIdPartial = $v;
    }

    /**
     * Initializes the collPegawaisRelatedById collection.
     *
     * By default this just sets the collPegawaisRelatedById collection to an empty array (like clearcollPegawaisRelatedById());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPegawaisRelatedById($overrideExisting = true)
    {
        if (null !== $this->collPegawaisRelatedById && !$overrideExisting) {
            return;
        }

        $collectionClassName = PegawaiTableMap::getTableMap()->getCollectionClassName();

        $this->collPegawaisRelatedById = new $collectionClassName;
        $this->collPegawaisRelatedById->setModel('\CoreBundle\Model\Eperformance\Pegawai');
    }

    /**
     * Gets an array of ChildPegawai objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPegawai is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     * @throws PropelException
     */
    public function getPegawaisRelatedById(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPegawaisRelatedByIdPartial && !$this->isNew();
        if (null === $this->collPegawaisRelatedById || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPegawaisRelatedById) {
                // return empty collection
                $this->initPegawaisRelatedById();
            } else {
                $collPegawaisRelatedById = ChildPegawaiQuery::create(null, $criteria)
                    ->filterByPegawaiAtasan($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPegawaisRelatedByIdPartial && count($collPegawaisRelatedById)) {
                        $this->initPegawaisRelatedById(false);

                        foreach ($collPegawaisRelatedById as $obj) {
                            if (false == $this->collPegawaisRelatedById->contains($obj)) {
                                $this->collPegawaisRelatedById->append($obj);
                            }
                        }

                        $this->collPegawaisRelatedByIdPartial = true;
                    }

                    return $collPegawaisRelatedById;
                }

                if ($partial && $this->collPegawaisRelatedById) {
                    foreach ($this->collPegawaisRelatedById as $obj) {
                        if ($obj->isNew()) {
                            $collPegawaisRelatedById[] = $obj;
                        }
                    }
                }

                $this->collPegawaisRelatedById = $collPegawaisRelatedById;
                $this->collPegawaisRelatedByIdPartial = false;
            }
        }

        return $this->collPegawaisRelatedById;
    }

    /**
     * Sets a collection of ChildPegawai objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $pegawaisRelatedById A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPegawai The current object (for fluent API support)
     */
    public function setPegawaisRelatedById(Collection $pegawaisRelatedById, ConnectionInterface $con = null)
    {
        /** @var ChildPegawai[] $pegawaisRelatedByIdToDelete */
        $pegawaisRelatedByIdToDelete = $this->getPegawaisRelatedById(new Criteria(), $con)->diff($pegawaisRelatedById);


        $this->pegawaisRelatedByIdScheduledForDeletion = $pegawaisRelatedByIdToDelete;

        foreach ($pegawaisRelatedByIdToDelete as $pegawaiRelatedByIdRemoved) {
            $pegawaiRelatedByIdRemoved->setPegawaiAtasan(null);
        }

        $this->collPegawaisRelatedById = null;
        foreach ($pegawaisRelatedById as $pegawaiRelatedById) {
            $this->addPegawaiRelatedById($pegawaiRelatedById);
        }

        $this->collPegawaisRelatedById = $pegawaisRelatedById;
        $this->collPegawaisRelatedByIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Pegawai objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Pegawai objects.
     * @throws PropelException
     */
    public function countPegawaisRelatedById(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPegawaisRelatedByIdPartial && !$this->isNew();
        if (null === $this->collPegawaisRelatedById || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPegawaisRelatedById) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPegawaisRelatedById());
            }

            $query = ChildPegawaiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPegawaiAtasan($this)
                ->count($con);
        }

        return count($this->collPegawaisRelatedById);
    }

    /**
     * Method called to associate a ChildPegawai object to this object
     * through the ChildPegawai foreign key attribute.
     *
     * @param  ChildPegawai $l ChildPegawai
     * @return $this|\CoreBundle\Model\Eperformance\Pegawai The current object (for fluent API support)
     */
    public function addPegawaiRelatedById(ChildPegawai $l)
    {
        if ($this->collPegawaisRelatedById === null) {
            $this->initPegawaisRelatedById();
            $this->collPegawaisRelatedByIdPartial = true;
        }

        if (!$this->collPegawaisRelatedById->contains($l)) {
            $this->doAddPegawaiRelatedById($l);

            if ($this->pegawaisRelatedByIdScheduledForDeletion and $this->pegawaisRelatedByIdScheduledForDeletion->contains($l)) {
                $this->pegawaisRelatedByIdScheduledForDeletion->remove($this->pegawaisRelatedByIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPegawai $pegawaiRelatedById The ChildPegawai object to add.
     */
    protected function doAddPegawaiRelatedById(ChildPegawai $pegawaiRelatedById)
    {
        $this->collPegawaisRelatedById[]= $pegawaiRelatedById;
        $pegawaiRelatedById->setPegawaiAtasan($this);
    }

    /**
     * @param  ChildPegawai $pegawaiRelatedById The ChildPegawai object to remove.
     * @return $this|ChildPegawai The current object (for fluent API support)
     */
    public function removePegawaiRelatedById(ChildPegawai $pegawaiRelatedById)
    {
        if ($this->getPegawaisRelatedById()->contains($pegawaiRelatedById)) {
            $pos = $this->collPegawaisRelatedById->search($pegawaiRelatedById);
            $this->collPegawaisRelatedById->remove($pos);
            if (null === $this->pegawaisRelatedByIdScheduledForDeletion) {
                $this->pegawaisRelatedByIdScheduledForDeletion = clone $this->collPegawaisRelatedById;
                $this->pegawaisRelatedByIdScheduledForDeletion->clear();
            }
            $this->pegawaisRelatedByIdScheduledForDeletion[]= $pegawaiRelatedById;
            $pegawaiRelatedById->setPegawaiAtasan(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pegawai is new, it will return
     * an empty collection; or if this Pegawai has previously
     * been saved, it will retrieve related PegawaisRelatedById from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pegawai.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     */
    public function getPegawaisRelatedByIdJoinCredential(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPegawaiQuery::create(null, $criteria);
        $query->joinWith('Credential', $joinBehavior);

        return $this->getPegawaisRelatedById($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pegawai is new, it will return
     * an empty collection; or if this Pegawai has previously
     * been saved, it will retrieve related PegawaisRelatedById from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pegawai.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     */
    public function getPegawaisRelatedByIdJoinPegawaiMaster(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPegawaiQuery::create(null, $criteria);
        $query->joinWith('PegawaiMaster', $joinBehavior);

        return $this->getPegawaisRelatedById($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pegawai is new, it will return
     * an empty collection; or if this Pegawai has previously
     * been saved, it will retrieve related PegawaisRelatedById from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pegawai.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     */
    public function getPegawaisRelatedByIdJoinUnitMaster(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPegawaiQuery::create(null, $criteria);
        $query->joinWith('UnitMaster', $joinBehavior);

        return $this->getPegawaisRelatedById($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pegawai is new, it will return
     * an empty collection; or if this Pegawai has previously
     * been saved, it will retrieve related PegawaisRelatedById from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pegawai.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     */
    public function getPegawaisRelatedByIdJoinPangkat(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPegawaiQuery::create(null, $criteria);
        $query->joinWith('Pangkat', $joinBehavior);

        return $this->getPegawaisRelatedById($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pegawai is new, it will return
     * an empty collection; or if this Pegawai has previously
     * been saved, it will retrieve related PegawaisRelatedById from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pegawai.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     */
    public function getPegawaisRelatedByIdJoinJabatan(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPegawaiQuery::create(null, $criteria);
        $query->joinWith('Jabatan', $joinBehavior);

        return $this->getPegawaisRelatedById($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pegawai is new, it will return
     * an empty collection; or if this Pegawai has previously
     * been saved, it will retrieve related PegawaisRelatedById from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pegawai.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPegawai[] List of ChildPegawai objects
     */
    public function getPegawaisRelatedByIdJoinPegawaiStatus(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPegawaiQuery::create(null, $criteria);
        $query->joinWith('PegawaiStatus', $joinBehavior);

        return $this->getPegawaisRelatedById($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aCredential) {
            $this->aCredential->removePegawai($this);
        }
        if (null !== $this->aPegawaiMaster) {
            $this->aPegawaiMaster->removePegawai($this);
        }
        if (null !== $this->aUnitMaster) {
            $this->aUnitMaster->removePegawai($this);
        }
        if (null !== $this->aPegawaiAtasan) {
            $this->aPegawaiAtasan->removePegawaiRelatedById($this);
        }
        if (null !== $this->aPangkat) {
            $this->aPangkat->removePegawai($this);
        }
        if (null !== $this->aJabatan) {
            $this->aJabatan->removePegawai($this);
        }
        if (null !== $this->aPegawaiStatus) {
            $this->aPegawaiStatus->removePegawai($this);
        }
        $this->id = null;
        $this->username = null;
        $this->password = null;
        $this->kode_rapor = null;
        $this->credential_id = null;
        $this->pegawai_master_id = null;
        $this->unit_master_id = null;
        $this->atasan_id = null;
        $this->pangkat_id = null;
        $this->jabatan_id = null;
        $this->pegawai_status_id = null;
        $this->eselon = null;
        $this->level = null;
        $this->is_aktif = null;
        $this->is_trantib = null;
        $this->is_lapangan = null;
        $this->tanggal_aktif = null;
        $this->tanggal_nonaktif = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->deleted_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collPegawaisRelatedById) {
                foreach ($this->collPegawaisRelatedById as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collPegawaisRelatedById = null;
        $this->aCredential = null;
        $this->aPegawaiMaster = null;
        $this->aUnitMaster = null;
        $this->aPegawaiAtasan = null;
        $this->aPangkat = null;
        $this->aJabatan = null;
        $this->aPegawaiStatus = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PegawaiTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildPegawai The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[PegawaiTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}

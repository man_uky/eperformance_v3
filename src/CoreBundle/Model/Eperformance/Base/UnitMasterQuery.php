<?php

namespace CoreBundle\Model\Eperformance\Base;

use \Exception;
use \PDO;
use CoreBundle\Model\Eperformance\UnitMaster as ChildUnitMaster;
use CoreBundle\Model\Eperformance\UnitMasterQuery as ChildUnitMasterQuery;
use CoreBundle\Model\Eperformance\Map\UnitMasterTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'eperformance.unit_master' table.
 *
 *
 *
 * @method     ChildUnitMasterQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUnitMasterQuery orderByKode($order = Criteria::ASC) Order by the kode column
 * @method     ChildUnitMasterQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method     ChildUnitMasterQuery orderByIsSekretariatDaerah($order = Criteria::ASC) Order by the is_sekretariat_daerah column
 * @method     ChildUnitMasterQuery orderByIsDinas($order = Criteria::ASC) Order by the is_dinas column
 * @method     ChildUnitMasterQuery orderByIsBadan($order = Criteria::ASC) Order by the is_badan column
 * @method     ChildUnitMasterQuery orderByIsRumahSakit($order = Criteria::ASC) Order by the is_rumah_sakit column
 * @method     ChildUnitMasterQuery orderByIsBagian($order = Criteria::ASC) Order by the is_bagian column
 * @method     ChildUnitMasterQuery orderByIsKecamatan($order = Criteria::ASC) Order by the is_kecamatan column
 * @method     ChildUnitMasterQuery orderByIsKelurahan($order = Criteria::ASC) Order by the is_kelurahan column
 * @method     ChildUnitMasterQuery orderByParentId($order = Criteria::ASC) Order by the parent_id column
 * @method     ChildUnitMasterQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildUnitMasterQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildUnitMasterQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 *
 * @method     ChildUnitMasterQuery groupById() Group by the id column
 * @method     ChildUnitMasterQuery groupByKode() Group by the kode column
 * @method     ChildUnitMasterQuery groupByNama() Group by the nama column
 * @method     ChildUnitMasterQuery groupByIsSekretariatDaerah() Group by the is_sekretariat_daerah column
 * @method     ChildUnitMasterQuery groupByIsDinas() Group by the is_dinas column
 * @method     ChildUnitMasterQuery groupByIsBadan() Group by the is_badan column
 * @method     ChildUnitMasterQuery groupByIsRumahSakit() Group by the is_rumah_sakit column
 * @method     ChildUnitMasterQuery groupByIsBagian() Group by the is_bagian column
 * @method     ChildUnitMasterQuery groupByIsKecamatan() Group by the is_kecamatan column
 * @method     ChildUnitMasterQuery groupByIsKelurahan() Group by the is_kelurahan column
 * @method     ChildUnitMasterQuery groupByParentId() Group by the parent_id column
 * @method     ChildUnitMasterQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildUnitMasterQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildUnitMasterQuery groupByDeletedAt() Group by the deleted_at column
 *
 * @method     ChildUnitMasterQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUnitMasterQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUnitMasterQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUnitMasterQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUnitMasterQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUnitMasterQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUnitMasterQuery leftJoinUnitParent($relationAlias = null) Adds a LEFT JOIN clause to the query using the UnitParent relation
 * @method     ChildUnitMasterQuery rightJoinUnitParent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UnitParent relation
 * @method     ChildUnitMasterQuery innerJoinUnitParent($relationAlias = null) Adds a INNER JOIN clause to the query using the UnitParent relation
 *
 * @method     ChildUnitMasterQuery joinWithUnitParent($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UnitParent relation
 *
 * @method     ChildUnitMasterQuery leftJoinWithUnitParent() Adds a LEFT JOIN clause and with to the query using the UnitParent relation
 * @method     ChildUnitMasterQuery rightJoinWithUnitParent() Adds a RIGHT JOIN clause and with to the query using the UnitParent relation
 * @method     ChildUnitMasterQuery innerJoinWithUnitParent() Adds a INNER JOIN clause and with to the query using the UnitParent relation
 *
 * @method     ChildUnitMasterQuery leftJoinJabatan($relationAlias = null) Adds a LEFT JOIN clause to the query using the Jabatan relation
 * @method     ChildUnitMasterQuery rightJoinJabatan($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Jabatan relation
 * @method     ChildUnitMasterQuery innerJoinJabatan($relationAlias = null) Adds a INNER JOIN clause to the query using the Jabatan relation
 *
 * @method     ChildUnitMasterQuery joinWithJabatan($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Jabatan relation
 *
 * @method     ChildUnitMasterQuery leftJoinWithJabatan() Adds a LEFT JOIN clause and with to the query using the Jabatan relation
 * @method     ChildUnitMasterQuery rightJoinWithJabatan() Adds a RIGHT JOIN clause and with to the query using the Jabatan relation
 * @method     ChildUnitMasterQuery innerJoinWithJabatan() Adds a INNER JOIN clause and with to the query using the Jabatan relation
 *
 * @method     ChildUnitMasterQuery leftJoinPegawai($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pegawai relation
 * @method     ChildUnitMasterQuery rightJoinPegawai($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pegawai relation
 * @method     ChildUnitMasterQuery innerJoinPegawai($relationAlias = null) Adds a INNER JOIN clause to the query using the Pegawai relation
 *
 * @method     ChildUnitMasterQuery joinWithPegawai($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Pegawai relation
 *
 * @method     ChildUnitMasterQuery leftJoinWithPegawai() Adds a LEFT JOIN clause and with to the query using the Pegawai relation
 * @method     ChildUnitMasterQuery rightJoinWithPegawai() Adds a RIGHT JOIN clause and with to the query using the Pegawai relation
 * @method     ChildUnitMasterQuery innerJoinWithPegawai() Adds a INNER JOIN clause and with to the query using the Pegawai relation
 *
 * @method     ChildUnitMasterQuery leftJoinPegawaiMaster($relationAlias = null) Adds a LEFT JOIN clause to the query using the PegawaiMaster relation
 * @method     ChildUnitMasterQuery rightJoinPegawaiMaster($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PegawaiMaster relation
 * @method     ChildUnitMasterQuery innerJoinPegawaiMaster($relationAlias = null) Adds a INNER JOIN clause to the query using the PegawaiMaster relation
 *
 * @method     ChildUnitMasterQuery joinWithPegawaiMaster($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PegawaiMaster relation
 *
 * @method     ChildUnitMasterQuery leftJoinWithPegawaiMaster() Adds a LEFT JOIN clause and with to the query using the PegawaiMaster relation
 * @method     ChildUnitMasterQuery rightJoinWithPegawaiMaster() Adds a RIGHT JOIN clause and with to the query using the PegawaiMaster relation
 * @method     ChildUnitMasterQuery innerJoinWithPegawaiMaster() Adds a INNER JOIN clause and with to the query using the PegawaiMaster relation
 *
 * @method     ChildUnitMasterQuery leftJoinUnitMasterRelatedById($relationAlias = null) Adds a LEFT JOIN clause to the query using the UnitMasterRelatedById relation
 * @method     ChildUnitMasterQuery rightJoinUnitMasterRelatedById($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UnitMasterRelatedById relation
 * @method     ChildUnitMasterQuery innerJoinUnitMasterRelatedById($relationAlias = null) Adds a INNER JOIN clause to the query using the UnitMasterRelatedById relation
 *
 * @method     ChildUnitMasterQuery joinWithUnitMasterRelatedById($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UnitMasterRelatedById relation
 *
 * @method     ChildUnitMasterQuery leftJoinWithUnitMasterRelatedById() Adds a LEFT JOIN clause and with to the query using the UnitMasterRelatedById relation
 * @method     ChildUnitMasterQuery rightJoinWithUnitMasterRelatedById() Adds a RIGHT JOIN clause and with to the query using the UnitMasterRelatedById relation
 * @method     ChildUnitMasterQuery innerJoinWithUnitMasterRelatedById() Adds a INNER JOIN clause and with to the query using the UnitMasterRelatedById relation
 *
 * @method     \CoreBundle\Model\Eperformance\UnitMasterQuery|\CoreBundle\Model\Eperformance\JabatanQuery|\CoreBundle\Model\Eperformance\PegawaiQuery|\CoreBundle\Model\Eperformance\PegawaiMasterQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUnitMaster findOne(ConnectionInterface $con = null) Return the first ChildUnitMaster matching the query
 * @method     ChildUnitMaster findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUnitMaster matching the query, or a new ChildUnitMaster object populated from the query conditions when no match is found
 *
 * @method     ChildUnitMaster findOneById(int $id) Return the first ChildUnitMaster filtered by the id column
 * @method     ChildUnitMaster findOneByKode(string $kode) Return the first ChildUnitMaster filtered by the kode column
 * @method     ChildUnitMaster findOneByNama(string $nama) Return the first ChildUnitMaster filtered by the nama column
 * @method     ChildUnitMaster findOneByIsSekretariatDaerah(boolean $is_sekretariat_daerah) Return the first ChildUnitMaster filtered by the is_sekretariat_daerah column
 * @method     ChildUnitMaster findOneByIsDinas(boolean $is_dinas) Return the first ChildUnitMaster filtered by the is_dinas column
 * @method     ChildUnitMaster findOneByIsBadan(boolean $is_badan) Return the first ChildUnitMaster filtered by the is_badan column
 * @method     ChildUnitMaster findOneByIsRumahSakit(boolean $is_rumah_sakit) Return the first ChildUnitMaster filtered by the is_rumah_sakit column
 * @method     ChildUnitMaster findOneByIsBagian(boolean $is_bagian) Return the first ChildUnitMaster filtered by the is_bagian column
 * @method     ChildUnitMaster findOneByIsKecamatan(boolean $is_kecamatan) Return the first ChildUnitMaster filtered by the is_kecamatan column
 * @method     ChildUnitMaster findOneByIsKelurahan(boolean $is_kelurahan) Return the first ChildUnitMaster filtered by the is_kelurahan column
 * @method     ChildUnitMaster findOneByParentId(int $parent_id) Return the first ChildUnitMaster filtered by the parent_id column
 * @method     ChildUnitMaster findOneByCreatedAt(string $created_at) Return the first ChildUnitMaster filtered by the created_at column
 * @method     ChildUnitMaster findOneByUpdatedAt(string $updated_at) Return the first ChildUnitMaster filtered by the updated_at column
 * @method     ChildUnitMaster findOneByDeletedAt(string $deleted_at) Return the first ChildUnitMaster filtered by the deleted_at column *

 * @method     ChildUnitMaster requirePk($key, ConnectionInterface $con = null) Return the ChildUnitMaster by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitMaster requireOne(ConnectionInterface $con = null) Return the first ChildUnitMaster matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUnitMaster requireOneById(int $id) Return the first ChildUnitMaster filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitMaster requireOneByKode(string $kode) Return the first ChildUnitMaster filtered by the kode column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitMaster requireOneByNama(string $nama) Return the first ChildUnitMaster filtered by the nama column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitMaster requireOneByIsSekretariatDaerah(boolean $is_sekretariat_daerah) Return the first ChildUnitMaster filtered by the is_sekretariat_daerah column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitMaster requireOneByIsDinas(boolean $is_dinas) Return the first ChildUnitMaster filtered by the is_dinas column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitMaster requireOneByIsBadan(boolean $is_badan) Return the first ChildUnitMaster filtered by the is_badan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitMaster requireOneByIsRumahSakit(boolean $is_rumah_sakit) Return the first ChildUnitMaster filtered by the is_rumah_sakit column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitMaster requireOneByIsBagian(boolean $is_bagian) Return the first ChildUnitMaster filtered by the is_bagian column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitMaster requireOneByIsKecamatan(boolean $is_kecamatan) Return the first ChildUnitMaster filtered by the is_kecamatan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitMaster requireOneByIsKelurahan(boolean $is_kelurahan) Return the first ChildUnitMaster filtered by the is_kelurahan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitMaster requireOneByParentId(int $parent_id) Return the first ChildUnitMaster filtered by the parent_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitMaster requireOneByCreatedAt(string $created_at) Return the first ChildUnitMaster filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitMaster requireOneByUpdatedAt(string $updated_at) Return the first ChildUnitMaster filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitMaster requireOneByDeletedAt(string $deleted_at) Return the first ChildUnitMaster filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUnitMaster[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUnitMaster objects based on current ModelCriteria
 * @method     ChildUnitMaster[]|ObjectCollection findById(int $id) Return ChildUnitMaster objects filtered by the id column
 * @method     ChildUnitMaster[]|ObjectCollection findByKode(string $kode) Return ChildUnitMaster objects filtered by the kode column
 * @method     ChildUnitMaster[]|ObjectCollection findByNama(string $nama) Return ChildUnitMaster objects filtered by the nama column
 * @method     ChildUnitMaster[]|ObjectCollection findByIsSekretariatDaerah(boolean $is_sekretariat_daerah) Return ChildUnitMaster objects filtered by the is_sekretariat_daerah column
 * @method     ChildUnitMaster[]|ObjectCollection findByIsDinas(boolean $is_dinas) Return ChildUnitMaster objects filtered by the is_dinas column
 * @method     ChildUnitMaster[]|ObjectCollection findByIsBadan(boolean $is_badan) Return ChildUnitMaster objects filtered by the is_badan column
 * @method     ChildUnitMaster[]|ObjectCollection findByIsRumahSakit(boolean $is_rumah_sakit) Return ChildUnitMaster objects filtered by the is_rumah_sakit column
 * @method     ChildUnitMaster[]|ObjectCollection findByIsBagian(boolean $is_bagian) Return ChildUnitMaster objects filtered by the is_bagian column
 * @method     ChildUnitMaster[]|ObjectCollection findByIsKecamatan(boolean $is_kecamatan) Return ChildUnitMaster objects filtered by the is_kecamatan column
 * @method     ChildUnitMaster[]|ObjectCollection findByIsKelurahan(boolean $is_kelurahan) Return ChildUnitMaster objects filtered by the is_kelurahan column
 * @method     ChildUnitMaster[]|ObjectCollection findByParentId(int $parent_id) Return ChildUnitMaster objects filtered by the parent_id column
 * @method     ChildUnitMaster[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildUnitMaster objects filtered by the created_at column
 * @method     ChildUnitMaster[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildUnitMaster objects filtered by the updated_at column
 * @method     ChildUnitMaster[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildUnitMaster objects filtered by the deleted_at column
 * @method     ChildUnitMaster[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UnitMasterQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \CoreBundle\Model\Eperformance\Base\UnitMasterQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\CoreBundle\\Model\\Eperformance\\UnitMaster', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUnitMasterQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUnitMasterQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUnitMasterQuery) {
            return $criteria;
        }
        $query = new ChildUnitMasterQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUnitMaster|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UnitMasterTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UnitMasterTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUnitMaster A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, kode, nama, is_sekretariat_daerah, is_dinas, is_badan, is_rumah_sakit, is_bagian, is_kecamatan, is_kelurahan, parent_id, created_at, updated_at, deleted_at FROM eperformance.unit_master WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUnitMaster $obj */
            $obj = new ChildUnitMaster();
            $obj->hydrate($row);
            UnitMasterTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUnitMaster|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UnitMasterTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UnitMasterTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UnitMasterTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UnitMasterTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UnitMasterTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the kode column
     *
     * Example usage:
     * <code>
     * $query->filterByKode('fooValue');   // WHERE kode = 'fooValue'
     * $query->filterByKode('%fooValue%', Criteria::LIKE); // WHERE kode LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kode The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterByKode($kode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kode)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UnitMasterTableMap::COL_KODE, $kode, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%', Criteria::LIKE); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UnitMasterTableMap::COL_NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the is_sekretariat_daerah column
     *
     * Example usage:
     * <code>
     * $query->filterByIsSekretariatDaerah(true); // WHERE is_sekretariat_daerah = true
     * $query->filterByIsSekretariatDaerah('yes'); // WHERE is_sekretariat_daerah = true
     * </code>
     *
     * @param     boolean|string $isSekretariatDaerah The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterByIsSekretariatDaerah($isSekretariatDaerah = null, $comparison = null)
    {
        if (is_string($isSekretariatDaerah)) {
            $isSekretariatDaerah = in_array(strtolower($isSekretariatDaerah), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UnitMasterTableMap::COL_IS_SEKRETARIAT_DAERAH, $isSekretariatDaerah, $comparison);
    }

    /**
     * Filter the query on the is_dinas column
     *
     * Example usage:
     * <code>
     * $query->filterByIsDinas(true); // WHERE is_dinas = true
     * $query->filterByIsDinas('yes'); // WHERE is_dinas = true
     * </code>
     *
     * @param     boolean|string $isDinas The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterByIsDinas($isDinas = null, $comparison = null)
    {
        if (is_string($isDinas)) {
            $isDinas = in_array(strtolower($isDinas), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UnitMasterTableMap::COL_IS_DINAS, $isDinas, $comparison);
    }

    /**
     * Filter the query on the is_badan column
     *
     * Example usage:
     * <code>
     * $query->filterByIsBadan(true); // WHERE is_badan = true
     * $query->filterByIsBadan('yes'); // WHERE is_badan = true
     * </code>
     *
     * @param     boolean|string $isBadan The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterByIsBadan($isBadan = null, $comparison = null)
    {
        if (is_string($isBadan)) {
            $isBadan = in_array(strtolower($isBadan), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UnitMasterTableMap::COL_IS_BADAN, $isBadan, $comparison);
    }

    /**
     * Filter the query on the is_rumah_sakit column
     *
     * Example usage:
     * <code>
     * $query->filterByIsRumahSakit(true); // WHERE is_rumah_sakit = true
     * $query->filterByIsRumahSakit('yes'); // WHERE is_rumah_sakit = true
     * </code>
     *
     * @param     boolean|string $isRumahSakit The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterByIsRumahSakit($isRumahSakit = null, $comparison = null)
    {
        if (is_string($isRumahSakit)) {
            $isRumahSakit = in_array(strtolower($isRumahSakit), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UnitMasterTableMap::COL_IS_RUMAH_SAKIT, $isRumahSakit, $comparison);
    }

    /**
     * Filter the query on the is_bagian column
     *
     * Example usage:
     * <code>
     * $query->filterByIsBagian(true); // WHERE is_bagian = true
     * $query->filterByIsBagian('yes'); // WHERE is_bagian = true
     * </code>
     *
     * @param     boolean|string $isBagian The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterByIsBagian($isBagian = null, $comparison = null)
    {
        if (is_string($isBagian)) {
            $isBagian = in_array(strtolower($isBagian), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UnitMasterTableMap::COL_IS_BAGIAN, $isBagian, $comparison);
    }

    /**
     * Filter the query on the is_kecamatan column
     *
     * Example usage:
     * <code>
     * $query->filterByIsKecamatan(true); // WHERE is_kecamatan = true
     * $query->filterByIsKecamatan('yes'); // WHERE is_kecamatan = true
     * </code>
     *
     * @param     boolean|string $isKecamatan The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterByIsKecamatan($isKecamatan = null, $comparison = null)
    {
        if (is_string($isKecamatan)) {
            $isKecamatan = in_array(strtolower($isKecamatan), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UnitMasterTableMap::COL_IS_KECAMATAN, $isKecamatan, $comparison);
    }

    /**
     * Filter the query on the is_kelurahan column
     *
     * Example usage:
     * <code>
     * $query->filterByIsKelurahan(true); // WHERE is_kelurahan = true
     * $query->filterByIsKelurahan('yes'); // WHERE is_kelurahan = true
     * </code>
     *
     * @param     boolean|string $isKelurahan The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterByIsKelurahan($isKelurahan = null, $comparison = null)
    {
        if (is_string($isKelurahan)) {
            $isKelurahan = in_array(strtolower($isKelurahan), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UnitMasterTableMap::COL_IS_KELURAHAN, $isKelurahan, $comparison);
    }

    /**
     * Filter the query on the parent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByParentId(1234); // WHERE parent_id = 1234
     * $query->filterByParentId(array(12, 34)); // WHERE parent_id IN (12, 34)
     * $query->filterByParentId(array('min' => 12)); // WHERE parent_id > 12
     * </code>
     *
     * @see       filterByUnitParent()
     *
     * @param     mixed $parentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterByParentId($parentId = null, $comparison = null)
    {
        if (is_array($parentId)) {
            $useMinMax = false;
            if (isset($parentId['min'])) {
                $this->addUsingAlias(UnitMasterTableMap::COL_PARENT_ID, $parentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($parentId['max'])) {
                $this->addUsingAlias(UnitMasterTableMap::COL_PARENT_ID, $parentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UnitMasterTableMap::COL_PARENT_ID, $parentId, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(UnitMasterTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(UnitMasterTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UnitMasterTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(UnitMasterTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(UnitMasterTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UnitMasterTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(UnitMasterTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(UnitMasterTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UnitMasterTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\UnitMaster object
     *
     * @param \CoreBundle\Model\Eperformance\UnitMaster|ObjectCollection $unitMaster The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterByUnitParent($unitMaster, $comparison = null)
    {
        if ($unitMaster instanceof \CoreBundle\Model\Eperformance\UnitMaster) {
            return $this
                ->addUsingAlias(UnitMasterTableMap::COL_PARENT_ID, $unitMaster->getId(), $comparison);
        } elseif ($unitMaster instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UnitMasterTableMap::COL_PARENT_ID, $unitMaster->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUnitParent() only accepts arguments of type \CoreBundle\Model\Eperformance\UnitMaster or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UnitParent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function joinUnitParent($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UnitParent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UnitParent');
        }

        return $this;
    }

    /**
     * Use the UnitParent relation UnitMaster object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\UnitMasterQuery A secondary query class using the current class as primary query
     */
    public function useUnitParentQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUnitParent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UnitParent', '\CoreBundle\Model\Eperformance\UnitMasterQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\Jabatan object
     *
     * @param \CoreBundle\Model\Eperformance\Jabatan|ObjectCollection $jabatan the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterByJabatan($jabatan, $comparison = null)
    {
        if ($jabatan instanceof \CoreBundle\Model\Eperformance\Jabatan) {
            return $this
                ->addUsingAlias(UnitMasterTableMap::COL_ID, $jabatan->getUnitMasterId(), $comparison);
        } elseif ($jabatan instanceof ObjectCollection) {
            return $this
                ->useJabatanQuery()
                ->filterByPrimaryKeys($jabatan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByJabatan() only accepts arguments of type \CoreBundle\Model\Eperformance\Jabatan or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Jabatan relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function joinJabatan($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Jabatan');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Jabatan');
        }

        return $this;
    }

    /**
     * Use the Jabatan relation Jabatan object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\JabatanQuery A secondary query class using the current class as primary query
     */
    public function useJabatanQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJabatan($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Jabatan', '\CoreBundle\Model\Eperformance\JabatanQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\Pegawai object
     *
     * @param \CoreBundle\Model\Eperformance\Pegawai|ObjectCollection $pegawai the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterByPegawai($pegawai, $comparison = null)
    {
        if ($pegawai instanceof \CoreBundle\Model\Eperformance\Pegawai) {
            return $this
                ->addUsingAlias(UnitMasterTableMap::COL_ID, $pegawai->getUnitMasterId(), $comparison);
        } elseif ($pegawai instanceof ObjectCollection) {
            return $this
                ->usePegawaiQuery()
                ->filterByPrimaryKeys($pegawai->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPegawai() only accepts arguments of type \CoreBundle\Model\Eperformance\Pegawai or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pegawai relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function joinPegawai($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pegawai');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pegawai');
        }

        return $this;
    }

    /**
     * Use the Pegawai relation Pegawai object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\PegawaiQuery A secondary query class using the current class as primary query
     */
    public function usePegawaiQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPegawai($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pegawai', '\CoreBundle\Model\Eperformance\PegawaiQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\PegawaiMaster object
     *
     * @param \CoreBundle\Model\Eperformance\PegawaiMaster|ObjectCollection $pegawaiMaster the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterByPegawaiMaster($pegawaiMaster, $comparison = null)
    {
        if ($pegawaiMaster instanceof \CoreBundle\Model\Eperformance\PegawaiMaster) {
            return $this
                ->addUsingAlias(UnitMasterTableMap::COL_ID, $pegawaiMaster->getUnitMasterId(), $comparison);
        } elseif ($pegawaiMaster instanceof ObjectCollection) {
            return $this
                ->usePegawaiMasterQuery()
                ->filterByPrimaryKeys($pegawaiMaster->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPegawaiMaster() only accepts arguments of type \CoreBundle\Model\Eperformance\PegawaiMaster or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PegawaiMaster relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function joinPegawaiMaster($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PegawaiMaster');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PegawaiMaster');
        }

        return $this;
    }

    /**
     * Use the PegawaiMaster relation PegawaiMaster object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\PegawaiMasterQuery A secondary query class using the current class as primary query
     */
    public function usePegawaiMasterQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPegawaiMaster($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PegawaiMaster', '\CoreBundle\Model\Eperformance\PegawaiMasterQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\UnitMaster object
     *
     * @param \CoreBundle\Model\Eperformance\UnitMaster|ObjectCollection $unitMaster the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUnitMasterQuery The current query, for fluid interface
     */
    public function filterByUnitMasterRelatedById($unitMaster, $comparison = null)
    {
        if ($unitMaster instanceof \CoreBundle\Model\Eperformance\UnitMaster) {
            return $this
                ->addUsingAlias(UnitMasterTableMap::COL_ID, $unitMaster->getParentId(), $comparison);
        } elseif ($unitMaster instanceof ObjectCollection) {
            return $this
                ->useUnitMasterRelatedByIdQuery()
                ->filterByPrimaryKeys($unitMaster->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUnitMasterRelatedById() only accepts arguments of type \CoreBundle\Model\Eperformance\UnitMaster or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UnitMasterRelatedById relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function joinUnitMasterRelatedById($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UnitMasterRelatedById');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UnitMasterRelatedById');
        }

        return $this;
    }

    /**
     * Use the UnitMasterRelatedById relation UnitMaster object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\UnitMasterQuery A secondary query class using the current class as primary query
     */
    public function useUnitMasterRelatedByIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUnitMasterRelatedById($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UnitMasterRelatedById', '\CoreBundle\Model\Eperformance\UnitMasterQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUnitMaster $unitMaster Object to remove from the list of results
     *
     * @return $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function prune($unitMaster = null)
    {
        if ($unitMaster) {
            $this->addUsingAlias(UnitMasterTableMap::COL_ID, $unitMaster->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the eperformance.unit_master table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UnitMasterTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UnitMasterTableMap::clearInstancePool();
            UnitMasterTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UnitMasterTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UnitMasterTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UnitMasterTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UnitMasterTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 7200)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UnitMasterTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(UnitMasterTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }


        return $con->getDataFetcher($stmt);
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(UnitMasterTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(UnitMasterTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(UnitMasterTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(UnitMasterTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(UnitMasterTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildUnitMasterQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(UnitMasterTableMap::COL_CREATED_AT);
    }

} // UnitMasterQuery

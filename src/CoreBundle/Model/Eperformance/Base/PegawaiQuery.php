<?php

namespace CoreBundle\Model\Eperformance\Base;

use \Exception;
use \PDO;
use CoreBundle\Model\Eperformance\Pegawai as ChildPegawai;
use CoreBundle\Model\Eperformance\PegawaiQuery as ChildPegawaiQuery;
use CoreBundle\Model\Eperformance\Map\PegawaiTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'eperformance.pegawai' table.
 *
 *
 *
 * @method     ChildPegawaiQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPegawaiQuery orderByUsername($order = Criteria::ASC) Order by the username column
 * @method     ChildPegawaiQuery orderByPassword($order = Criteria::ASC) Order by the password column
 * @method     ChildPegawaiQuery orderByKodeRapor($order = Criteria::ASC) Order by the kode_rapor column
 * @method     ChildPegawaiQuery orderByCredentialId($order = Criteria::ASC) Order by the credential_id column
 * @method     ChildPegawaiQuery orderByPegawaiMasterId($order = Criteria::ASC) Order by the pegawai_master_id column
 * @method     ChildPegawaiQuery orderByUnitMasterId($order = Criteria::ASC) Order by the unit_master_id column
 * @method     ChildPegawaiQuery orderByAtasanId($order = Criteria::ASC) Order by the atasan_id column
 * @method     ChildPegawaiQuery orderByPangkatId($order = Criteria::ASC) Order by the pangkat_id column
 * @method     ChildPegawaiQuery orderByJabatanId($order = Criteria::ASC) Order by the jabatan_id column
 * @method     ChildPegawaiQuery orderByPegawaiStatusId($order = Criteria::ASC) Order by the pegawai_status_id column
 * @method     ChildPegawaiQuery orderByEselon($order = Criteria::ASC) Order by the eselon column
 * @method     ChildPegawaiQuery orderByLevel($order = Criteria::ASC) Order by the level column
 * @method     ChildPegawaiQuery orderByIsAktif($order = Criteria::ASC) Order by the is_aktif column
 * @method     ChildPegawaiQuery orderByIsTrantib($order = Criteria::ASC) Order by the is_trantib column
 * @method     ChildPegawaiQuery orderByIsLapangan($order = Criteria::ASC) Order by the is_lapangan column
 * @method     ChildPegawaiQuery orderByTanggalAktif($order = Criteria::ASC) Order by the tanggal_aktif column
 * @method     ChildPegawaiQuery orderByTanggalNonaktif($order = Criteria::ASC) Order by the tanggal_nonaktif column
 * @method     ChildPegawaiQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPegawaiQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildPegawaiQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 *
 * @method     ChildPegawaiQuery groupById() Group by the id column
 * @method     ChildPegawaiQuery groupByUsername() Group by the username column
 * @method     ChildPegawaiQuery groupByPassword() Group by the password column
 * @method     ChildPegawaiQuery groupByKodeRapor() Group by the kode_rapor column
 * @method     ChildPegawaiQuery groupByCredentialId() Group by the credential_id column
 * @method     ChildPegawaiQuery groupByPegawaiMasterId() Group by the pegawai_master_id column
 * @method     ChildPegawaiQuery groupByUnitMasterId() Group by the unit_master_id column
 * @method     ChildPegawaiQuery groupByAtasanId() Group by the atasan_id column
 * @method     ChildPegawaiQuery groupByPangkatId() Group by the pangkat_id column
 * @method     ChildPegawaiQuery groupByJabatanId() Group by the jabatan_id column
 * @method     ChildPegawaiQuery groupByPegawaiStatusId() Group by the pegawai_status_id column
 * @method     ChildPegawaiQuery groupByEselon() Group by the eselon column
 * @method     ChildPegawaiQuery groupByLevel() Group by the level column
 * @method     ChildPegawaiQuery groupByIsAktif() Group by the is_aktif column
 * @method     ChildPegawaiQuery groupByIsTrantib() Group by the is_trantib column
 * @method     ChildPegawaiQuery groupByIsLapangan() Group by the is_lapangan column
 * @method     ChildPegawaiQuery groupByTanggalAktif() Group by the tanggal_aktif column
 * @method     ChildPegawaiQuery groupByTanggalNonaktif() Group by the tanggal_nonaktif column
 * @method     ChildPegawaiQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPegawaiQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildPegawaiQuery groupByDeletedAt() Group by the deleted_at column
 *
 * @method     ChildPegawaiQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPegawaiQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPegawaiQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPegawaiQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPegawaiQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPegawaiQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPegawaiQuery leftJoinCredential($relationAlias = null) Adds a LEFT JOIN clause to the query using the Credential relation
 * @method     ChildPegawaiQuery rightJoinCredential($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Credential relation
 * @method     ChildPegawaiQuery innerJoinCredential($relationAlias = null) Adds a INNER JOIN clause to the query using the Credential relation
 *
 * @method     ChildPegawaiQuery joinWithCredential($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Credential relation
 *
 * @method     ChildPegawaiQuery leftJoinWithCredential() Adds a LEFT JOIN clause and with to the query using the Credential relation
 * @method     ChildPegawaiQuery rightJoinWithCredential() Adds a RIGHT JOIN clause and with to the query using the Credential relation
 * @method     ChildPegawaiQuery innerJoinWithCredential() Adds a INNER JOIN clause and with to the query using the Credential relation
 *
 * @method     ChildPegawaiQuery leftJoinPegawaiMaster($relationAlias = null) Adds a LEFT JOIN clause to the query using the PegawaiMaster relation
 * @method     ChildPegawaiQuery rightJoinPegawaiMaster($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PegawaiMaster relation
 * @method     ChildPegawaiQuery innerJoinPegawaiMaster($relationAlias = null) Adds a INNER JOIN clause to the query using the PegawaiMaster relation
 *
 * @method     ChildPegawaiQuery joinWithPegawaiMaster($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PegawaiMaster relation
 *
 * @method     ChildPegawaiQuery leftJoinWithPegawaiMaster() Adds a LEFT JOIN clause and with to the query using the PegawaiMaster relation
 * @method     ChildPegawaiQuery rightJoinWithPegawaiMaster() Adds a RIGHT JOIN clause and with to the query using the PegawaiMaster relation
 * @method     ChildPegawaiQuery innerJoinWithPegawaiMaster() Adds a INNER JOIN clause and with to the query using the PegawaiMaster relation
 *
 * @method     ChildPegawaiQuery leftJoinUnitMaster($relationAlias = null) Adds a LEFT JOIN clause to the query using the UnitMaster relation
 * @method     ChildPegawaiQuery rightJoinUnitMaster($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UnitMaster relation
 * @method     ChildPegawaiQuery innerJoinUnitMaster($relationAlias = null) Adds a INNER JOIN clause to the query using the UnitMaster relation
 *
 * @method     ChildPegawaiQuery joinWithUnitMaster($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UnitMaster relation
 *
 * @method     ChildPegawaiQuery leftJoinWithUnitMaster() Adds a LEFT JOIN clause and with to the query using the UnitMaster relation
 * @method     ChildPegawaiQuery rightJoinWithUnitMaster() Adds a RIGHT JOIN clause and with to the query using the UnitMaster relation
 * @method     ChildPegawaiQuery innerJoinWithUnitMaster() Adds a INNER JOIN clause and with to the query using the UnitMaster relation
 *
 * @method     ChildPegawaiQuery leftJoinPegawaiAtasan($relationAlias = null) Adds a LEFT JOIN clause to the query using the PegawaiAtasan relation
 * @method     ChildPegawaiQuery rightJoinPegawaiAtasan($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PegawaiAtasan relation
 * @method     ChildPegawaiQuery innerJoinPegawaiAtasan($relationAlias = null) Adds a INNER JOIN clause to the query using the PegawaiAtasan relation
 *
 * @method     ChildPegawaiQuery joinWithPegawaiAtasan($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PegawaiAtasan relation
 *
 * @method     ChildPegawaiQuery leftJoinWithPegawaiAtasan() Adds a LEFT JOIN clause and with to the query using the PegawaiAtasan relation
 * @method     ChildPegawaiQuery rightJoinWithPegawaiAtasan() Adds a RIGHT JOIN clause and with to the query using the PegawaiAtasan relation
 * @method     ChildPegawaiQuery innerJoinWithPegawaiAtasan() Adds a INNER JOIN clause and with to the query using the PegawaiAtasan relation
 *
 * @method     ChildPegawaiQuery leftJoinPangkat($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pangkat relation
 * @method     ChildPegawaiQuery rightJoinPangkat($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pangkat relation
 * @method     ChildPegawaiQuery innerJoinPangkat($relationAlias = null) Adds a INNER JOIN clause to the query using the Pangkat relation
 *
 * @method     ChildPegawaiQuery joinWithPangkat($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Pangkat relation
 *
 * @method     ChildPegawaiQuery leftJoinWithPangkat() Adds a LEFT JOIN clause and with to the query using the Pangkat relation
 * @method     ChildPegawaiQuery rightJoinWithPangkat() Adds a RIGHT JOIN clause and with to the query using the Pangkat relation
 * @method     ChildPegawaiQuery innerJoinWithPangkat() Adds a INNER JOIN clause and with to the query using the Pangkat relation
 *
 * @method     ChildPegawaiQuery leftJoinJabatan($relationAlias = null) Adds a LEFT JOIN clause to the query using the Jabatan relation
 * @method     ChildPegawaiQuery rightJoinJabatan($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Jabatan relation
 * @method     ChildPegawaiQuery innerJoinJabatan($relationAlias = null) Adds a INNER JOIN clause to the query using the Jabatan relation
 *
 * @method     ChildPegawaiQuery joinWithJabatan($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Jabatan relation
 *
 * @method     ChildPegawaiQuery leftJoinWithJabatan() Adds a LEFT JOIN clause and with to the query using the Jabatan relation
 * @method     ChildPegawaiQuery rightJoinWithJabatan() Adds a RIGHT JOIN clause and with to the query using the Jabatan relation
 * @method     ChildPegawaiQuery innerJoinWithJabatan() Adds a INNER JOIN clause and with to the query using the Jabatan relation
 *
 * @method     ChildPegawaiQuery leftJoinPegawaiStatus($relationAlias = null) Adds a LEFT JOIN clause to the query using the PegawaiStatus relation
 * @method     ChildPegawaiQuery rightJoinPegawaiStatus($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PegawaiStatus relation
 * @method     ChildPegawaiQuery innerJoinPegawaiStatus($relationAlias = null) Adds a INNER JOIN clause to the query using the PegawaiStatus relation
 *
 * @method     ChildPegawaiQuery joinWithPegawaiStatus($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PegawaiStatus relation
 *
 * @method     ChildPegawaiQuery leftJoinWithPegawaiStatus() Adds a LEFT JOIN clause and with to the query using the PegawaiStatus relation
 * @method     ChildPegawaiQuery rightJoinWithPegawaiStatus() Adds a RIGHT JOIN clause and with to the query using the PegawaiStatus relation
 * @method     ChildPegawaiQuery innerJoinWithPegawaiStatus() Adds a INNER JOIN clause and with to the query using the PegawaiStatus relation
 *
 * @method     ChildPegawaiQuery leftJoinPegawaiRelatedById($relationAlias = null) Adds a LEFT JOIN clause to the query using the PegawaiRelatedById relation
 * @method     ChildPegawaiQuery rightJoinPegawaiRelatedById($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PegawaiRelatedById relation
 * @method     ChildPegawaiQuery innerJoinPegawaiRelatedById($relationAlias = null) Adds a INNER JOIN clause to the query using the PegawaiRelatedById relation
 *
 * @method     ChildPegawaiQuery joinWithPegawaiRelatedById($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PegawaiRelatedById relation
 *
 * @method     ChildPegawaiQuery leftJoinWithPegawaiRelatedById() Adds a LEFT JOIN clause and with to the query using the PegawaiRelatedById relation
 * @method     ChildPegawaiQuery rightJoinWithPegawaiRelatedById() Adds a RIGHT JOIN clause and with to the query using the PegawaiRelatedById relation
 * @method     ChildPegawaiQuery innerJoinWithPegawaiRelatedById() Adds a INNER JOIN clause and with to the query using the PegawaiRelatedById relation
 *
 * @method     \CoreBundle\Model\Eperformance\CredentialQuery|\CoreBundle\Model\Eperformance\PegawaiMasterQuery|\CoreBundle\Model\Eperformance\UnitMasterQuery|\CoreBundle\Model\Eperformance\PegawaiQuery|\CoreBundle\Model\Eperformance\PangkatQuery|\CoreBundle\Model\Eperformance\JabatanQuery|\CoreBundle\Model\Eperformance\PegawaiStatusQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPegawai findOne(ConnectionInterface $con = null) Return the first ChildPegawai matching the query
 * @method     ChildPegawai findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPegawai matching the query, or a new ChildPegawai object populated from the query conditions when no match is found
 *
 * @method     ChildPegawai findOneById(int $id) Return the first ChildPegawai filtered by the id column
 * @method     ChildPegawai findOneByUsername(string $username) Return the first ChildPegawai filtered by the username column
 * @method     ChildPegawai findOneByPassword(string $password) Return the first ChildPegawai filtered by the password column
 * @method     ChildPegawai findOneByKodeRapor(string $kode_rapor) Return the first ChildPegawai filtered by the kode_rapor column
 * @method     ChildPegawai findOneByCredentialId(int $credential_id) Return the first ChildPegawai filtered by the credential_id column
 * @method     ChildPegawai findOneByPegawaiMasterId(int $pegawai_master_id) Return the first ChildPegawai filtered by the pegawai_master_id column
 * @method     ChildPegawai findOneByUnitMasterId(int $unit_master_id) Return the first ChildPegawai filtered by the unit_master_id column
 * @method     ChildPegawai findOneByAtasanId(int $atasan_id) Return the first ChildPegawai filtered by the atasan_id column
 * @method     ChildPegawai findOneByPangkatId(int $pangkat_id) Return the first ChildPegawai filtered by the pangkat_id column
 * @method     ChildPegawai findOneByJabatanId(int $jabatan_id) Return the first ChildPegawai filtered by the jabatan_id column
 * @method     ChildPegawai findOneByPegawaiStatusId(int $pegawai_status_id) Return the first ChildPegawai filtered by the pegawai_status_id column
 * @method     ChildPegawai findOneByEselon(int $eselon) Return the first ChildPegawai filtered by the eselon column
 * @method     ChildPegawai findOneByLevel(int $level) Return the first ChildPegawai filtered by the level column
 * @method     ChildPegawai findOneByIsAktif(boolean $is_aktif) Return the first ChildPegawai filtered by the is_aktif column
 * @method     ChildPegawai findOneByIsTrantib(boolean $is_trantib) Return the first ChildPegawai filtered by the is_trantib column
 * @method     ChildPegawai findOneByIsLapangan(boolean $is_lapangan) Return the first ChildPegawai filtered by the is_lapangan column
 * @method     ChildPegawai findOneByTanggalAktif(string $tanggal_aktif) Return the first ChildPegawai filtered by the tanggal_aktif column
 * @method     ChildPegawai findOneByTanggalNonaktif(string $tanggal_nonaktif) Return the first ChildPegawai filtered by the tanggal_nonaktif column
 * @method     ChildPegawai findOneByCreatedAt(string $created_at) Return the first ChildPegawai filtered by the created_at column
 * @method     ChildPegawai findOneByUpdatedAt(string $updated_at) Return the first ChildPegawai filtered by the updated_at column
 * @method     ChildPegawai findOneByDeletedAt(string $deleted_at) Return the first ChildPegawai filtered by the deleted_at column *

 * @method     ChildPegawai requirePk($key, ConnectionInterface $con = null) Return the ChildPegawai by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOne(ConnectionInterface $con = null) Return the first ChildPegawai matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPegawai requireOneById(int $id) Return the first ChildPegawai filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOneByUsername(string $username) Return the first ChildPegawai filtered by the username column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOneByPassword(string $password) Return the first ChildPegawai filtered by the password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOneByKodeRapor(string $kode_rapor) Return the first ChildPegawai filtered by the kode_rapor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOneByCredentialId(int $credential_id) Return the first ChildPegawai filtered by the credential_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOneByPegawaiMasterId(int $pegawai_master_id) Return the first ChildPegawai filtered by the pegawai_master_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOneByUnitMasterId(int $unit_master_id) Return the first ChildPegawai filtered by the unit_master_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOneByAtasanId(int $atasan_id) Return the first ChildPegawai filtered by the atasan_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOneByPangkatId(int $pangkat_id) Return the first ChildPegawai filtered by the pangkat_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOneByJabatanId(int $jabatan_id) Return the first ChildPegawai filtered by the jabatan_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOneByPegawaiStatusId(int $pegawai_status_id) Return the first ChildPegawai filtered by the pegawai_status_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOneByEselon(int $eselon) Return the first ChildPegawai filtered by the eselon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOneByLevel(int $level) Return the first ChildPegawai filtered by the level column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOneByIsAktif(boolean $is_aktif) Return the first ChildPegawai filtered by the is_aktif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOneByIsTrantib(boolean $is_trantib) Return the first ChildPegawai filtered by the is_trantib column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOneByIsLapangan(boolean $is_lapangan) Return the first ChildPegawai filtered by the is_lapangan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOneByTanggalAktif(string $tanggal_aktif) Return the first ChildPegawai filtered by the tanggal_aktif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOneByTanggalNonaktif(string $tanggal_nonaktif) Return the first ChildPegawai filtered by the tanggal_nonaktif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOneByCreatedAt(string $created_at) Return the first ChildPegawai filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOneByUpdatedAt(string $updated_at) Return the first ChildPegawai filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawai requireOneByDeletedAt(string $deleted_at) Return the first ChildPegawai filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPegawai[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPegawai objects based on current ModelCriteria
 * @method     ChildPegawai[]|ObjectCollection findById(int $id) Return ChildPegawai objects filtered by the id column
 * @method     ChildPegawai[]|ObjectCollection findByUsername(string $username) Return ChildPegawai objects filtered by the username column
 * @method     ChildPegawai[]|ObjectCollection findByPassword(string $password) Return ChildPegawai objects filtered by the password column
 * @method     ChildPegawai[]|ObjectCollection findByKodeRapor(string $kode_rapor) Return ChildPegawai objects filtered by the kode_rapor column
 * @method     ChildPegawai[]|ObjectCollection findByCredentialId(int $credential_id) Return ChildPegawai objects filtered by the credential_id column
 * @method     ChildPegawai[]|ObjectCollection findByPegawaiMasterId(int $pegawai_master_id) Return ChildPegawai objects filtered by the pegawai_master_id column
 * @method     ChildPegawai[]|ObjectCollection findByUnitMasterId(int $unit_master_id) Return ChildPegawai objects filtered by the unit_master_id column
 * @method     ChildPegawai[]|ObjectCollection findByAtasanId(int $atasan_id) Return ChildPegawai objects filtered by the atasan_id column
 * @method     ChildPegawai[]|ObjectCollection findByPangkatId(int $pangkat_id) Return ChildPegawai objects filtered by the pangkat_id column
 * @method     ChildPegawai[]|ObjectCollection findByJabatanId(int $jabatan_id) Return ChildPegawai objects filtered by the jabatan_id column
 * @method     ChildPegawai[]|ObjectCollection findByPegawaiStatusId(int $pegawai_status_id) Return ChildPegawai objects filtered by the pegawai_status_id column
 * @method     ChildPegawai[]|ObjectCollection findByEselon(int $eselon) Return ChildPegawai objects filtered by the eselon column
 * @method     ChildPegawai[]|ObjectCollection findByLevel(int $level) Return ChildPegawai objects filtered by the level column
 * @method     ChildPegawai[]|ObjectCollection findByIsAktif(boolean $is_aktif) Return ChildPegawai objects filtered by the is_aktif column
 * @method     ChildPegawai[]|ObjectCollection findByIsTrantib(boolean $is_trantib) Return ChildPegawai objects filtered by the is_trantib column
 * @method     ChildPegawai[]|ObjectCollection findByIsLapangan(boolean $is_lapangan) Return ChildPegawai objects filtered by the is_lapangan column
 * @method     ChildPegawai[]|ObjectCollection findByTanggalAktif(string $tanggal_aktif) Return ChildPegawai objects filtered by the tanggal_aktif column
 * @method     ChildPegawai[]|ObjectCollection findByTanggalNonaktif(string $tanggal_nonaktif) Return ChildPegawai objects filtered by the tanggal_nonaktif column
 * @method     ChildPegawai[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildPegawai objects filtered by the created_at column
 * @method     ChildPegawai[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildPegawai objects filtered by the updated_at column
 * @method     ChildPegawai[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildPegawai objects filtered by the deleted_at column
 * @method     ChildPegawai[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PegawaiQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \CoreBundle\Model\Eperformance\Base\PegawaiQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\CoreBundle\\Model\\Eperformance\\Pegawai', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPegawaiQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPegawaiQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPegawaiQuery) {
            return $criteria;
        }
        $query = new ChildPegawaiQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPegawai|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PegawaiTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PegawaiTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPegawai A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, username, password, kode_rapor, credential_id, pegawai_master_id, unit_master_id, atasan_id, pangkat_id, jabatan_id, pegawai_status_id, eselon, level, is_aktif, is_trantib, is_lapangan, tanggal_aktif, tanggal_nonaktif, created_at, updated_at, deleted_at FROM eperformance.pegawai WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPegawai $obj */
            $obj = new ChildPegawai();
            $obj->hydrate($row);
            PegawaiTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPegawai|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PegawaiTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PegawaiTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the username column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE username = 'fooValue'
     * $query->filterByUsername('%fooValue%', Criteria::LIKE); // WHERE username LIKE '%fooValue%'
     * </code>
     *
     * @param     string $username The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByUsername($username = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_USERNAME, $username, $comparison);
    }

    /**
     * Filter the query on the password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE password = 'fooValue'
     * $query->filterByPassword('%fooValue%', Criteria::LIKE); // WHERE password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the kode_rapor column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeRapor('fooValue');   // WHERE kode_rapor = 'fooValue'
     * $query->filterByKodeRapor('%fooValue%', Criteria::LIKE); // WHERE kode_rapor LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeRapor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByKodeRapor($kodeRapor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeRapor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_KODE_RAPOR, $kodeRapor, $comparison);
    }

    /**
     * Filter the query on the credential_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCredentialId(1234); // WHERE credential_id = 1234
     * $query->filterByCredentialId(array(12, 34)); // WHERE credential_id IN (12, 34)
     * $query->filterByCredentialId(array('min' => 12)); // WHERE credential_id > 12
     * </code>
     *
     * @see       filterByCredential()
     *
     * @param     mixed $credentialId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByCredentialId($credentialId = null, $comparison = null)
    {
        if (is_array($credentialId)) {
            $useMinMax = false;
            if (isset($credentialId['min'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_CREDENTIAL_ID, $credentialId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($credentialId['max'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_CREDENTIAL_ID, $credentialId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_CREDENTIAL_ID, $credentialId, $comparison);
    }

    /**
     * Filter the query on the pegawai_master_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPegawaiMasterId(1234); // WHERE pegawai_master_id = 1234
     * $query->filterByPegawaiMasterId(array(12, 34)); // WHERE pegawai_master_id IN (12, 34)
     * $query->filterByPegawaiMasterId(array('min' => 12)); // WHERE pegawai_master_id > 12
     * </code>
     *
     * @see       filterByPegawaiMaster()
     *
     * @param     mixed $pegawaiMasterId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByPegawaiMasterId($pegawaiMasterId = null, $comparison = null)
    {
        if (is_array($pegawaiMasterId)) {
            $useMinMax = false;
            if (isset($pegawaiMasterId['min'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_PEGAWAI_MASTER_ID, $pegawaiMasterId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pegawaiMasterId['max'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_PEGAWAI_MASTER_ID, $pegawaiMasterId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_PEGAWAI_MASTER_ID, $pegawaiMasterId, $comparison);
    }

    /**
     * Filter the query on the unit_master_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitMasterId(1234); // WHERE unit_master_id = 1234
     * $query->filterByUnitMasterId(array(12, 34)); // WHERE unit_master_id IN (12, 34)
     * $query->filterByUnitMasterId(array('min' => 12)); // WHERE unit_master_id > 12
     * </code>
     *
     * @see       filterByUnitMaster()
     *
     * @param     mixed $unitMasterId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByUnitMasterId($unitMasterId = null, $comparison = null)
    {
        if (is_array($unitMasterId)) {
            $useMinMax = false;
            if (isset($unitMasterId['min'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_UNIT_MASTER_ID, $unitMasterId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($unitMasterId['max'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_UNIT_MASTER_ID, $unitMasterId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_UNIT_MASTER_ID, $unitMasterId, $comparison);
    }

    /**
     * Filter the query on the atasan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAtasanId(1234); // WHERE atasan_id = 1234
     * $query->filterByAtasanId(array(12, 34)); // WHERE atasan_id IN (12, 34)
     * $query->filterByAtasanId(array('min' => 12)); // WHERE atasan_id > 12
     * </code>
     *
     * @see       filterByPegawaiAtasan()
     *
     * @param     mixed $atasanId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByAtasanId($atasanId = null, $comparison = null)
    {
        if (is_array($atasanId)) {
            $useMinMax = false;
            if (isset($atasanId['min'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_ATASAN_ID, $atasanId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($atasanId['max'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_ATASAN_ID, $atasanId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_ATASAN_ID, $atasanId, $comparison);
    }

    /**
     * Filter the query on the pangkat_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPangkatId(1234); // WHERE pangkat_id = 1234
     * $query->filterByPangkatId(array(12, 34)); // WHERE pangkat_id IN (12, 34)
     * $query->filterByPangkatId(array('min' => 12)); // WHERE pangkat_id > 12
     * </code>
     *
     * @see       filterByPangkat()
     *
     * @param     mixed $pangkatId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByPangkatId($pangkatId = null, $comparison = null)
    {
        if (is_array($pangkatId)) {
            $useMinMax = false;
            if (isset($pangkatId['min'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_PANGKAT_ID, $pangkatId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pangkatId['max'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_PANGKAT_ID, $pangkatId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_PANGKAT_ID, $pangkatId, $comparison);
    }

    /**
     * Filter the query on the jabatan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJabatanId(1234); // WHERE jabatan_id = 1234
     * $query->filterByJabatanId(array(12, 34)); // WHERE jabatan_id IN (12, 34)
     * $query->filterByJabatanId(array('min' => 12)); // WHERE jabatan_id > 12
     * </code>
     *
     * @see       filterByJabatan()
     *
     * @param     mixed $jabatanId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByJabatanId($jabatanId = null, $comparison = null)
    {
        if (is_array($jabatanId)) {
            $useMinMax = false;
            if (isset($jabatanId['min'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_JABATAN_ID, $jabatanId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jabatanId['max'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_JABATAN_ID, $jabatanId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_JABATAN_ID, $jabatanId, $comparison);
    }

    /**
     * Filter the query on the pegawai_status_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPegawaiStatusId(1234); // WHERE pegawai_status_id = 1234
     * $query->filterByPegawaiStatusId(array(12, 34)); // WHERE pegawai_status_id IN (12, 34)
     * $query->filterByPegawaiStatusId(array('min' => 12)); // WHERE pegawai_status_id > 12
     * </code>
     *
     * @see       filterByPegawaiStatus()
     *
     * @param     mixed $pegawaiStatusId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByPegawaiStatusId($pegawaiStatusId = null, $comparison = null)
    {
        if (is_array($pegawaiStatusId)) {
            $useMinMax = false;
            if (isset($pegawaiStatusId['min'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_PEGAWAI_STATUS_ID, $pegawaiStatusId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pegawaiStatusId['max'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_PEGAWAI_STATUS_ID, $pegawaiStatusId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_PEGAWAI_STATUS_ID, $pegawaiStatusId, $comparison);
    }

    /**
     * Filter the query on the eselon column
     *
     * Example usage:
     * <code>
     * $query->filterByEselon(1234); // WHERE eselon = 1234
     * $query->filterByEselon(array(12, 34)); // WHERE eselon IN (12, 34)
     * $query->filterByEselon(array('min' => 12)); // WHERE eselon > 12
     * </code>
     *
     * @param     mixed $eselon The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByEselon($eselon = null, $comparison = null)
    {
        if (is_array($eselon)) {
            $useMinMax = false;
            if (isset($eselon['min'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_ESELON, $eselon['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($eselon['max'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_ESELON, $eselon['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_ESELON, $eselon, $comparison);
    }

    /**
     * Filter the query on the level column
     *
     * Example usage:
     * <code>
     * $query->filterByLevel(1234); // WHERE level = 1234
     * $query->filterByLevel(array(12, 34)); // WHERE level IN (12, 34)
     * $query->filterByLevel(array('min' => 12)); // WHERE level > 12
     * </code>
     *
     * @param     mixed $level The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByLevel($level = null, $comparison = null)
    {
        if (is_array($level)) {
            $useMinMax = false;
            if (isset($level['min'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_LEVEL, $level['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($level['max'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_LEVEL, $level['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_LEVEL, $level, $comparison);
    }

    /**
     * Filter the query on the is_aktif column
     *
     * Example usage:
     * <code>
     * $query->filterByIsAktif(true); // WHERE is_aktif = true
     * $query->filterByIsAktif('yes'); // WHERE is_aktif = true
     * </code>
     *
     * @param     boolean|string $isAktif The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByIsAktif($isAktif = null, $comparison = null)
    {
        if (is_string($isAktif)) {
            $isAktif = in_array(strtolower($isAktif), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_IS_AKTIF, $isAktif, $comparison);
    }

    /**
     * Filter the query on the is_trantib column
     *
     * Example usage:
     * <code>
     * $query->filterByIsTrantib(true); // WHERE is_trantib = true
     * $query->filterByIsTrantib('yes'); // WHERE is_trantib = true
     * </code>
     *
     * @param     boolean|string $isTrantib The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByIsTrantib($isTrantib = null, $comparison = null)
    {
        if (is_string($isTrantib)) {
            $isTrantib = in_array(strtolower($isTrantib), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_IS_TRANTIB, $isTrantib, $comparison);
    }

    /**
     * Filter the query on the is_lapangan column
     *
     * Example usage:
     * <code>
     * $query->filterByIsLapangan(true); // WHERE is_lapangan = true
     * $query->filterByIsLapangan('yes'); // WHERE is_lapangan = true
     * </code>
     *
     * @param     boolean|string $isLapangan The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByIsLapangan($isLapangan = null, $comparison = null)
    {
        if (is_string($isLapangan)) {
            $isLapangan = in_array(strtolower($isLapangan), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_IS_LAPANGAN, $isLapangan, $comparison);
    }

    /**
     * Filter the query on the tanggal_aktif column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggalAktif('2011-03-14'); // WHERE tanggal_aktif = '2011-03-14'
     * $query->filterByTanggalAktif('now'); // WHERE tanggal_aktif = '2011-03-14'
     * $query->filterByTanggalAktif(array('max' => 'yesterday')); // WHERE tanggal_aktif > '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggalAktif The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByTanggalAktif($tanggalAktif = null, $comparison = null)
    {
        if (is_array($tanggalAktif)) {
            $useMinMax = false;
            if (isset($tanggalAktif['min'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_TANGGAL_AKTIF, $tanggalAktif['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggalAktif['max'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_TANGGAL_AKTIF, $tanggalAktif['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_TANGGAL_AKTIF, $tanggalAktif, $comparison);
    }

    /**
     * Filter the query on the tanggal_nonaktif column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggalNonaktif('2011-03-14'); // WHERE tanggal_nonaktif = '2011-03-14'
     * $query->filterByTanggalNonaktif('now'); // WHERE tanggal_nonaktif = '2011-03-14'
     * $query->filterByTanggalNonaktif(array('max' => 'yesterday')); // WHERE tanggal_nonaktif > '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggalNonaktif The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByTanggalNonaktif($tanggalNonaktif = null, $comparison = null)
    {
        if (is_array($tanggalNonaktif)) {
            $useMinMax = false;
            if (isset($tanggalNonaktif['min'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_TANGGAL_NONAKTIF, $tanggalNonaktif['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggalNonaktif['max'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_TANGGAL_NONAKTIF, $tanggalNonaktif['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_TANGGAL_NONAKTIF, $tanggalNonaktif, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(PegawaiTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PegawaiTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\Credential object
     *
     * @param \CoreBundle\Model\Eperformance\Credential|ObjectCollection $credential The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByCredential($credential, $comparison = null)
    {
        if ($credential instanceof \CoreBundle\Model\Eperformance\Credential) {
            return $this
                ->addUsingAlias(PegawaiTableMap::COL_CREDENTIAL_ID, $credential->getId(), $comparison);
        } elseif ($credential instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PegawaiTableMap::COL_CREDENTIAL_ID, $credential->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCredential() only accepts arguments of type \CoreBundle\Model\Eperformance\Credential or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Credential relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function joinCredential($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Credential');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Credential');
        }

        return $this;
    }

    /**
     * Use the Credential relation Credential object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\CredentialQuery A secondary query class using the current class as primary query
     */
    public function useCredentialQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCredential($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Credential', '\CoreBundle\Model\Eperformance\CredentialQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\PegawaiMaster object
     *
     * @param \CoreBundle\Model\Eperformance\PegawaiMaster|ObjectCollection $pegawaiMaster The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByPegawaiMaster($pegawaiMaster, $comparison = null)
    {
        if ($pegawaiMaster instanceof \CoreBundle\Model\Eperformance\PegawaiMaster) {
            return $this
                ->addUsingAlias(PegawaiTableMap::COL_PEGAWAI_MASTER_ID, $pegawaiMaster->getId(), $comparison);
        } elseif ($pegawaiMaster instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PegawaiTableMap::COL_PEGAWAI_MASTER_ID, $pegawaiMaster->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPegawaiMaster() only accepts arguments of type \CoreBundle\Model\Eperformance\PegawaiMaster or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PegawaiMaster relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function joinPegawaiMaster($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PegawaiMaster');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PegawaiMaster');
        }

        return $this;
    }

    /**
     * Use the PegawaiMaster relation PegawaiMaster object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\PegawaiMasterQuery A secondary query class using the current class as primary query
     */
    public function usePegawaiMasterQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPegawaiMaster($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PegawaiMaster', '\CoreBundle\Model\Eperformance\PegawaiMasterQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\UnitMaster object
     *
     * @param \CoreBundle\Model\Eperformance\UnitMaster|ObjectCollection $unitMaster The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByUnitMaster($unitMaster, $comparison = null)
    {
        if ($unitMaster instanceof \CoreBundle\Model\Eperformance\UnitMaster) {
            return $this
                ->addUsingAlias(PegawaiTableMap::COL_UNIT_MASTER_ID, $unitMaster->getId(), $comparison);
        } elseif ($unitMaster instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PegawaiTableMap::COL_UNIT_MASTER_ID, $unitMaster->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUnitMaster() only accepts arguments of type \CoreBundle\Model\Eperformance\UnitMaster or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UnitMaster relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function joinUnitMaster($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UnitMaster');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UnitMaster');
        }

        return $this;
    }

    /**
     * Use the UnitMaster relation UnitMaster object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\UnitMasterQuery A secondary query class using the current class as primary query
     */
    public function useUnitMasterQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUnitMaster($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UnitMaster', '\CoreBundle\Model\Eperformance\UnitMasterQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\Pegawai object
     *
     * @param \CoreBundle\Model\Eperformance\Pegawai|ObjectCollection $pegawai The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByPegawaiAtasan($pegawai, $comparison = null)
    {
        if ($pegawai instanceof \CoreBundle\Model\Eperformance\Pegawai) {
            return $this
                ->addUsingAlias(PegawaiTableMap::COL_ATASAN_ID, $pegawai->getId(), $comparison);
        } elseif ($pegawai instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PegawaiTableMap::COL_ATASAN_ID, $pegawai->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPegawaiAtasan() only accepts arguments of type \CoreBundle\Model\Eperformance\Pegawai or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PegawaiAtasan relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function joinPegawaiAtasan($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PegawaiAtasan');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PegawaiAtasan');
        }

        return $this;
    }

    /**
     * Use the PegawaiAtasan relation Pegawai object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\PegawaiQuery A secondary query class using the current class as primary query
     */
    public function usePegawaiAtasanQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPegawaiAtasan($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PegawaiAtasan', '\CoreBundle\Model\Eperformance\PegawaiQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\Pangkat object
     *
     * @param \CoreBundle\Model\Eperformance\Pangkat|ObjectCollection $pangkat The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByPangkat($pangkat, $comparison = null)
    {
        if ($pangkat instanceof \CoreBundle\Model\Eperformance\Pangkat) {
            return $this
                ->addUsingAlias(PegawaiTableMap::COL_PANGKAT_ID, $pangkat->getId(), $comparison);
        } elseif ($pangkat instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PegawaiTableMap::COL_PANGKAT_ID, $pangkat->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPangkat() only accepts arguments of type \CoreBundle\Model\Eperformance\Pangkat or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pangkat relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function joinPangkat($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pangkat');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pangkat');
        }

        return $this;
    }

    /**
     * Use the Pangkat relation Pangkat object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\PangkatQuery A secondary query class using the current class as primary query
     */
    public function usePangkatQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPangkat($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pangkat', '\CoreBundle\Model\Eperformance\PangkatQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\Jabatan object
     *
     * @param \CoreBundle\Model\Eperformance\Jabatan|ObjectCollection $jabatan The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByJabatan($jabatan, $comparison = null)
    {
        if ($jabatan instanceof \CoreBundle\Model\Eperformance\Jabatan) {
            return $this
                ->addUsingAlias(PegawaiTableMap::COL_JABATAN_ID, $jabatan->getId(), $comparison);
        } elseif ($jabatan instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PegawaiTableMap::COL_JABATAN_ID, $jabatan->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByJabatan() only accepts arguments of type \CoreBundle\Model\Eperformance\Jabatan or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Jabatan relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function joinJabatan($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Jabatan');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Jabatan');
        }

        return $this;
    }

    /**
     * Use the Jabatan relation Jabatan object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\JabatanQuery A secondary query class using the current class as primary query
     */
    public function useJabatanQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJabatan($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Jabatan', '\CoreBundle\Model\Eperformance\JabatanQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\PegawaiStatus object
     *
     * @param \CoreBundle\Model\Eperformance\PegawaiStatus|ObjectCollection $pegawaiStatus The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByPegawaiStatus($pegawaiStatus, $comparison = null)
    {
        if ($pegawaiStatus instanceof \CoreBundle\Model\Eperformance\PegawaiStatus) {
            return $this
                ->addUsingAlias(PegawaiTableMap::COL_PEGAWAI_STATUS_ID, $pegawaiStatus->getId(), $comparison);
        } elseif ($pegawaiStatus instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PegawaiTableMap::COL_PEGAWAI_STATUS_ID, $pegawaiStatus->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPegawaiStatus() only accepts arguments of type \CoreBundle\Model\Eperformance\PegawaiStatus or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PegawaiStatus relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function joinPegawaiStatus($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PegawaiStatus');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PegawaiStatus');
        }

        return $this;
    }

    /**
     * Use the PegawaiStatus relation PegawaiStatus object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\PegawaiStatusQuery A secondary query class using the current class as primary query
     */
    public function usePegawaiStatusQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPegawaiStatus($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PegawaiStatus', '\CoreBundle\Model\Eperformance\PegawaiStatusQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\Pegawai object
     *
     * @param \CoreBundle\Model\Eperformance\Pegawai|ObjectCollection $pegawai the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPegawaiQuery The current query, for fluid interface
     */
    public function filterByPegawaiRelatedById($pegawai, $comparison = null)
    {
        if ($pegawai instanceof \CoreBundle\Model\Eperformance\Pegawai) {
            return $this
                ->addUsingAlias(PegawaiTableMap::COL_ID, $pegawai->getAtasanId(), $comparison);
        } elseif ($pegawai instanceof ObjectCollection) {
            return $this
                ->usePegawaiRelatedByIdQuery()
                ->filterByPrimaryKeys($pegawai->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPegawaiRelatedById() only accepts arguments of type \CoreBundle\Model\Eperformance\Pegawai or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PegawaiRelatedById relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function joinPegawaiRelatedById($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PegawaiRelatedById');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PegawaiRelatedById');
        }

        return $this;
    }

    /**
     * Use the PegawaiRelatedById relation Pegawai object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\PegawaiQuery A secondary query class using the current class as primary query
     */
    public function usePegawaiRelatedByIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPegawaiRelatedById($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PegawaiRelatedById', '\CoreBundle\Model\Eperformance\PegawaiQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPegawai $pegawai Object to remove from the list of results
     *
     * @return $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function prune($pegawai = null)
    {
        if ($pegawai) {
            $this->addUsingAlias(PegawaiTableMap::COL_ID, $pegawai->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the eperformance.pegawai table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PegawaiTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PegawaiTableMap::clearInstancePool();
            PegawaiTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PegawaiTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PegawaiTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PegawaiTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PegawaiTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 7200)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PegawaiTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(PegawaiTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }


        return $con->getDataFetcher($stmt);
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(PegawaiTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(PegawaiTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(PegawaiTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(PegawaiTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(PegawaiTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildPegawaiQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(PegawaiTableMap::COL_CREATED_AT);
    }

} // PegawaiQuery

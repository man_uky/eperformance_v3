<?php

namespace CoreBundle\Model\Eperformance\Base;

use \Exception;
use \PDO;
use CoreBundle\Model\Eperformance\PegawaiStatus as ChildPegawaiStatus;
use CoreBundle\Model\Eperformance\PegawaiStatusQuery as ChildPegawaiStatusQuery;
use CoreBundle\Model\Eperformance\Map\PegawaiStatusTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'eperformance.pegawai_status' table.
 *
 *
 *
 * @method     ChildPegawaiStatusQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPegawaiStatusQuery orderByNama($order = Criteria::ASC) Order by the nama column
 *
 * @method     ChildPegawaiStatusQuery groupById() Group by the id column
 * @method     ChildPegawaiStatusQuery groupByNama() Group by the nama column
 *
 * @method     ChildPegawaiStatusQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPegawaiStatusQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPegawaiStatusQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPegawaiStatusQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPegawaiStatusQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPegawaiStatusQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPegawaiStatusQuery leftJoinPegawai($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pegawai relation
 * @method     ChildPegawaiStatusQuery rightJoinPegawai($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pegawai relation
 * @method     ChildPegawaiStatusQuery innerJoinPegawai($relationAlias = null) Adds a INNER JOIN clause to the query using the Pegawai relation
 *
 * @method     ChildPegawaiStatusQuery joinWithPegawai($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Pegawai relation
 *
 * @method     ChildPegawaiStatusQuery leftJoinWithPegawai() Adds a LEFT JOIN clause and with to the query using the Pegawai relation
 * @method     ChildPegawaiStatusQuery rightJoinWithPegawai() Adds a RIGHT JOIN clause and with to the query using the Pegawai relation
 * @method     ChildPegawaiStatusQuery innerJoinWithPegawai() Adds a INNER JOIN clause and with to the query using the Pegawai relation
 *
 * @method     \CoreBundle\Model\Eperformance\PegawaiQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPegawaiStatus findOne(ConnectionInterface $con = null) Return the first ChildPegawaiStatus matching the query
 * @method     ChildPegawaiStatus findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPegawaiStatus matching the query, or a new ChildPegawaiStatus object populated from the query conditions when no match is found
 *
 * @method     ChildPegawaiStatus findOneById(int $id) Return the first ChildPegawaiStatus filtered by the id column
 * @method     ChildPegawaiStatus findOneByNama(string $nama) Return the first ChildPegawaiStatus filtered by the nama column *

 * @method     ChildPegawaiStatus requirePk($key, ConnectionInterface $con = null) Return the ChildPegawaiStatus by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawaiStatus requireOne(ConnectionInterface $con = null) Return the first ChildPegawaiStatus matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPegawaiStatus requireOneById(int $id) Return the first ChildPegawaiStatus filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPegawaiStatus requireOneByNama(string $nama) Return the first ChildPegawaiStatus filtered by the nama column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPegawaiStatus[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPegawaiStatus objects based on current ModelCriteria
 * @method     ChildPegawaiStatus[]|ObjectCollection findById(int $id) Return ChildPegawaiStatus objects filtered by the id column
 * @method     ChildPegawaiStatus[]|ObjectCollection findByNama(string $nama) Return ChildPegawaiStatus objects filtered by the nama column
 * @method     ChildPegawaiStatus[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PegawaiStatusQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \CoreBundle\Model\Eperformance\Base\PegawaiStatusQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\CoreBundle\\Model\\Eperformance\\PegawaiStatus', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPegawaiStatusQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPegawaiStatusQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPegawaiStatusQuery) {
            return $criteria;
        }
        $query = new ChildPegawaiStatusQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPegawaiStatus|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PegawaiStatusTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PegawaiStatusTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPegawaiStatus A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, nama FROM eperformance.pegawai_status WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPegawaiStatus $obj */
            $obj = new ChildPegawaiStatus();
            $obj->hydrate($row);
            PegawaiStatusTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPegawaiStatus|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPegawaiStatusQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PegawaiStatusTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPegawaiStatusQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PegawaiStatusTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiStatusQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PegawaiStatusTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PegawaiStatusTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PegawaiStatusTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%', Criteria::LIKE); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPegawaiStatusQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PegawaiStatusTableMap::COL_NAMA, $nama, $comparison);
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\Pegawai object
     *
     * @param \CoreBundle\Model\Eperformance\Pegawai|ObjectCollection $pegawai the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPegawaiStatusQuery The current query, for fluid interface
     */
    public function filterByPegawai($pegawai, $comparison = null)
    {
        if ($pegawai instanceof \CoreBundle\Model\Eperformance\Pegawai) {
            return $this
                ->addUsingAlias(PegawaiStatusTableMap::COL_ID, $pegawai->getPegawaiStatusId(), $comparison);
        } elseif ($pegawai instanceof ObjectCollection) {
            return $this
                ->usePegawaiQuery()
                ->filterByPrimaryKeys($pegawai->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPegawai() only accepts arguments of type \CoreBundle\Model\Eperformance\Pegawai or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pegawai relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPegawaiStatusQuery The current query, for fluid interface
     */
    public function joinPegawai($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pegawai');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pegawai');
        }

        return $this;
    }

    /**
     * Use the Pegawai relation Pegawai object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\PegawaiQuery A secondary query class using the current class as primary query
     */
    public function usePegawaiQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPegawai($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pegawai', '\CoreBundle\Model\Eperformance\PegawaiQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPegawaiStatus $pegawaiStatus Object to remove from the list of results
     *
     * @return $this|ChildPegawaiStatusQuery The current query, for fluid interface
     */
    public function prune($pegawaiStatus = null)
    {
        if ($pegawaiStatus) {
            $this->addUsingAlias(PegawaiStatusTableMap::COL_ID, $pegawaiStatus->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the eperformance.pegawai_status table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PegawaiStatusTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PegawaiStatusTableMap::clearInstancePool();
            PegawaiStatusTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PegawaiStatusTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PegawaiStatusTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PegawaiStatusTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PegawaiStatusTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PegawaiStatusQuery

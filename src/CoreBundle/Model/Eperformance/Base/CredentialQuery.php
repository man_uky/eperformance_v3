<?php

namespace CoreBundle\Model\Eperformance\Base;

use \Exception;
use \PDO;
use CoreBundle\Model\Eperformance\Credential as ChildCredential;
use CoreBundle\Model\Eperformance\CredentialQuery as ChildCredentialQuery;
use CoreBundle\Model\Eperformance\Map\CredentialTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'eperformance.credential' table.
 *
 *
 *
 * @method     ChildCredentialQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCredentialQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method     ChildCredentialQuery orderByIsAktif($order = Criteria::ASC) Order by the is_aktif column
 *
 * @method     ChildCredentialQuery groupById() Group by the id column
 * @method     ChildCredentialQuery groupByNama() Group by the nama column
 * @method     ChildCredentialQuery groupByIsAktif() Group by the is_aktif column
 *
 * @method     ChildCredentialQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCredentialQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCredentialQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCredentialQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCredentialQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCredentialQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCredentialQuery leftJoinUserLog($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserLog relation
 * @method     ChildCredentialQuery rightJoinUserLog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserLog relation
 * @method     ChildCredentialQuery innerJoinUserLog($relationAlias = null) Adds a INNER JOIN clause to the query using the UserLog relation
 *
 * @method     ChildCredentialQuery joinWithUserLog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserLog relation
 *
 * @method     ChildCredentialQuery leftJoinWithUserLog() Adds a LEFT JOIN clause and with to the query using the UserLog relation
 * @method     ChildCredentialQuery rightJoinWithUserLog() Adds a RIGHT JOIN clause and with to the query using the UserLog relation
 * @method     ChildCredentialQuery innerJoinWithUserLog() Adds a INNER JOIN clause and with to the query using the UserLog relation
 *
 * @method     ChildCredentialQuery leftJoinPenggunaMaster($relationAlias = null) Adds a LEFT JOIN clause to the query using the PenggunaMaster relation
 * @method     ChildCredentialQuery rightJoinPenggunaMaster($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PenggunaMaster relation
 * @method     ChildCredentialQuery innerJoinPenggunaMaster($relationAlias = null) Adds a INNER JOIN clause to the query using the PenggunaMaster relation
 *
 * @method     ChildCredentialQuery joinWithPenggunaMaster($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PenggunaMaster relation
 *
 * @method     ChildCredentialQuery leftJoinWithPenggunaMaster() Adds a LEFT JOIN clause and with to the query using the PenggunaMaster relation
 * @method     ChildCredentialQuery rightJoinWithPenggunaMaster() Adds a RIGHT JOIN clause and with to the query using the PenggunaMaster relation
 * @method     ChildCredentialQuery innerJoinWithPenggunaMaster() Adds a INNER JOIN clause and with to the query using the PenggunaMaster relation
 *
 * @method     ChildCredentialQuery leftJoinPegawai($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pegawai relation
 * @method     ChildCredentialQuery rightJoinPegawai($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pegawai relation
 * @method     ChildCredentialQuery innerJoinPegawai($relationAlias = null) Adds a INNER JOIN clause to the query using the Pegawai relation
 *
 * @method     ChildCredentialQuery joinWithPegawai($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Pegawai relation
 *
 * @method     ChildCredentialQuery leftJoinWithPegawai() Adds a LEFT JOIN clause and with to the query using the Pegawai relation
 * @method     ChildCredentialQuery rightJoinWithPegawai() Adds a RIGHT JOIN clause and with to the query using the Pegawai relation
 * @method     ChildCredentialQuery innerJoinWithPegawai() Adds a INNER JOIN clause and with to the query using the Pegawai relation
 *
 * @method     \CoreBundle\Model\Eperformance\UserLogQuery|\CoreBundle\Model\Eperformance\PenggunaMasterQuery|\CoreBundle\Model\Eperformance\PegawaiQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCredential findOne(ConnectionInterface $con = null) Return the first ChildCredential matching the query
 * @method     ChildCredential findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCredential matching the query, or a new ChildCredential object populated from the query conditions when no match is found
 *
 * @method     ChildCredential findOneById(int $id) Return the first ChildCredential filtered by the id column
 * @method     ChildCredential findOneByNama(string $nama) Return the first ChildCredential filtered by the nama column
 * @method     ChildCredential findOneByIsAktif(boolean $is_aktif) Return the first ChildCredential filtered by the is_aktif column *

 * @method     ChildCredential requirePk($key, ConnectionInterface $con = null) Return the ChildCredential by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCredential requireOne(ConnectionInterface $con = null) Return the first ChildCredential matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCredential requireOneById(int $id) Return the first ChildCredential filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCredential requireOneByNama(string $nama) Return the first ChildCredential filtered by the nama column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCredential requireOneByIsAktif(boolean $is_aktif) Return the first ChildCredential filtered by the is_aktif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCredential[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCredential objects based on current ModelCriteria
 * @method     ChildCredential[]|ObjectCollection findById(int $id) Return ChildCredential objects filtered by the id column
 * @method     ChildCredential[]|ObjectCollection findByNama(string $nama) Return ChildCredential objects filtered by the nama column
 * @method     ChildCredential[]|ObjectCollection findByIsAktif(boolean $is_aktif) Return ChildCredential objects filtered by the is_aktif column
 * @method     ChildCredential[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CredentialQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \CoreBundle\Model\Eperformance\Base\CredentialQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\CoreBundle\\Model\\Eperformance\\Credential', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCredentialQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCredentialQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCredentialQuery) {
            return $criteria;
        }
        $query = new ChildCredentialQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCredential|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CredentialTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CredentialTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCredential A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, nama, is_aktif FROM eperformance.credential WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCredential $obj */
            $obj = new ChildCredential();
            $obj->hydrate($row);
            CredentialTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCredential|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCredentialQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CredentialTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCredentialQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CredentialTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCredentialQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CredentialTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CredentialTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CredentialTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%', Criteria::LIKE); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCredentialQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CredentialTableMap::COL_NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the is_aktif column
     *
     * Example usage:
     * <code>
     * $query->filterByIsAktif(true); // WHERE is_aktif = true
     * $query->filterByIsAktif('yes'); // WHERE is_aktif = true
     * </code>
     *
     * @param     boolean|string $isAktif The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCredentialQuery The current query, for fluid interface
     */
    public function filterByIsAktif($isAktif = null, $comparison = null)
    {
        if (is_string($isAktif)) {
            $isAktif = in_array(strtolower($isAktif), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CredentialTableMap::COL_IS_AKTIF, $isAktif, $comparison);
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\UserLog object
     *
     * @param \CoreBundle\Model\Eperformance\UserLog|ObjectCollection $userLog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCredentialQuery The current query, for fluid interface
     */
    public function filterByUserLog($userLog, $comparison = null)
    {
        if ($userLog instanceof \CoreBundle\Model\Eperformance\UserLog) {
            return $this
                ->addUsingAlias(CredentialTableMap::COL_ID, $userLog->getCredentialId(), $comparison);
        } elseif ($userLog instanceof ObjectCollection) {
            return $this
                ->useUserLogQuery()
                ->filterByPrimaryKeys($userLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserLog() only accepts arguments of type \CoreBundle\Model\Eperformance\UserLog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserLog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCredentialQuery The current query, for fluid interface
     */
    public function joinUserLog($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserLog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserLog');
        }

        return $this;
    }

    /**
     * Use the UserLog relation UserLog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\UserLogQuery A secondary query class using the current class as primary query
     */
    public function useUserLogQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserLog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserLog', '\CoreBundle\Model\Eperformance\UserLogQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\PenggunaMaster object
     *
     * @param \CoreBundle\Model\Eperformance\PenggunaMaster|ObjectCollection $penggunaMaster the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCredentialQuery The current query, for fluid interface
     */
    public function filterByPenggunaMaster($penggunaMaster, $comparison = null)
    {
        if ($penggunaMaster instanceof \CoreBundle\Model\Eperformance\PenggunaMaster) {
            return $this
                ->addUsingAlias(CredentialTableMap::COL_ID, $penggunaMaster->getCredentialId(), $comparison);
        } elseif ($penggunaMaster instanceof ObjectCollection) {
            return $this
                ->usePenggunaMasterQuery()
                ->filterByPrimaryKeys($penggunaMaster->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPenggunaMaster() only accepts arguments of type \CoreBundle\Model\Eperformance\PenggunaMaster or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PenggunaMaster relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCredentialQuery The current query, for fluid interface
     */
    public function joinPenggunaMaster($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PenggunaMaster');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PenggunaMaster');
        }

        return $this;
    }

    /**
     * Use the PenggunaMaster relation PenggunaMaster object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\PenggunaMasterQuery A secondary query class using the current class as primary query
     */
    public function usePenggunaMasterQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPenggunaMaster($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PenggunaMaster', '\CoreBundle\Model\Eperformance\PenggunaMasterQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\Pegawai object
     *
     * @param \CoreBundle\Model\Eperformance\Pegawai|ObjectCollection $pegawai the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCredentialQuery The current query, for fluid interface
     */
    public function filterByPegawai($pegawai, $comparison = null)
    {
        if ($pegawai instanceof \CoreBundle\Model\Eperformance\Pegawai) {
            return $this
                ->addUsingAlias(CredentialTableMap::COL_ID, $pegawai->getCredentialId(), $comparison);
        } elseif ($pegawai instanceof ObjectCollection) {
            return $this
                ->usePegawaiQuery()
                ->filterByPrimaryKeys($pegawai->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPegawai() only accepts arguments of type \CoreBundle\Model\Eperformance\Pegawai or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pegawai relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCredentialQuery The current query, for fluid interface
     */
    public function joinPegawai($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pegawai');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pegawai');
        }

        return $this;
    }

    /**
     * Use the Pegawai relation Pegawai object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\PegawaiQuery A secondary query class using the current class as primary query
     */
    public function usePegawaiQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPegawai($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pegawai', '\CoreBundle\Model\Eperformance\PegawaiQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCredential $credential Object to remove from the list of results
     *
     * @return $this|ChildCredentialQuery The current query, for fluid interface
     */
    public function prune($credential = null)
    {
        if ($credential) {
            $this->addUsingAlias(CredentialTableMap::COL_ID, $credential->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the eperformance.credential table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CredentialTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CredentialTableMap::clearInstancePool();
            CredentialTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CredentialTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CredentialTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CredentialTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CredentialTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CredentialQuery

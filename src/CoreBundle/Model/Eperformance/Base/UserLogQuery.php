<?php

namespace CoreBundle\Model\Eperformance\Base;

use \Exception;
use \PDO;
use CoreBundle\Model\Eperformance\UserLog as ChildUserLog;
use CoreBundle\Model\Eperformance\UserLogQuery as ChildUserLogQuery;
use CoreBundle\Model\Eperformance\Map\UserLogTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'eperformance.user_log' table.
 *
 *
 *
 * @method     ChildUserLogQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUserLogQuery orderByParentId($order = Criteria::ASC) Order by the parent_id column
 * @method     ChildUserLogQuery orderByUsername($order = Criteria::ASC) Order by the username column
 * @method     ChildUserLogQuery orderByAksi($order = Criteria::ASC) Order by the aksi column
 * @method     ChildUserLogQuery orderByParameter($order = Criteria::ASC) Order by the parameter column
 * @method     ChildUserLogQuery orderByIpAddress($order = Criteria::ASC) Order by the ip_address column
 * @method     ChildUserLogQuery orderByCredentialId($order = Criteria::ASC) Order by the credential_id column
 * @method     ChildUserLogQuery orderByMacAddress($order = Criteria::ASC) Order by the mac_address column
 * @method     ChildUserLogQuery orderByKeterangan($order = Criteria::ASC) Order by the keterangan column
 * @method     ChildUserLogQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildUserLogQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildUserLogQuery groupById() Group by the id column
 * @method     ChildUserLogQuery groupByParentId() Group by the parent_id column
 * @method     ChildUserLogQuery groupByUsername() Group by the username column
 * @method     ChildUserLogQuery groupByAksi() Group by the aksi column
 * @method     ChildUserLogQuery groupByParameter() Group by the parameter column
 * @method     ChildUserLogQuery groupByIpAddress() Group by the ip_address column
 * @method     ChildUserLogQuery groupByCredentialId() Group by the credential_id column
 * @method     ChildUserLogQuery groupByMacAddress() Group by the mac_address column
 * @method     ChildUserLogQuery groupByKeterangan() Group by the keterangan column
 * @method     ChildUserLogQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildUserLogQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildUserLogQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUserLogQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUserLogQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUserLogQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUserLogQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUserLogQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUserLogQuery leftJoinCredential($relationAlias = null) Adds a LEFT JOIN clause to the query using the Credential relation
 * @method     ChildUserLogQuery rightJoinCredential($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Credential relation
 * @method     ChildUserLogQuery innerJoinCredential($relationAlias = null) Adds a INNER JOIN clause to the query using the Credential relation
 *
 * @method     ChildUserLogQuery joinWithCredential($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Credential relation
 *
 * @method     ChildUserLogQuery leftJoinWithCredential() Adds a LEFT JOIN clause and with to the query using the Credential relation
 * @method     ChildUserLogQuery rightJoinWithCredential() Adds a RIGHT JOIN clause and with to the query using the Credential relation
 * @method     ChildUserLogQuery innerJoinWithCredential() Adds a INNER JOIN clause and with to the query using the Credential relation
 *
 * @method     \CoreBundle\Model\Eperformance\CredentialQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUserLog findOne(ConnectionInterface $con = null) Return the first ChildUserLog matching the query
 * @method     ChildUserLog findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUserLog matching the query, or a new ChildUserLog object populated from the query conditions when no match is found
 *
 * @method     ChildUserLog findOneById(string $id) Return the first ChildUserLog filtered by the id column
 * @method     ChildUserLog findOneByParentId(string $parent_id) Return the first ChildUserLog filtered by the parent_id column
 * @method     ChildUserLog findOneByUsername(string $username) Return the first ChildUserLog filtered by the username column
 * @method     ChildUserLog findOneByAksi(string $aksi) Return the first ChildUserLog filtered by the aksi column
 * @method     ChildUserLog findOneByParameter(string $parameter) Return the first ChildUserLog filtered by the parameter column
 * @method     ChildUserLog findOneByIpAddress(string $ip_address) Return the first ChildUserLog filtered by the ip_address column
 * @method     ChildUserLog findOneByCredentialId(int $credential_id) Return the first ChildUserLog filtered by the credential_id column
 * @method     ChildUserLog findOneByMacAddress(string $mac_address) Return the first ChildUserLog filtered by the mac_address column
 * @method     ChildUserLog findOneByKeterangan(string $keterangan) Return the first ChildUserLog filtered by the keterangan column
 * @method     ChildUserLog findOneByCreatedAt(string $created_at) Return the first ChildUserLog filtered by the created_at column
 * @method     ChildUserLog findOneByUpdatedAt(string $updated_at) Return the first ChildUserLog filtered by the updated_at column *

 * @method     ChildUserLog requirePk($key, ConnectionInterface $con = null) Return the ChildUserLog by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserLog requireOne(ConnectionInterface $con = null) Return the first ChildUserLog matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserLog requireOneById(string $id) Return the first ChildUserLog filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserLog requireOneByParentId(string $parent_id) Return the first ChildUserLog filtered by the parent_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserLog requireOneByUsername(string $username) Return the first ChildUserLog filtered by the username column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserLog requireOneByAksi(string $aksi) Return the first ChildUserLog filtered by the aksi column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserLog requireOneByParameter(string $parameter) Return the first ChildUserLog filtered by the parameter column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserLog requireOneByIpAddress(string $ip_address) Return the first ChildUserLog filtered by the ip_address column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserLog requireOneByCredentialId(int $credential_id) Return the first ChildUserLog filtered by the credential_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserLog requireOneByMacAddress(string $mac_address) Return the first ChildUserLog filtered by the mac_address column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserLog requireOneByKeterangan(string $keterangan) Return the first ChildUserLog filtered by the keterangan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserLog requireOneByCreatedAt(string $created_at) Return the first ChildUserLog filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserLog requireOneByUpdatedAt(string $updated_at) Return the first ChildUserLog filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserLog[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUserLog objects based on current ModelCriteria
 * @method     ChildUserLog[]|ObjectCollection findById(string $id) Return ChildUserLog objects filtered by the id column
 * @method     ChildUserLog[]|ObjectCollection findByParentId(string $parent_id) Return ChildUserLog objects filtered by the parent_id column
 * @method     ChildUserLog[]|ObjectCollection findByUsername(string $username) Return ChildUserLog objects filtered by the username column
 * @method     ChildUserLog[]|ObjectCollection findByAksi(string $aksi) Return ChildUserLog objects filtered by the aksi column
 * @method     ChildUserLog[]|ObjectCollection findByParameter(string $parameter) Return ChildUserLog objects filtered by the parameter column
 * @method     ChildUserLog[]|ObjectCollection findByIpAddress(string $ip_address) Return ChildUserLog objects filtered by the ip_address column
 * @method     ChildUserLog[]|ObjectCollection findByCredentialId(int $credential_id) Return ChildUserLog objects filtered by the credential_id column
 * @method     ChildUserLog[]|ObjectCollection findByMacAddress(string $mac_address) Return ChildUserLog objects filtered by the mac_address column
 * @method     ChildUserLog[]|ObjectCollection findByKeterangan(string $keterangan) Return ChildUserLog objects filtered by the keterangan column
 * @method     ChildUserLog[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildUserLog objects filtered by the created_at column
 * @method     ChildUserLog[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildUserLog objects filtered by the updated_at column
 * @method     ChildUserLog[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UserLogQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \CoreBundle\Model\Eperformance\Base\UserLogQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\CoreBundle\\Model\\Eperformance\\UserLog', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUserLogQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUserLogQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUserLogQuery) {
            return $criteria;
        }
        $query = new ChildUserLogQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUserLog|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserLogTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UserLogTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserLog A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, parent_id, username, aksi, parameter, ip_address, credential_id, mac_address, keterangan, created_at, updated_at FROM eperformance.user_log WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUserLog $obj */
            $obj = new ChildUserLog();
            $obj->hydrate($row);
            UserLogTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUserLog|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UserLogTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UserLogTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UserLogTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UserLogTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserLogTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the parent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByParentId(1234); // WHERE parent_id = 1234
     * $query->filterByParentId(array(12, 34)); // WHERE parent_id IN (12, 34)
     * $query->filterByParentId(array('min' => 12)); // WHERE parent_id > 12
     * </code>
     *
     * @param     mixed $parentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function filterByParentId($parentId = null, $comparison = null)
    {
        if (is_array($parentId)) {
            $useMinMax = false;
            if (isset($parentId['min'])) {
                $this->addUsingAlias(UserLogTableMap::COL_PARENT_ID, $parentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($parentId['max'])) {
                $this->addUsingAlias(UserLogTableMap::COL_PARENT_ID, $parentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserLogTableMap::COL_PARENT_ID, $parentId, $comparison);
    }

    /**
     * Filter the query on the username column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE username = 'fooValue'
     * $query->filterByUsername('%fooValue%', Criteria::LIKE); // WHERE username LIKE '%fooValue%'
     * </code>
     *
     * @param     string $username The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function filterByUsername($username = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserLogTableMap::COL_USERNAME, $username, $comparison);
    }

    /**
     * Filter the query on the aksi column
     *
     * Example usage:
     * <code>
     * $query->filterByAksi('fooValue');   // WHERE aksi = 'fooValue'
     * $query->filterByAksi('%fooValue%', Criteria::LIKE); // WHERE aksi LIKE '%fooValue%'
     * </code>
     *
     * @param     string $aksi The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function filterByAksi($aksi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($aksi)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserLogTableMap::COL_AKSI, $aksi, $comparison);
    }

    /**
     * Filter the query on the parameter column
     *
     * Example usage:
     * <code>
     * $query->filterByParameter('fooValue');   // WHERE parameter = 'fooValue'
     * $query->filterByParameter('%fooValue%', Criteria::LIKE); // WHERE parameter LIKE '%fooValue%'
     * </code>
     *
     * @param     string $parameter The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function filterByParameter($parameter = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($parameter)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserLogTableMap::COL_PARAMETER, $parameter, $comparison);
    }

    /**
     * Filter the query on the ip_address column
     *
     * Example usage:
     * <code>
     * $query->filterByIpAddress('fooValue');   // WHERE ip_address = 'fooValue'
     * $query->filterByIpAddress('%fooValue%', Criteria::LIKE); // WHERE ip_address LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ipAddress The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function filterByIpAddress($ipAddress = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ipAddress)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserLogTableMap::COL_IP_ADDRESS, $ipAddress, $comparison);
    }

    /**
     * Filter the query on the credential_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCredentialId(1234); // WHERE credential_id = 1234
     * $query->filterByCredentialId(array(12, 34)); // WHERE credential_id IN (12, 34)
     * $query->filterByCredentialId(array('min' => 12)); // WHERE credential_id > 12
     * </code>
     *
     * @see       filterByCredential()
     *
     * @param     mixed $credentialId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function filterByCredentialId($credentialId = null, $comparison = null)
    {
        if (is_array($credentialId)) {
            $useMinMax = false;
            if (isset($credentialId['min'])) {
                $this->addUsingAlias(UserLogTableMap::COL_CREDENTIAL_ID, $credentialId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($credentialId['max'])) {
                $this->addUsingAlias(UserLogTableMap::COL_CREDENTIAL_ID, $credentialId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserLogTableMap::COL_CREDENTIAL_ID, $credentialId, $comparison);
    }

    /**
     * Filter the query on the mac_address column
     *
     * Example usage:
     * <code>
     * $query->filterByMacAddress('fooValue');   // WHERE mac_address = 'fooValue'
     * $query->filterByMacAddress('%fooValue%', Criteria::LIKE); // WHERE mac_address LIKE '%fooValue%'
     * </code>
     *
     * @param     string $macAddress The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function filterByMacAddress($macAddress = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($macAddress)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserLogTableMap::COL_MAC_ADDRESS, $macAddress, $comparison);
    }

    /**
     * Filter the query on the keterangan column
     *
     * Example usage:
     * <code>
     * $query->filterByKeterangan('fooValue');   // WHERE keterangan = 'fooValue'
     * $query->filterByKeterangan('%fooValue%', Criteria::LIKE); // WHERE keterangan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $keterangan The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function filterByKeterangan($keterangan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($keterangan)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserLogTableMap::COL_KETERANGAN, $keterangan, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(UserLogTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(UserLogTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserLogTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(UserLogTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(UserLogTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserLogTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \CoreBundle\Model\Eperformance\Credential object
     *
     * @param \CoreBundle\Model\Eperformance\Credential|ObjectCollection $credential The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserLogQuery The current query, for fluid interface
     */
    public function filterByCredential($credential, $comparison = null)
    {
        if ($credential instanceof \CoreBundle\Model\Eperformance\Credential) {
            return $this
                ->addUsingAlias(UserLogTableMap::COL_CREDENTIAL_ID, $credential->getId(), $comparison);
        } elseif ($credential instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserLogTableMap::COL_CREDENTIAL_ID, $credential->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCredential() only accepts arguments of type \CoreBundle\Model\Eperformance\Credential or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Credential relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function joinCredential($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Credential');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Credential');
        }

        return $this;
    }

    /**
     * Use the Credential relation Credential object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\Eperformance\CredentialQuery A secondary query class using the current class as primary query
     */
    public function useCredentialQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCredential($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Credential', '\CoreBundle\Model\Eperformance\CredentialQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUserLog $userLog Object to remove from the list of results
     *
     * @return $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function prune($userLog = null)
    {
        if ($userLog) {
            $this->addUsingAlias(UserLogTableMap::COL_ID, $userLog->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the eperformance.user_log table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserLogTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UserLogTableMap::clearInstancePool();
            UserLogTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserLogTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UserLogTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UserLogTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UserLogTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 7200)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UserLogTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(UserLogTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }


        return $con->getDataFetcher($stmt);
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(UserLogTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(UserLogTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(UserLogTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(UserLogTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(UserLogTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildUserLogQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(UserLogTableMap::COL_CREATED_AT);
    }

} // UserLogQuery

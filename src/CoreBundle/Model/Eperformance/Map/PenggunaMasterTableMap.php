<?php

namespace CoreBundle\Model\Eperformance\Map;

use CoreBundle\Model\Eperformance\PenggunaMaster;
use CoreBundle\Model\Eperformance\PenggunaMasterQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'eperformance.pengguna_master' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PenggunaMasterTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.CoreBundle.Model.Eperformance.Map.PenggunaMasterTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'eperformance.pengguna_master';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\CoreBundle\\Model\\Eperformance\\PenggunaMaster';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.CoreBundle.Model.Eperformance.PenggunaMaster';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the id field
     */
    const COL_ID = 'eperformance.pengguna_master.id';

    /**
     * the column name for the username field
     */
    const COL_USERNAME = 'eperformance.pengguna_master.username';

    /**
     * the column name for the password field
     */
    const COL_PASSWORD = 'eperformance.pengguna_master.password';

    /**
     * the column name for the nip field
     */
    const COL_NIP = 'eperformance.pengguna_master.nip';

    /**
     * the column name for the nama field
     */
    const COL_NAMA = 'eperformance.pengguna_master.nama';

    /**
     * the column name for the is_aktif field
     */
    const COL_IS_AKTIF = 'eperformance.pengguna_master.is_aktif';

    /**
     * the column name for the credential_id field
     */
    const COL_CREDENTIAL_ID = 'eperformance.pengguna_master.credential_id';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'eperformance.pengguna_master.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'eperformance.pengguna_master.updated_at';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'eperformance.pengguna_master.deleted_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Username', 'Password', 'Nip', 'Nama', 'IsAktif', 'CredentialId', 'CreatedAt', 'UpdatedAt', 'DeletedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'username', 'password', 'nip', 'nama', 'isAktif', 'credentialId', 'createdAt', 'updatedAt', 'deletedAt', ),
        self::TYPE_COLNAME       => array(PenggunaMasterTableMap::COL_ID, PenggunaMasterTableMap::COL_USERNAME, PenggunaMasterTableMap::COL_PASSWORD, PenggunaMasterTableMap::COL_NIP, PenggunaMasterTableMap::COL_NAMA, PenggunaMasterTableMap::COL_IS_AKTIF, PenggunaMasterTableMap::COL_CREDENTIAL_ID, PenggunaMasterTableMap::COL_CREATED_AT, PenggunaMasterTableMap::COL_UPDATED_AT, PenggunaMasterTableMap::COL_DELETED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'username', 'password', 'nip', 'nama', 'is_aktif', 'credential_id', 'created_at', 'updated_at', 'deleted_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Username' => 1, 'Password' => 2, 'Nip' => 3, 'Nama' => 4, 'IsAktif' => 5, 'CredentialId' => 6, 'CreatedAt' => 7, 'UpdatedAt' => 8, 'DeletedAt' => 9, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'username' => 1, 'password' => 2, 'nip' => 3, 'nama' => 4, 'isAktif' => 5, 'credentialId' => 6, 'createdAt' => 7, 'updatedAt' => 8, 'deletedAt' => 9, ),
        self::TYPE_COLNAME       => array(PenggunaMasterTableMap::COL_ID => 0, PenggunaMasterTableMap::COL_USERNAME => 1, PenggunaMasterTableMap::COL_PASSWORD => 2, PenggunaMasterTableMap::COL_NIP => 3, PenggunaMasterTableMap::COL_NAMA => 4, PenggunaMasterTableMap::COL_IS_AKTIF => 5, PenggunaMasterTableMap::COL_CREDENTIAL_ID => 6, PenggunaMasterTableMap::COL_CREATED_AT => 7, PenggunaMasterTableMap::COL_UPDATED_AT => 8, PenggunaMasterTableMap::COL_DELETED_AT => 9, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'username' => 1, 'password' => 2, 'nip' => 3, 'nama' => 4, 'is_aktif' => 5, 'credential_id' => 6, 'created_at' => 7, 'updated_at' => 8, 'deleted_at' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('eperformance.pengguna_master');
        $this->setPhpName('PenggunaMaster');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\CoreBundle\\Model\\Eperformance\\PenggunaMaster');
        $this->setPackage('src.CoreBundle.Model.Eperformance');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('eperformance.pengguna_master_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('username', 'Username', 'VARCHAR', true, 30, null);
        $this->addColumn('password', 'Password', 'VARCHAR', true, 255, null);
        $this->addColumn('nip', 'Nip', 'VARCHAR', false, 18, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 30, null);
        $this->addColumn('is_aktif', 'IsAktif', 'BOOLEAN', false, null, false);
        $this->addForeignKey('credential_id', 'CredentialId', 'SMALLINT', 'eperformance.credential', 'id', true, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Credential', '\\CoreBundle\\Model\\Eperformance\\Credential', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':credential_id',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'query_cache' => array('backend' => 'apc', 'lifetime' => '7200', ),
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PenggunaMasterTableMap::CLASS_DEFAULT : PenggunaMasterTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (PenggunaMaster object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PenggunaMasterTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PenggunaMasterTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PenggunaMasterTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PenggunaMasterTableMap::OM_CLASS;
            /** @var PenggunaMaster $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PenggunaMasterTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PenggunaMasterTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PenggunaMasterTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var PenggunaMaster $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PenggunaMasterTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PenggunaMasterTableMap::COL_ID);
            $criteria->addSelectColumn(PenggunaMasterTableMap::COL_USERNAME);
            $criteria->addSelectColumn(PenggunaMasterTableMap::COL_PASSWORD);
            $criteria->addSelectColumn(PenggunaMasterTableMap::COL_NIP);
            $criteria->addSelectColumn(PenggunaMasterTableMap::COL_NAMA);
            $criteria->addSelectColumn(PenggunaMasterTableMap::COL_IS_AKTIF);
            $criteria->addSelectColumn(PenggunaMasterTableMap::COL_CREDENTIAL_ID);
            $criteria->addSelectColumn(PenggunaMasterTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(PenggunaMasterTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(PenggunaMasterTableMap::COL_DELETED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.username');
            $criteria->addSelectColumn($alias . '.password');
            $criteria->addSelectColumn($alias . '.nip');
            $criteria->addSelectColumn($alias . '.nama');
            $criteria->addSelectColumn($alias . '.is_aktif');
            $criteria->addSelectColumn($alias . '.credential_id');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.deleted_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PenggunaMasterTableMap::DATABASE_NAME)->getTable(PenggunaMasterTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PenggunaMasterTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PenggunaMasterTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PenggunaMasterTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a PenggunaMaster or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or PenggunaMaster object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PenggunaMasterTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \CoreBundle\Model\Eperformance\PenggunaMaster) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PenggunaMasterTableMap::DATABASE_NAME);
            $criteria->add(PenggunaMasterTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = PenggunaMasterQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PenggunaMasterTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PenggunaMasterTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the eperformance.pengguna_master table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PenggunaMasterQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a PenggunaMaster or Criteria object.
     *
     * @param mixed               $criteria Criteria or PenggunaMaster object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PenggunaMasterTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from PenggunaMaster object
        }

        if ($criteria->containsKey(PenggunaMasterTableMap::COL_ID) && $criteria->keyContainsValue(PenggunaMasterTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PenggunaMasterTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = PenggunaMasterQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PenggunaMasterTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PenggunaMasterTableMap::buildTableMap();

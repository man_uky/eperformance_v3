<?php

namespace CoreBundle\Model\Eperformance\Map;

use CoreBundle\Model\Eperformance\Jabatan;
use CoreBundle\Model\Eperformance\JabatanQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'eperformance.jabatan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class JabatanTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.CoreBundle.Model.Eperformance.Map.JabatanTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'eperformance.jabatan';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\CoreBundle\\Model\\Eperformance\\Jabatan';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.CoreBundle.Model.Eperformance.Jabatan';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 13;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 13;

    /**
     * the column name for the id field
     */
    const COL_ID = 'eperformance.jabatan.id';

    /**
     * the column name for the nama field
     */
    const COL_NAMA = 'eperformance.jabatan.nama';

    /**
     * the column name for the unit_master_id field
     */
    const COL_UNIT_MASTER_ID = 'eperformance.jabatan.unit_master_id';

    /**
     * the column name for the urut field
     */
    const COL_URUT = 'eperformance.jabatan.urut';

    /**
     * the column name for the is_staf field
     */
    const COL_IS_STAF = 'eperformance.jabatan.is_staf';

    /**
     * the column name for the is_jfu field
     */
    const COL_IS_JFU = 'eperformance.jabatan.is_jfu';

    /**
     * the column name for the is_jft field
     */
    const COL_IS_JFT = 'eperformance.jabatan.is_jft';

    /**
     * the column name for the is_staf_ahli field
     */
    const COL_IS_STAF_AHLI = 'eperformance.jabatan.is_staf_ahli';

    /**
     * the column name for the is_asisten field
     */
    const COL_IS_ASISTEN = 'eperformance.jabatan.is_asisten';

    /**
     * the column name for the is_sekda field
     */
    const COL_IS_SEKDA = 'eperformance.jabatan.is_sekda';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'eperformance.jabatan.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'eperformance.jabatan.updated_at';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'eperformance.jabatan.deleted_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Nama', 'UnitMasterId', 'Urut', 'IsStaf', 'IsJfu', 'IsJft', 'IsStafAhli', 'IsAsisten', 'IsSekda', 'CreatedAt', 'UpdatedAt', 'DeletedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'nama', 'unitMasterId', 'urut', 'isStaf', 'isJfu', 'isJft', 'isStafAhli', 'isAsisten', 'isSekda', 'createdAt', 'updatedAt', 'deletedAt', ),
        self::TYPE_COLNAME       => array(JabatanTableMap::COL_ID, JabatanTableMap::COL_NAMA, JabatanTableMap::COL_UNIT_MASTER_ID, JabatanTableMap::COL_URUT, JabatanTableMap::COL_IS_STAF, JabatanTableMap::COL_IS_JFU, JabatanTableMap::COL_IS_JFT, JabatanTableMap::COL_IS_STAF_AHLI, JabatanTableMap::COL_IS_ASISTEN, JabatanTableMap::COL_IS_SEKDA, JabatanTableMap::COL_CREATED_AT, JabatanTableMap::COL_UPDATED_AT, JabatanTableMap::COL_DELETED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'nama', 'unit_master_id', 'urut', 'is_staf', 'is_jfu', 'is_jft', 'is_staf_ahli', 'is_asisten', 'is_sekda', 'created_at', 'updated_at', 'deleted_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Nama' => 1, 'UnitMasterId' => 2, 'Urut' => 3, 'IsStaf' => 4, 'IsJfu' => 5, 'IsJft' => 6, 'IsStafAhli' => 7, 'IsAsisten' => 8, 'IsSekda' => 9, 'CreatedAt' => 10, 'UpdatedAt' => 11, 'DeletedAt' => 12, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'nama' => 1, 'unitMasterId' => 2, 'urut' => 3, 'isStaf' => 4, 'isJfu' => 5, 'isJft' => 6, 'isStafAhli' => 7, 'isAsisten' => 8, 'isSekda' => 9, 'createdAt' => 10, 'updatedAt' => 11, 'deletedAt' => 12, ),
        self::TYPE_COLNAME       => array(JabatanTableMap::COL_ID => 0, JabatanTableMap::COL_NAMA => 1, JabatanTableMap::COL_UNIT_MASTER_ID => 2, JabatanTableMap::COL_URUT => 3, JabatanTableMap::COL_IS_STAF => 4, JabatanTableMap::COL_IS_JFU => 5, JabatanTableMap::COL_IS_JFT => 6, JabatanTableMap::COL_IS_STAF_AHLI => 7, JabatanTableMap::COL_IS_ASISTEN => 8, JabatanTableMap::COL_IS_SEKDA => 9, JabatanTableMap::COL_CREATED_AT => 10, JabatanTableMap::COL_UPDATED_AT => 11, JabatanTableMap::COL_DELETED_AT => 12, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'nama' => 1, 'unit_master_id' => 2, 'urut' => 3, 'is_staf' => 4, 'is_jfu' => 5, 'is_jft' => 6, 'is_staf_ahli' => 7, 'is_asisten' => 8, 'is_sekda' => 9, 'created_at' => 10, 'updated_at' => 11, 'deleted_at' => 12, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('eperformance.jabatan');
        $this->setPhpName('Jabatan');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\CoreBundle\\Model\\Eperformance\\Jabatan');
        $this->setPackage('src.CoreBundle.Model.Eperformance');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('eperformance.jabatan_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 80, null);
        $this->addForeignKey('unit_master_id', 'UnitMasterId', 'SMALLINT', 'eperformance.unit_master', 'id', true, null, null);
        $this->addColumn('urut', 'Urut', 'SMALLINT', true, null, null);
        $this->addColumn('is_staf', 'IsStaf', 'BOOLEAN', false, null, false);
        $this->addColumn('is_jfu', 'IsJfu', 'BOOLEAN', false, null, false);
        $this->addColumn('is_jft', 'IsJft', 'BOOLEAN', false, null, false);
        $this->addColumn('is_staf_ahli', 'IsStafAhli', 'BOOLEAN', false, null, false);
        $this->addColumn('is_asisten', 'IsAsisten', 'BOOLEAN', false, null, false);
        $this->addColumn('is_sekda', 'IsSekda', 'BOOLEAN', false, null, false);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('UnitMaster', '\\CoreBundle\\Model\\Eperformance\\UnitMaster', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':unit_master_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Pegawai', '\\CoreBundle\\Model\\Eperformance\\Pegawai', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':jabatan_id',
    1 => ':id',
  ),
), null, null, 'Pegawais', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'query_cache' => array('backend' => 'apc', 'lifetime' => '7200', ),
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? JabatanTableMap::CLASS_DEFAULT : JabatanTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Jabatan object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = JabatanTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = JabatanTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + JabatanTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = JabatanTableMap::OM_CLASS;
            /** @var Jabatan $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            JabatanTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = JabatanTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = JabatanTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Jabatan $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                JabatanTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(JabatanTableMap::COL_ID);
            $criteria->addSelectColumn(JabatanTableMap::COL_NAMA);
            $criteria->addSelectColumn(JabatanTableMap::COL_UNIT_MASTER_ID);
            $criteria->addSelectColumn(JabatanTableMap::COL_URUT);
            $criteria->addSelectColumn(JabatanTableMap::COL_IS_STAF);
            $criteria->addSelectColumn(JabatanTableMap::COL_IS_JFU);
            $criteria->addSelectColumn(JabatanTableMap::COL_IS_JFT);
            $criteria->addSelectColumn(JabatanTableMap::COL_IS_STAF_AHLI);
            $criteria->addSelectColumn(JabatanTableMap::COL_IS_ASISTEN);
            $criteria->addSelectColumn(JabatanTableMap::COL_IS_SEKDA);
            $criteria->addSelectColumn(JabatanTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(JabatanTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(JabatanTableMap::COL_DELETED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.nama');
            $criteria->addSelectColumn($alias . '.unit_master_id');
            $criteria->addSelectColumn($alias . '.urut');
            $criteria->addSelectColumn($alias . '.is_staf');
            $criteria->addSelectColumn($alias . '.is_jfu');
            $criteria->addSelectColumn($alias . '.is_jft');
            $criteria->addSelectColumn($alias . '.is_staf_ahli');
            $criteria->addSelectColumn($alias . '.is_asisten');
            $criteria->addSelectColumn($alias . '.is_sekda');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.deleted_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(JabatanTableMap::DATABASE_NAME)->getTable(JabatanTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(JabatanTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(JabatanTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new JabatanTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Jabatan or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Jabatan object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(JabatanTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \CoreBundle\Model\Eperformance\Jabatan) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(JabatanTableMap::DATABASE_NAME);
            $criteria->add(JabatanTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = JabatanQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            JabatanTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                JabatanTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the eperformance.jabatan table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return JabatanQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Jabatan or Criteria object.
     *
     * @param mixed               $criteria Criteria or Jabatan object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(JabatanTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Jabatan object
        }

        if ($criteria->containsKey(JabatanTableMap::COL_ID) && $criteria->keyContainsValue(JabatanTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.JabatanTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = JabatanQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // JabatanTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
JabatanTableMap::buildTableMap();

<?php

namespace CoreBundle\Model\Eperformance\Map;

use CoreBundle\Model\Eperformance\UserLog;
use CoreBundle\Model\Eperformance\UserLogQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'eperformance.user_log' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class UserLogTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.CoreBundle.Model.Eperformance.Map.UserLogTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'eperformance.user_log';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\CoreBundle\\Model\\Eperformance\\UserLog';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.CoreBundle.Model.Eperformance.UserLog';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the id field
     */
    const COL_ID = 'eperformance.user_log.id';

    /**
     * the column name for the parent_id field
     */
    const COL_PARENT_ID = 'eperformance.user_log.parent_id';

    /**
     * the column name for the username field
     */
    const COL_USERNAME = 'eperformance.user_log.username';

    /**
     * the column name for the aksi field
     */
    const COL_AKSI = 'eperformance.user_log.aksi';

    /**
     * the column name for the parameter field
     */
    const COL_PARAMETER = 'eperformance.user_log.parameter';

    /**
     * the column name for the ip_address field
     */
    const COL_IP_ADDRESS = 'eperformance.user_log.ip_address';

    /**
     * the column name for the credential_id field
     */
    const COL_CREDENTIAL_ID = 'eperformance.user_log.credential_id';

    /**
     * the column name for the mac_address field
     */
    const COL_MAC_ADDRESS = 'eperformance.user_log.mac_address';

    /**
     * the column name for the keterangan field
     */
    const COL_KETERANGAN = 'eperformance.user_log.keterangan';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'eperformance.user_log.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'eperformance.user_log.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ParentId', 'Username', 'Aksi', 'Parameter', 'IpAddress', 'CredentialId', 'MacAddress', 'Keterangan', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'parentId', 'username', 'aksi', 'parameter', 'ipAddress', 'credentialId', 'macAddress', 'keterangan', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(UserLogTableMap::COL_ID, UserLogTableMap::COL_PARENT_ID, UserLogTableMap::COL_USERNAME, UserLogTableMap::COL_AKSI, UserLogTableMap::COL_PARAMETER, UserLogTableMap::COL_IP_ADDRESS, UserLogTableMap::COL_CREDENTIAL_ID, UserLogTableMap::COL_MAC_ADDRESS, UserLogTableMap::COL_KETERANGAN, UserLogTableMap::COL_CREATED_AT, UserLogTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'parent_id', 'username', 'aksi', 'parameter', 'ip_address', 'credential_id', 'mac_address', 'keterangan', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ParentId' => 1, 'Username' => 2, 'Aksi' => 3, 'Parameter' => 4, 'IpAddress' => 5, 'CredentialId' => 6, 'MacAddress' => 7, 'Keterangan' => 8, 'CreatedAt' => 9, 'UpdatedAt' => 10, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'parentId' => 1, 'username' => 2, 'aksi' => 3, 'parameter' => 4, 'ipAddress' => 5, 'credentialId' => 6, 'macAddress' => 7, 'keterangan' => 8, 'createdAt' => 9, 'updatedAt' => 10, ),
        self::TYPE_COLNAME       => array(UserLogTableMap::COL_ID => 0, UserLogTableMap::COL_PARENT_ID => 1, UserLogTableMap::COL_USERNAME => 2, UserLogTableMap::COL_AKSI => 3, UserLogTableMap::COL_PARAMETER => 4, UserLogTableMap::COL_IP_ADDRESS => 5, UserLogTableMap::COL_CREDENTIAL_ID => 6, UserLogTableMap::COL_MAC_ADDRESS => 7, UserLogTableMap::COL_KETERANGAN => 8, UserLogTableMap::COL_CREATED_AT => 9, UserLogTableMap::COL_UPDATED_AT => 10, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'parent_id' => 1, 'username' => 2, 'aksi' => 3, 'parameter' => 4, 'ip_address' => 5, 'credential_id' => 6, 'mac_address' => 7, 'keterangan' => 8, 'created_at' => 9, 'updated_at' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('eperformance.user_log');
        $this->setPhpName('UserLog');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\CoreBundle\\Model\\Eperformance\\UserLog');
        $this->setPackage('src.CoreBundle.Model.Eperformance');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('eperformance.user_log_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'BIGINT', true, null, null);
        $this->addColumn('parent_id', 'ParentId', 'BIGINT', true, null, null);
        $this->addColumn('username', 'Username', 'VARCHAR', true, 30, null);
        $this->addColumn('aksi', 'Aksi', 'VARCHAR', false, 50, null);
        $this->addColumn('parameter', 'Parameter', 'VARCHAR', false, 50, null);
        $this->addColumn('ip_address', 'IpAddress', 'VARCHAR', true, 50, null);
        $this->addForeignKey('credential_id', 'CredentialId', 'SMALLINT', 'eperformance.credential', 'id', true, null, null);
        $this->addColumn('mac_address', 'MacAddress', 'VARCHAR', true, 255, null);
        $this->addColumn('keterangan', 'Keterangan', 'VARCHAR', false, 255, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Credential', '\\CoreBundle\\Model\\Eperformance\\Credential', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':credential_id',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'query_cache' => array('backend' => 'apc', 'lifetime' => '7200', ),
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? UserLogTableMap::CLASS_DEFAULT : UserLogTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (UserLog object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = UserLogTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UserLogTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UserLogTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UserLogTableMap::OM_CLASS;
            /** @var UserLog $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UserLogTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UserLogTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UserLogTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var UserLog $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UserLogTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UserLogTableMap::COL_ID);
            $criteria->addSelectColumn(UserLogTableMap::COL_PARENT_ID);
            $criteria->addSelectColumn(UserLogTableMap::COL_USERNAME);
            $criteria->addSelectColumn(UserLogTableMap::COL_AKSI);
            $criteria->addSelectColumn(UserLogTableMap::COL_PARAMETER);
            $criteria->addSelectColumn(UserLogTableMap::COL_IP_ADDRESS);
            $criteria->addSelectColumn(UserLogTableMap::COL_CREDENTIAL_ID);
            $criteria->addSelectColumn(UserLogTableMap::COL_MAC_ADDRESS);
            $criteria->addSelectColumn(UserLogTableMap::COL_KETERANGAN);
            $criteria->addSelectColumn(UserLogTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(UserLogTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.parent_id');
            $criteria->addSelectColumn($alias . '.username');
            $criteria->addSelectColumn($alias . '.aksi');
            $criteria->addSelectColumn($alias . '.parameter');
            $criteria->addSelectColumn($alias . '.ip_address');
            $criteria->addSelectColumn($alias . '.credential_id');
            $criteria->addSelectColumn($alias . '.mac_address');
            $criteria->addSelectColumn($alias . '.keterangan');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(UserLogTableMap::DATABASE_NAME)->getTable(UserLogTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UserLogTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(UserLogTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new UserLogTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a UserLog or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or UserLog object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserLogTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \CoreBundle\Model\Eperformance\UserLog) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UserLogTableMap::DATABASE_NAME);
            $criteria->add(UserLogTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = UserLogQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            UserLogTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                UserLogTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the eperformance.user_log table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return UserLogQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a UserLog or Criteria object.
     *
     * @param mixed               $criteria Criteria or UserLog object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserLogTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from UserLog object
        }

        if ($criteria->containsKey(UserLogTableMap::COL_ID) && $criteria->keyContainsValue(UserLogTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.UserLogTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = UserLogQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // UserLogTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
UserLogTableMap::buildTableMap();

<?php

namespace CoreBundle\Model\Eperformance\Map;

use CoreBundle\Model\Eperformance\UnitMaster;
use CoreBundle\Model\Eperformance\UnitMasterQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'eperformance.unit_master' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class UnitMasterTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.CoreBundle.Model.Eperformance.Map.UnitMasterTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'eperformance.unit_master';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\CoreBundle\\Model\\Eperformance\\UnitMaster';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.CoreBundle.Model.Eperformance.UnitMaster';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the id field
     */
    const COL_ID = 'eperformance.unit_master.id';

    /**
     * the column name for the kode field
     */
    const COL_KODE = 'eperformance.unit_master.kode';

    /**
     * the column name for the nama field
     */
    const COL_NAMA = 'eperformance.unit_master.nama';

    /**
     * the column name for the is_sekretariat_daerah field
     */
    const COL_IS_SEKRETARIAT_DAERAH = 'eperformance.unit_master.is_sekretariat_daerah';

    /**
     * the column name for the is_dinas field
     */
    const COL_IS_DINAS = 'eperformance.unit_master.is_dinas';

    /**
     * the column name for the is_badan field
     */
    const COL_IS_BADAN = 'eperformance.unit_master.is_badan';

    /**
     * the column name for the is_rumah_sakit field
     */
    const COL_IS_RUMAH_SAKIT = 'eperformance.unit_master.is_rumah_sakit';

    /**
     * the column name for the is_bagian field
     */
    const COL_IS_BAGIAN = 'eperformance.unit_master.is_bagian';

    /**
     * the column name for the is_kecamatan field
     */
    const COL_IS_KECAMATAN = 'eperformance.unit_master.is_kecamatan';

    /**
     * the column name for the is_kelurahan field
     */
    const COL_IS_KELURAHAN = 'eperformance.unit_master.is_kelurahan';

    /**
     * the column name for the parent_id field
     */
    const COL_PARENT_ID = 'eperformance.unit_master.parent_id';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'eperformance.unit_master.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'eperformance.unit_master.updated_at';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'eperformance.unit_master.deleted_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Kode', 'Nama', 'IsSekretariatDaerah', 'IsDinas', 'IsBadan', 'IsRumahSakit', 'IsBagian', 'IsKecamatan', 'IsKelurahan', 'ParentId', 'CreatedAt', 'UpdatedAt', 'DeletedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'kode', 'nama', 'isSekretariatDaerah', 'isDinas', 'isBadan', 'isRumahSakit', 'isBagian', 'isKecamatan', 'isKelurahan', 'parentId', 'createdAt', 'updatedAt', 'deletedAt', ),
        self::TYPE_COLNAME       => array(UnitMasterTableMap::COL_ID, UnitMasterTableMap::COL_KODE, UnitMasterTableMap::COL_NAMA, UnitMasterTableMap::COL_IS_SEKRETARIAT_DAERAH, UnitMasterTableMap::COL_IS_DINAS, UnitMasterTableMap::COL_IS_BADAN, UnitMasterTableMap::COL_IS_RUMAH_SAKIT, UnitMasterTableMap::COL_IS_BAGIAN, UnitMasterTableMap::COL_IS_KECAMATAN, UnitMasterTableMap::COL_IS_KELURAHAN, UnitMasterTableMap::COL_PARENT_ID, UnitMasterTableMap::COL_CREATED_AT, UnitMasterTableMap::COL_UPDATED_AT, UnitMasterTableMap::COL_DELETED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'kode', 'nama', 'is_sekretariat_daerah', 'is_dinas', 'is_badan', 'is_rumah_sakit', 'is_bagian', 'is_kecamatan', 'is_kelurahan', 'parent_id', 'created_at', 'updated_at', 'deleted_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Kode' => 1, 'Nama' => 2, 'IsSekretariatDaerah' => 3, 'IsDinas' => 4, 'IsBadan' => 5, 'IsRumahSakit' => 6, 'IsBagian' => 7, 'IsKecamatan' => 8, 'IsKelurahan' => 9, 'ParentId' => 10, 'CreatedAt' => 11, 'UpdatedAt' => 12, 'DeletedAt' => 13, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'kode' => 1, 'nama' => 2, 'isSekretariatDaerah' => 3, 'isDinas' => 4, 'isBadan' => 5, 'isRumahSakit' => 6, 'isBagian' => 7, 'isKecamatan' => 8, 'isKelurahan' => 9, 'parentId' => 10, 'createdAt' => 11, 'updatedAt' => 12, 'deletedAt' => 13, ),
        self::TYPE_COLNAME       => array(UnitMasterTableMap::COL_ID => 0, UnitMasterTableMap::COL_KODE => 1, UnitMasterTableMap::COL_NAMA => 2, UnitMasterTableMap::COL_IS_SEKRETARIAT_DAERAH => 3, UnitMasterTableMap::COL_IS_DINAS => 4, UnitMasterTableMap::COL_IS_BADAN => 5, UnitMasterTableMap::COL_IS_RUMAH_SAKIT => 6, UnitMasterTableMap::COL_IS_BAGIAN => 7, UnitMasterTableMap::COL_IS_KECAMATAN => 8, UnitMasterTableMap::COL_IS_KELURAHAN => 9, UnitMasterTableMap::COL_PARENT_ID => 10, UnitMasterTableMap::COL_CREATED_AT => 11, UnitMasterTableMap::COL_UPDATED_AT => 12, UnitMasterTableMap::COL_DELETED_AT => 13, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'kode' => 1, 'nama' => 2, 'is_sekretariat_daerah' => 3, 'is_dinas' => 4, 'is_badan' => 5, 'is_rumah_sakit' => 6, 'is_bagian' => 7, 'is_kecamatan' => 8, 'is_kelurahan' => 9, 'parent_id' => 10, 'created_at' => 11, 'updated_at' => 12, 'deleted_at' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('eperformance.unit_master');
        $this->setPhpName('UnitMaster');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\CoreBundle\\Model\\Eperformance\\UnitMaster');
        $this->setPackage('src.CoreBundle.Model.Eperformance');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('eperformance.unit_master_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'SMALLINT', true, null, null);
        $this->addColumn('kode', 'Kode', 'VARCHAR', true, 4, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 100, null);
        $this->addColumn('is_sekretariat_daerah', 'IsSekretariatDaerah', 'BOOLEAN', true, null, false);
        $this->addColumn('is_dinas', 'IsDinas', 'BOOLEAN', true, null, false);
        $this->addColumn('is_badan', 'IsBadan', 'BOOLEAN', true, null, false);
        $this->addColumn('is_rumah_sakit', 'IsRumahSakit', 'BOOLEAN', true, null, false);
        $this->addColumn('is_bagian', 'IsBagian', 'BOOLEAN', true, null, false);
        $this->addColumn('is_kecamatan', 'IsKecamatan', 'BOOLEAN', true, null, false);
        $this->addColumn('is_kelurahan', 'IsKelurahan', 'BOOLEAN', true, null, false);
        $this->addForeignKey('parent_id', 'ParentId', 'SMALLINT', 'eperformance.unit_master', 'id', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('UnitParent', '\\CoreBundle\\Model\\Eperformance\\UnitMaster', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':parent_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Jabatan', '\\CoreBundle\\Model\\Eperformance\\Jabatan', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':unit_master_id',
    1 => ':id',
  ),
), null, null, 'Jabatans', false);
        $this->addRelation('Pegawai', '\\CoreBundle\\Model\\Eperformance\\Pegawai', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':unit_master_id',
    1 => ':id',
  ),
), null, null, 'Pegawais', false);
        $this->addRelation('PegawaiMaster', '\\CoreBundle\\Model\\Eperformance\\PegawaiMaster', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':unit_master_id',
    1 => ':id',
  ),
), null, null, 'PegawaiMasters', false);
        $this->addRelation('UnitMasterRelatedById', '\\CoreBundle\\Model\\Eperformance\\UnitMaster', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':parent_id',
    1 => ':id',
  ),
), null, null, 'UnitMastersRelatedById', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'query_cache' => array('backend' => 'apc', 'lifetime' => '7200', ),
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? UnitMasterTableMap::CLASS_DEFAULT : UnitMasterTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (UnitMaster object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = UnitMasterTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UnitMasterTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UnitMasterTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UnitMasterTableMap::OM_CLASS;
            /** @var UnitMaster $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UnitMasterTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UnitMasterTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UnitMasterTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var UnitMaster $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UnitMasterTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UnitMasterTableMap::COL_ID);
            $criteria->addSelectColumn(UnitMasterTableMap::COL_KODE);
            $criteria->addSelectColumn(UnitMasterTableMap::COL_NAMA);
            $criteria->addSelectColumn(UnitMasterTableMap::COL_IS_SEKRETARIAT_DAERAH);
            $criteria->addSelectColumn(UnitMasterTableMap::COL_IS_DINAS);
            $criteria->addSelectColumn(UnitMasterTableMap::COL_IS_BADAN);
            $criteria->addSelectColumn(UnitMasterTableMap::COL_IS_RUMAH_SAKIT);
            $criteria->addSelectColumn(UnitMasterTableMap::COL_IS_BAGIAN);
            $criteria->addSelectColumn(UnitMasterTableMap::COL_IS_KECAMATAN);
            $criteria->addSelectColumn(UnitMasterTableMap::COL_IS_KELURAHAN);
            $criteria->addSelectColumn(UnitMasterTableMap::COL_PARENT_ID);
            $criteria->addSelectColumn(UnitMasterTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(UnitMasterTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(UnitMasterTableMap::COL_DELETED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.kode');
            $criteria->addSelectColumn($alias . '.nama');
            $criteria->addSelectColumn($alias . '.is_sekretariat_daerah');
            $criteria->addSelectColumn($alias . '.is_dinas');
            $criteria->addSelectColumn($alias . '.is_badan');
            $criteria->addSelectColumn($alias . '.is_rumah_sakit');
            $criteria->addSelectColumn($alias . '.is_bagian');
            $criteria->addSelectColumn($alias . '.is_kecamatan');
            $criteria->addSelectColumn($alias . '.is_kelurahan');
            $criteria->addSelectColumn($alias . '.parent_id');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.deleted_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(UnitMasterTableMap::DATABASE_NAME)->getTable(UnitMasterTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UnitMasterTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(UnitMasterTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new UnitMasterTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a UnitMaster or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or UnitMaster object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UnitMasterTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \CoreBundle\Model\Eperformance\UnitMaster) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UnitMasterTableMap::DATABASE_NAME);
            $criteria->add(UnitMasterTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = UnitMasterQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            UnitMasterTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                UnitMasterTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the eperformance.unit_master table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return UnitMasterQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a UnitMaster or Criteria object.
     *
     * @param mixed               $criteria Criteria or UnitMaster object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UnitMasterTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from UnitMaster object
        }

        if ($criteria->containsKey(UnitMasterTableMap::COL_ID) && $criteria->keyContainsValue(UnitMasterTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.UnitMasterTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = UnitMasterQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // UnitMasterTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
UnitMasterTableMap::buildTableMap();

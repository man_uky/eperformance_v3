<?php

namespace CoreBundle\Model\Eperformance\Map;

use CoreBundle\Model\Eperformance\Pegawai;
use CoreBundle\Model\Eperformance\PegawaiQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'eperformance.pegawai' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PegawaiTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.CoreBundle.Model.Eperformance.Map.PegawaiTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'eperformance.pegawai';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\CoreBundle\\Model\\Eperformance\\Pegawai';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.CoreBundle.Model.Eperformance.Pegawai';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 21;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 21;

    /**
     * the column name for the id field
     */
    const COL_ID = 'eperformance.pegawai.id';

    /**
     * the column name for the username field
     */
    const COL_USERNAME = 'eperformance.pegawai.username';

    /**
     * the column name for the password field
     */
    const COL_PASSWORD = 'eperformance.pegawai.password';

    /**
     * the column name for the kode_rapor field
     */
    const COL_KODE_RAPOR = 'eperformance.pegawai.kode_rapor';

    /**
     * the column name for the credential_id field
     */
    const COL_CREDENTIAL_ID = 'eperformance.pegawai.credential_id';

    /**
     * the column name for the pegawai_master_id field
     */
    const COL_PEGAWAI_MASTER_ID = 'eperformance.pegawai.pegawai_master_id';

    /**
     * the column name for the unit_master_id field
     */
    const COL_UNIT_MASTER_ID = 'eperformance.pegawai.unit_master_id';

    /**
     * the column name for the atasan_id field
     */
    const COL_ATASAN_ID = 'eperformance.pegawai.atasan_id';

    /**
     * the column name for the pangkat_id field
     */
    const COL_PANGKAT_ID = 'eperformance.pegawai.pangkat_id';

    /**
     * the column name for the jabatan_id field
     */
    const COL_JABATAN_ID = 'eperformance.pegawai.jabatan_id';

    /**
     * the column name for the pegawai_status_id field
     */
    const COL_PEGAWAI_STATUS_ID = 'eperformance.pegawai.pegawai_status_id';

    /**
     * the column name for the eselon field
     */
    const COL_ESELON = 'eperformance.pegawai.eselon';

    /**
     * the column name for the level field
     */
    const COL_LEVEL = 'eperformance.pegawai.level';

    /**
     * the column name for the is_aktif field
     */
    const COL_IS_AKTIF = 'eperformance.pegawai.is_aktif';

    /**
     * the column name for the is_trantib field
     */
    const COL_IS_TRANTIB = 'eperformance.pegawai.is_trantib';

    /**
     * the column name for the is_lapangan field
     */
    const COL_IS_LAPANGAN = 'eperformance.pegawai.is_lapangan';

    /**
     * the column name for the tanggal_aktif field
     */
    const COL_TANGGAL_AKTIF = 'eperformance.pegawai.tanggal_aktif';

    /**
     * the column name for the tanggal_nonaktif field
     */
    const COL_TANGGAL_NONAKTIF = 'eperformance.pegawai.tanggal_nonaktif';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'eperformance.pegawai.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'eperformance.pegawai.updated_at';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'eperformance.pegawai.deleted_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Username', 'Password', 'KodeRapor', 'CredentialId', 'PegawaiMasterId', 'UnitMasterId', 'AtasanId', 'PangkatId', 'JabatanId', 'PegawaiStatusId', 'Eselon', 'Level', 'IsAktif', 'IsTrantib', 'IsLapangan', 'TanggalAktif', 'TanggalNonaktif', 'CreatedAt', 'UpdatedAt', 'DeletedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'username', 'password', 'kodeRapor', 'credentialId', 'pegawaiMasterId', 'unitMasterId', 'atasanId', 'pangkatId', 'jabatanId', 'pegawaiStatusId', 'eselon', 'level', 'isAktif', 'isTrantib', 'isLapangan', 'tanggalAktif', 'tanggalNonaktif', 'createdAt', 'updatedAt', 'deletedAt', ),
        self::TYPE_COLNAME       => array(PegawaiTableMap::COL_ID, PegawaiTableMap::COL_USERNAME, PegawaiTableMap::COL_PASSWORD, PegawaiTableMap::COL_KODE_RAPOR, PegawaiTableMap::COL_CREDENTIAL_ID, PegawaiTableMap::COL_PEGAWAI_MASTER_ID, PegawaiTableMap::COL_UNIT_MASTER_ID, PegawaiTableMap::COL_ATASAN_ID, PegawaiTableMap::COL_PANGKAT_ID, PegawaiTableMap::COL_JABATAN_ID, PegawaiTableMap::COL_PEGAWAI_STATUS_ID, PegawaiTableMap::COL_ESELON, PegawaiTableMap::COL_LEVEL, PegawaiTableMap::COL_IS_AKTIF, PegawaiTableMap::COL_IS_TRANTIB, PegawaiTableMap::COL_IS_LAPANGAN, PegawaiTableMap::COL_TANGGAL_AKTIF, PegawaiTableMap::COL_TANGGAL_NONAKTIF, PegawaiTableMap::COL_CREATED_AT, PegawaiTableMap::COL_UPDATED_AT, PegawaiTableMap::COL_DELETED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'username', 'password', 'kode_rapor', 'credential_id', 'pegawai_master_id', 'unit_master_id', 'atasan_id', 'pangkat_id', 'jabatan_id', 'pegawai_status_id', 'eselon', 'level', 'is_aktif', 'is_trantib', 'is_lapangan', 'tanggal_aktif', 'tanggal_nonaktif', 'created_at', 'updated_at', 'deleted_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Username' => 1, 'Password' => 2, 'KodeRapor' => 3, 'CredentialId' => 4, 'PegawaiMasterId' => 5, 'UnitMasterId' => 6, 'AtasanId' => 7, 'PangkatId' => 8, 'JabatanId' => 9, 'PegawaiStatusId' => 10, 'Eselon' => 11, 'Level' => 12, 'IsAktif' => 13, 'IsTrantib' => 14, 'IsLapangan' => 15, 'TanggalAktif' => 16, 'TanggalNonaktif' => 17, 'CreatedAt' => 18, 'UpdatedAt' => 19, 'DeletedAt' => 20, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'username' => 1, 'password' => 2, 'kodeRapor' => 3, 'credentialId' => 4, 'pegawaiMasterId' => 5, 'unitMasterId' => 6, 'atasanId' => 7, 'pangkatId' => 8, 'jabatanId' => 9, 'pegawaiStatusId' => 10, 'eselon' => 11, 'level' => 12, 'isAktif' => 13, 'isTrantib' => 14, 'isLapangan' => 15, 'tanggalAktif' => 16, 'tanggalNonaktif' => 17, 'createdAt' => 18, 'updatedAt' => 19, 'deletedAt' => 20, ),
        self::TYPE_COLNAME       => array(PegawaiTableMap::COL_ID => 0, PegawaiTableMap::COL_USERNAME => 1, PegawaiTableMap::COL_PASSWORD => 2, PegawaiTableMap::COL_KODE_RAPOR => 3, PegawaiTableMap::COL_CREDENTIAL_ID => 4, PegawaiTableMap::COL_PEGAWAI_MASTER_ID => 5, PegawaiTableMap::COL_UNIT_MASTER_ID => 6, PegawaiTableMap::COL_ATASAN_ID => 7, PegawaiTableMap::COL_PANGKAT_ID => 8, PegawaiTableMap::COL_JABATAN_ID => 9, PegawaiTableMap::COL_PEGAWAI_STATUS_ID => 10, PegawaiTableMap::COL_ESELON => 11, PegawaiTableMap::COL_LEVEL => 12, PegawaiTableMap::COL_IS_AKTIF => 13, PegawaiTableMap::COL_IS_TRANTIB => 14, PegawaiTableMap::COL_IS_LAPANGAN => 15, PegawaiTableMap::COL_TANGGAL_AKTIF => 16, PegawaiTableMap::COL_TANGGAL_NONAKTIF => 17, PegawaiTableMap::COL_CREATED_AT => 18, PegawaiTableMap::COL_UPDATED_AT => 19, PegawaiTableMap::COL_DELETED_AT => 20, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'username' => 1, 'password' => 2, 'kode_rapor' => 3, 'credential_id' => 4, 'pegawai_master_id' => 5, 'unit_master_id' => 6, 'atasan_id' => 7, 'pangkat_id' => 8, 'jabatan_id' => 9, 'pegawai_status_id' => 10, 'eselon' => 11, 'level' => 12, 'is_aktif' => 13, 'is_trantib' => 14, 'is_lapangan' => 15, 'tanggal_aktif' => 16, 'tanggal_nonaktif' => 17, 'created_at' => 18, 'updated_at' => 19, 'deleted_at' => 20, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('eperformance.pegawai');
        $this->setPhpName('Pegawai');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\CoreBundle\\Model\\Eperformance\\Pegawai');
        $this->setPackage('src.CoreBundle.Model.Eperformance');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('eperformance.pegawai_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('username', 'Username', 'VARCHAR', true, 15, null);
        $this->addColumn('password', 'Password', 'VARCHAR', true, 255, null);
        $this->addColumn('kode_rapor', 'KodeRapor', 'VARCHAR', true, 50, null);
        $this->addForeignKey('credential_id', 'CredentialId', 'SMALLINT', 'eperformance.credential', 'id', true, null, null);
        $this->addForeignKey('pegawai_master_id', 'PegawaiMasterId', 'INTEGER', 'eperformance.pegawai_master', 'id', true, null, null);
        $this->addForeignKey('unit_master_id', 'UnitMasterId', 'SMALLINT', 'eperformance.unit_master', 'id', true, null, null);
        $this->addForeignKey('atasan_id', 'AtasanId', 'INTEGER', 'eperformance.pegawai', 'id', false, null, null);
        $this->addForeignKey('pangkat_id', 'PangkatId', 'SMALLINT', 'eperformance.pangkat', 'id', true, null, null);
        $this->addForeignKey('jabatan_id', 'JabatanId', 'SMALLINT', 'eperformance.jabatan', 'id', true, null, null);
        $this->addForeignKey('pegawai_status_id', 'PegawaiStatusId', 'SMALLINT', 'eperformance.pegawai_status', 'id', true, null, null);
        $this->addColumn('eselon', 'Eselon', 'SMALLINT', false, null, 0);
        $this->addColumn('level', 'Level', 'SMALLINT', true, null, null);
        $this->addColumn('is_aktif', 'IsAktif', 'BOOLEAN', false, null, false);
        $this->addColumn('is_trantib', 'IsTrantib', 'BOOLEAN', false, null, false);
        $this->addColumn('is_lapangan', 'IsLapangan', 'BOOLEAN', false, null, false);
        $this->addColumn('tanggal_aktif', 'TanggalAktif', 'DATE', false, null, null);
        $this->addColumn('tanggal_nonaktif', 'TanggalNonaktif', 'DATE', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Credential', '\\CoreBundle\\Model\\Eperformance\\Credential', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':credential_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('PegawaiMaster', '\\CoreBundle\\Model\\Eperformance\\PegawaiMaster', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':pegawai_master_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('UnitMaster', '\\CoreBundle\\Model\\Eperformance\\UnitMaster', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':unit_master_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('PegawaiAtasan', '\\CoreBundle\\Model\\Eperformance\\Pegawai', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':atasan_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Pangkat', '\\CoreBundle\\Model\\Eperformance\\Pangkat', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':pangkat_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Jabatan', '\\CoreBundle\\Model\\Eperformance\\Jabatan', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':jabatan_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('PegawaiStatus', '\\CoreBundle\\Model\\Eperformance\\PegawaiStatus', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':pegawai_status_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('PegawaiRelatedById', '\\CoreBundle\\Model\\Eperformance\\Pegawai', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':atasan_id',
    1 => ':id',
  ),
), null, null, 'PegawaisRelatedById', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'query_cache' => array('backend' => 'apc', 'lifetime' => '7200', ),
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PegawaiTableMap::CLASS_DEFAULT : PegawaiTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Pegawai object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PegawaiTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PegawaiTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PegawaiTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PegawaiTableMap::OM_CLASS;
            /** @var Pegawai $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PegawaiTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PegawaiTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PegawaiTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Pegawai $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PegawaiTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PegawaiTableMap::COL_ID);
            $criteria->addSelectColumn(PegawaiTableMap::COL_USERNAME);
            $criteria->addSelectColumn(PegawaiTableMap::COL_PASSWORD);
            $criteria->addSelectColumn(PegawaiTableMap::COL_KODE_RAPOR);
            $criteria->addSelectColumn(PegawaiTableMap::COL_CREDENTIAL_ID);
            $criteria->addSelectColumn(PegawaiTableMap::COL_PEGAWAI_MASTER_ID);
            $criteria->addSelectColumn(PegawaiTableMap::COL_UNIT_MASTER_ID);
            $criteria->addSelectColumn(PegawaiTableMap::COL_ATASAN_ID);
            $criteria->addSelectColumn(PegawaiTableMap::COL_PANGKAT_ID);
            $criteria->addSelectColumn(PegawaiTableMap::COL_JABATAN_ID);
            $criteria->addSelectColumn(PegawaiTableMap::COL_PEGAWAI_STATUS_ID);
            $criteria->addSelectColumn(PegawaiTableMap::COL_ESELON);
            $criteria->addSelectColumn(PegawaiTableMap::COL_LEVEL);
            $criteria->addSelectColumn(PegawaiTableMap::COL_IS_AKTIF);
            $criteria->addSelectColumn(PegawaiTableMap::COL_IS_TRANTIB);
            $criteria->addSelectColumn(PegawaiTableMap::COL_IS_LAPANGAN);
            $criteria->addSelectColumn(PegawaiTableMap::COL_TANGGAL_AKTIF);
            $criteria->addSelectColumn(PegawaiTableMap::COL_TANGGAL_NONAKTIF);
            $criteria->addSelectColumn(PegawaiTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(PegawaiTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(PegawaiTableMap::COL_DELETED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.username');
            $criteria->addSelectColumn($alias . '.password');
            $criteria->addSelectColumn($alias . '.kode_rapor');
            $criteria->addSelectColumn($alias . '.credential_id');
            $criteria->addSelectColumn($alias . '.pegawai_master_id');
            $criteria->addSelectColumn($alias . '.unit_master_id');
            $criteria->addSelectColumn($alias . '.atasan_id');
            $criteria->addSelectColumn($alias . '.pangkat_id');
            $criteria->addSelectColumn($alias . '.jabatan_id');
            $criteria->addSelectColumn($alias . '.pegawai_status_id');
            $criteria->addSelectColumn($alias . '.eselon');
            $criteria->addSelectColumn($alias . '.level');
            $criteria->addSelectColumn($alias . '.is_aktif');
            $criteria->addSelectColumn($alias . '.is_trantib');
            $criteria->addSelectColumn($alias . '.is_lapangan');
            $criteria->addSelectColumn($alias . '.tanggal_aktif');
            $criteria->addSelectColumn($alias . '.tanggal_nonaktif');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.deleted_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PegawaiTableMap::DATABASE_NAME)->getTable(PegawaiTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PegawaiTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PegawaiTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PegawaiTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Pegawai or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Pegawai object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PegawaiTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \CoreBundle\Model\Eperformance\Pegawai) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PegawaiTableMap::DATABASE_NAME);
            $criteria->add(PegawaiTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = PegawaiQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PegawaiTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PegawaiTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the eperformance.pegawai table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PegawaiQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Pegawai or Criteria object.
     *
     * @param mixed               $criteria Criteria or Pegawai object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PegawaiTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Pegawai object
        }

        if ($criteria->containsKey(PegawaiTableMap::COL_ID) && $criteria->keyContainsValue(PegawaiTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PegawaiTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = PegawaiQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PegawaiTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PegawaiTableMap::buildTableMap();

<?php

namespace CoreBundle\Model\Eperformance;

use CoreBundle\Model\Eperformance\Base\UserLogQuery as BaseUserLogQuery;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for performing query and update operations on the 'eperformance.user_log' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UserLogQuery extends BaseUserLogQuery
{
    public function getListQuery($loginId = null)
    {   
        return $this
            ->filterByCreatedAt(array('min' => time() - 30 * 24 * 60 * 60))
            ->leftJoinCredential()
            ->_if(null !== $loginId)
                ->filterByParentId($loginId)
                ->_or()
                ->filterById($loginId)
            ->_endif();
    }
}

<?php

namespace CoreBundle\Model\OldEperformance;

use CoreBundle\Model\OldEperformance\Base\OldJabatanStruktural as BaseOldJabatanStruktural;

/**
 * Skeleton subclass for representing a row from the 'eperformance.jabatan_struktural' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class OldJabatanStruktural extends BaseOldJabatanStruktural
{
    public function __toString() 
    {
        return $this->getNama();
    }
}

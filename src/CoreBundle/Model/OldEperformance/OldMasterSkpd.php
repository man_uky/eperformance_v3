<?php

namespace CoreBundle\Model\OldEperformance;

use CoreBundle\Model\OldEperformance\Base\OldMasterSkpd as BaseOldMasterSkpd;

/**
 * Skeleton subclass for representing a row from the 'eperformance.master_skpd' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class OldMasterSkpd extends BaseOldMasterSkpd
{
    public function __toString() 
    {
        return $this->getSkpdNama();
    }
}

<?php

namespace CoreBundle\Model\OldEperformance;

use CoreBundle\Model\OldEperformance\Base\OldMonevTspQuery as BaseOldMonevTspQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'eperformance.monev_tsp' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class OldMonevTspQuery extends BaseOldMonevTspQuery
{
    public function getData($unitId, $pegawaiId)
    {
        return $this
            ->filterByPegawaiId($pegawaiId)
            ->filterByUnitId($unitId)
            ->find();
    }
}

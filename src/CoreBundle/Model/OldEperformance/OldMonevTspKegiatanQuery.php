<?php

namespace CoreBundle\Model\OldEperformance;

use CoreBundle\Model\OldEperformance\Base\OldMonevTspKegiatanQuery as BaseOldMonevTspKegiatanQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'eperformance.monev_tsp_kegiatan' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class OldMonevTspKegiatanQuery extends BaseOldMonevTspKegiatanQuery
{
    public function getData($bulan, $unitId, $pegawaiId)
    {
        $kegiatanIds = OldKegiatanPegawaiQuery::create()
            ->filterByBulan($bulan)
            ->filterByPegawaiId($pegawaiId)
            ->withColumn('OldKegiatanPegawai.KegiatanId', 'KegiatanId')
            ->find()->toKeyValue('KegiatanId', 'KegiatanId');
        
        return $this
            ->filterByUnitId($unitId)
            ->filterByKegiatanId($kegiatanIds, \Propel\Runtime\ActiveQuery\Criteria::IN)
            ->withColumn('OldMonevTspKegiatan.Id', 'Id')
            ->withColumn('OldMonevTspKegiatan.KegiatanNama', 'IndikatorNama')
            ->withColumn('OldMonevTspKegiatan.target', 'IndikatorTarget')
            ->withColumn('OldMonevTspKegiatan.realisasi', 'IndikatorRealisasi')
            ->withColumn('OldMonevTspKegiatan.satuan', 'IndikatorSatuan')
            ->withColumn('OldMonevTspKegiatan.capaian', 'IndikatorCapaian')
            ->find();
    }
}

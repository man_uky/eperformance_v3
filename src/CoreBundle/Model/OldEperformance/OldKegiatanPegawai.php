<?php

namespace CoreBundle\Model\OldEperformance;

use CoreBundle\Model\OldEperformance\Base\OldKegiatanPegawai as BaseOldKegiatanPegawai;

/**
 * Skeleton subclass for representing a row from the 'eperformance.kegiatan_pegawai' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class OldKegiatanPegawai extends BaseOldKegiatanPegawai
{

}

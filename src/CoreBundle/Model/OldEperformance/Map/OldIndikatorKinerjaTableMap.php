<?php

namespace CoreBundle\Model\OldEperformance\Map;

use CoreBundle\Model\OldEperformance\OldIndikatorKinerja;
use CoreBundle\Model\OldEperformance\OldIndikatorKinerjaQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'eperformance.indikator_kinerja' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class OldIndikatorKinerjaTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.CoreBundle.Model.OldEperformance.Map.OldIndikatorKinerjaTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'old_eperformance';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'eperformance.indikator_kinerja';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\CoreBundle\\Model\\OldEperformance\\OldIndikatorKinerja';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.CoreBundle.Model.OldEperformance.OldIndikatorKinerja';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 21;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 21;

    /**
     * the column name for the id field
     */
    const COL_ID = 'eperformance.indikator_kinerja.id';

    /**
     * the column name for the pegawai_id field
     */
    const COL_PEGAWAI_ID = 'eperformance.indikator_kinerja.pegawai_id';

    /**
     * the column name for the nama field
     */
    const COL_NAMA = 'eperformance.indikator_kinerja.nama';

    /**
     * the column name for the deskripsi field
     */
    const COL_DESKRIPSI = 'eperformance.indikator_kinerja.deskripsi';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'eperformance.indikator_kinerja.status';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'eperformance.indikator_kinerja.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'eperformance.indikator_kinerja.updated_at';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'eperformance.indikator_kinerja.deleted_at';

    /**
     * the column name for the skpd_id field
     */
    const COL_SKPD_ID = 'eperformance.indikator_kinerja.skpd_id';

    /**
     * the column name for the target field
     */
    const COL_TARGET = 'eperformance.indikator_kinerja.target';

    /**
     * the column name for the satuan field
     */
    const COL_SATUAN = 'eperformance.indikator_kinerja.satuan';

    /**
     * the column name for the target_kemarin field
     */
    const COL_TARGET_KEMARIN = 'eperformance.indikator_kinerja.target_kemarin';

    /**
     * the column name for the cara_perhitungan field
     */
    const COL_CARA_PERHITUNGAN = 'eperformance.indikator_kinerja.cara_perhitungan';

    /**
     * the column name for the capaian_akhir field
     */
    const COL_CAPAIAN_AKHIR = 'eperformance.indikator_kinerja.capaian_akhir';

    /**
     * the column name for the jenis field
     */
    const COL_JENIS = 'eperformance.indikator_kinerja.jenis';

    /**
     * the column name for the indikator_id_atasan field
     */
    const COL_INDIKATOR_ID_ATASAN = 'eperformance.indikator_kinerja.indikator_id_atasan';

    /**
     * the column name for the is_tambahan field
     */
    const COL_IS_TAMBAHAN = 'eperformance.indikator_kinerja.is_tambahan';

    /**
     * the column name for the is_tahun_lalu field
     */
    const COL_IS_TAHUN_LALU = 'eperformance.indikator_kinerja.is_tahun_lalu';

    /**
     * the column name for the is_tarik_ebudgeting field
     */
    const COL_IS_TARIK_EBUDGETING = 'eperformance.indikator_kinerja.is_tarik_ebudgeting';

    /**
     * the column name for the is_perhitungan_terbalik field
     */
    const COL_IS_PERHITUNGAN_TERBALIK = 'eperformance.indikator_kinerja.is_perhitungan_terbalik';

    /**
     * the column name for the is_realisasi_angka_prediksi field
     */
    const COL_IS_REALISASI_ANGKA_PREDIKSI = 'eperformance.indikator_kinerja.is_realisasi_angka_prediksi';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'PegawaiId', 'Nama', 'Deskripsi', 'Status', 'CreatedAt', 'UpdatedAt', 'DeletedAt', 'SkpdId', 'Target', 'Satuan', 'TargetKemarin', 'CaraPerhitungan', 'CapaianAkhir', 'Jenis', 'IndikatorIdAtasan', 'IsTambahan', 'IsTahunLalu', 'IsTarikEbudgeting', 'IsPerhitunganTerbalik', 'IsRealisasiAngkaPrediksi', ),
        self::TYPE_CAMELNAME     => array('id', 'pegawaiId', 'nama', 'deskripsi', 'status', 'createdAt', 'updatedAt', 'deletedAt', 'skpdId', 'target', 'satuan', 'targetKemarin', 'caraPerhitungan', 'capaianAkhir', 'jenis', 'indikatorIdAtasan', 'isTambahan', 'isTahunLalu', 'isTarikEbudgeting', 'isPerhitunganTerbalik', 'isRealisasiAngkaPrediksi', ),
        self::TYPE_COLNAME       => array(OldIndikatorKinerjaTableMap::COL_ID, OldIndikatorKinerjaTableMap::COL_PEGAWAI_ID, OldIndikatorKinerjaTableMap::COL_NAMA, OldIndikatorKinerjaTableMap::COL_DESKRIPSI, OldIndikatorKinerjaTableMap::COL_STATUS, OldIndikatorKinerjaTableMap::COL_CREATED_AT, OldIndikatorKinerjaTableMap::COL_UPDATED_AT, OldIndikatorKinerjaTableMap::COL_DELETED_AT, OldIndikatorKinerjaTableMap::COL_SKPD_ID, OldIndikatorKinerjaTableMap::COL_TARGET, OldIndikatorKinerjaTableMap::COL_SATUAN, OldIndikatorKinerjaTableMap::COL_TARGET_KEMARIN, OldIndikatorKinerjaTableMap::COL_CARA_PERHITUNGAN, OldIndikatorKinerjaTableMap::COL_CAPAIAN_AKHIR, OldIndikatorKinerjaTableMap::COL_JENIS, OldIndikatorKinerjaTableMap::COL_INDIKATOR_ID_ATASAN, OldIndikatorKinerjaTableMap::COL_IS_TAMBAHAN, OldIndikatorKinerjaTableMap::COL_IS_TAHUN_LALU, OldIndikatorKinerjaTableMap::COL_IS_TARIK_EBUDGETING, OldIndikatorKinerjaTableMap::COL_IS_PERHITUNGAN_TERBALIK, OldIndikatorKinerjaTableMap::COL_IS_REALISASI_ANGKA_PREDIKSI, ),
        self::TYPE_FIELDNAME     => array('id', 'pegawai_id', 'nama', 'deskripsi', 'status', 'created_at', 'updated_at', 'deleted_at', 'skpd_id', 'target', 'satuan', 'target_kemarin', 'cara_perhitungan', 'capaian_akhir', 'jenis', 'indikator_id_atasan', 'is_tambahan', 'is_tahun_lalu', 'is_tarik_ebudgeting', 'is_perhitungan_terbalik', 'is_realisasi_angka_prediksi', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'PegawaiId' => 1, 'Nama' => 2, 'Deskripsi' => 3, 'Status' => 4, 'CreatedAt' => 5, 'UpdatedAt' => 6, 'DeletedAt' => 7, 'SkpdId' => 8, 'Target' => 9, 'Satuan' => 10, 'TargetKemarin' => 11, 'CaraPerhitungan' => 12, 'CapaianAkhir' => 13, 'Jenis' => 14, 'IndikatorIdAtasan' => 15, 'IsTambahan' => 16, 'IsTahunLalu' => 17, 'IsTarikEbudgeting' => 18, 'IsPerhitunganTerbalik' => 19, 'IsRealisasiAngkaPrediksi' => 20, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'pegawaiId' => 1, 'nama' => 2, 'deskripsi' => 3, 'status' => 4, 'createdAt' => 5, 'updatedAt' => 6, 'deletedAt' => 7, 'skpdId' => 8, 'target' => 9, 'satuan' => 10, 'targetKemarin' => 11, 'caraPerhitungan' => 12, 'capaianAkhir' => 13, 'jenis' => 14, 'indikatorIdAtasan' => 15, 'isTambahan' => 16, 'isTahunLalu' => 17, 'isTarikEbudgeting' => 18, 'isPerhitunganTerbalik' => 19, 'isRealisasiAngkaPrediksi' => 20, ),
        self::TYPE_COLNAME       => array(OldIndikatorKinerjaTableMap::COL_ID => 0, OldIndikatorKinerjaTableMap::COL_PEGAWAI_ID => 1, OldIndikatorKinerjaTableMap::COL_NAMA => 2, OldIndikatorKinerjaTableMap::COL_DESKRIPSI => 3, OldIndikatorKinerjaTableMap::COL_STATUS => 4, OldIndikatorKinerjaTableMap::COL_CREATED_AT => 5, OldIndikatorKinerjaTableMap::COL_UPDATED_AT => 6, OldIndikatorKinerjaTableMap::COL_DELETED_AT => 7, OldIndikatorKinerjaTableMap::COL_SKPD_ID => 8, OldIndikatorKinerjaTableMap::COL_TARGET => 9, OldIndikatorKinerjaTableMap::COL_SATUAN => 10, OldIndikatorKinerjaTableMap::COL_TARGET_KEMARIN => 11, OldIndikatorKinerjaTableMap::COL_CARA_PERHITUNGAN => 12, OldIndikatorKinerjaTableMap::COL_CAPAIAN_AKHIR => 13, OldIndikatorKinerjaTableMap::COL_JENIS => 14, OldIndikatorKinerjaTableMap::COL_INDIKATOR_ID_ATASAN => 15, OldIndikatorKinerjaTableMap::COL_IS_TAMBAHAN => 16, OldIndikatorKinerjaTableMap::COL_IS_TAHUN_LALU => 17, OldIndikatorKinerjaTableMap::COL_IS_TARIK_EBUDGETING => 18, OldIndikatorKinerjaTableMap::COL_IS_PERHITUNGAN_TERBALIK => 19, OldIndikatorKinerjaTableMap::COL_IS_REALISASI_ANGKA_PREDIKSI => 20, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'pegawai_id' => 1, 'nama' => 2, 'deskripsi' => 3, 'status' => 4, 'created_at' => 5, 'updated_at' => 6, 'deleted_at' => 7, 'skpd_id' => 8, 'target' => 9, 'satuan' => 10, 'target_kemarin' => 11, 'cara_perhitungan' => 12, 'capaian_akhir' => 13, 'jenis' => 14, 'indikator_id_atasan' => 15, 'is_tambahan' => 16, 'is_tahun_lalu' => 17, 'is_tarik_ebudgeting' => 18, 'is_perhitungan_terbalik' => 19, 'is_realisasi_angka_prediksi' => 20, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('eperformance.indikator_kinerja');
        $this->setPhpName('OldIndikatorKinerja');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\CoreBundle\\Model\\OldEperformance\\OldIndikatorKinerja');
        $this->setPackage('src.CoreBundle.Model.OldEperformance');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('eperformance.indikator_kinerja_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('pegawai_id', 'PegawaiId', 'INTEGER', 'eperformance.pegawai', 'id', false, null, null);
        $this->addColumn('nama', 'Nama', 'LONGVARCHAR', true, null, null);
        $this->addColumn('deskripsi', 'Deskripsi', 'LONGVARCHAR', false, null, null);
        $this->addColumn('status', 'Status', 'SMALLINT', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('skpd_id', 'SkpdId', 'INTEGER', 'eperformance.master_skpd', 'skpd_id', false, null, null);
        $this->addColumn('target', 'Target', 'VARCHAR', false, 15, null);
        $this->addColumn('satuan', 'Satuan', 'VARCHAR', false, 30, null);
        $this->addColumn('target_kemarin', 'TargetKemarin', 'NUMERIC', false, null, null);
        $this->addColumn('cara_perhitungan', 'CaraPerhitungan', 'LONGVARCHAR', false, null, null);
        $this->addColumn('capaian_akhir', 'CapaianAkhir', 'VARCHAR', false, 15, null);
        $this->addColumn('jenis', 'Jenis', 'SMALLINT', false, null, null);
        $this->addForeignKey('indikator_id_atasan', 'IndikatorIdAtasan', 'INTEGER', 'eperformance.indikator_kinerja', 'id', false, null, null);
        $this->addColumn('is_tambahan', 'IsTambahan', 'BOOLEAN', false, null, false);
        $this->addColumn('is_tahun_lalu', 'IsTahunLalu', 'BOOLEAN', false, null, false);
        $this->addColumn('is_tarik_ebudgeting', 'IsTarikEbudgeting', 'BOOLEAN', false, null, false);
        $this->addColumn('is_perhitungan_terbalik', 'IsPerhitunganTerbalik', 'BOOLEAN', false, null, false);
        $this->addColumn('is_realisasi_angka_prediksi', 'IsRealisasiAngkaPrediksi', 'BOOLEAN', false, null, false);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('OldPegawai', '\\CoreBundle\\Model\\OldEperformance\\OldPegawai', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':pegawai_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('OldMasterSkpd', '\\CoreBundle\\Model\\OldEperformance\\OldMasterSkpd', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':skpd_id',
    1 => ':skpd_id',
  ),
), null, null, null, false);
        $this->addRelation('OldIndikatorKinerja', '\\CoreBundle\\Model\\OldEperformance\\OldIndikatorKinerja', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':indikator_id_atasan',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('OldIndikatorKinerjaRelatedById', '\\CoreBundle\\Model\\OldEperformance\\OldIndikatorKinerja', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':indikator_id_atasan',
    1 => ':id',
  ),
), null, null, 'OldIndikatorKinerjasRelatedById', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'query_cache' => array('backend' => 'apc', 'lifetime' => '7200', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? OldIndikatorKinerjaTableMap::CLASS_DEFAULT : OldIndikatorKinerjaTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (OldIndikatorKinerja object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = OldIndikatorKinerjaTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = OldIndikatorKinerjaTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + OldIndikatorKinerjaTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = OldIndikatorKinerjaTableMap::OM_CLASS;
            /** @var OldIndikatorKinerja $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            OldIndikatorKinerjaTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = OldIndikatorKinerjaTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = OldIndikatorKinerjaTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var OldIndikatorKinerja $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                OldIndikatorKinerjaTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_ID);
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_PEGAWAI_ID);
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_NAMA);
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_DESKRIPSI);
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_STATUS);
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_DELETED_AT);
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_SKPD_ID);
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_TARGET);
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_SATUAN);
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_TARGET_KEMARIN);
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_CARA_PERHITUNGAN);
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_CAPAIAN_AKHIR);
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_JENIS);
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_INDIKATOR_ID_ATASAN);
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_IS_TAMBAHAN);
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_IS_TAHUN_LALU);
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_IS_TARIK_EBUDGETING);
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_IS_PERHITUNGAN_TERBALIK);
            $criteria->addSelectColumn(OldIndikatorKinerjaTableMap::COL_IS_REALISASI_ANGKA_PREDIKSI);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.pegawai_id');
            $criteria->addSelectColumn($alias . '.nama');
            $criteria->addSelectColumn($alias . '.deskripsi');
            $criteria->addSelectColumn($alias . '.status');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.deleted_at');
            $criteria->addSelectColumn($alias . '.skpd_id');
            $criteria->addSelectColumn($alias . '.target');
            $criteria->addSelectColumn($alias . '.satuan');
            $criteria->addSelectColumn($alias . '.target_kemarin');
            $criteria->addSelectColumn($alias . '.cara_perhitungan');
            $criteria->addSelectColumn($alias . '.capaian_akhir');
            $criteria->addSelectColumn($alias . '.jenis');
            $criteria->addSelectColumn($alias . '.indikator_id_atasan');
            $criteria->addSelectColumn($alias . '.is_tambahan');
            $criteria->addSelectColumn($alias . '.is_tahun_lalu');
            $criteria->addSelectColumn($alias . '.is_tarik_ebudgeting');
            $criteria->addSelectColumn($alias . '.is_perhitungan_terbalik');
            $criteria->addSelectColumn($alias . '.is_realisasi_angka_prediksi');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(OldIndikatorKinerjaTableMap::DATABASE_NAME)->getTable(OldIndikatorKinerjaTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(OldIndikatorKinerjaTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(OldIndikatorKinerjaTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new OldIndikatorKinerjaTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a OldIndikatorKinerja or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or OldIndikatorKinerja object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldIndikatorKinerjaTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \CoreBundle\Model\OldEperformance\OldIndikatorKinerja) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(OldIndikatorKinerjaTableMap::DATABASE_NAME);
            $criteria->add(OldIndikatorKinerjaTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = OldIndikatorKinerjaQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            OldIndikatorKinerjaTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                OldIndikatorKinerjaTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the eperformance.indikator_kinerja table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return OldIndikatorKinerjaQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a OldIndikatorKinerja or Criteria object.
     *
     * @param mixed               $criteria Criteria or OldIndikatorKinerja object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldIndikatorKinerjaTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from OldIndikatorKinerja object
        }

        if ($criteria->containsKey(OldIndikatorKinerjaTableMap::COL_ID) && $criteria->keyContainsValue(OldIndikatorKinerjaTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.OldIndikatorKinerjaTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = OldIndikatorKinerjaQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // OldIndikatorKinerjaTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
OldIndikatorKinerjaTableMap::buildTableMap();

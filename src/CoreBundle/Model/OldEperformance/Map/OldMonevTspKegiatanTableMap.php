<?php

namespace CoreBundle\Model\OldEperformance\Map;

use CoreBundle\Model\OldEperformance\OldMonevTspKegiatan;
use CoreBundle\Model\OldEperformance\OldMonevTspKegiatanQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'eperformance.monev_tsp_kegiatan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class OldMonevTspKegiatanTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.CoreBundle.Model.OldEperformance.Map.OldMonevTspKegiatanTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'old_eperformance';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'eperformance.monev_tsp_kegiatan';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\CoreBundle\\Model\\OldEperformance\\OldMonevTspKegiatan';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.CoreBundle.Model.OldEperformance.OldMonevTspKegiatan';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the id field
     */
    const COL_ID = 'eperformance.monev_tsp_kegiatan.id';

    /**
     * the column name for the kegiatan_id field
     */
    const COL_KEGIATAN_ID = 'eperformance.monev_tsp_kegiatan.kegiatan_id';

    /**
     * the column name for the kegiatan_kode field
     */
    const COL_KEGIATAN_KODE = 'eperformance.monev_tsp_kegiatan.kegiatan_kode';

    /**
     * the column name for the kegiatan_nama field
     */
    const COL_KEGIATAN_NAMA = 'eperformance.monev_tsp_kegiatan.kegiatan_nama';

    /**
     * the column name for the unit_id field
     */
    const COL_UNIT_ID = 'eperformance.monev_tsp_kegiatan.unit_id';

    /**
     * the column name for the unit_kode field
     */
    const COL_UNIT_KODE = 'eperformance.monev_tsp_kegiatan.unit_kode';

    /**
     * the column name for the unit_nama field
     */
    const COL_UNIT_NAMA = 'eperformance.monev_tsp_kegiatan.unit_nama';

    /**
     * the column name for the tahun field
     */
    const COL_TAHUN = 'eperformance.monev_tsp_kegiatan.tahun';

    /**
     * the column name for the target field
     */
    const COL_TARGET = 'eperformance.monev_tsp_kegiatan.target';

    /**
     * the column name for the realisasi field
     */
    const COL_REALISASI = 'eperformance.monev_tsp_kegiatan.realisasi';

    /**
     * the column name for the satuan field
     */
    const COL_SATUAN = 'eperformance.monev_tsp_kegiatan.satuan';

    /**
     * the column name for the capaian field
     */
    const COL_CAPAIAN = 'eperformance.monev_tsp_kegiatan.capaian';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'eperformance.monev_tsp_kegiatan.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'eperformance.monev_tsp_kegiatan.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'KegiatanId', 'KegiatanKode', 'KegiatanNama', 'UnitId', 'UnitKode', 'UnitNama', 'Tahun', 'Target', 'Realisasi', 'Satuan', 'Capaian', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'kegiatanId', 'kegiatanKode', 'kegiatanNama', 'unitId', 'unitKode', 'unitNama', 'tahun', 'target', 'realisasi', 'satuan', 'capaian', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(OldMonevTspKegiatanTableMap::COL_ID, OldMonevTspKegiatanTableMap::COL_KEGIATAN_ID, OldMonevTspKegiatanTableMap::COL_KEGIATAN_KODE, OldMonevTspKegiatanTableMap::COL_KEGIATAN_NAMA, OldMonevTspKegiatanTableMap::COL_UNIT_ID, OldMonevTspKegiatanTableMap::COL_UNIT_KODE, OldMonevTspKegiatanTableMap::COL_UNIT_NAMA, OldMonevTspKegiatanTableMap::COL_TAHUN, OldMonevTspKegiatanTableMap::COL_TARGET, OldMonevTspKegiatanTableMap::COL_REALISASI, OldMonevTspKegiatanTableMap::COL_SATUAN, OldMonevTspKegiatanTableMap::COL_CAPAIAN, OldMonevTspKegiatanTableMap::COL_CREATED_AT, OldMonevTspKegiatanTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'kegiatan_id', 'kegiatan_kode', 'kegiatan_nama', 'unit_id', 'unit_kode', 'unit_nama', 'tahun', 'target', 'realisasi', 'satuan', 'capaian', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'KegiatanId' => 1, 'KegiatanKode' => 2, 'KegiatanNama' => 3, 'UnitId' => 4, 'UnitKode' => 5, 'UnitNama' => 6, 'Tahun' => 7, 'Target' => 8, 'Realisasi' => 9, 'Satuan' => 10, 'Capaian' => 11, 'CreatedAt' => 12, 'UpdatedAt' => 13, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'kegiatanId' => 1, 'kegiatanKode' => 2, 'kegiatanNama' => 3, 'unitId' => 4, 'unitKode' => 5, 'unitNama' => 6, 'tahun' => 7, 'target' => 8, 'realisasi' => 9, 'satuan' => 10, 'capaian' => 11, 'createdAt' => 12, 'updatedAt' => 13, ),
        self::TYPE_COLNAME       => array(OldMonevTspKegiatanTableMap::COL_ID => 0, OldMonevTspKegiatanTableMap::COL_KEGIATAN_ID => 1, OldMonevTspKegiatanTableMap::COL_KEGIATAN_KODE => 2, OldMonevTspKegiatanTableMap::COL_KEGIATAN_NAMA => 3, OldMonevTspKegiatanTableMap::COL_UNIT_ID => 4, OldMonevTspKegiatanTableMap::COL_UNIT_KODE => 5, OldMonevTspKegiatanTableMap::COL_UNIT_NAMA => 6, OldMonevTspKegiatanTableMap::COL_TAHUN => 7, OldMonevTspKegiatanTableMap::COL_TARGET => 8, OldMonevTspKegiatanTableMap::COL_REALISASI => 9, OldMonevTspKegiatanTableMap::COL_SATUAN => 10, OldMonevTspKegiatanTableMap::COL_CAPAIAN => 11, OldMonevTspKegiatanTableMap::COL_CREATED_AT => 12, OldMonevTspKegiatanTableMap::COL_UPDATED_AT => 13, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'kegiatan_id' => 1, 'kegiatan_kode' => 2, 'kegiatan_nama' => 3, 'unit_id' => 4, 'unit_kode' => 5, 'unit_nama' => 6, 'tahun' => 7, 'target' => 8, 'realisasi' => 9, 'satuan' => 10, 'capaian' => 11, 'created_at' => 12, 'updated_at' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('eperformance.monev_tsp_kegiatan');
        $this->setPhpName('OldMonevTspKegiatan');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\CoreBundle\\Model\\OldEperformance\\OldMonevTspKegiatan');
        $this->setPackage('src.CoreBundle.Model.OldEperformance');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('kegiatan_id', 'KegiatanId', 'INTEGER', true, null, null);
        $this->addColumn('kegiatan_kode', 'KegiatanKode', 'VARCHAR', true, 20, null);
        $this->addColumn('kegiatan_nama', 'KegiatanNama', 'VARCHAR', true, 200, null);
        $this->addForeignKey('unit_id', 'UnitId', 'INTEGER', 'eperformance.master_skpd', 'skpd_id', true, null, null);
        $this->addColumn('unit_kode', 'UnitKode', 'VARCHAR', true, 5, null);
        $this->addColumn('unit_nama', 'UnitNama', 'VARCHAR', true, 100, null);
        $this->addColumn('tahun', 'Tahun', 'INTEGER', true, null, null);
        $this->addColumn('target', 'Target', 'NUMERIC', false, null, null);
        $this->addColumn('realisasi', 'Realisasi', 'NUMERIC', false, null, null);
        $this->addColumn('satuan', 'Satuan', 'VARCHAR', true, 50, null);
        $this->addColumn('capaian', 'Capaian', 'NUMERIC', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('OldMasterSkpd', '\\CoreBundle\\Model\\OldEperformance\\OldMasterSkpd', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':unit_id',
    1 => ':skpd_id',
  ),
), null, null, null, false);
        $this->addRelation('OldKegiatanPegawai', '\\CoreBundle\\Model\\OldEperformance\\OldKegiatanPegawai', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':kegiatan_id',
    1 => ':id',
  ),
), null, null, 'OldKegiatanPegawais', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'query_cache' => array('backend' => 'apc', 'lifetime' => '7200', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? OldMonevTspKegiatanTableMap::CLASS_DEFAULT : OldMonevTspKegiatanTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (OldMonevTspKegiatan object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = OldMonevTspKegiatanTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = OldMonevTspKegiatanTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + OldMonevTspKegiatanTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = OldMonevTspKegiatanTableMap::OM_CLASS;
            /** @var OldMonevTspKegiatan $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            OldMonevTspKegiatanTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = OldMonevTspKegiatanTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = OldMonevTspKegiatanTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var OldMonevTspKegiatan $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                OldMonevTspKegiatanTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(OldMonevTspKegiatanTableMap::COL_ID);
            $criteria->addSelectColumn(OldMonevTspKegiatanTableMap::COL_KEGIATAN_ID);
            $criteria->addSelectColumn(OldMonevTspKegiatanTableMap::COL_KEGIATAN_KODE);
            $criteria->addSelectColumn(OldMonevTspKegiatanTableMap::COL_KEGIATAN_NAMA);
            $criteria->addSelectColumn(OldMonevTspKegiatanTableMap::COL_UNIT_ID);
            $criteria->addSelectColumn(OldMonevTspKegiatanTableMap::COL_UNIT_KODE);
            $criteria->addSelectColumn(OldMonevTspKegiatanTableMap::COL_UNIT_NAMA);
            $criteria->addSelectColumn(OldMonevTspKegiatanTableMap::COL_TAHUN);
            $criteria->addSelectColumn(OldMonevTspKegiatanTableMap::COL_TARGET);
            $criteria->addSelectColumn(OldMonevTspKegiatanTableMap::COL_REALISASI);
            $criteria->addSelectColumn(OldMonevTspKegiatanTableMap::COL_SATUAN);
            $criteria->addSelectColumn(OldMonevTspKegiatanTableMap::COL_CAPAIAN);
            $criteria->addSelectColumn(OldMonevTspKegiatanTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(OldMonevTspKegiatanTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.kegiatan_id');
            $criteria->addSelectColumn($alias . '.kegiatan_kode');
            $criteria->addSelectColumn($alias . '.kegiatan_nama');
            $criteria->addSelectColumn($alias . '.unit_id');
            $criteria->addSelectColumn($alias . '.unit_kode');
            $criteria->addSelectColumn($alias . '.unit_nama');
            $criteria->addSelectColumn($alias . '.tahun');
            $criteria->addSelectColumn($alias . '.target');
            $criteria->addSelectColumn($alias . '.realisasi');
            $criteria->addSelectColumn($alias . '.satuan');
            $criteria->addSelectColumn($alias . '.capaian');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(OldMonevTspKegiatanTableMap::DATABASE_NAME)->getTable(OldMonevTspKegiatanTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(OldMonevTspKegiatanTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(OldMonevTspKegiatanTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new OldMonevTspKegiatanTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a OldMonevTspKegiatan or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or OldMonevTspKegiatan object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldMonevTspKegiatanTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \CoreBundle\Model\OldEperformance\OldMonevTspKegiatan) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(OldMonevTspKegiatanTableMap::DATABASE_NAME);
            $criteria->add(OldMonevTspKegiatanTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = OldMonevTspKegiatanQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            OldMonevTspKegiatanTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                OldMonevTspKegiatanTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the eperformance.monev_tsp_kegiatan table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return OldMonevTspKegiatanQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a OldMonevTspKegiatan or Criteria object.
     *
     * @param mixed               $criteria Criteria or OldMonevTspKegiatan object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldMonevTspKegiatanTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from OldMonevTspKegiatan object
        }


        // Set the correct dbName
        $query = OldMonevTspKegiatanQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // OldMonevTspKegiatanTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
OldMonevTspKegiatanTableMap::buildTableMap();

<?php

namespace CoreBundle\Model\OldEperformance\Map;

use CoreBundle\Model\OldEperformance\OldMonevTsp;
use CoreBundle\Model\OldEperformance\OldMonevTspQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'eperformance.monev_tsp' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class OldMonevTspTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.CoreBundle.Model.OldEperformance.Map.OldMonevTspTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'old_eperformance';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'eperformance.monev_tsp';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\CoreBundle\\Model\\OldEperformance\\OldMonevTsp';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.CoreBundle.Model.OldEperformance.OldMonevTsp';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 19;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 19;

    /**
     * the column name for the id field
     */
    const COL_ID = 'eperformance.monev_tsp.id';

    /**
     * the column name for the pegawai_id field
     */
    const COL_PEGAWAI_ID = 'eperformance.monev_tsp.pegawai_id';

    /**
     * the column name for the pegawai_nip field
     */
    const COL_PEGAWAI_NIP = 'eperformance.monev_tsp.pegawai_nip';

    /**
     * the column name for the pegawai_nama field
     */
    const COL_PEGAWAI_NAMA = 'eperformance.monev_tsp.pegawai_nama';

    /**
     * the column name for the unit_id field
     */
    const COL_UNIT_ID = 'eperformance.monev_tsp.unit_id';

    /**
     * the column name for the unit_kode field
     */
    const COL_UNIT_KODE = 'eperformance.monev_tsp.unit_kode';

    /**
     * the column name for the tsp_id field
     */
    const COL_TSP_ID = 'eperformance.monev_tsp.tsp_id';

    /**
     * the column name for the indikator_id field
     */
    const COL_INDIKATOR_ID = 'eperformance.monev_tsp.indikator_id';

    /**
     * the column name for the indikator_level field
     */
    const COL_INDIKATOR_LEVEL = 'eperformance.monev_tsp.indikator_level';

    /**
     * the column name for the indikator_nama field
     */
    const COL_INDIKATOR_NAMA = 'eperformance.monev_tsp.indikator_nama';

    /**
     * the column name for the indikator_formulasi field
     */
    const COL_INDIKATOR_FORMULASI = 'eperformance.monev_tsp.indikator_formulasi';

    /**
     * the column name for the indikator_target field
     */
    const COL_INDIKATOR_TARGET = 'eperformance.monev_tsp.indikator_target';

    /**
     * the column name for the indikator_realisasi field
     */
    const COL_INDIKATOR_REALISASI = 'eperformance.monev_tsp.indikator_realisasi';

    /**
     * the column name for the indikator_satuan field
     */
    const COL_INDIKATOR_SATUAN = 'eperformance.monev_tsp.indikator_satuan';

    /**
     * the column name for the indikator_bobot field
     */
    const COL_INDIKATOR_BOBOT = 'eperformance.monev_tsp.indikator_bobot';

    /**
     * the column name for the indikator_capaian field
     */
    const COL_INDIKATOR_CAPAIAN = 'eperformance.monev_tsp.indikator_capaian';

    /**
     * the column name for the kinerja_capaian field
     */
    const COL_KINERJA_CAPAIAN = 'eperformance.monev_tsp.kinerja_capaian';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'eperformance.monev_tsp.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'eperformance.monev_tsp.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'PegawaiId', 'PegawaiNip', 'PegawaiNama', 'UnitId', 'UnitKode', 'TspId', 'IndikatorId', 'IndikatorLevel', 'IndikatorNama', 'IndikatorFormulasi', 'IndikatorTarget', 'IndikatorRealisasi', 'IndikatorSatuan', 'IndikatorBobot', 'IndikatorCapaian', 'KinerjaCapaian', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'pegawaiId', 'pegawaiNip', 'pegawaiNama', 'unitId', 'unitKode', 'tspId', 'indikatorId', 'indikatorLevel', 'indikatorNama', 'indikatorFormulasi', 'indikatorTarget', 'indikatorRealisasi', 'indikatorSatuan', 'indikatorBobot', 'indikatorCapaian', 'kinerjaCapaian', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(OldMonevTspTableMap::COL_ID, OldMonevTspTableMap::COL_PEGAWAI_ID, OldMonevTspTableMap::COL_PEGAWAI_NIP, OldMonevTspTableMap::COL_PEGAWAI_NAMA, OldMonevTspTableMap::COL_UNIT_ID, OldMonevTspTableMap::COL_UNIT_KODE, OldMonevTspTableMap::COL_TSP_ID, OldMonevTspTableMap::COL_INDIKATOR_ID, OldMonevTspTableMap::COL_INDIKATOR_LEVEL, OldMonevTspTableMap::COL_INDIKATOR_NAMA, OldMonevTspTableMap::COL_INDIKATOR_FORMULASI, OldMonevTspTableMap::COL_INDIKATOR_TARGET, OldMonevTspTableMap::COL_INDIKATOR_REALISASI, OldMonevTspTableMap::COL_INDIKATOR_SATUAN, OldMonevTspTableMap::COL_INDIKATOR_BOBOT, OldMonevTspTableMap::COL_INDIKATOR_CAPAIAN, OldMonevTspTableMap::COL_KINERJA_CAPAIAN, OldMonevTspTableMap::COL_CREATED_AT, OldMonevTspTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'pegawai_id', 'pegawai_nip', 'pegawai_nama', 'unit_id', 'unit_kode', 'tsp_id', 'indikator_id', 'indikator_level', 'indikator_nama', 'indikator_formulasi', 'indikator_target', 'indikator_realisasi', 'indikator_satuan', 'indikator_bobot', 'indikator_capaian', 'kinerja_capaian', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'PegawaiId' => 1, 'PegawaiNip' => 2, 'PegawaiNama' => 3, 'UnitId' => 4, 'UnitKode' => 5, 'TspId' => 6, 'IndikatorId' => 7, 'IndikatorLevel' => 8, 'IndikatorNama' => 9, 'IndikatorFormulasi' => 10, 'IndikatorTarget' => 11, 'IndikatorRealisasi' => 12, 'IndikatorSatuan' => 13, 'IndikatorBobot' => 14, 'IndikatorCapaian' => 15, 'KinerjaCapaian' => 16, 'CreatedAt' => 17, 'UpdatedAt' => 18, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'pegawaiId' => 1, 'pegawaiNip' => 2, 'pegawaiNama' => 3, 'unitId' => 4, 'unitKode' => 5, 'tspId' => 6, 'indikatorId' => 7, 'indikatorLevel' => 8, 'indikatorNama' => 9, 'indikatorFormulasi' => 10, 'indikatorTarget' => 11, 'indikatorRealisasi' => 12, 'indikatorSatuan' => 13, 'indikatorBobot' => 14, 'indikatorCapaian' => 15, 'kinerjaCapaian' => 16, 'createdAt' => 17, 'updatedAt' => 18, ),
        self::TYPE_COLNAME       => array(OldMonevTspTableMap::COL_ID => 0, OldMonevTspTableMap::COL_PEGAWAI_ID => 1, OldMonevTspTableMap::COL_PEGAWAI_NIP => 2, OldMonevTspTableMap::COL_PEGAWAI_NAMA => 3, OldMonevTspTableMap::COL_UNIT_ID => 4, OldMonevTspTableMap::COL_UNIT_KODE => 5, OldMonevTspTableMap::COL_TSP_ID => 6, OldMonevTspTableMap::COL_INDIKATOR_ID => 7, OldMonevTspTableMap::COL_INDIKATOR_LEVEL => 8, OldMonevTspTableMap::COL_INDIKATOR_NAMA => 9, OldMonevTspTableMap::COL_INDIKATOR_FORMULASI => 10, OldMonevTspTableMap::COL_INDIKATOR_TARGET => 11, OldMonevTspTableMap::COL_INDIKATOR_REALISASI => 12, OldMonevTspTableMap::COL_INDIKATOR_SATUAN => 13, OldMonevTspTableMap::COL_INDIKATOR_BOBOT => 14, OldMonevTspTableMap::COL_INDIKATOR_CAPAIAN => 15, OldMonevTspTableMap::COL_KINERJA_CAPAIAN => 16, OldMonevTspTableMap::COL_CREATED_AT => 17, OldMonevTspTableMap::COL_UPDATED_AT => 18, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'pegawai_id' => 1, 'pegawai_nip' => 2, 'pegawai_nama' => 3, 'unit_id' => 4, 'unit_kode' => 5, 'tsp_id' => 6, 'indikator_id' => 7, 'indikator_level' => 8, 'indikator_nama' => 9, 'indikator_formulasi' => 10, 'indikator_target' => 11, 'indikator_realisasi' => 12, 'indikator_satuan' => 13, 'indikator_bobot' => 14, 'indikator_capaian' => 15, 'kinerja_capaian' => 16, 'created_at' => 17, 'updated_at' => 18, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('eperformance.monev_tsp');
        $this->setPhpName('OldMonevTsp');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\CoreBundle\\Model\\OldEperformance\\OldMonevTsp');
        $this->setPackage('src.CoreBundle.Model.OldEperformance');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('pegawai_id', 'PegawaiId', 'INTEGER', true, null, null);
        $this->addColumn('pegawai_nip', 'PegawaiNip', 'VARCHAR', true, 25, null);
        $this->addColumn('pegawai_nama', 'PegawaiNama', 'VARCHAR', true, 100, null);
        $this->addForeignKey('unit_id', 'UnitId', 'INTEGER', 'eperformance.master_skpd', 'skpd_id', true, null, null);
        $this->addColumn('unit_kode', 'UnitKode', 'VARCHAR', true, 5, null);
        $this->addColumn('tsp_id', 'TspId', 'INTEGER', true, null, null);
        $this->addColumn('indikator_id', 'IndikatorId', 'VARCHAR', false, 15, null);
        $this->addColumn('indikator_level', 'IndikatorLevel', 'SMALLINT', true, null, null);
        $this->addColumn('indikator_nama', 'IndikatorNama', 'VARCHAR', true, 255, null);
        $this->addColumn('indikator_formulasi', 'IndikatorFormulasi', 'LONGVARCHAR', false, null, null);
        $this->addColumn('indikator_target', 'IndikatorTarget', 'NUMERIC', false, null, null);
        $this->addColumn('indikator_realisasi', 'IndikatorRealisasi', 'NUMERIC', false, null, null);
        $this->addColumn('indikator_satuan', 'IndikatorSatuan', 'VARCHAR', true, 50, null);
        $this->addColumn('indikator_bobot', 'IndikatorBobot', 'NUMERIC', false, null, null);
        $this->addColumn('indikator_capaian', 'IndikatorCapaian', 'NUMERIC', false, null, null);
        $this->addColumn('kinerja_capaian', 'KinerjaCapaian', 'NUMERIC', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('OldMasterSkpd', '\\CoreBundle\\Model\\OldEperformance\\OldMasterSkpd', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':unit_id',
    1 => ':skpd_id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'query_cache' => array('backend' => 'apc', 'lifetime' => '7200', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? OldMonevTspTableMap::CLASS_DEFAULT : OldMonevTspTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (OldMonevTsp object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = OldMonevTspTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = OldMonevTspTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + OldMonevTspTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = OldMonevTspTableMap::OM_CLASS;
            /** @var OldMonevTsp $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            OldMonevTspTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = OldMonevTspTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = OldMonevTspTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var OldMonevTsp $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                OldMonevTspTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(OldMonevTspTableMap::COL_ID);
            $criteria->addSelectColumn(OldMonevTspTableMap::COL_PEGAWAI_ID);
            $criteria->addSelectColumn(OldMonevTspTableMap::COL_PEGAWAI_NIP);
            $criteria->addSelectColumn(OldMonevTspTableMap::COL_PEGAWAI_NAMA);
            $criteria->addSelectColumn(OldMonevTspTableMap::COL_UNIT_ID);
            $criteria->addSelectColumn(OldMonevTspTableMap::COL_UNIT_KODE);
            $criteria->addSelectColumn(OldMonevTspTableMap::COL_TSP_ID);
            $criteria->addSelectColumn(OldMonevTspTableMap::COL_INDIKATOR_ID);
            $criteria->addSelectColumn(OldMonevTspTableMap::COL_INDIKATOR_LEVEL);
            $criteria->addSelectColumn(OldMonevTspTableMap::COL_INDIKATOR_NAMA);
            $criteria->addSelectColumn(OldMonevTspTableMap::COL_INDIKATOR_FORMULASI);
            $criteria->addSelectColumn(OldMonevTspTableMap::COL_INDIKATOR_TARGET);
            $criteria->addSelectColumn(OldMonevTspTableMap::COL_INDIKATOR_REALISASI);
            $criteria->addSelectColumn(OldMonevTspTableMap::COL_INDIKATOR_SATUAN);
            $criteria->addSelectColumn(OldMonevTspTableMap::COL_INDIKATOR_BOBOT);
            $criteria->addSelectColumn(OldMonevTspTableMap::COL_INDIKATOR_CAPAIAN);
            $criteria->addSelectColumn(OldMonevTspTableMap::COL_KINERJA_CAPAIAN);
            $criteria->addSelectColumn(OldMonevTspTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(OldMonevTspTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.pegawai_id');
            $criteria->addSelectColumn($alias . '.pegawai_nip');
            $criteria->addSelectColumn($alias . '.pegawai_nama');
            $criteria->addSelectColumn($alias . '.unit_id');
            $criteria->addSelectColumn($alias . '.unit_kode');
            $criteria->addSelectColumn($alias . '.tsp_id');
            $criteria->addSelectColumn($alias . '.indikator_id');
            $criteria->addSelectColumn($alias . '.indikator_level');
            $criteria->addSelectColumn($alias . '.indikator_nama');
            $criteria->addSelectColumn($alias . '.indikator_formulasi');
            $criteria->addSelectColumn($alias . '.indikator_target');
            $criteria->addSelectColumn($alias . '.indikator_realisasi');
            $criteria->addSelectColumn($alias . '.indikator_satuan');
            $criteria->addSelectColumn($alias . '.indikator_bobot');
            $criteria->addSelectColumn($alias . '.indikator_capaian');
            $criteria->addSelectColumn($alias . '.kinerja_capaian');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(OldMonevTspTableMap::DATABASE_NAME)->getTable(OldMonevTspTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(OldMonevTspTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(OldMonevTspTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new OldMonevTspTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a OldMonevTsp or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or OldMonevTsp object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldMonevTspTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \CoreBundle\Model\OldEperformance\OldMonevTsp) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(OldMonevTspTableMap::DATABASE_NAME);
            $criteria->add(OldMonevTspTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = OldMonevTspQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            OldMonevTspTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                OldMonevTspTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the eperformance.monev_tsp table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return OldMonevTspQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a OldMonevTsp or Criteria object.
     *
     * @param mixed               $criteria Criteria or OldMonevTsp object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldMonevTspTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from OldMonevTsp object
        }


        // Set the correct dbName
        $query = OldMonevTspQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // OldMonevTspTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
OldMonevTspTableMap::buildTableMap();

<?php

namespace CoreBundle\Model\OldEperformance\Map;

use CoreBundle\Model\OldEperformance\OldMasterSkpd;
use CoreBundle\Model\OldEperformance\OldMasterSkpdQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'eperformance.master_skpd' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class OldMasterSkpdTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.CoreBundle.Model.OldEperformance.Map.OldMasterSkpdTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'old_eperformance';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'eperformance.master_skpd';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\CoreBundle\\Model\\OldEperformance\\OldMasterSkpd';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.CoreBundle.Model.OldEperformance.OldMasterSkpd';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 4;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 4;

    /**
     * the column name for the skpd_id field
     */
    const COL_SKPD_ID = 'eperformance.master_skpd.skpd_id';

    /**
     * the column name for the skpd_kode field
     */
    const COL_SKPD_KODE = 'eperformance.master_skpd.skpd_kode';

    /**
     * the column name for the skpd_nama field
     */
    const COL_SKPD_NAMA = 'eperformance.master_skpd.skpd_nama';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'eperformance.master_skpd.deleted_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('SkpdId', 'SkpdKode', 'SkpdNama', 'DeletedAt', ),
        self::TYPE_CAMELNAME     => array('skpdId', 'skpdKode', 'skpdNama', 'deletedAt', ),
        self::TYPE_COLNAME       => array(OldMasterSkpdTableMap::COL_SKPD_ID, OldMasterSkpdTableMap::COL_SKPD_KODE, OldMasterSkpdTableMap::COL_SKPD_NAMA, OldMasterSkpdTableMap::COL_DELETED_AT, ),
        self::TYPE_FIELDNAME     => array('skpd_id', 'skpd_kode', 'skpd_nama', 'deleted_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('SkpdId' => 0, 'SkpdKode' => 1, 'SkpdNama' => 2, 'DeletedAt' => 3, ),
        self::TYPE_CAMELNAME     => array('skpdId' => 0, 'skpdKode' => 1, 'skpdNama' => 2, 'deletedAt' => 3, ),
        self::TYPE_COLNAME       => array(OldMasterSkpdTableMap::COL_SKPD_ID => 0, OldMasterSkpdTableMap::COL_SKPD_KODE => 1, OldMasterSkpdTableMap::COL_SKPD_NAMA => 2, OldMasterSkpdTableMap::COL_DELETED_AT => 3, ),
        self::TYPE_FIELDNAME     => array('skpd_id' => 0, 'skpd_kode' => 1, 'skpd_nama' => 2, 'deleted_at' => 3, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('eperformance.master_skpd');
        $this->setPhpName('OldMasterSkpd');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\CoreBundle\\Model\\OldEperformance\\OldMasterSkpd');
        $this->setPackage('src.CoreBundle.Model.OldEperformance');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('eperformance.master_skpd_skpd_id_seq');
        // columns
        $this->addPrimaryKey('skpd_id', 'SkpdId', 'SMALLINT', true, null, null);
        $this->addColumn('skpd_kode', 'SkpdKode', 'VARCHAR', true, 8, null);
        $this->addColumn('skpd_nama', 'SkpdNama', 'VARCHAR', true, 128, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('OldPerilakuTes', '\\CoreBundle\\Model\\OldEperformance\\OldPerilakuTes', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':skpd_id',
    1 => ':skpd_id',
  ),
), null, null, 'OldPerilakuTess', false);
        $this->addRelation('OldPegawai', '\\CoreBundle\\Model\\OldEperformance\\OldPegawai', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':skpd_id',
    1 => ':skpd_id',
  ),
), null, null, 'OldPegawais', false);
        $this->addRelation('OldIndikatorKinerja', '\\CoreBundle\\Model\\OldEperformance\\OldIndikatorKinerja', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':skpd_id',
    1 => ':skpd_id',
  ),
), null, null, 'OldIndikatorKinerjas', false);
        $this->addRelation('OldJabatanStruktural', '\\CoreBundle\\Model\\OldEperformance\\OldJabatanStruktural', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':skpd_id',
    1 => ':skpd_id',
  ),
), null, null, 'OldJabatanStrukturals', false);
        $this->addRelation('OldMonevTsp', '\\CoreBundle\\Model\\OldEperformance\\OldMonevTsp', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':unit_id',
    1 => ':skpd_id',
  ),
), null, null, 'OldMonevTsps', false);
        $this->addRelation('OldMonevTspKegiatan', '\\CoreBundle\\Model\\OldEperformance\\OldMonevTspKegiatan', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':unit_id',
    1 => ':skpd_id',
  ),
), null, null, 'OldMonevTspKegiatans', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'query_cache' => array('backend' => 'apc', 'lifetime' => '7200', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('SkpdId', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('SkpdId', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('SkpdId', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('SkpdId', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('SkpdId', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('SkpdId', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('SkpdId', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? OldMasterSkpdTableMap::CLASS_DEFAULT : OldMasterSkpdTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (OldMasterSkpd object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = OldMasterSkpdTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = OldMasterSkpdTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + OldMasterSkpdTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = OldMasterSkpdTableMap::OM_CLASS;
            /** @var OldMasterSkpd $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            OldMasterSkpdTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = OldMasterSkpdTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = OldMasterSkpdTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var OldMasterSkpd $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                OldMasterSkpdTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(OldMasterSkpdTableMap::COL_SKPD_ID);
            $criteria->addSelectColumn(OldMasterSkpdTableMap::COL_SKPD_KODE);
            $criteria->addSelectColumn(OldMasterSkpdTableMap::COL_SKPD_NAMA);
            $criteria->addSelectColumn(OldMasterSkpdTableMap::COL_DELETED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.skpd_id');
            $criteria->addSelectColumn($alias . '.skpd_kode');
            $criteria->addSelectColumn($alias . '.skpd_nama');
            $criteria->addSelectColumn($alias . '.deleted_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(OldMasterSkpdTableMap::DATABASE_NAME)->getTable(OldMasterSkpdTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(OldMasterSkpdTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(OldMasterSkpdTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new OldMasterSkpdTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a OldMasterSkpd or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or OldMasterSkpd object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldMasterSkpdTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \CoreBundle\Model\OldEperformance\OldMasterSkpd) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(OldMasterSkpdTableMap::DATABASE_NAME);
            $criteria->add(OldMasterSkpdTableMap::COL_SKPD_ID, (array) $values, Criteria::IN);
        }

        $query = OldMasterSkpdQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            OldMasterSkpdTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                OldMasterSkpdTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the eperformance.master_skpd table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return OldMasterSkpdQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a OldMasterSkpd or Criteria object.
     *
     * @param mixed               $criteria Criteria or OldMasterSkpd object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldMasterSkpdTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from OldMasterSkpd object
        }

        if ($criteria->containsKey(OldMasterSkpdTableMap::COL_SKPD_ID) && $criteria->keyContainsValue(OldMasterSkpdTableMap::COL_SKPD_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.OldMasterSkpdTableMap::COL_SKPD_ID.')');
        }


        // Set the correct dbName
        $query = OldMasterSkpdQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // OldMasterSkpdTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
OldMasterSkpdTableMap::buildTableMap();

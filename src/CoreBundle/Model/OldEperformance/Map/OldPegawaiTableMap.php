<?php

namespace CoreBundle\Model\OldEperformance\Map;

use CoreBundle\Model\OldEperformance\OldPegawai;
use CoreBundle\Model\OldEperformance\OldPegawaiQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'eperformance.pegawai' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class OldPegawaiTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.CoreBundle.Model.OldEperformance.Map.OldPegawaiTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'old_eperformance';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'eperformance.pegawai';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\CoreBundle\\Model\\OldEperformance\\OldPegawai';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.CoreBundle.Model.OldEperformance.OldPegawai';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 25;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 25;

    /**
     * the column name for the id field
     */
    const COL_ID = 'eperformance.pegawai.id';

    /**
     * the column name for the nip field
     */
    const COL_NIP = 'eperformance.pegawai.nip';

    /**
     * the column name for the nama field
     */
    const COL_NAMA = 'eperformance.pegawai.nama';

    /**
     * the column name for the golongan field
     */
    const COL_GOLONGAN = 'eperformance.pegawai.golongan';

    /**
     * the column name for the tmt field
     */
    const COL_TMT = 'eperformance.pegawai.tmt';

    /**
     * the column name for the pendidikan_terakhir field
     */
    const COL_PENDIDIKAN_TERAKHIR = 'eperformance.pegawai.pendidikan_terakhir';

    /**
     * the column name for the skpd_id field
     */
    const COL_SKPD_ID = 'eperformance.pegawai.skpd_id';

    /**
     * the column name for the atasan_id field
     */
    const COL_ATASAN_ID = 'eperformance.pegawai.atasan_id';

    /**
     * the column name for the is_atasan field
     */
    const COL_IS_ATASAN = 'eperformance.pegawai.is_atasan';

    /**
     * the column name for the level_struktural field
     */
    const COL_LEVEL_STRUKTURAL = 'eperformance.pegawai.level_struktural';

    /**
     * the column name for the level_penilaian field
     */
    const COL_LEVEL_PENILAIAN = 'eperformance.pegawai.level_penilaian';

    /**
     * the column name for the password field
     */
    const COL_PASSWORD = 'eperformance.pegawai.password';

    /**
     * the column name for the user_id field
     */
    const COL_USER_ID = 'eperformance.pegawai.user_id';

    /**
     * the column name for the username_eproject field
     */
    const COL_USERNAME_EPROJECT = 'eperformance.pegawai.username_eproject';

    /**
     * the column name for the tgl_masuk field
     */
    const COL_TGL_MASUK = 'eperformance.pegawai.tgl_masuk';

    /**
     * the column name for the tgl_keluar field
     */
    const COL_TGL_KELUAR = 'eperformance.pegawai.tgl_keluar';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'eperformance.pegawai.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'eperformance.pegawai.updated_at';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'eperformance.pegawai.deleted_at';

    /**
     * the column name for the is_out field
     */
    const COL_IS_OUT = 'eperformance.pegawai.is_out';

    /**
     * the column name for the is_ka_uptd field
     */
    const COL_IS_KA_UPTD = 'eperformance.pegawai.is_ka_uptd';

    /**
     * the column name for the is_struktural field
     */
    const COL_IS_STRUKTURAL = 'eperformance.pegawai.is_struktural';

    /**
     * the column name for the is_kunci_aktifitas field
     */
    const COL_IS_KUNCI_AKTIFITAS = 'eperformance.pegawai.is_kunci_aktifitas';

    /**
     * the column name for the is_only_skp field
     */
    const COL_IS_ONLY_SKP = 'eperformance.pegawai.is_only_skp';

    /**
     * the column name for the jabatan_struktural_id field
     */
    const COL_JABATAN_STRUKTURAL_ID = 'eperformance.pegawai.jabatan_struktural_id';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Nip', 'Nama', 'Golongan', 'Tmt', 'PendidikanTerakhir', 'SkpdId', 'AtasanId', 'IsAtasan', 'LevelStruktural', 'LevelPenilaian', 'Password', 'UserId', 'UsernameEproject', 'TglMasuk', 'TglKeluar', 'CreatedAt', 'UpdatedAt', 'DeletedAt', 'IsOut', 'IsKaUptd', 'IsStruktural', 'IsKunciAktifitas', 'IsOnlySkp', 'JabatanStrukturalId', ),
        self::TYPE_CAMELNAME     => array('id', 'nip', 'nama', 'golongan', 'tmt', 'pendidikanTerakhir', 'skpdId', 'atasanId', 'isAtasan', 'levelStruktural', 'levelPenilaian', 'password', 'userId', 'usernameEproject', 'tglMasuk', 'tglKeluar', 'createdAt', 'updatedAt', 'deletedAt', 'isOut', 'isKaUptd', 'isStruktural', 'isKunciAktifitas', 'isOnlySkp', 'jabatanStrukturalId', ),
        self::TYPE_COLNAME       => array(OldPegawaiTableMap::COL_ID, OldPegawaiTableMap::COL_NIP, OldPegawaiTableMap::COL_NAMA, OldPegawaiTableMap::COL_GOLONGAN, OldPegawaiTableMap::COL_TMT, OldPegawaiTableMap::COL_PENDIDIKAN_TERAKHIR, OldPegawaiTableMap::COL_SKPD_ID, OldPegawaiTableMap::COL_ATASAN_ID, OldPegawaiTableMap::COL_IS_ATASAN, OldPegawaiTableMap::COL_LEVEL_STRUKTURAL, OldPegawaiTableMap::COL_LEVEL_PENILAIAN, OldPegawaiTableMap::COL_PASSWORD, OldPegawaiTableMap::COL_USER_ID, OldPegawaiTableMap::COL_USERNAME_EPROJECT, OldPegawaiTableMap::COL_TGL_MASUK, OldPegawaiTableMap::COL_TGL_KELUAR, OldPegawaiTableMap::COL_CREATED_AT, OldPegawaiTableMap::COL_UPDATED_AT, OldPegawaiTableMap::COL_DELETED_AT, OldPegawaiTableMap::COL_IS_OUT, OldPegawaiTableMap::COL_IS_KA_UPTD, OldPegawaiTableMap::COL_IS_STRUKTURAL, OldPegawaiTableMap::COL_IS_KUNCI_AKTIFITAS, OldPegawaiTableMap::COL_IS_ONLY_SKP, OldPegawaiTableMap::COL_JABATAN_STRUKTURAL_ID, ),
        self::TYPE_FIELDNAME     => array('id', 'nip', 'nama', 'golongan', 'tmt', 'pendidikan_terakhir', 'skpd_id', 'atasan_id', 'is_atasan', 'level_struktural', 'level_penilaian', 'password', 'user_id', 'username_eproject', 'tgl_masuk', 'tgl_keluar', 'created_at', 'updated_at', 'deleted_at', 'is_out', 'is_ka_uptd', 'is_struktural', 'is_kunci_aktifitas', 'is_only_skp', 'jabatan_struktural_id', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Nip' => 1, 'Nama' => 2, 'Golongan' => 3, 'Tmt' => 4, 'PendidikanTerakhir' => 5, 'SkpdId' => 6, 'AtasanId' => 7, 'IsAtasan' => 8, 'LevelStruktural' => 9, 'LevelPenilaian' => 10, 'Password' => 11, 'UserId' => 12, 'UsernameEproject' => 13, 'TglMasuk' => 14, 'TglKeluar' => 15, 'CreatedAt' => 16, 'UpdatedAt' => 17, 'DeletedAt' => 18, 'IsOut' => 19, 'IsKaUptd' => 20, 'IsStruktural' => 21, 'IsKunciAktifitas' => 22, 'IsOnlySkp' => 23, 'JabatanStrukturalId' => 24, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'nip' => 1, 'nama' => 2, 'golongan' => 3, 'tmt' => 4, 'pendidikanTerakhir' => 5, 'skpdId' => 6, 'atasanId' => 7, 'isAtasan' => 8, 'levelStruktural' => 9, 'levelPenilaian' => 10, 'password' => 11, 'userId' => 12, 'usernameEproject' => 13, 'tglMasuk' => 14, 'tglKeluar' => 15, 'createdAt' => 16, 'updatedAt' => 17, 'deletedAt' => 18, 'isOut' => 19, 'isKaUptd' => 20, 'isStruktural' => 21, 'isKunciAktifitas' => 22, 'isOnlySkp' => 23, 'jabatanStrukturalId' => 24, ),
        self::TYPE_COLNAME       => array(OldPegawaiTableMap::COL_ID => 0, OldPegawaiTableMap::COL_NIP => 1, OldPegawaiTableMap::COL_NAMA => 2, OldPegawaiTableMap::COL_GOLONGAN => 3, OldPegawaiTableMap::COL_TMT => 4, OldPegawaiTableMap::COL_PENDIDIKAN_TERAKHIR => 5, OldPegawaiTableMap::COL_SKPD_ID => 6, OldPegawaiTableMap::COL_ATASAN_ID => 7, OldPegawaiTableMap::COL_IS_ATASAN => 8, OldPegawaiTableMap::COL_LEVEL_STRUKTURAL => 9, OldPegawaiTableMap::COL_LEVEL_PENILAIAN => 10, OldPegawaiTableMap::COL_PASSWORD => 11, OldPegawaiTableMap::COL_USER_ID => 12, OldPegawaiTableMap::COL_USERNAME_EPROJECT => 13, OldPegawaiTableMap::COL_TGL_MASUK => 14, OldPegawaiTableMap::COL_TGL_KELUAR => 15, OldPegawaiTableMap::COL_CREATED_AT => 16, OldPegawaiTableMap::COL_UPDATED_AT => 17, OldPegawaiTableMap::COL_DELETED_AT => 18, OldPegawaiTableMap::COL_IS_OUT => 19, OldPegawaiTableMap::COL_IS_KA_UPTD => 20, OldPegawaiTableMap::COL_IS_STRUKTURAL => 21, OldPegawaiTableMap::COL_IS_KUNCI_AKTIFITAS => 22, OldPegawaiTableMap::COL_IS_ONLY_SKP => 23, OldPegawaiTableMap::COL_JABATAN_STRUKTURAL_ID => 24, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'nip' => 1, 'nama' => 2, 'golongan' => 3, 'tmt' => 4, 'pendidikan_terakhir' => 5, 'skpd_id' => 6, 'atasan_id' => 7, 'is_atasan' => 8, 'level_struktural' => 9, 'level_penilaian' => 10, 'password' => 11, 'user_id' => 12, 'username_eproject' => 13, 'tgl_masuk' => 14, 'tgl_keluar' => 15, 'created_at' => 16, 'updated_at' => 17, 'deleted_at' => 18, 'is_out' => 19, 'is_ka_uptd' => 20, 'is_struktural' => 21, 'is_kunci_aktifitas' => 22, 'is_only_skp' => 23, 'jabatan_struktural_id' => 24, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('eperformance.pegawai');
        $this->setPhpName('OldPegawai');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\CoreBundle\\Model\\OldEperformance\\OldPegawai');
        $this->setPackage('src.CoreBundle.Model.OldEperformance');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('eperformance.pegawai_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('nip', 'Nip', 'VARCHAR', false, 25, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', false, 60, null);
        $this->addColumn('golongan', 'Golongan', 'VARCHAR', false, 50, null);
        $this->addColumn('tmt', 'Tmt', 'DATE', false, null, null);
        $this->addColumn('pendidikan_terakhir', 'PendidikanTerakhir', 'VARCHAR', false, 256, null);
        $this->addForeignKey('skpd_id', 'SkpdId', 'INTEGER', 'eperformance.master_skpd', 'skpd_id', false, null, null);
        $this->addForeignKey('atasan_id', 'AtasanId', 'INTEGER', 'eperformance.pegawai', 'id', false, null, null);
        $this->addColumn('is_atasan', 'IsAtasan', 'BOOLEAN', false, null, false);
        $this->addColumn('level_struktural', 'LevelStruktural', 'SMALLINT', false, null, null);
        $this->addColumn('level_penilaian', 'LevelPenilaian', 'SMALLINT', false, null, null);
        $this->addColumn('password', 'Password', 'VARCHAR', false, 36, null);
        $this->addColumn('user_id', 'UserId', 'INTEGER', false, null, null);
        $this->addColumn('username_eproject', 'UsernameEproject', 'VARCHAR', false, 255, null);
        $this->addColumn('tgl_masuk', 'TglMasuk', 'DATE', false, null, null);
        $this->addColumn('tgl_keluar', 'TglKeluar', 'DATE', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('is_out', 'IsOut', 'BOOLEAN', false, null, false);
        $this->addColumn('is_ka_uptd', 'IsKaUptd', 'BOOLEAN', false, null, false);
        $this->addColumn('is_struktural', 'IsStruktural', 'BOOLEAN', false, null, false);
        $this->addColumn('is_kunci_aktifitas', 'IsKunciAktifitas', 'BOOLEAN', false, null, true);
        $this->addColumn('is_only_skp', 'IsOnlySkp', 'BOOLEAN', false, null, false);
        $this->addForeignKey('jabatan_struktural_id', 'JabatanStrukturalId', 'INTEGER', 'eperformance.jabatan_struktural', 'id', false, null, 0);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('OldMasterSkpd', '\\CoreBundle\\Model\\OldEperformance\\OldMasterSkpd', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':skpd_id',
    1 => ':skpd_id',
  ),
), null, null, null, false);
        $this->addRelation('OldPegawai', '\\CoreBundle\\Model\\OldEperformance\\OldPegawai', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':atasan_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('OldJabatanStruktural', '\\CoreBundle\\Model\\OldEperformance\\OldJabatanStruktural', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':jabatan_struktural_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('OldPerilakuHasil', '\\CoreBundle\\Model\\OldEperformance\\OldPerilakuHasil', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':pegawai_id',
    1 => ':id',
  ),
), null, null, 'OldPerilakuHasils', false);
        $this->addRelation('OldPegawaiRelatedById', '\\CoreBundle\\Model\\OldEperformance\\OldPegawai', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':atasan_id',
    1 => ':id',
  ),
), null, null, 'OldPegawaisRelatedById', false);
        $this->addRelation('OldIndikatorKinerja', '\\CoreBundle\\Model\\OldEperformance\\OldIndikatorKinerja', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':pegawai_id',
    1 => ':id',
  ),
), null, null, 'OldIndikatorKinerjas', false);
        $this->addRelation('OldKegiatanPegawai', '\\CoreBundle\\Model\\OldEperformance\\OldKegiatanPegawai', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':pegawai_id',
    1 => ':id',
  ),
), null, null, 'OldKegiatanPegawais', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'query_cache' => array('backend' => 'apc', 'lifetime' => '7200', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? OldPegawaiTableMap::CLASS_DEFAULT : OldPegawaiTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (OldPegawai object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = OldPegawaiTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = OldPegawaiTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + OldPegawaiTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = OldPegawaiTableMap::OM_CLASS;
            /** @var OldPegawai $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            OldPegawaiTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = OldPegawaiTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = OldPegawaiTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var OldPegawai $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                OldPegawaiTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_ID);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_NIP);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_NAMA);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_GOLONGAN);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_TMT);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_PENDIDIKAN_TERAKHIR);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_SKPD_ID);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_ATASAN_ID);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_IS_ATASAN);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_LEVEL_STRUKTURAL);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_LEVEL_PENILAIAN);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_PASSWORD);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_USER_ID);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_USERNAME_EPROJECT);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_TGL_MASUK);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_TGL_KELUAR);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_DELETED_AT);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_IS_OUT);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_IS_KA_UPTD);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_IS_STRUKTURAL);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_IS_KUNCI_AKTIFITAS);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_IS_ONLY_SKP);
            $criteria->addSelectColumn(OldPegawaiTableMap::COL_JABATAN_STRUKTURAL_ID);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.nip');
            $criteria->addSelectColumn($alias . '.nama');
            $criteria->addSelectColumn($alias . '.golongan');
            $criteria->addSelectColumn($alias . '.tmt');
            $criteria->addSelectColumn($alias . '.pendidikan_terakhir');
            $criteria->addSelectColumn($alias . '.skpd_id');
            $criteria->addSelectColumn($alias . '.atasan_id');
            $criteria->addSelectColumn($alias . '.is_atasan');
            $criteria->addSelectColumn($alias . '.level_struktural');
            $criteria->addSelectColumn($alias . '.level_penilaian');
            $criteria->addSelectColumn($alias . '.password');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.username_eproject');
            $criteria->addSelectColumn($alias . '.tgl_masuk');
            $criteria->addSelectColumn($alias . '.tgl_keluar');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.deleted_at');
            $criteria->addSelectColumn($alias . '.is_out');
            $criteria->addSelectColumn($alias . '.is_ka_uptd');
            $criteria->addSelectColumn($alias . '.is_struktural');
            $criteria->addSelectColumn($alias . '.is_kunci_aktifitas');
            $criteria->addSelectColumn($alias . '.is_only_skp');
            $criteria->addSelectColumn($alias . '.jabatan_struktural_id');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(OldPegawaiTableMap::DATABASE_NAME)->getTable(OldPegawaiTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(OldPegawaiTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(OldPegawaiTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new OldPegawaiTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a OldPegawai or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or OldPegawai object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldPegawaiTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \CoreBundle\Model\OldEperformance\OldPegawai) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(OldPegawaiTableMap::DATABASE_NAME);
            $criteria->add(OldPegawaiTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = OldPegawaiQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            OldPegawaiTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                OldPegawaiTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the eperformance.pegawai table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return OldPegawaiQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a OldPegawai or Criteria object.
     *
     * @param mixed               $criteria Criteria or OldPegawai object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldPegawaiTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from OldPegawai object
        }

        if ($criteria->containsKey(OldPegawaiTableMap::COL_ID) && $criteria->keyContainsValue(OldPegawaiTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.OldPegawaiTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = OldPegawaiQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // OldPegawaiTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
OldPegawaiTableMap::buildTableMap();

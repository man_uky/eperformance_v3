<?php

namespace CoreBundle\Model\OldEperformance\Map;

use CoreBundle\Model\OldEperformance\OldPerilakuHasil;
use CoreBundle\Model\OldEperformance\OldPerilakuHasilQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'eperformance.skp_perilaku_kerja_hasil' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class OldPerilakuHasilTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.CoreBundle.Model.OldEperformance.Map.OldPerilakuHasilTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'old_eperformance';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'eperformance.skp_perilaku_kerja_hasil';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\CoreBundle\\Model\\OldEperformance\\OldPerilakuHasil';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.CoreBundle.Model.OldEperformance.OldPerilakuHasil';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the id field
     */
    const COL_ID = 'eperformance.skp_perilaku_kerja_hasil.id';

    /**
     * the column name for the pegawai_id field
     */
    const COL_PEGAWAI_ID = 'eperformance.skp_perilaku_kerja_hasil.pegawai_id';

    /**
     * the column name for the skp_perilaku_kerja_tes_id field
     */
    const COL_SKP_PERILAKU_KERJA_TES_ID = 'eperformance.skp_perilaku_kerja_hasil.skp_perilaku_kerja_tes_id';

    /**
     * the column name for the skp_perilaku_kerja_aspek_id field
     */
    const COL_SKP_PERILAKU_KERJA_ASPEK_ID = 'eperformance.skp_perilaku_kerja_hasil.skp_perilaku_kerja_aspek_id';

    /**
     * the column name for the mean field
     */
    const COL_MEAN = 'eperformance.skp_perilaku_kerja_hasil.mean';

    /**
     * the column name for the standar_deviasi field
     */
    const COL_STANDAR_DEVIASI = 'eperformance.skp_perilaku_kerja_hasil.standar_deviasi';

    /**
     * the column name for the icc field
     */
    const COL_ICC = 'eperformance.skp_perilaku_kerja_hasil.icc';

    /**
     * the column name for the rsp field
     */
    const COL_RSP = 'eperformance.skp_perilaku_kerja_hasil.rsp';

    /**
     * the column name for the rsrk field
     */
    const COL_RSRK = 'eperformance.skp_perilaku_kerja_hasil.rsrk';

    /**
     * the column name for the spk field
     */
    const COL_SPK = 'eperformance.skp_perilaku_kerja_hasil.spk';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'PegawaiId', 'PerilakuTesId', 'PerilakuAspekId', 'Mean', 'StandarDeviasi', 'Icc', 'Rsp', 'Rsrk', 'Spk', ),
        self::TYPE_CAMELNAME     => array('id', 'pegawaiId', 'perilakuTesId', 'perilakuAspekId', 'mean', 'standarDeviasi', 'icc', 'rsp', 'rsrk', 'spk', ),
        self::TYPE_COLNAME       => array(OldPerilakuHasilTableMap::COL_ID, OldPerilakuHasilTableMap::COL_PEGAWAI_ID, OldPerilakuHasilTableMap::COL_SKP_PERILAKU_KERJA_TES_ID, OldPerilakuHasilTableMap::COL_SKP_PERILAKU_KERJA_ASPEK_ID, OldPerilakuHasilTableMap::COL_MEAN, OldPerilakuHasilTableMap::COL_STANDAR_DEVIASI, OldPerilakuHasilTableMap::COL_ICC, OldPerilakuHasilTableMap::COL_RSP, OldPerilakuHasilTableMap::COL_RSRK, OldPerilakuHasilTableMap::COL_SPK, ),
        self::TYPE_FIELDNAME     => array('id', 'pegawai_id', 'skp_perilaku_kerja_tes_id', 'skp_perilaku_kerja_aspek_id', 'mean', 'standar_deviasi', 'icc', 'rsp', 'rsrk', 'spk', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'PegawaiId' => 1, 'PerilakuTesId' => 2, 'PerilakuAspekId' => 3, 'Mean' => 4, 'StandarDeviasi' => 5, 'Icc' => 6, 'Rsp' => 7, 'Rsrk' => 8, 'Spk' => 9, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'pegawaiId' => 1, 'perilakuTesId' => 2, 'perilakuAspekId' => 3, 'mean' => 4, 'standarDeviasi' => 5, 'icc' => 6, 'rsp' => 7, 'rsrk' => 8, 'spk' => 9, ),
        self::TYPE_COLNAME       => array(OldPerilakuHasilTableMap::COL_ID => 0, OldPerilakuHasilTableMap::COL_PEGAWAI_ID => 1, OldPerilakuHasilTableMap::COL_SKP_PERILAKU_KERJA_TES_ID => 2, OldPerilakuHasilTableMap::COL_SKP_PERILAKU_KERJA_ASPEK_ID => 3, OldPerilakuHasilTableMap::COL_MEAN => 4, OldPerilakuHasilTableMap::COL_STANDAR_DEVIASI => 5, OldPerilakuHasilTableMap::COL_ICC => 6, OldPerilakuHasilTableMap::COL_RSP => 7, OldPerilakuHasilTableMap::COL_RSRK => 8, OldPerilakuHasilTableMap::COL_SPK => 9, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'pegawai_id' => 1, 'skp_perilaku_kerja_tes_id' => 2, 'skp_perilaku_kerja_aspek_id' => 3, 'mean' => 4, 'standar_deviasi' => 5, 'icc' => 6, 'rsp' => 7, 'rsrk' => 8, 'spk' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('eperformance.skp_perilaku_kerja_hasil');
        $this->setPhpName('OldPerilakuHasil');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\CoreBundle\\Model\\OldEperformance\\OldPerilakuHasil');
        $this->setPackage('src.CoreBundle.Model.OldEperformance');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('eperformance.skp_perilaku_kerja_hasil_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('pegawai_id', 'PegawaiId', 'INTEGER', 'eperformance.pegawai', 'id', false, null, null);
        $this->addForeignKey('skp_perilaku_kerja_tes_id', 'PerilakuTesId', 'INTEGER', 'eperformance.skp_perilaku_kerja_tes', 'id', false, null, null);
        $this->addForeignKey('skp_perilaku_kerja_aspek_id', 'PerilakuAspekId', 'SMALLINT', 'eperformance.skp_perilaku_kerja_aspek', 'id', false, null, null);
        $this->addColumn('mean', 'Mean', 'NUMERIC', false, null, null);
        $this->addColumn('standar_deviasi', 'StandarDeviasi', 'NUMERIC', false, null, null);
        $this->addColumn('icc', 'Icc', 'NUMERIC', false, null, null);
        $this->addColumn('rsp', 'Rsp', 'NUMERIC', false, null, null);
        $this->addColumn('rsrk', 'Rsrk', 'NUMERIC', false, null, null);
        $this->addColumn('spk', 'Spk', 'NUMERIC', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('OldPegawai', '\\CoreBundle\\Model\\OldEperformance\\OldPegawai', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':pegawai_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('OldPerilakuTes', '\\CoreBundle\\Model\\OldEperformance\\OldPerilakuTes', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':skp_perilaku_kerja_tes_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('OldPerilakuAspek', '\\CoreBundle\\Model\\OldEperformance\\OldPerilakuAspek', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':skp_perilaku_kerja_aspek_id',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'query_cache' => array('backend' => 'apc', 'lifetime' => '7200', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? OldPerilakuHasilTableMap::CLASS_DEFAULT : OldPerilakuHasilTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (OldPerilakuHasil object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = OldPerilakuHasilTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = OldPerilakuHasilTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + OldPerilakuHasilTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = OldPerilakuHasilTableMap::OM_CLASS;
            /** @var OldPerilakuHasil $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            OldPerilakuHasilTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = OldPerilakuHasilTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = OldPerilakuHasilTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var OldPerilakuHasil $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                OldPerilakuHasilTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(OldPerilakuHasilTableMap::COL_ID);
            $criteria->addSelectColumn(OldPerilakuHasilTableMap::COL_PEGAWAI_ID);
            $criteria->addSelectColumn(OldPerilakuHasilTableMap::COL_SKP_PERILAKU_KERJA_TES_ID);
            $criteria->addSelectColumn(OldPerilakuHasilTableMap::COL_SKP_PERILAKU_KERJA_ASPEK_ID);
            $criteria->addSelectColumn(OldPerilakuHasilTableMap::COL_MEAN);
            $criteria->addSelectColumn(OldPerilakuHasilTableMap::COL_STANDAR_DEVIASI);
            $criteria->addSelectColumn(OldPerilakuHasilTableMap::COL_ICC);
            $criteria->addSelectColumn(OldPerilakuHasilTableMap::COL_RSP);
            $criteria->addSelectColumn(OldPerilakuHasilTableMap::COL_RSRK);
            $criteria->addSelectColumn(OldPerilakuHasilTableMap::COL_SPK);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.pegawai_id');
            $criteria->addSelectColumn($alias . '.skp_perilaku_kerja_tes_id');
            $criteria->addSelectColumn($alias . '.skp_perilaku_kerja_aspek_id');
            $criteria->addSelectColumn($alias . '.mean');
            $criteria->addSelectColumn($alias . '.standar_deviasi');
            $criteria->addSelectColumn($alias . '.icc');
            $criteria->addSelectColumn($alias . '.rsp');
            $criteria->addSelectColumn($alias . '.rsrk');
            $criteria->addSelectColumn($alias . '.spk');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(OldPerilakuHasilTableMap::DATABASE_NAME)->getTable(OldPerilakuHasilTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(OldPerilakuHasilTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(OldPerilakuHasilTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new OldPerilakuHasilTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a OldPerilakuHasil or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or OldPerilakuHasil object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldPerilakuHasilTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \CoreBundle\Model\OldEperformance\OldPerilakuHasil) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(OldPerilakuHasilTableMap::DATABASE_NAME);
            $criteria->add(OldPerilakuHasilTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = OldPerilakuHasilQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            OldPerilakuHasilTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                OldPerilakuHasilTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the eperformance.skp_perilaku_kerja_hasil table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return OldPerilakuHasilQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a OldPerilakuHasil or Criteria object.
     *
     * @param mixed               $criteria Criteria or OldPerilakuHasil object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldPerilakuHasilTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from OldPerilakuHasil object
        }

        if ($criteria->containsKey(OldPerilakuHasilTableMap::COL_ID) && $criteria->keyContainsValue(OldPerilakuHasilTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.OldPerilakuHasilTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = OldPerilakuHasilQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // OldPerilakuHasilTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
OldPerilakuHasilTableMap::buildTableMap();

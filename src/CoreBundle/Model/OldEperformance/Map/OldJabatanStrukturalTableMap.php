<?php

namespace CoreBundle\Model\OldEperformance\Map;

use CoreBundle\Model\OldEperformance\OldJabatanStruktural;
use CoreBundle\Model\OldEperformance\OldJabatanStrukturalQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'eperformance.jabatan_struktural' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class OldJabatanStrukturalTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.CoreBundle.Model.OldEperformance.Map.OldJabatanStrukturalTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'old_eperformance';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'eperformance.jabatan_struktural';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\CoreBundle\\Model\\OldEperformance\\OldJabatanStruktural';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.CoreBundle.Model.OldEperformance.OldJabatanStruktural';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the id field
     */
    const COL_ID = 'eperformance.jabatan_struktural.id';

    /**
     * the column name for the nama field
     */
    const COL_NAMA = 'eperformance.jabatan_struktural.nama';

    /**
     * the column name for the eselon field
     */
    const COL_ESELON = 'eperformance.jabatan_struktural.eselon';

    /**
     * the column name for the jenis_indikator_kinerja field
     */
    const COL_JENIS_INDIKATOR_KINERJA = 'eperformance.jabatan_struktural.jenis_indikator_kinerja';

    /**
     * the column name for the skpd_id field
     */
    const COL_SKPD_ID = 'eperformance.jabatan_struktural.skpd_id';

    /**
     * the column name for the nomer field
     */
    const COL_NOMER = 'eperformance.jabatan_struktural.nomer';

    /**
     * the column name for the is_asisten field
     */
    const COL_IS_ASISTEN = 'eperformance.jabatan_struktural.is_asisten';

    /**
     * the column name for the nomer_urut field
     */
    const COL_NOMER_URUT = 'eperformance.jabatan_struktural.nomer_urut';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Nama', 'Eselon', 'JenisIndikatorKinerja', 'SkpdId', 'Nomer', 'IsAsisten', 'NomerUrut', ),
        self::TYPE_CAMELNAME     => array('id', 'nama', 'eselon', 'jenisIndikatorKinerja', 'skpdId', 'nomer', 'isAsisten', 'nomerUrut', ),
        self::TYPE_COLNAME       => array(OldJabatanStrukturalTableMap::COL_ID, OldJabatanStrukturalTableMap::COL_NAMA, OldJabatanStrukturalTableMap::COL_ESELON, OldJabatanStrukturalTableMap::COL_JENIS_INDIKATOR_KINERJA, OldJabatanStrukturalTableMap::COL_SKPD_ID, OldJabatanStrukturalTableMap::COL_NOMER, OldJabatanStrukturalTableMap::COL_IS_ASISTEN, OldJabatanStrukturalTableMap::COL_NOMER_URUT, ),
        self::TYPE_FIELDNAME     => array('id', 'nama', 'eselon', 'jenis_indikator_kinerja', 'skpd_id', 'nomer', 'is_asisten', 'nomer_urut', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Nama' => 1, 'Eselon' => 2, 'JenisIndikatorKinerja' => 3, 'SkpdId' => 4, 'Nomer' => 5, 'IsAsisten' => 6, 'NomerUrut' => 7, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'nama' => 1, 'eselon' => 2, 'jenisIndikatorKinerja' => 3, 'skpdId' => 4, 'nomer' => 5, 'isAsisten' => 6, 'nomerUrut' => 7, ),
        self::TYPE_COLNAME       => array(OldJabatanStrukturalTableMap::COL_ID => 0, OldJabatanStrukturalTableMap::COL_NAMA => 1, OldJabatanStrukturalTableMap::COL_ESELON => 2, OldJabatanStrukturalTableMap::COL_JENIS_INDIKATOR_KINERJA => 3, OldJabatanStrukturalTableMap::COL_SKPD_ID => 4, OldJabatanStrukturalTableMap::COL_NOMER => 5, OldJabatanStrukturalTableMap::COL_IS_ASISTEN => 6, OldJabatanStrukturalTableMap::COL_NOMER_URUT => 7, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'nama' => 1, 'eselon' => 2, 'jenis_indikator_kinerja' => 3, 'skpd_id' => 4, 'nomer' => 5, 'is_asisten' => 6, 'nomer_urut' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('eperformance.jabatan_struktural');
        $this->setPhpName('OldJabatanStruktural');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\CoreBundle\\Model\\OldEperformance\\OldJabatanStruktural');
        $this->setPackage('src.CoreBundle.Model.OldEperformance');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', false, 50, null);
        $this->addColumn('eselon', 'Eselon', 'VARCHAR', false, 5, null);
        $this->addColumn('jenis_indikator_kinerja', 'JenisIndikatorKinerja', 'SMALLINT', false, null, null);
        $this->addForeignKey('skpd_id', 'SkpdId', 'INTEGER', 'eperformance.master_skpd', 'skpd_id', false, null, null);
        $this->addColumn('nomer', 'Nomer', 'SMALLINT', false, null, null);
        $this->addColumn('is_asisten', 'IsAsisten', 'BOOLEAN', false, null, null);
        $this->addColumn('nomer_urut', 'NomerUrut', 'SMALLINT', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('OldMasterSkpd', '\\CoreBundle\\Model\\OldEperformance\\OldMasterSkpd', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':skpd_id',
    1 => ':skpd_id',
  ),
), null, null, null, false);
        $this->addRelation('OldPegawai', '\\CoreBundle\\Model\\OldEperformance\\OldPegawai', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':jabatan_struktural_id',
    1 => ':id',
  ),
), null, null, 'OldPegawais', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'query_cache' => array('backend' => 'apc', 'lifetime' => '7200', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? OldJabatanStrukturalTableMap::CLASS_DEFAULT : OldJabatanStrukturalTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (OldJabatanStruktural object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = OldJabatanStrukturalTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = OldJabatanStrukturalTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + OldJabatanStrukturalTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = OldJabatanStrukturalTableMap::OM_CLASS;
            /** @var OldJabatanStruktural $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            OldJabatanStrukturalTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = OldJabatanStrukturalTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = OldJabatanStrukturalTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var OldJabatanStruktural $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                OldJabatanStrukturalTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(OldJabatanStrukturalTableMap::COL_ID);
            $criteria->addSelectColumn(OldJabatanStrukturalTableMap::COL_NAMA);
            $criteria->addSelectColumn(OldJabatanStrukturalTableMap::COL_ESELON);
            $criteria->addSelectColumn(OldJabatanStrukturalTableMap::COL_JENIS_INDIKATOR_KINERJA);
            $criteria->addSelectColumn(OldJabatanStrukturalTableMap::COL_SKPD_ID);
            $criteria->addSelectColumn(OldJabatanStrukturalTableMap::COL_NOMER);
            $criteria->addSelectColumn(OldJabatanStrukturalTableMap::COL_IS_ASISTEN);
            $criteria->addSelectColumn(OldJabatanStrukturalTableMap::COL_NOMER_URUT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.nama');
            $criteria->addSelectColumn($alias . '.eselon');
            $criteria->addSelectColumn($alias . '.jenis_indikator_kinerja');
            $criteria->addSelectColumn($alias . '.skpd_id');
            $criteria->addSelectColumn($alias . '.nomer');
            $criteria->addSelectColumn($alias . '.is_asisten');
            $criteria->addSelectColumn($alias . '.nomer_urut');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(OldJabatanStrukturalTableMap::DATABASE_NAME)->getTable(OldJabatanStrukturalTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(OldJabatanStrukturalTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(OldJabatanStrukturalTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new OldJabatanStrukturalTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a OldJabatanStruktural or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or OldJabatanStruktural object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldJabatanStrukturalTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \CoreBundle\Model\OldEperformance\OldJabatanStruktural) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(OldJabatanStrukturalTableMap::DATABASE_NAME);
            $criteria->add(OldJabatanStrukturalTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = OldJabatanStrukturalQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            OldJabatanStrukturalTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                OldJabatanStrukturalTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the eperformance.jabatan_struktural table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return OldJabatanStrukturalQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a OldJabatanStruktural or Criteria object.
     *
     * @param mixed               $criteria Criteria or OldJabatanStruktural object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldJabatanStrukturalTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from OldJabatanStruktural object
        }


        // Set the correct dbName
        $query = OldJabatanStrukturalQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // OldJabatanStrukturalTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
OldJabatanStrukturalTableMap::buildTableMap();

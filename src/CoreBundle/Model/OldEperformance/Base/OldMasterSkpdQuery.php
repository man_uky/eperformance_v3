<?php

namespace CoreBundle\Model\OldEperformance\Base;

use \Exception;
use \PDO;
use CoreBundle\Model\OldEperformance\OldMasterSkpd as ChildOldMasterSkpd;
use CoreBundle\Model\OldEperformance\OldMasterSkpdQuery as ChildOldMasterSkpdQuery;
use CoreBundle\Model\OldEperformance\Map\OldMasterSkpdTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'eperformance.master_skpd' table.
 *
 *
 *
 * @method     ChildOldMasterSkpdQuery orderBySkpdId($order = Criteria::ASC) Order by the skpd_id column
 * @method     ChildOldMasterSkpdQuery orderBySkpdKode($order = Criteria::ASC) Order by the skpd_kode column
 * @method     ChildOldMasterSkpdQuery orderBySkpdNama($order = Criteria::ASC) Order by the skpd_nama column
 * @method     ChildOldMasterSkpdQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 *
 * @method     ChildOldMasterSkpdQuery groupBySkpdId() Group by the skpd_id column
 * @method     ChildOldMasterSkpdQuery groupBySkpdKode() Group by the skpd_kode column
 * @method     ChildOldMasterSkpdQuery groupBySkpdNama() Group by the skpd_nama column
 * @method     ChildOldMasterSkpdQuery groupByDeletedAt() Group by the deleted_at column
 *
 * @method     ChildOldMasterSkpdQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildOldMasterSkpdQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildOldMasterSkpdQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildOldMasterSkpdQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildOldMasterSkpdQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildOldMasterSkpdQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildOldMasterSkpdQuery leftJoinOldPerilakuTes($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldPerilakuTes relation
 * @method     ChildOldMasterSkpdQuery rightJoinOldPerilakuTes($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldPerilakuTes relation
 * @method     ChildOldMasterSkpdQuery innerJoinOldPerilakuTes($relationAlias = null) Adds a INNER JOIN clause to the query using the OldPerilakuTes relation
 *
 * @method     ChildOldMasterSkpdQuery joinWithOldPerilakuTes($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldPerilakuTes relation
 *
 * @method     ChildOldMasterSkpdQuery leftJoinWithOldPerilakuTes() Adds a LEFT JOIN clause and with to the query using the OldPerilakuTes relation
 * @method     ChildOldMasterSkpdQuery rightJoinWithOldPerilakuTes() Adds a RIGHT JOIN clause and with to the query using the OldPerilakuTes relation
 * @method     ChildOldMasterSkpdQuery innerJoinWithOldPerilakuTes() Adds a INNER JOIN clause and with to the query using the OldPerilakuTes relation
 *
 * @method     ChildOldMasterSkpdQuery leftJoinOldPegawai($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldPegawai relation
 * @method     ChildOldMasterSkpdQuery rightJoinOldPegawai($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldPegawai relation
 * @method     ChildOldMasterSkpdQuery innerJoinOldPegawai($relationAlias = null) Adds a INNER JOIN clause to the query using the OldPegawai relation
 *
 * @method     ChildOldMasterSkpdQuery joinWithOldPegawai($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldPegawai relation
 *
 * @method     ChildOldMasterSkpdQuery leftJoinWithOldPegawai() Adds a LEFT JOIN clause and with to the query using the OldPegawai relation
 * @method     ChildOldMasterSkpdQuery rightJoinWithOldPegawai() Adds a RIGHT JOIN clause and with to the query using the OldPegawai relation
 * @method     ChildOldMasterSkpdQuery innerJoinWithOldPegawai() Adds a INNER JOIN clause and with to the query using the OldPegawai relation
 *
 * @method     ChildOldMasterSkpdQuery leftJoinOldIndikatorKinerja($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldIndikatorKinerja relation
 * @method     ChildOldMasterSkpdQuery rightJoinOldIndikatorKinerja($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldIndikatorKinerja relation
 * @method     ChildOldMasterSkpdQuery innerJoinOldIndikatorKinerja($relationAlias = null) Adds a INNER JOIN clause to the query using the OldIndikatorKinerja relation
 *
 * @method     ChildOldMasterSkpdQuery joinWithOldIndikatorKinerja($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldIndikatorKinerja relation
 *
 * @method     ChildOldMasterSkpdQuery leftJoinWithOldIndikatorKinerja() Adds a LEFT JOIN clause and with to the query using the OldIndikatorKinerja relation
 * @method     ChildOldMasterSkpdQuery rightJoinWithOldIndikatorKinerja() Adds a RIGHT JOIN clause and with to the query using the OldIndikatorKinerja relation
 * @method     ChildOldMasterSkpdQuery innerJoinWithOldIndikatorKinerja() Adds a INNER JOIN clause and with to the query using the OldIndikatorKinerja relation
 *
 * @method     ChildOldMasterSkpdQuery leftJoinOldJabatanStruktural($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldJabatanStruktural relation
 * @method     ChildOldMasterSkpdQuery rightJoinOldJabatanStruktural($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldJabatanStruktural relation
 * @method     ChildOldMasterSkpdQuery innerJoinOldJabatanStruktural($relationAlias = null) Adds a INNER JOIN clause to the query using the OldJabatanStruktural relation
 *
 * @method     ChildOldMasterSkpdQuery joinWithOldJabatanStruktural($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldJabatanStruktural relation
 *
 * @method     ChildOldMasterSkpdQuery leftJoinWithOldJabatanStruktural() Adds a LEFT JOIN clause and with to the query using the OldJabatanStruktural relation
 * @method     ChildOldMasterSkpdQuery rightJoinWithOldJabatanStruktural() Adds a RIGHT JOIN clause and with to the query using the OldJabatanStruktural relation
 * @method     ChildOldMasterSkpdQuery innerJoinWithOldJabatanStruktural() Adds a INNER JOIN clause and with to the query using the OldJabatanStruktural relation
 *
 * @method     ChildOldMasterSkpdQuery leftJoinOldMonevTsp($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldMonevTsp relation
 * @method     ChildOldMasterSkpdQuery rightJoinOldMonevTsp($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldMonevTsp relation
 * @method     ChildOldMasterSkpdQuery innerJoinOldMonevTsp($relationAlias = null) Adds a INNER JOIN clause to the query using the OldMonevTsp relation
 *
 * @method     ChildOldMasterSkpdQuery joinWithOldMonevTsp($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldMonevTsp relation
 *
 * @method     ChildOldMasterSkpdQuery leftJoinWithOldMonevTsp() Adds a LEFT JOIN clause and with to the query using the OldMonevTsp relation
 * @method     ChildOldMasterSkpdQuery rightJoinWithOldMonevTsp() Adds a RIGHT JOIN clause and with to the query using the OldMonevTsp relation
 * @method     ChildOldMasterSkpdQuery innerJoinWithOldMonevTsp() Adds a INNER JOIN clause and with to the query using the OldMonevTsp relation
 *
 * @method     ChildOldMasterSkpdQuery leftJoinOldMonevTspKegiatan($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldMonevTspKegiatan relation
 * @method     ChildOldMasterSkpdQuery rightJoinOldMonevTspKegiatan($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldMonevTspKegiatan relation
 * @method     ChildOldMasterSkpdQuery innerJoinOldMonevTspKegiatan($relationAlias = null) Adds a INNER JOIN clause to the query using the OldMonevTspKegiatan relation
 *
 * @method     ChildOldMasterSkpdQuery joinWithOldMonevTspKegiatan($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldMonevTspKegiatan relation
 *
 * @method     ChildOldMasterSkpdQuery leftJoinWithOldMonevTspKegiatan() Adds a LEFT JOIN clause and with to the query using the OldMonevTspKegiatan relation
 * @method     ChildOldMasterSkpdQuery rightJoinWithOldMonevTspKegiatan() Adds a RIGHT JOIN clause and with to the query using the OldMonevTspKegiatan relation
 * @method     ChildOldMasterSkpdQuery innerJoinWithOldMonevTspKegiatan() Adds a INNER JOIN clause and with to the query using the OldMonevTspKegiatan relation
 *
 * @method     \CoreBundle\Model\OldEperformance\OldPerilakuTesQuery|\CoreBundle\Model\OldEperformance\OldPegawaiQuery|\CoreBundle\Model\OldEperformance\OldIndikatorKinerjaQuery|\CoreBundle\Model\OldEperformance\OldJabatanStrukturalQuery|\CoreBundle\Model\OldEperformance\OldMonevTspQuery|\CoreBundle\Model\OldEperformance\OldMonevTspKegiatanQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildOldMasterSkpd findOne(ConnectionInterface $con = null) Return the first ChildOldMasterSkpd matching the query
 * @method     ChildOldMasterSkpd findOneOrCreate(ConnectionInterface $con = null) Return the first ChildOldMasterSkpd matching the query, or a new ChildOldMasterSkpd object populated from the query conditions when no match is found
 *
 * @method     ChildOldMasterSkpd findOneBySkpdId(int $skpd_id) Return the first ChildOldMasterSkpd filtered by the skpd_id column
 * @method     ChildOldMasterSkpd findOneBySkpdKode(string $skpd_kode) Return the first ChildOldMasterSkpd filtered by the skpd_kode column
 * @method     ChildOldMasterSkpd findOneBySkpdNama(string $skpd_nama) Return the first ChildOldMasterSkpd filtered by the skpd_nama column
 * @method     ChildOldMasterSkpd findOneByDeletedAt(string $deleted_at) Return the first ChildOldMasterSkpd filtered by the deleted_at column *

 * @method     ChildOldMasterSkpd requirePk($key, ConnectionInterface $con = null) Return the ChildOldMasterSkpd by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMasterSkpd requireOne(ConnectionInterface $con = null) Return the first ChildOldMasterSkpd matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOldMasterSkpd requireOneBySkpdId(int $skpd_id) Return the first ChildOldMasterSkpd filtered by the skpd_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMasterSkpd requireOneBySkpdKode(string $skpd_kode) Return the first ChildOldMasterSkpd filtered by the skpd_kode column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMasterSkpd requireOneBySkpdNama(string $skpd_nama) Return the first ChildOldMasterSkpd filtered by the skpd_nama column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMasterSkpd requireOneByDeletedAt(string $deleted_at) Return the first ChildOldMasterSkpd filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOldMasterSkpd[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildOldMasterSkpd objects based on current ModelCriteria
 * @method     ChildOldMasterSkpd[]|ObjectCollection findBySkpdId(int $skpd_id) Return ChildOldMasterSkpd objects filtered by the skpd_id column
 * @method     ChildOldMasterSkpd[]|ObjectCollection findBySkpdKode(string $skpd_kode) Return ChildOldMasterSkpd objects filtered by the skpd_kode column
 * @method     ChildOldMasterSkpd[]|ObjectCollection findBySkpdNama(string $skpd_nama) Return ChildOldMasterSkpd objects filtered by the skpd_nama column
 * @method     ChildOldMasterSkpd[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildOldMasterSkpd objects filtered by the deleted_at column
 * @method     ChildOldMasterSkpd[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class OldMasterSkpdQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \CoreBundle\Model\OldEperformance\Base\OldMasterSkpdQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'old_eperformance', $modelName = '\\CoreBundle\\Model\\OldEperformance\\OldMasterSkpd', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildOldMasterSkpdQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildOldMasterSkpdQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildOldMasterSkpdQuery) {
            return $criteria;
        }
        $query = new ChildOldMasterSkpdQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildOldMasterSkpd|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(OldMasterSkpdTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = OldMasterSkpdTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldMasterSkpd A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT skpd_id, skpd_kode, skpd_nama, deleted_at FROM eperformance.master_skpd WHERE skpd_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildOldMasterSkpd $obj */
            $obj = new ChildOldMasterSkpd();
            $obj->hydrate($row);
            OldMasterSkpdTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildOldMasterSkpd|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildOldMasterSkpdQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(OldMasterSkpdTableMap::COL_SKPD_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildOldMasterSkpdQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(OldMasterSkpdTableMap::COL_SKPD_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the skpd_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySkpdId(1234); // WHERE skpd_id = 1234
     * $query->filterBySkpdId(array(12, 34)); // WHERE skpd_id IN (12, 34)
     * $query->filterBySkpdId(array('min' => 12)); // WHERE skpd_id > 12
     * </code>
     *
     * @param     mixed $skpdId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMasterSkpdQuery The current query, for fluid interface
     */
    public function filterBySkpdId($skpdId = null, $comparison = null)
    {
        if (is_array($skpdId)) {
            $useMinMax = false;
            if (isset($skpdId['min'])) {
                $this->addUsingAlias(OldMasterSkpdTableMap::COL_SKPD_ID, $skpdId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($skpdId['max'])) {
                $this->addUsingAlias(OldMasterSkpdTableMap::COL_SKPD_ID, $skpdId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMasterSkpdTableMap::COL_SKPD_ID, $skpdId, $comparison);
    }

    /**
     * Filter the query on the skpd_kode column
     *
     * Example usage:
     * <code>
     * $query->filterBySkpdKode('fooValue');   // WHERE skpd_kode = 'fooValue'
     * $query->filterBySkpdKode('%fooValue%', Criteria::LIKE); // WHERE skpd_kode LIKE '%fooValue%'
     * </code>
     *
     * @param     string $skpdKode The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMasterSkpdQuery The current query, for fluid interface
     */
    public function filterBySkpdKode($skpdKode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($skpdKode)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMasterSkpdTableMap::COL_SKPD_KODE, $skpdKode, $comparison);
    }

    /**
     * Filter the query on the skpd_nama column
     *
     * Example usage:
     * <code>
     * $query->filterBySkpdNama('fooValue');   // WHERE skpd_nama = 'fooValue'
     * $query->filterBySkpdNama('%fooValue%', Criteria::LIKE); // WHERE skpd_nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $skpdNama The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMasterSkpdQuery The current query, for fluid interface
     */
    public function filterBySkpdNama($skpdNama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($skpdNama)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMasterSkpdTableMap::COL_SKPD_NAMA, $skpdNama, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMasterSkpdQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(OldMasterSkpdTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(OldMasterSkpdTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMasterSkpdTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldPerilakuTes object
     *
     * @param \CoreBundle\Model\OldEperformance\OldPerilakuTes|ObjectCollection $oldPerilakuTes the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildOldMasterSkpdQuery The current query, for fluid interface
     */
    public function filterByOldPerilakuTes($oldPerilakuTes, $comparison = null)
    {
        if ($oldPerilakuTes instanceof \CoreBundle\Model\OldEperformance\OldPerilakuTes) {
            return $this
                ->addUsingAlias(OldMasterSkpdTableMap::COL_SKPD_ID, $oldPerilakuTes->getSkpdId(), $comparison);
        } elseif ($oldPerilakuTes instanceof ObjectCollection) {
            return $this
                ->useOldPerilakuTesQuery()
                ->filterByPrimaryKeys($oldPerilakuTes->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOldPerilakuTes() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldPerilakuTes or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldPerilakuTes relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldMasterSkpdQuery The current query, for fluid interface
     */
    public function joinOldPerilakuTes($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldPerilakuTes');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldPerilakuTes');
        }

        return $this;
    }

    /**
     * Use the OldPerilakuTes relation OldPerilakuTes object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldPerilakuTesQuery A secondary query class using the current class as primary query
     */
    public function useOldPerilakuTesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldPerilakuTes($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldPerilakuTes', '\CoreBundle\Model\OldEperformance\OldPerilakuTesQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldPegawai object
     *
     * @param \CoreBundle\Model\OldEperformance\OldPegawai|ObjectCollection $oldPegawai the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildOldMasterSkpdQuery The current query, for fluid interface
     */
    public function filterByOldPegawai($oldPegawai, $comparison = null)
    {
        if ($oldPegawai instanceof \CoreBundle\Model\OldEperformance\OldPegawai) {
            return $this
                ->addUsingAlias(OldMasterSkpdTableMap::COL_SKPD_ID, $oldPegawai->getSkpdId(), $comparison);
        } elseif ($oldPegawai instanceof ObjectCollection) {
            return $this
                ->useOldPegawaiQuery()
                ->filterByPrimaryKeys($oldPegawai->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOldPegawai() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldPegawai or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldPegawai relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldMasterSkpdQuery The current query, for fluid interface
     */
    public function joinOldPegawai($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldPegawai');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldPegawai');
        }

        return $this;
    }

    /**
     * Use the OldPegawai relation OldPegawai object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldPegawaiQuery A secondary query class using the current class as primary query
     */
    public function useOldPegawaiQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldPegawai($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldPegawai', '\CoreBundle\Model\OldEperformance\OldPegawaiQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldIndikatorKinerja object
     *
     * @param \CoreBundle\Model\OldEperformance\OldIndikatorKinerja|ObjectCollection $oldIndikatorKinerja the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildOldMasterSkpdQuery The current query, for fluid interface
     */
    public function filterByOldIndikatorKinerja($oldIndikatorKinerja, $comparison = null)
    {
        if ($oldIndikatorKinerja instanceof \CoreBundle\Model\OldEperformance\OldIndikatorKinerja) {
            return $this
                ->addUsingAlias(OldMasterSkpdTableMap::COL_SKPD_ID, $oldIndikatorKinerja->getSkpdId(), $comparison);
        } elseif ($oldIndikatorKinerja instanceof ObjectCollection) {
            return $this
                ->useOldIndikatorKinerjaQuery()
                ->filterByPrimaryKeys($oldIndikatorKinerja->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOldIndikatorKinerja() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldIndikatorKinerja or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldIndikatorKinerja relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldMasterSkpdQuery The current query, for fluid interface
     */
    public function joinOldIndikatorKinerja($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldIndikatorKinerja');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldIndikatorKinerja');
        }

        return $this;
    }

    /**
     * Use the OldIndikatorKinerja relation OldIndikatorKinerja object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldIndikatorKinerjaQuery A secondary query class using the current class as primary query
     */
    public function useOldIndikatorKinerjaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldIndikatorKinerja($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldIndikatorKinerja', '\CoreBundle\Model\OldEperformance\OldIndikatorKinerjaQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldJabatanStruktural object
     *
     * @param \CoreBundle\Model\OldEperformance\OldJabatanStruktural|ObjectCollection $oldJabatanStruktural the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildOldMasterSkpdQuery The current query, for fluid interface
     */
    public function filterByOldJabatanStruktural($oldJabatanStruktural, $comparison = null)
    {
        if ($oldJabatanStruktural instanceof \CoreBundle\Model\OldEperformance\OldJabatanStruktural) {
            return $this
                ->addUsingAlias(OldMasterSkpdTableMap::COL_SKPD_ID, $oldJabatanStruktural->getSkpdId(), $comparison);
        } elseif ($oldJabatanStruktural instanceof ObjectCollection) {
            return $this
                ->useOldJabatanStrukturalQuery()
                ->filterByPrimaryKeys($oldJabatanStruktural->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOldJabatanStruktural() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldJabatanStruktural or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldJabatanStruktural relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldMasterSkpdQuery The current query, for fluid interface
     */
    public function joinOldJabatanStruktural($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldJabatanStruktural');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldJabatanStruktural');
        }

        return $this;
    }

    /**
     * Use the OldJabatanStruktural relation OldJabatanStruktural object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldJabatanStrukturalQuery A secondary query class using the current class as primary query
     */
    public function useOldJabatanStrukturalQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldJabatanStruktural($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldJabatanStruktural', '\CoreBundle\Model\OldEperformance\OldJabatanStrukturalQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldMonevTsp object
     *
     * @param \CoreBundle\Model\OldEperformance\OldMonevTsp|ObjectCollection $oldMonevTsp the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildOldMasterSkpdQuery The current query, for fluid interface
     */
    public function filterByOldMonevTsp($oldMonevTsp, $comparison = null)
    {
        if ($oldMonevTsp instanceof \CoreBundle\Model\OldEperformance\OldMonevTsp) {
            return $this
                ->addUsingAlias(OldMasterSkpdTableMap::COL_SKPD_ID, $oldMonevTsp->getUnitId(), $comparison);
        } elseif ($oldMonevTsp instanceof ObjectCollection) {
            return $this
                ->useOldMonevTspQuery()
                ->filterByPrimaryKeys($oldMonevTsp->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOldMonevTsp() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldMonevTsp or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldMonevTsp relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldMasterSkpdQuery The current query, for fluid interface
     */
    public function joinOldMonevTsp($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldMonevTsp');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldMonevTsp');
        }

        return $this;
    }

    /**
     * Use the OldMonevTsp relation OldMonevTsp object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldMonevTspQuery A secondary query class using the current class as primary query
     */
    public function useOldMonevTspQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinOldMonevTsp($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldMonevTsp', '\CoreBundle\Model\OldEperformance\OldMonevTspQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldMonevTspKegiatan object
     *
     * @param \CoreBundle\Model\OldEperformance\OldMonevTspKegiatan|ObjectCollection $oldMonevTspKegiatan the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildOldMasterSkpdQuery The current query, for fluid interface
     */
    public function filterByOldMonevTspKegiatan($oldMonevTspKegiatan, $comparison = null)
    {
        if ($oldMonevTspKegiatan instanceof \CoreBundle\Model\OldEperformance\OldMonevTspKegiatan) {
            return $this
                ->addUsingAlias(OldMasterSkpdTableMap::COL_SKPD_ID, $oldMonevTspKegiatan->getUnitId(), $comparison);
        } elseif ($oldMonevTspKegiatan instanceof ObjectCollection) {
            return $this
                ->useOldMonevTspKegiatanQuery()
                ->filterByPrimaryKeys($oldMonevTspKegiatan->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOldMonevTspKegiatan() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldMonevTspKegiatan or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldMonevTspKegiatan relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldMasterSkpdQuery The current query, for fluid interface
     */
    public function joinOldMonevTspKegiatan($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldMonevTspKegiatan');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldMonevTspKegiatan');
        }

        return $this;
    }

    /**
     * Use the OldMonevTspKegiatan relation OldMonevTspKegiatan object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldMonevTspKegiatanQuery A secondary query class using the current class as primary query
     */
    public function useOldMonevTspKegiatanQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinOldMonevTspKegiatan($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldMonevTspKegiatan', '\CoreBundle\Model\OldEperformance\OldMonevTspKegiatanQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildOldMasterSkpd $oldMasterSkpd Object to remove from the list of results
     *
     * @return $this|ChildOldMasterSkpdQuery The current query, for fluid interface
     */
    public function prune($oldMasterSkpd = null)
    {
        if ($oldMasterSkpd) {
            $this->addUsingAlias(OldMasterSkpdTableMap::COL_SKPD_ID, $oldMasterSkpd->getSkpdId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the eperformance.master_skpd table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldMasterSkpdTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            OldMasterSkpdTableMap::clearInstancePool();
            OldMasterSkpdTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldMasterSkpdTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(OldMasterSkpdTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            OldMasterSkpdTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            OldMasterSkpdTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 7200)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(OldMasterSkpdTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(OldMasterSkpdTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }


        return $con->getDataFetcher($stmt);
    }

} // OldMasterSkpdQuery

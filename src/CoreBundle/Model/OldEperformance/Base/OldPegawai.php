<?php

namespace CoreBundle\Model\OldEperformance\Base;

use \DateTime;
use \Exception;
use \PDO;
use CoreBundle\Model\OldEperformance\OldIndikatorKinerja as ChildOldIndikatorKinerja;
use CoreBundle\Model\OldEperformance\OldIndikatorKinerjaQuery as ChildOldIndikatorKinerjaQuery;
use CoreBundle\Model\OldEperformance\OldJabatanStruktural as ChildOldJabatanStruktural;
use CoreBundle\Model\OldEperformance\OldJabatanStrukturalQuery as ChildOldJabatanStrukturalQuery;
use CoreBundle\Model\OldEperformance\OldKegiatanPegawai as ChildOldKegiatanPegawai;
use CoreBundle\Model\OldEperformance\OldKegiatanPegawaiQuery as ChildOldKegiatanPegawaiQuery;
use CoreBundle\Model\OldEperformance\OldMasterSkpd as ChildOldMasterSkpd;
use CoreBundle\Model\OldEperformance\OldMasterSkpdQuery as ChildOldMasterSkpdQuery;
use CoreBundle\Model\OldEperformance\OldPegawai as ChildOldPegawai;
use CoreBundle\Model\OldEperformance\OldPegawaiQuery as ChildOldPegawaiQuery;
use CoreBundle\Model\OldEperformance\OldPerilakuHasil as ChildOldPerilakuHasil;
use CoreBundle\Model\OldEperformance\OldPerilakuHasilQuery as ChildOldPerilakuHasilQuery;
use CoreBundle\Model\OldEperformance\Map\OldIndikatorKinerjaTableMap;
use CoreBundle\Model\OldEperformance\Map\OldKegiatanPegawaiTableMap;
use CoreBundle\Model\OldEperformance\Map\OldPegawaiTableMap;
use CoreBundle\Model\OldEperformance\Map\OldPerilakuHasilTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'eperformance.pegawai' table.
 *
 *
 *
 * @package    propel.generator.src.CoreBundle.Model.OldEperformance.Base
 */
abstract class OldPegawai implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\CoreBundle\\Model\\OldEperformance\\Map\\OldPegawaiTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the nip field.
     *
     * @var        string
     */
    protected $nip;

    /**
     * The value for the nama field.
     *
     * @var        string
     */
    protected $nama;

    /**
     * The value for the golongan field.
     *
     * @var        string
     */
    protected $golongan;

    /**
     * The value for the tmt field.
     * terhitung mulai tanggal -> tgl masuk
     * @var        DateTime
     */
    protected $tmt;

    /**
     * The value for the pendidikan_terakhir field.
     *
     * @var        string
     */
    protected $pendidikan_terakhir;

    /**
     * The value for the skpd_id field.
     *
     * @var        int
     */
    protected $skpd_id;

    /**
     * The value for the atasan_id field.
     *
     * @var        int
     */
    protected $atasan_id;

    /**
     * The value for the is_atasan field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_atasan;

    /**
     * The value for the level_struktural field.
     *
     * @var        int
     */
    protected $level_struktural;

    /**
     * The value for the level_penilaian field.
     *
     * @var        int
     */
    protected $level_penilaian;

    /**
     * The value for the password field.
     *
     * @var        string
     */
    protected $password;

    /**
     * The value for the user_id field.
     *
     * @var        int
     */
    protected $user_id;

    /**
     * The value for the username_eproject field.
     *
     * @var        string
     */
    protected $username_eproject;

    /**
     * The value for the tgl_masuk field.
     * tgl masuk SKPD baru
     * @var        DateTime
     */
    protected $tgl_masuk;

    /**
     * The value for the tgl_keluar field.
     * tgl keluar
     * @var        DateTime
     */
    protected $tgl_keluar;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * The value for the deleted_at field.
     *
     * @var        DateTime
     */
    protected $deleted_at;

    /**
     * The value for the is_out field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_out;

    /**
     * The value for the is_ka_uptd field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_ka_uptd;

    /**
     * The value for the is_struktural field.
     * digunakan untuk pegawai kecamatan
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_struktural;

    /**
     * The value for the is_kunci_aktifitas field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $is_kunci_aktifitas;

    /**
     * The value for the is_only_skp field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_only_skp;

    /**
     * The value for the jabatan_struktural_id field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $jabatan_struktural_id;

    /**
     * @var        ChildOldMasterSkpd
     */
    protected $aOldMasterSkpd;

    /**
     * @var        ChildOldPegawai
     */
    protected $aOldPegawai;

    /**
     * @var        ChildOldJabatanStruktural
     */
    protected $aOldJabatanStruktural;

    /**
     * @var        ObjectCollection|ChildOldPerilakuHasil[] Collection to store aggregation of ChildOldPerilakuHasil objects.
     */
    protected $collOldPerilakuHasils;
    protected $collOldPerilakuHasilsPartial;

    /**
     * @var        ObjectCollection|ChildOldPegawai[] Collection to store aggregation of ChildOldPegawai objects.
     */
    protected $collOldPegawaisRelatedById;
    protected $collOldPegawaisRelatedByIdPartial;

    /**
     * @var        ObjectCollection|ChildOldIndikatorKinerja[] Collection to store aggregation of ChildOldIndikatorKinerja objects.
     */
    protected $collOldIndikatorKinerjas;
    protected $collOldIndikatorKinerjasPartial;

    /**
     * @var        ObjectCollection|ChildOldKegiatanPegawai[] Collection to store aggregation of ChildOldKegiatanPegawai objects.
     */
    protected $collOldKegiatanPegawais;
    protected $collOldKegiatanPegawaisPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildOldPerilakuHasil[]
     */
    protected $oldPerilakuHasilsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildOldPegawai[]
     */
    protected $oldPegawaisRelatedByIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildOldIndikatorKinerja[]
     */
    protected $oldIndikatorKinerjasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildOldKegiatanPegawai[]
     */
    protected $oldKegiatanPegawaisScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->is_atasan = false;
        $this->is_out = false;
        $this->is_ka_uptd = false;
        $this->is_struktural = false;
        $this->is_kunci_aktifitas = true;
        $this->is_only_skp = false;
        $this->jabatan_struktural_id = 0;
    }

    /**
     * Initializes internal state of CoreBundle\Model\OldEperformance\Base\OldPegawai object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>OldPegawai</code> instance.  If
     * <code>obj</code> is an instance of <code>OldPegawai</code>, delegates to
     * <code>equals(OldPegawai)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|OldPegawai The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [nip] column value.
     *
     * @return string
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * Get the [nama] column value.
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [golongan] column value.
     *
     * @return string
     */
    public function getGolongan()
    {
        return $this->golongan;
    }

    /**
     * Get the [optionally formatted] temporal [tmt] column value.
     * terhitung mulai tanggal -> tgl masuk
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getTmt($format = NULL)
    {
        if ($format === null) {
            return $this->tmt;
        } else {
            return $this->tmt instanceof \DateTimeInterface ? $this->tmt->format($format) : null;
        }
    }

    /**
     * Get the [pendidikan_terakhir] column value.
     *
     * @return string
     */
    public function getPendidikanTerakhir()
    {
        return $this->pendidikan_terakhir;
    }

    /**
     * Get the [skpd_id] column value.
     *
     * @return int
     */
    public function getSkpdId()
    {
        return $this->skpd_id;
    }

    /**
     * Get the [atasan_id] column value.
     *
     * @return int
     */
    public function getAtasanId()
    {
        return $this->atasan_id;
    }

    /**
     * Get the [is_atasan] column value.
     *
     * @return boolean
     */
    public function getIsAtasan()
    {
        return $this->is_atasan;
    }

    /**
     * Get the [is_atasan] column value.
     *
     * @return boolean
     */
    public function isAtasan()
    {
        return $this->getIsAtasan();
    }

    /**
     * Get the [level_struktural] column value.
     *
     * @return int
     */
    public function getLevelStruktural()
    {
        return $this->level_struktural;
    }

    /**
     * Get the [level_penilaian] column value.
     *
     * @return int
     */
    public function getLevelPenilaian()
    {
        return $this->level_penilaian;
    }

    /**
     * Get the [password] column value.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get the [user_id] column value.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Get the [username_eproject] column value.
     *
     * @return string
     */
    public function getUsernameEproject()
    {
        return $this->username_eproject;
    }

    /**
     * Get the [optionally formatted] temporal [tgl_masuk] column value.
     * tgl masuk SKPD baru
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getTglMasuk($format = NULL)
    {
        if ($format === null) {
            return $this->tgl_masuk;
        } else {
            return $this->tgl_masuk instanceof \DateTimeInterface ? $this->tgl_masuk->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [tgl_keluar] column value.
     * tgl keluar
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getTglKeluar($format = NULL)
    {
        if ($format === null) {
            return $this->tgl_keluar;
        } else {
            return $this->tgl_keluar instanceof \DateTimeInterface ? $this->tgl_keluar->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [deleted_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDeletedAt($format = NULL)
    {
        if ($format === null) {
            return $this->deleted_at;
        } else {
            return $this->deleted_at instanceof \DateTimeInterface ? $this->deleted_at->format($format) : null;
        }
    }

    /**
     * Get the [is_out] column value.
     *
     * @return boolean
     */
    public function getIsOut()
    {
        return $this->is_out;
    }

    /**
     * Get the [is_out] column value.
     *
     * @return boolean
     */
    public function isOut()
    {
        return $this->getIsOut();
    }

    /**
     * Get the [is_ka_uptd] column value.
     *
     * @return boolean
     */
    public function getIsKaUptd()
    {
        return $this->is_ka_uptd;
    }

    /**
     * Get the [is_ka_uptd] column value.
     *
     * @return boolean
     */
    public function isKaUptd()
    {
        return $this->getIsKaUptd();
    }

    /**
     * Get the [is_struktural] column value.
     * digunakan untuk pegawai kecamatan
     * @return boolean
     */
    public function getIsStruktural()
    {
        return $this->is_struktural;
    }

    /**
     * Get the [is_struktural] column value.
     * digunakan untuk pegawai kecamatan
     * @return boolean
     */
    public function isStruktural()
    {
        return $this->getIsStruktural();
    }

    /**
     * Get the [is_kunci_aktifitas] column value.
     *
     * @return boolean
     */
    public function getIsKunciAktifitas()
    {
        return $this->is_kunci_aktifitas;
    }

    /**
     * Get the [is_kunci_aktifitas] column value.
     *
     * @return boolean
     */
    public function isKunciAktifitas()
    {
        return $this->getIsKunciAktifitas();
    }

    /**
     * Get the [is_only_skp] column value.
     *
     * @return boolean
     */
    public function getIsOnlySkp()
    {
        return $this->is_only_skp;
    }

    /**
     * Get the [is_only_skp] column value.
     *
     * @return boolean
     */
    public function isOnlySkp()
    {
        return $this->getIsOnlySkp();
    }

    /**
     * Get the [jabatan_struktural_id] column value.
     *
     * @return int
     */
    public function getJabatanStrukturalId()
    {
        return $this->jabatan_struktural_id;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[OldPegawaiTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [nip] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setNip($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nip !== $v) {
            $this->nip = $v;
            $this->modifiedColumns[OldPegawaiTableMap::COL_NIP] = true;
        }

        return $this;
    } // setNip()

    /**
     * Set the value of [nama] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[OldPegawaiTableMap::COL_NAMA] = true;
        }

        return $this;
    } // setNama()

    /**
     * Set the value of [golongan] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setGolongan($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->golongan !== $v) {
            $this->golongan = $v;
            $this->modifiedColumns[OldPegawaiTableMap::COL_GOLONGAN] = true;
        }

        return $this;
    } // setGolongan()

    /**
     * Sets the value of [tmt] column to a normalized version of the date/time value specified.
     * terhitung mulai tanggal -> tgl masuk
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setTmt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->tmt !== null || $dt !== null) {
            if ($this->tmt === null || $dt === null || $dt->format("Y-m-d") !== $this->tmt->format("Y-m-d")) {
                $this->tmt = $dt === null ? null : clone $dt;
                $this->modifiedColumns[OldPegawaiTableMap::COL_TMT] = true;
            }
        } // if either are not null

        return $this;
    } // setTmt()

    /**
     * Set the value of [pendidikan_terakhir] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setPendidikanTerakhir($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->pendidikan_terakhir !== $v) {
            $this->pendidikan_terakhir = $v;
            $this->modifiedColumns[OldPegawaiTableMap::COL_PENDIDIKAN_TERAKHIR] = true;
        }

        return $this;
    } // setPendidikanTerakhir()

    /**
     * Set the value of [skpd_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setSkpdId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->skpd_id !== $v) {
            $this->skpd_id = $v;
            $this->modifiedColumns[OldPegawaiTableMap::COL_SKPD_ID] = true;
        }

        if ($this->aOldMasterSkpd !== null && $this->aOldMasterSkpd->getSkpdId() !== $v) {
            $this->aOldMasterSkpd = null;
        }

        return $this;
    } // setSkpdId()

    /**
     * Set the value of [atasan_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setAtasanId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->atasan_id !== $v) {
            $this->atasan_id = $v;
            $this->modifiedColumns[OldPegawaiTableMap::COL_ATASAN_ID] = true;
        }

        if ($this->aOldPegawai !== null && $this->aOldPegawai->getId() !== $v) {
            $this->aOldPegawai = null;
        }

        return $this;
    } // setAtasanId()

    /**
     * Sets the value of the [is_atasan] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setIsAtasan($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_atasan !== $v) {
            $this->is_atasan = $v;
            $this->modifiedColumns[OldPegawaiTableMap::COL_IS_ATASAN] = true;
        }

        return $this;
    } // setIsAtasan()

    /**
     * Set the value of [level_struktural] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setLevelStruktural($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->level_struktural !== $v) {
            $this->level_struktural = $v;
            $this->modifiedColumns[OldPegawaiTableMap::COL_LEVEL_STRUKTURAL] = true;
        }

        return $this;
    } // setLevelStruktural()

    /**
     * Set the value of [level_penilaian] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setLevelPenilaian($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->level_penilaian !== $v) {
            $this->level_penilaian = $v;
            $this->modifiedColumns[OldPegawaiTableMap::COL_LEVEL_PENILAIAN] = true;
        }

        return $this;
    } // setLevelPenilaian()

    /**
     * Set the value of [password] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[OldPegawaiTableMap::COL_PASSWORD] = true;
        }

        return $this;
    } // setPassword()

    /**
     * Set the value of [user_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->user_id !== $v) {
            $this->user_id = $v;
            $this->modifiedColumns[OldPegawaiTableMap::COL_USER_ID] = true;
        }

        return $this;
    } // setUserId()

    /**
     * Set the value of [username_eproject] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setUsernameEproject($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->username_eproject !== $v) {
            $this->username_eproject = $v;
            $this->modifiedColumns[OldPegawaiTableMap::COL_USERNAME_EPROJECT] = true;
        }

        return $this;
    } // setUsernameEproject()

    /**
     * Sets the value of [tgl_masuk] column to a normalized version of the date/time value specified.
     * tgl masuk SKPD baru
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setTglMasuk($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->tgl_masuk !== null || $dt !== null) {
            if ($this->tgl_masuk === null || $dt === null || $dt->format("Y-m-d") !== $this->tgl_masuk->format("Y-m-d")) {
                $this->tgl_masuk = $dt === null ? null : clone $dt;
                $this->modifiedColumns[OldPegawaiTableMap::COL_TGL_MASUK] = true;
            }
        } // if either are not null

        return $this;
    } // setTglMasuk()

    /**
     * Sets the value of [tgl_keluar] column to a normalized version of the date/time value specified.
     * tgl keluar
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setTglKeluar($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->tgl_keluar !== null || $dt !== null) {
            if ($this->tgl_keluar === null || $dt === null || $dt->format("Y-m-d") !== $this->tgl_keluar->format("Y-m-d")) {
                $this->tgl_keluar = $dt === null ? null : clone $dt;
                $this->modifiedColumns[OldPegawaiTableMap::COL_TGL_KELUAR] = true;
            }
        } // if either are not null

        return $this;
    } // setTglKeluar()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[OldPegawaiTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[OldPegawaiTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Sets the value of [deleted_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setDeletedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->deleted_at !== null || $dt !== null) {
            if ($this->deleted_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->deleted_at->format("Y-m-d H:i:s.u")) {
                $this->deleted_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[OldPegawaiTableMap::COL_DELETED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setDeletedAt()

    /**
     * Sets the value of the [is_out] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setIsOut($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_out !== $v) {
            $this->is_out = $v;
            $this->modifiedColumns[OldPegawaiTableMap::COL_IS_OUT] = true;
        }

        return $this;
    } // setIsOut()

    /**
     * Sets the value of the [is_ka_uptd] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setIsKaUptd($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_ka_uptd !== $v) {
            $this->is_ka_uptd = $v;
            $this->modifiedColumns[OldPegawaiTableMap::COL_IS_KA_UPTD] = true;
        }

        return $this;
    } // setIsKaUptd()

    /**
     * Sets the value of the [is_struktural] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * digunakan untuk pegawai kecamatan
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setIsStruktural($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_struktural !== $v) {
            $this->is_struktural = $v;
            $this->modifiedColumns[OldPegawaiTableMap::COL_IS_STRUKTURAL] = true;
        }

        return $this;
    } // setIsStruktural()

    /**
     * Sets the value of the [is_kunci_aktifitas] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setIsKunciAktifitas($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_kunci_aktifitas !== $v) {
            $this->is_kunci_aktifitas = $v;
            $this->modifiedColumns[OldPegawaiTableMap::COL_IS_KUNCI_AKTIFITAS] = true;
        }

        return $this;
    } // setIsKunciAktifitas()

    /**
     * Sets the value of the [is_only_skp] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setIsOnlySkp($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_only_skp !== $v) {
            $this->is_only_skp = $v;
            $this->modifiedColumns[OldPegawaiTableMap::COL_IS_ONLY_SKP] = true;
        }

        return $this;
    } // setIsOnlySkp()

    /**
     * Set the value of [jabatan_struktural_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function setJabatanStrukturalId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->jabatan_struktural_id !== $v) {
            $this->jabatan_struktural_id = $v;
            $this->modifiedColumns[OldPegawaiTableMap::COL_JABATAN_STRUKTURAL_ID] = true;
        }

        if ($this->aOldJabatanStruktural !== null && $this->aOldJabatanStruktural->getId() !== $v) {
            $this->aOldJabatanStruktural = null;
        }

        return $this;
    } // setJabatanStrukturalId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->is_atasan !== false) {
                return false;
            }

            if ($this->is_out !== false) {
                return false;
            }

            if ($this->is_ka_uptd !== false) {
                return false;
            }

            if ($this->is_struktural !== false) {
                return false;
            }

            if ($this->is_kunci_aktifitas !== true) {
                return false;
            }

            if ($this->is_only_skp !== false) {
                return false;
            }

            if ($this->jabatan_struktural_id !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : OldPegawaiTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : OldPegawaiTableMap::translateFieldName('Nip', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nip = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : OldPegawaiTableMap::translateFieldName('Nama', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nama = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : OldPegawaiTableMap::translateFieldName('Golongan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->golongan = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : OldPegawaiTableMap::translateFieldName('Tmt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tmt = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : OldPegawaiTableMap::translateFieldName('PendidikanTerakhir', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pendidikan_terakhir = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : OldPegawaiTableMap::translateFieldName('SkpdId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->skpd_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : OldPegawaiTableMap::translateFieldName('AtasanId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->atasan_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : OldPegawaiTableMap::translateFieldName('IsAtasan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_atasan = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : OldPegawaiTableMap::translateFieldName('LevelStruktural', TableMap::TYPE_PHPNAME, $indexType)];
            $this->level_struktural = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : OldPegawaiTableMap::translateFieldName('LevelPenilaian', TableMap::TYPE_PHPNAME, $indexType)];
            $this->level_penilaian = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : OldPegawaiTableMap::translateFieldName('Password', TableMap::TYPE_PHPNAME, $indexType)];
            $this->password = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : OldPegawaiTableMap::translateFieldName('UserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : OldPegawaiTableMap::translateFieldName('UsernameEproject', TableMap::TYPE_PHPNAME, $indexType)];
            $this->username_eproject = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : OldPegawaiTableMap::translateFieldName('TglMasuk', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tgl_masuk = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : OldPegawaiTableMap::translateFieldName('TglKeluar', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tgl_keluar = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : OldPegawaiTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : OldPegawaiTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : OldPegawaiTableMap::translateFieldName('DeletedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->deleted_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : OldPegawaiTableMap::translateFieldName('IsOut', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_out = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : OldPegawaiTableMap::translateFieldName('IsKaUptd', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_ka_uptd = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : OldPegawaiTableMap::translateFieldName('IsStruktural', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_struktural = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : OldPegawaiTableMap::translateFieldName('IsKunciAktifitas', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_kunci_aktifitas = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : OldPegawaiTableMap::translateFieldName('IsOnlySkp', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_only_skp = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : OldPegawaiTableMap::translateFieldName('JabatanStrukturalId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->jabatan_struktural_id = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 25; // 25 = OldPegawaiTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\CoreBundle\\Model\\OldEperformance\\OldPegawai'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aOldMasterSkpd !== null && $this->skpd_id !== $this->aOldMasterSkpd->getSkpdId()) {
            $this->aOldMasterSkpd = null;
        }
        if ($this->aOldPegawai !== null && $this->atasan_id !== $this->aOldPegawai->getId()) {
            $this->aOldPegawai = null;
        }
        if ($this->aOldJabatanStruktural !== null && $this->jabatan_struktural_id !== $this->aOldJabatanStruktural->getId()) {
            $this->aOldJabatanStruktural = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(OldPegawaiTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildOldPegawaiQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aOldMasterSkpd = null;
            $this->aOldPegawai = null;
            $this->aOldJabatanStruktural = null;
            $this->collOldPerilakuHasils = null;

            $this->collOldPegawaisRelatedById = null;

            $this->collOldIndikatorKinerjas = null;

            $this->collOldKegiatanPegawais = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see OldPegawai::setDeleted()
     * @see OldPegawai::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldPegawaiTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildOldPegawaiQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldPegawaiTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                OldPegawaiTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aOldMasterSkpd !== null) {
                if ($this->aOldMasterSkpd->isModified() || $this->aOldMasterSkpd->isNew()) {
                    $affectedRows += $this->aOldMasterSkpd->save($con);
                }
                $this->setOldMasterSkpd($this->aOldMasterSkpd);
            }

            if ($this->aOldPegawai !== null) {
                if ($this->aOldPegawai->isModified() || $this->aOldPegawai->isNew()) {
                    $affectedRows += $this->aOldPegawai->save($con);
                }
                $this->setOldPegawai($this->aOldPegawai);
            }

            if ($this->aOldJabatanStruktural !== null) {
                if ($this->aOldJabatanStruktural->isModified() || $this->aOldJabatanStruktural->isNew()) {
                    $affectedRows += $this->aOldJabatanStruktural->save($con);
                }
                $this->setOldJabatanStruktural($this->aOldJabatanStruktural);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->oldPerilakuHasilsScheduledForDeletion !== null) {
                if (!$this->oldPerilakuHasilsScheduledForDeletion->isEmpty()) {
                    foreach ($this->oldPerilakuHasilsScheduledForDeletion as $oldPerilakuHasil) {
                        // need to save related object because we set the relation to null
                        $oldPerilakuHasil->save($con);
                    }
                    $this->oldPerilakuHasilsScheduledForDeletion = null;
                }
            }

            if ($this->collOldPerilakuHasils !== null) {
                foreach ($this->collOldPerilakuHasils as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->oldPegawaisRelatedByIdScheduledForDeletion !== null) {
                if (!$this->oldPegawaisRelatedByIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->oldPegawaisRelatedByIdScheduledForDeletion as $oldPegawaiRelatedById) {
                        // need to save related object because we set the relation to null
                        $oldPegawaiRelatedById->save($con);
                    }
                    $this->oldPegawaisRelatedByIdScheduledForDeletion = null;
                }
            }

            if ($this->collOldPegawaisRelatedById !== null) {
                foreach ($this->collOldPegawaisRelatedById as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->oldIndikatorKinerjasScheduledForDeletion !== null) {
                if (!$this->oldIndikatorKinerjasScheduledForDeletion->isEmpty()) {
                    foreach ($this->oldIndikatorKinerjasScheduledForDeletion as $oldIndikatorKinerja) {
                        // need to save related object because we set the relation to null
                        $oldIndikatorKinerja->save($con);
                    }
                    $this->oldIndikatorKinerjasScheduledForDeletion = null;
                }
            }

            if ($this->collOldIndikatorKinerjas !== null) {
                foreach ($this->collOldIndikatorKinerjas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->oldKegiatanPegawaisScheduledForDeletion !== null) {
                if (!$this->oldKegiatanPegawaisScheduledForDeletion->isEmpty()) {
                    \CoreBundle\Model\OldEperformance\OldKegiatanPegawaiQuery::create()
                        ->filterByPrimaryKeys($this->oldKegiatanPegawaisScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->oldKegiatanPegawaisScheduledForDeletion = null;
                }
            }

            if ($this->collOldKegiatanPegawais !== null) {
                foreach ($this->collOldKegiatanPegawais as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[OldPegawaiTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . OldPegawaiTableMap::COL_ID . ')');
        }
        if (null === $this->id) {
            try {
                $dataFetcher = $con->query("SELECT nextval('eperformance.pegawai_id_seq')");
                $this->id = (int) $dataFetcher->fetchColumn();
            } catch (Exception $e) {
                throw new PropelException('Unable to get sequence id.', 0, $e);
            }
        }


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(OldPegawaiTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_NIP)) {
            $modifiedColumns[':p' . $index++]  = 'nip';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_NAMA)) {
            $modifiedColumns[':p' . $index++]  = 'nama';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_GOLONGAN)) {
            $modifiedColumns[':p' . $index++]  = 'golongan';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_TMT)) {
            $modifiedColumns[':p' . $index++]  = 'tmt';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_PENDIDIKAN_TERAKHIR)) {
            $modifiedColumns[':p' . $index++]  = 'pendidikan_terakhir';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_SKPD_ID)) {
            $modifiedColumns[':p' . $index++]  = 'skpd_id';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_ATASAN_ID)) {
            $modifiedColumns[':p' . $index++]  = 'atasan_id';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_IS_ATASAN)) {
            $modifiedColumns[':p' . $index++]  = 'is_atasan';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_LEVEL_STRUKTURAL)) {
            $modifiedColumns[':p' . $index++]  = 'level_struktural';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_LEVEL_PENILAIAN)) {
            $modifiedColumns[':p' . $index++]  = 'level_penilaian';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = 'password';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'user_id';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_USERNAME_EPROJECT)) {
            $modifiedColumns[':p' . $index++]  = 'username_eproject';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_TGL_MASUK)) {
            $modifiedColumns[':p' . $index++]  = 'tgl_masuk';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_TGL_KELUAR)) {
            $modifiedColumns[':p' . $index++]  = 'tgl_keluar';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_DELETED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'deleted_at';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_IS_OUT)) {
            $modifiedColumns[':p' . $index++]  = 'is_out';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_IS_KA_UPTD)) {
            $modifiedColumns[':p' . $index++]  = 'is_ka_uptd';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_IS_STRUKTURAL)) {
            $modifiedColumns[':p' . $index++]  = 'is_struktural';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_IS_KUNCI_AKTIFITAS)) {
            $modifiedColumns[':p' . $index++]  = 'is_kunci_aktifitas';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_IS_ONLY_SKP)) {
            $modifiedColumns[':p' . $index++]  = 'is_only_skp';
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_JABATAN_STRUKTURAL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'jabatan_struktural_id';
        }

        $sql = sprintf(
            'INSERT INTO eperformance.pegawai (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'nip':
                        $stmt->bindValue($identifier, $this->nip, PDO::PARAM_STR);
                        break;
                    case 'nama':
                        $stmt->bindValue($identifier, $this->nama, PDO::PARAM_STR);
                        break;
                    case 'golongan':
                        $stmt->bindValue($identifier, $this->golongan, PDO::PARAM_STR);
                        break;
                    case 'tmt':
                        $stmt->bindValue($identifier, $this->tmt ? $this->tmt->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'pendidikan_terakhir':
                        $stmt->bindValue($identifier, $this->pendidikan_terakhir, PDO::PARAM_STR);
                        break;
                    case 'skpd_id':
                        $stmt->bindValue($identifier, $this->skpd_id, PDO::PARAM_INT);
                        break;
                    case 'atasan_id':
                        $stmt->bindValue($identifier, $this->atasan_id, PDO::PARAM_INT);
                        break;
                    case 'is_atasan':
                        $stmt->bindValue($identifier, $this->is_atasan, PDO::PARAM_BOOL);
                        break;
                    case 'level_struktural':
                        $stmt->bindValue($identifier, $this->level_struktural, PDO::PARAM_INT);
                        break;
                    case 'level_penilaian':
                        $stmt->bindValue($identifier, $this->level_penilaian, PDO::PARAM_INT);
                        break;
                    case 'password':
                        $stmt->bindValue($identifier, $this->password, PDO::PARAM_STR);
                        break;
                    case 'user_id':
                        $stmt->bindValue($identifier, $this->user_id, PDO::PARAM_INT);
                        break;
                    case 'username_eproject':
                        $stmt->bindValue($identifier, $this->username_eproject, PDO::PARAM_STR);
                        break;
                    case 'tgl_masuk':
                        $stmt->bindValue($identifier, $this->tgl_masuk ? $this->tgl_masuk->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'tgl_keluar':
                        $stmt->bindValue($identifier, $this->tgl_keluar ? $this->tgl_keluar->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'deleted_at':
                        $stmt->bindValue($identifier, $this->deleted_at ? $this->deleted_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'is_out':
                        $stmt->bindValue($identifier, $this->is_out, PDO::PARAM_BOOL);
                        break;
                    case 'is_ka_uptd':
                        $stmt->bindValue($identifier, $this->is_ka_uptd, PDO::PARAM_BOOL);
                        break;
                    case 'is_struktural':
                        $stmt->bindValue($identifier, $this->is_struktural, PDO::PARAM_BOOL);
                        break;
                    case 'is_kunci_aktifitas':
                        $stmt->bindValue($identifier, $this->is_kunci_aktifitas, PDO::PARAM_BOOL);
                        break;
                    case 'is_only_skp':
                        $stmt->bindValue($identifier, $this->is_only_skp, PDO::PARAM_BOOL);
                        break;
                    case 'jabatan_struktural_id':
                        $stmt->bindValue($identifier, $this->jabatan_struktural_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = OldPegawaiTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getNip();
                break;
            case 2:
                return $this->getNama();
                break;
            case 3:
                return $this->getGolongan();
                break;
            case 4:
                return $this->getTmt();
                break;
            case 5:
                return $this->getPendidikanTerakhir();
                break;
            case 6:
                return $this->getSkpdId();
                break;
            case 7:
                return $this->getAtasanId();
                break;
            case 8:
                return $this->getIsAtasan();
                break;
            case 9:
                return $this->getLevelStruktural();
                break;
            case 10:
                return $this->getLevelPenilaian();
                break;
            case 11:
                return $this->getPassword();
                break;
            case 12:
                return $this->getUserId();
                break;
            case 13:
                return $this->getUsernameEproject();
                break;
            case 14:
                return $this->getTglMasuk();
                break;
            case 15:
                return $this->getTglKeluar();
                break;
            case 16:
                return $this->getCreatedAt();
                break;
            case 17:
                return $this->getUpdatedAt();
                break;
            case 18:
                return $this->getDeletedAt();
                break;
            case 19:
                return $this->getIsOut();
                break;
            case 20:
                return $this->getIsKaUptd();
                break;
            case 21:
                return $this->getIsStruktural();
                break;
            case 22:
                return $this->getIsKunciAktifitas();
                break;
            case 23:
                return $this->getIsOnlySkp();
                break;
            case 24:
                return $this->getJabatanStrukturalId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['OldPegawai'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['OldPegawai'][$this->hashCode()] = true;
        $keys = OldPegawaiTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getNip(),
            $keys[2] => $this->getNama(),
            $keys[3] => $this->getGolongan(),
            $keys[4] => $this->getTmt(),
            $keys[5] => $this->getPendidikanTerakhir(),
            $keys[6] => $this->getSkpdId(),
            $keys[7] => $this->getAtasanId(),
            $keys[8] => $this->getIsAtasan(),
            $keys[9] => $this->getLevelStruktural(),
            $keys[10] => $this->getLevelPenilaian(),
            $keys[11] => $this->getPassword(),
            $keys[12] => $this->getUserId(),
            $keys[13] => $this->getUsernameEproject(),
            $keys[14] => $this->getTglMasuk(),
            $keys[15] => $this->getTglKeluar(),
            $keys[16] => $this->getCreatedAt(),
            $keys[17] => $this->getUpdatedAt(),
            $keys[18] => $this->getDeletedAt(),
            $keys[19] => $this->getIsOut(),
            $keys[20] => $this->getIsKaUptd(),
            $keys[21] => $this->getIsStruktural(),
            $keys[22] => $this->getIsKunciAktifitas(),
            $keys[23] => $this->getIsOnlySkp(),
            $keys[24] => $this->getJabatanStrukturalId(),
        );
        if ($result[$keys[4]] instanceof \DateTime) {
            $result[$keys[4]] = $result[$keys[4]]->format('c');
        }

        if ($result[$keys[14]] instanceof \DateTime) {
            $result[$keys[14]] = $result[$keys[14]]->format('c');
        }

        if ($result[$keys[15]] instanceof \DateTime) {
            $result[$keys[15]] = $result[$keys[15]]->format('c');
        }

        if ($result[$keys[16]] instanceof \DateTime) {
            $result[$keys[16]] = $result[$keys[16]]->format('c');
        }

        if ($result[$keys[17]] instanceof \DateTime) {
            $result[$keys[17]] = $result[$keys[17]]->format('c');
        }

        if ($result[$keys[18]] instanceof \DateTime) {
            $result[$keys[18]] = $result[$keys[18]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aOldMasterSkpd) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldMasterSkpd';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.master_skpd';
                        break;
                    default:
                        $key = 'OldMasterSkpd';
                }

                $result[$key] = $this->aOldMasterSkpd->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aOldPegawai) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldPegawai';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.pegawai';
                        break;
                    default:
                        $key = 'OldPegawai';
                }

                $result[$key] = $this->aOldPegawai->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aOldJabatanStruktural) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldJabatanStruktural';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.jabatan_struktural';
                        break;
                    default:
                        $key = 'OldJabatanStruktural';
                }

                $result[$key] = $this->aOldJabatanStruktural->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collOldPerilakuHasils) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldPerilakuHasils';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.skp_perilaku_kerja_hasils';
                        break;
                    default:
                        $key = 'OldPerilakuHasils';
                }

                $result[$key] = $this->collOldPerilakuHasils->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collOldPegawaisRelatedById) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldPegawais';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.pegawais';
                        break;
                    default:
                        $key = 'OldPegawais';
                }

                $result[$key] = $this->collOldPegawaisRelatedById->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collOldIndikatorKinerjas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldIndikatorKinerjas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.indikator_kinerjas';
                        break;
                    default:
                        $key = 'OldIndikatorKinerjas';
                }

                $result[$key] = $this->collOldIndikatorKinerjas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collOldKegiatanPegawais) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldKegiatanPegawais';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.kegiatan_pegawais';
                        break;
                    default:
                        $key = 'OldKegiatanPegawais';
                }

                $result[$key] = $this->collOldKegiatanPegawais->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = OldPegawaiTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setNip($value);
                break;
            case 2:
                $this->setNama($value);
                break;
            case 3:
                $this->setGolongan($value);
                break;
            case 4:
                $this->setTmt($value);
                break;
            case 5:
                $this->setPendidikanTerakhir($value);
                break;
            case 6:
                $this->setSkpdId($value);
                break;
            case 7:
                $this->setAtasanId($value);
                break;
            case 8:
                $this->setIsAtasan($value);
                break;
            case 9:
                $this->setLevelStruktural($value);
                break;
            case 10:
                $this->setLevelPenilaian($value);
                break;
            case 11:
                $this->setPassword($value);
                break;
            case 12:
                $this->setUserId($value);
                break;
            case 13:
                $this->setUsernameEproject($value);
                break;
            case 14:
                $this->setTglMasuk($value);
                break;
            case 15:
                $this->setTglKeluar($value);
                break;
            case 16:
                $this->setCreatedAt($value);
                break;
            case 17:
                $this->setUpdatedAt($value);
                break;
            case 18:
                $this->setDeletedAt($value);
                break;
            case 19:
                $this->setIsOut($value);
                break;
            case 20:
                $this->setIsKaUptd($value);
                break;
            case 21:
                $this->setIsStruktural($value);
                break;
            case 22:
                $this->setIsKunciAktifitas($value);
                break;
            case 23:
                $this->setIsOnlySkp($value);
                break;
            case 24:
                $this->setJabatanStrukturalId($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = OldPegawaiTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setNip($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setNama($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setGolongan($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setTmt($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setPendidikanTerakhir($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setSkpdId($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setAtasanId($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setIsAtasan($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setLevelStruktural($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setLevelPenilaian($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setPassword($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setUserId($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setUsernameEproject($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setTglMasuk($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setTglKeluar($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setCreatedAt($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setUpdatedAt($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setDeletedAt($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setIsOut($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setIsKaUptd($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setIsStruktural($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setIsKunciAktifitas($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setIsOnlySkp($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setJabatanStrukturalId($arr[$keys[24]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(OldPegawaiTableMap::DATABASE_NAME);

        if ($this->isColumnModified(OldPegawaiTableMap::COL_ID)) {
            $criteria->add(OldPegawaiTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_NIP)) {
            $criteria->add(OldPegawaiTableMap::COL_NIP, $this->nip);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_NAMA)) {
            $criteria->add(OldPegawaiTableMap::COL_NAMA, $this->nama);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_GOLONGAN)) {
            $criteria->add(OldPegawaiTableMap::COL_GOLONGAN, $this->golongan);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_TMT)) {
            $criteria->add(OldPegawaiTableMap::COL_TMT, $this->tmt);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_PENDIDIKAN_TERAKHIR)) {
            $criteria->add(OldPegawaiTableMap::COL_PENDIDIKAN_TERAKHIR, $this->pendidikan_terakhir);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_SKPD_ID)) {
            $criteria->add(OldPegawaiTableMap::COL_SKPD_ID, $this->skpd_id);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_ATASAN_ID)) {
            $criteria->add(OldPegawaiTableMap::COL_ATASAN_ID, $this->atasan_id);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_IS_ATASAN)) {
            $criteria->add(OldPegawaiTableMap::COL_IS_ATASAN, $this->is_atasan);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_LEVEL_STRUKTURAL)) {
            $criteria->add(OldPegawaiTableMap::COL_LEVEL_STRUKTURAL, $this->level_struktural);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_LEVEL_PENILAIAN)) {
            $criteria->add(OldPegawaiTableMap::COL_LEVEL_PENILAIAN, $this->level_penilaian);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_PASSWORD)) {
            $criteria->add(OldPegawaiTableMap::COL_PASSWORD, $this->password);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_USER_ID)) {
            $criteria->add(OldPegawaiTableMap::COL_USER_ID, $this->user_id);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_USERNAME_EPROJECT)) {
            $criteria->add(OldPegawaiTableMap::COL_USERNAME_EPROJECT, $this->username_eproject);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_TGL_MASUK)) {
            $criteria->add(OldPegawaiTableMap::COL_TGL_MASUK, $this->tgl_masuk);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_TGL_KELUAR)) {
            $criteria->add(OldPegawaiTableMap::COL_TGL_KELUAR, $this->tgl_keluar);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_CREATED_AT)) {
            $criteria->add(OldPegawaiTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_UPDATED_AT)) {
            $criteria->add(OldPegawaiTableMap::COL_UPDATED_AT, $this->updated_at);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_DELETED_AT)) {
            $criteria->add(OldPegawaiTableMap::COL_DELETED_AT, $this->deleted_at);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_IS_OUT)) {
            $criteria->add(OldPegawaiTableMap::COL_IS_OUT, $this->is_out);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_IS_KA_UPTD)) {
            $criteria->add(OldPegawaiTableMap::COL_IS_KA_UPTD, $this->is_ka_uptd);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_IS_STRUKTURAL)) {
            $criteria->add(OldPegawaiTableMap::COL_IS_STRUKTURAL, $this->is_struktural);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_IS_KUNCI_AKTIFITAS)) {
            $criteria->add(OldPegawaiTableMap::COL_IS_KUNCI_AKTIFITAS, $this->is_kunci_aktifitas);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_IS_ONLY_SKP)) {
            $criteria->add(OldPegawaiTableMap::COL_IS_ONLY_SKP, $this->is_only_skp);
        }
        if ($this->isColumnModified(OldPegawaiTableMap::COL_JABATAN_STRUKTURAL_ID)) {
            $criteria->add(OldPegawaiTableMap::COL_JABATAN_STRUKTURAL_ID, $this->jabatan_struktural_id);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildOldPegawaiQuery::create();
        $criteria->add(OldPegawaiTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \CoreBundle\Model\OldEperformance\OldPegawai (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNip($this->getNip());
        $copyObj->setNama($this->getNama());
        $copyObj->setGolongan($this->getGolongan());
        $copyObj->setTmt($this->getTmt());
        $copyObj->setPendidikanTerakhir($this->getPendidikanTerakhir());
        $copyObj->setSkpdId($this->getSkpdId());
        $copyObj->setAtasanId($this->getAtasanId());
        $copyObj->setIsAtasan($this->getIsAtasan());
        $copyObj->setLevelStruktural($this->getLevelStruktural());
        $copyObj->setLevelPenilaian($this->getLevelPenilaian());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setUserId($this->getUserId());
        $copyObj->setUsernameEproject($this->getUsernameEproject());
        $copyObj->setTglMasuk($this->getTglMasuk());
        $copyObj->setTglKeluar($this->getTglKeluar());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        $copyObj->setDeletedAt($this->getDeletedAt());
        $copyObj->setIsOut($this->getIsOut());
        $copyObj->setIsKaUptd($this->getIsKaUptd());
        $copyObj->setIsStruktural($this->getIsStruktural());
        $copyObj->setIsKunciAktifitas($this->getIsKunciAktifitas());
        $copyObj->setIsOnlySkp($this->getIsOnlySkp());
        $copyObj->setJabatanStrukturalId($this->getJabatanStrukturalId());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getOldPerilakuHasils() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addOldPerilakuHasil($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getOldPegawaisRelatedById() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addOldPegawaiRelatedById($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getOldIndikatorKinerjas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addOldIndikatorKinerja($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getOldKegiatanPegawais() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addOldKegiatanPegawai($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \CoreBundle\Model\OldEperformance\OldPegawai Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildOldMasterSkpd object.
     *
     * @param  ChildOldMasterSkpd $v
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     * @throws PropelException
     */
    public function setOldMasterSkpd(ChildOldMasterSkpd $v = null)
    {
        if ($v === null) {
            $this->setSkpdId(NULL);
        } else {
            $this->setSkpdId($v->getSkpdId());
        }

        $this->aOldMasterSkpd = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildOldMasterSkpd object, it will not be re-added.
        if ($v !== null) {
            $v->addOldPegawai($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildOldMasterSkpd object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildOldMasterSkpd The associated ChildOldMasterSkpd object.
     * @throws PropelException
     */
    public function getOldMasterSkpd(ConnectionInterface $con = null)
    {
        if ($this->aOldMasterSkpd === null && ($this->skpd_id !== null)) {
            $this->aOldMasterSkpd = ChildOldMasterSkpdQuery::create()->findPk($this->skpd_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aOldMasterSkpd->addOldPegawais($this);
             */
        }

        return $this->aOldMasterSkpd;
    }

    /**
     * Declares an association between this object and a ChildOldPegawai object.
     *
     * @param  ChildOldPegawai $v
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     * @throws PropelException
     */
    public function setOldPegawai(ChildOldPegawai $v = null)
    {
        if ($v === null) {
            $this->setAtasanId(NULL);
        } else {
            $this->setAtasanId($v->getId());
        }

        $this->aOldPegawai = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildOldPegawai object, it will not be re-added.
        if ($v !== null) {
            $v->addOldPegawaiRelatedById($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildOldPegawai object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildOldPegawai The associated ChildOldPegawai object.
     * @throws PropelException
     */
    public function getOldPegawai(ConnectionInterface $con = null)
    {
        if ($this->aOldPegawai === null && ($this->atasan_id !== null)) {
            $this->aOldPegawai = ChildOldPegawaiQuery::create()->findPk($this->atasan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aOldPegawai->addOldPegawaisRelatedById($this);
             */
        }

        return $this->aOldPegawai;
    }

    /**
     * Declares an association between this object and a ChildOldJabatanStruktural object.
     *
     * @param  ChildOldJabatanStruktural $v
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     * @throws PropelException
     */
    public function setOldJabatanStruktural(ChildOldJabatanStruktural $v = null)
    {
        if ($v === null) {
            $this->setJabatanStrukturalId(0);
        } else {
            $this->setJabatanStrukturalId($v->getId());
        }

        $this->aOldJabatanStruktural = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildOldJabatanStruktural object, it will not be re-added.
        if ($v !== null) {
            $v->addOldPegawai($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildOldJabatanStruktural object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildOldJabatanStruktural The associated ChildOldJabatanStruktural object.
     * @throws PropelException
     */
    public function getOldJabatanStruktural(ConnectionInterface $con = null)
    {
        if ($this->aOldJabatanStruktural === null && ($this->jabatan_struktural_id !== null)) {
            $this->aOldJabatanStruktural = ChildOldJabatanStrukturalQuery::create()->findPk($this->jabatan_struktural_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aOldJabatanStruktural->addOldPegawais($this);
             */
        }

        return $this->aOldJabatanStruktural;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('OldPerilakuHasil' == $relationName) {
            return $this->initOldPerilakuHasils();
        }
        if ('OldPegawaiRelatedById' == $relationName) {
            return $this->initOldPegawaisRelatedById();
        }
        if ('OldIndikatorKinerja' == $relationName) {
            return $this->initOldIndikatorKinerjas();
        }
        if ('OldKegiatanPegawai' == $relationName) {
            return $this->initOldKegiatanPegawais();
        }
    }

    /**
     * Clears out the collOldPerilakuHasils collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addOldPerilakuHasils()
     */
    public function clearOldPerilakuHasils()
    {
        $this->collOldPerilakuHasils = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collOldPerilakuHasils collection loaded partially.
     */
    public function resetPartialOldPerilakuHasils($v = true)
    {
        $this->collOldPerilakuHasilsPartial = $v;
    }

    /**
     * Initializes the collOldPerilakuHasils collection.
     *
     * By default this just sets the collOldPerilakuHasils collection to an empty array (like clearcollOldPerilakuHasils());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initOldPerilakuHasils($overrideExisting = true)
    {
        if (null !== $this->collOldPerilakuHasils && !$overrideExisting) {
            return;
        }

        $collectionClassName = OldPerilakuHasilTableMap::getTableMap()->getCollectionClassName();

        $this->collOldPerilakuHasils = new $collectionClassName;
        $this->collOldPerilakuHasils->setModel('\CoreBundle\Model\OldEperformance\OldPerilakuHasil');
    }

    /**
     * Gets an array of ChildOldPerilakuHasil objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildOldPegawai is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildOldPerilakuHasil[] List of ChildOldPerilakuHasil objects
     * @throws PropelException
     */
    public function getOldPerilakuHasils(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collOldPerilakuHasilsPartial && !$this->isNew();
        if (null === $this->collOldPerilakuHasils || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collOldPerilakuHasils) {
                // return empty collection
                $this->initOldPerilakuHasils();
            } else {
                $collOldPerilakuHasils = ChildOldPerilakuHasilQuery::create(null, $criteria)
                    ->filterByOldPegawai($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collOldPerilakuHasilsPartial && count($collOldPerilakuHasils)) {
                        $this->initOldPerilakuHasils(false);

                        foreach ($collOldPerilakuHasils as $obj) {
                            if (false == $this->collOldPerilakuHasils->contains($obj)) {
                                $this->collOldPerilakuHasils->append($obj);
                            }
                        }

                        $this->collOldPerilakuHasilsPartial = true;
                    }

                    return $collOldPerilakuHasils;
                }

                if ($partial && $this->collOldPerilakuHasils) {
                    foreach ($this->collOldPerilakuHasils as $obj) {
                        if ($obj->isNew()) {
                            $collOldPerilakuHasils[] = $obj;
                        }
                    }
                }

                $this->collOldPerilakuHasils = $collOldPerilakuHasils;
                $this->collOldPerilakuHasilsPartial = false;
            }
        }

        return $this->collOldPerilakuHasils;
    }

    /**
     * Sets a collection of ChildOldPerilakuHasil objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $oldPerilakuHasils A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildOldPegawai The current object (for fluent API support)
     */
    public function setOldPerilakuHasils(Collection $oldPerilakuHasils, ConnectionInterface $con = null)
    {
        /** @var ChildOldPerilakuHasil[] $oldPerilakuHasilsToDelete */
        $oldPerilakuHasilsToDelete = $this->getOldPerilakuHasils(new Criteria(), $con)->diff($oldPerilakuHasils);


        $this->oldPerilakuHasilsScheduledForDeletion = $oldPerilakuHasilsToDelete;

        foreach ($oldPerilakuHasilsToDelete as $oldPerilakuHasilRemoved) {
            $oldPerilakuHasilRemoved->setOldPegawai(null);
        }

        $this->collOldPerilakuHasils = null;
        foreach ($oldPerilakuHasils as $oldPerilakuHasil) {
            $this->addOldPerilakuHasil($oldPerilakuHasil);
        }

        $this->collOldPerilakuHasils = $oldPerilakuHasils;
        $this->collOldPerilakuHasilsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related OldPerilakuHasil objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related OldPerilakuHasil objects.
     * @throws PropelException
     */
    public function countOldPerilakuHasils(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collOldPerilakuHasilsPartial && !$this->isNew();
        if (null === $this->collOldPerilakuHasils || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collOldPerilakuHasils) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getOldPerilakuHasils());
            }

            $query = ChildOldPerilakuHasilQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByOldPegawai($this)
                ->count($con);
        }

        return count($this->collOldPerilakuHasils);
    }

    /**
     * Method called to associate a ChildOldPerilakuHasil object to this object
     * through the ChildOldPerilakuHasil foreign key attribute.
     *
     * @param  ChildOldPerilakuHasil $l ChildOldPerilakuHasil
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function addOldPerilakuHasil(ChildOldPerilakuHasil $l)
    {
        if ($this->collOldPerilakuHasils === null) {
            $this->initOldPerilakuHasils();
            $this->collOldPerilakuHasilsPartial = true;
        }

        if (!$this->collOldPerilakuHasils->contains($l)) {
            $this->doAddOldPerilakuHasil($l);

            if ($this->oldPerilakuHasilsScheduledForDeletion and $this->oldPerilakuHasilsScheduledForDeletion->contains($l)) {
                $this->oldPerilakuHasilsScheduledForDeletion->remove($this->oldPerilakuHasilsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildOldPerilakuHasil $oldPerilakuHasil The ChildOldPerilakuHasil object to add.
     */
    protected function doAddOldPerilakuHasil(ChildOldPerilakuHasil $oldPerilakuHasil)
    {
        $this->collOldPerilakuHasils[]= $oldPerilakuHasil;
        $oldPerilakuHasil->setOldPegawai($this);
    }

    /**
     * @param  ChildOldPerilakuHasil $oldPerilakuHasil The ChildOldPerilakuHasil object to remove.
     * @return $this|ChildOldPegawai The current object (for fluent API support)
     */
    public function removeOldPerilakuHasil(ChildOldPerilakuHasil $oldPerilakuHasil)
    {
        if ($this->getOldPerilakuHasils()->contains($oldPerilakuHasil)) {
            $pos = $this->collOldPerilakuHasils->search($oldPerilakuHasil);
            $this->collOldPerilakuHasils->remove($pos);
            if (null === $this->oldPerilakuHasilsScheduledForDeletion) {
                $this->oldPerilakuHasilsScheduledForDeletion = clone $this->collOldPerilakuHasils;
                $this->oldPerilakuHasilsScheduledForDeletion->clear();
            }
            $this->oldPerilakuHasilsScheduledForDeletion[]= $oldPerilakuHasil;
            $oldPerilakuHasil->setOldPegawai(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this OldPegawai is new, it will return
     * an empty collection; or if this OldPegawai has previously
     * been saved, it will retrieve related OldPerilakuHasils from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in OldPegawai.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOldPerilakuHasil[] List of ChildOldPerilakuHasil objects
     */
    public function getOldPerilakuHasilsJoinOldPerilakuTes(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOldPerilakuHasilQuery::create(null, $criteria);
        $query->joinWith('OldPerilakuTes', $joinBehavior);

        return $this->getOldPerilakuHasils($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this OldPegawai is new, it will return
     * an empty collection; or if this OldPegawai has previously
     * been saved, it will retrieve related OldPerilakuHasils from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in OldPegawai.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOldPerilakuHasil[] List of ChildOldPerilakuHasil objects
     */
    public function getOldPerilakuHasilsJoinOldPerilakuAspek(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOldPerilakuHasilQuery::create(null, $criteria);
        $query->joinWith('OldPerilakuAspek', $joinBehavior);

        return $this->getOldPerilakuHasils($query, $con);
    }

    /**
     * Clears out the collOldPegawaisRelatedById collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addOldPegawaisRelatedById()
     */
    public function clearOldPegawaisRelatedById()
    {
        $this->collOldPegawaisRelatedById = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collOldPegawaisRelatedById collection loaded partially.
     */
    public function resetPartialOldPegawaisRelatedById($v = true)
    {
        $this->collOldPegawaisRelatedByIdPartial = $v;
    }

    /**
     * Initializes the collOldPegawaisRelatedById collection.
     *
     * By default this just sets the collOldPegawaisRelatedById collection to an empty array (like clearcollOldPegawaisRelatedById());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initOldPegawaisRelatedById($overrideExisting = true)
    {
        if (null !== $this->collOldPegawaisRelatedById && !$overrideExisting) {
            return;
        }

        $collectionClassName = OldPegawaiTableMap::getTableMap()->getCollectionClassName();

        $this->collOldPegawaisRelatedById = new $collectionClassName;
        $this->collOldPegawaisRelatedById->setModel('\CoreBundle\Model\OldEperformance\OldPegawai');
    }

    /**
     * Gets an array of ChildOldPegawai objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildOldPegawai is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildOldPegawai[] List of ChildOldPegawai objects
     * @throws PropelException
     */
    public function getOldPegawaisRelatedById(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collOldPegawaisRelatedByIdPartial && !$this->isNew();
        if (null === $this->collOldPegawaisRelatedById || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collOldPegawaisRelatedById) {
                // return empty collection
                $this->initOldPegawaisRelatedById();
            } else {
                $collOldPegawaisRelatedById = ChildOldPegawaiQuery::create(null, $criteria)
                    ->filterByOldPegawai($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collOldPegawaisRelatedByIdPartial && count($collOldPegawaisRelatedById)) {
                        $this->initOldPegawaisRelatedById(false);

                        foreach ($collOldPegawaisRelatedById as $obj) {
                            if (false == $this->collOldPegawaisRelatedById->contains($obj)) {
                                $this->collOldPegawaisRelatedById->append($obj);
                            }
                        }

                        $this->collOldPegawaisRelatedByIdPartial = true;
                    }

                    return $collOldPegawaisRelatedById;
                }

                if ($partial && $this->collOldPegawaisRelatedById) {
                    foreach ($this->collOldPegawaisRelatedById as $obj) {
                        if ($obj->isNew()) {
                            $collOldPegawaisRelatedById[] = $obj;
                        }
                    }
                }

                $this->collOldPegawaisRelatedById = $collOldPegawaisRelatedById;
                $this->collOldPegawaisRelatedByIdPartial = false;
            }
        }

        return $this->collOldPegawaisRelatedById;
    }

    /**
     * Sets a collection of ChildOldPegawai objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $oldPegawaisRelatedById A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildOldPegawai The current object (for fluent API support)
     */
    public function setOldPegawaisRelatedById(Collection $oldPegawaisRelatedById, ConnectionInterface $con = null)
    {
        /** @var ChildOldPegawai[] $oldPegawaisRelatedByIdToDelete */
        $oldPegawaisRelatedByIdToDelete = $this->getOldPegawaisRelatedById(new Criteria(), $con)->diff($oldPegawaisRelatedById);


        $this->oldPegawaisRelatedByIdScheduledForDeletion = $oldPegawaisRelatedByIdToDelete;

        foreach ($oldPegawaisRelatedByIdToDelete as $oldPegawaiRelatedByIdRemoved) {
            $oldPegawaiRelatedByIdRemoved->setOldPegawai(null);
        }

        $this->collOldPegawaisRelatedById = null;
        foreach ($oldPegawaisRelatedById as $oldPegawaiRelatedById) {
            $this->addOldPegawaiRelatedById($oldPegawaiRelatedById);
        }

        $this->collOldPegawaisRelatedById = $oldPegawaisRelatedById;
        $this->collOldPegawaisRelatedByIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related OldPegawai objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related OldPegawai objects.
     * @throws PropelException
     */
    public function countOldPegawaisRelatedById(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collOldPegawaisRelatedByIdPartial && !$this->isNew();
        if (null === $this->collOldPegawaisRelatedById || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collOldPegawaisRelatedById) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getOldPegawaisRelatedById());
            }

            $query = ChildOldPegawaiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByOldPegawai($this)
                ->count($con);
        }

        return count($this->collOldPegawaisRelatedById);
    }

    /**
     * Method called to associate a ChildOldPegawai object to this object
     * through the ChildOldPegawai foreign key attribute.
     *
     * @param  ChildOldPegawai $l ChildOldPegawai
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function addOldPegawaiRelatedById(ChildOldPegawai $l)
    {
        if ($this->collOldPegawaisRelatedById === null) {
            $this->initOldPegawaisRelatedById();
            $this->collOldPegawaisRelatedByIdPartial = true;
        }

        if (!$this->collOldPegawaisRelatedById->contains($l)) {
            $this->doAddOldPegawaiRelatedById($l);

            if ($this->oldPegawaisRelatedByIdScheduledForDeletion and $this->oldPegawaisRelatedByIdScheduledForDeletion->contains($l)) {
                $this->oldPegawaisRelatedByIdScheduledForDeletion->remove($this->oldPegawaisRelatedByIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildOldPegawai $oldPegawaiRelatedById The ChildOldPegawai object to add.
     */
    protected function doAddOldPegawaiRelatedById(ChildOldPegawai $oldPegawaiRelatedById)
    {
        $this->collOldPegawaisRelatedById[]= $oldPegawaiRelatedById;
        $oldPegawaiRelatedById->setOldPegawai($this);
    }

    /**
     * @param  ChildOldPegawai $oldPegawaiRelatedById The ChildOldPegawai object to remove.
     * @return $this|ChildOldPegawai The current object (for fluent API support)
     */
    public function removeOldPegawaiRelatedById(ChildOldPegawai $oldPegawaiRelatedById)
    {
        if ($this->getOldPegawaisRelatedById()->contains($oldPegawaiRelatedById)) {
            $pos = $this->collOldPegawaisRelatedById->search($oldPegawaiRelatedById);
            $this->collOldPegawaisRelatedById->remove($pos);
            if (null === $this->oldPegawaisRelatedByIdScheduledForDeletion) {
                $this->oldPegawaisRelatedByIdScheduledForDeletion = clone $this->collOldPegawaisRelatedById;
                $this->oldPegawaisRelatedByIdScheduledForDeletion->clear();
            }
            $this->oldPegawaisRelatedByIdScheduledForDeletion[]= $oldPegawaiRelatedById;
            $oldPegawaiRelatedById->setOldPegawai(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this OldPegawai is new, it will return
     * an empty collection; or if this OldPegawai has previously
     * been saved, it will retrieve related OldPegawaisRelatedById from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in OldPegawai.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOldPegawai[] List of ChildOldPegawai objects
     */
    public function getOldPegawaisRelatedByIdJoinOldMasterSkpd(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOldPegawaiQuery::create(null, $criteria);
        $query->joinWith('OldMasterSkpd', $joinBehavior);

        return $this->getOldPegawaisRelatedById($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this OldPegawai is new, it will return
     * an empty collection; or if this OldPegawai has previously
     * been saved, it will retrieve related OldPegawaisRelatedById from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in OldPegawai.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOldPegawai[] List of ChildOldPegawai objects
     */
    public function getOldPegawaisRelatedByIdJoinOldJabatanStruktural(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOldPegawaiQuery::create(null, $criteria);
        $query->joinWith('OldJabatanStruktural', $joinBehavior);

        return $this->getOldPegawaisRelatedById($query, $con);
    }

    /**
     * Clears out the collOldIndikatorKinerjas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addOldIndikatorKinerjas()
     */
    public function clearOldIndikatorKinerjas()
    {
        $this->collOldIndikatorKinerjas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collOldIndikatorKinerjas collection loaded partially.
     */
    public function resetPartialOldIndikatorKinerjas($v = true)
    {
        $this->collOldIndikatorKinerjasPartial = $v;
    }

    /**
     * Initializes the collOldIndikatorKinerjas collection.
     *
     * By default this just sets the collOldIndikatorKinerjas collection to an empty array (like clearcollOldIndikatorKinerjas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initOldIndikatorKinerjas($overrideExisting = true)
    {
        if (null !== $this->collOldIndikatorKinerjas && !$overrideExisting) {
            return;
        }

        $collectionClassName = OldIndikatorKinerjaTableMap::getTableMap()->getCollectionClassName();

        $this->collOldIndikatorKinerjas = new $collectionClassName;
        $this->collOldIndikatorKinerjas->setModel('\CoreBundle\Model\OldEperformance\OldIndikatorKinerja');
    }

    /**
     * Gets an array of ChildOldIndikatorKinerja objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildOldPegawai is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildOldIndikatorKinerja[] List of ChildOldIndikatorKinerja objects
     * @throws PropelException
     */
    public function getOldIndikatorKinerjas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collOldIndikatorKinerjasPartial && !$this->isNew();
        if (null === $this->collOldIndikatorKinerjas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collOldIndikatorKinerjas) {
                // return empty collection
                $this->initOldIndikatorKinerjas();
            } else {
                $collOldIndikatorKinerjas = ChildOldIndikatorKinerjaQuery::create(null, $criteria)
                    ->filterByOldPegawai($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collOldIndikatorKinerjasPartial && count($collOldIndikatorKinerjas)) {
                        $this->initOldIndikatorKinerjas(false);

                        foreach ($collOldIndikatorKinerjas as $obj) {
                            if (false == $this->collOldIndikatorKinerjas->contains($obj)) {
                                $this->collOldIndikatorKinerjas->append($obj);
                            }
                        }

                        $this->collOldIndikatorKinerjasPartial = true;
                    }

                    return $collOldIndikatorKinerjas;
                }

                if ($partial && $this->collOldIndikatorKinerjas) {
                    foreach ($this->collOldIndikatorKinerjas as $obj) {
                        if ($obj->isNew()) {
                            $collOldIndikatorKinerjas[] = $obj;
                        }
                    }
                }

                $this->collOldIndikatorKinerjas = $collOldIndikatorKinerjas;
                $this->collOldIndikatorKinerjasPartial = false;
            }
        }

        return $this->collOldIndikatorKinerjas;
    }

    /**
     * Sets a collection of ChildOldIndikatorKinerja objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $oldIndikatorKinerjas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildOldPegawai The current object (for fluent API support)
     */
    public function setOldIndikatorKinerjas(Collection $oldIndikatorKinerjas, ConnectionInterface $con = null)
    {
        /** @var ChildOldIndikatorKinerja[] $oldIndikatorKinerjasToDelete */
        $oldIndikatorKinerjasToDelete = $this->getOldIndikatorKinerjas(new Criteria(), $con)->diff($oldIndikatorKinerjas);


        $this->oldIndikatorKinerjasScheduledForDeletion = $oldIndikatorKinerjasToDelete;

        foreach ($oldIndikatorKinerjasToDelete as $oldIndikatorKinerjaRemoved) {
            $oldIndikatorKinerjaRemoved->setOldPegawai(null);
        }

        $this->collOldIndikatorKinerjas = null;
        foreach ($oldIndikatorKinerjas as $oldIndikatorKinerja) {
            $this->addOldIndikatorKinerja($oldIndikatorKinerja);
        }

        $this->collOldIndikatorKinerjas = $oldIndikatorKinerjas;
        $this->collOldIndikatorKinerjasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related OldIndikatorKinerja objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related OldIndikatorKinerja objects.
     * @throws PropelException
     */
    public function countOldIndikatorKinerjas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collOldIndikatorKinerjasPartial && !$this->isNew();
        if (null === $this->collOldIndikatorKinerjas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collOldIndikatorKinerjas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getOldIndikatorKinerjas());
            }

            $query = ChildOldIndikatorKinerjaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByOldPegawai($this)
                ->count($con);
        }

        return count($this->collOldIndikatorKinerjas);
    }

    /**
     * Method called to associate a ChildOldIndikatorKinerja object to this object
     * through the ChildOldIndikatorKinerja foreign key attribute.
     *
     * @param  ChildOldIndikatorKinerja $l ChildOldIndikatorKinerja
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function addOldIndikatorKinerja(ChildOldIndikatorKinerja $l)
    {
        if ($this->collOldIndikatorKinerjas === null) {
            $this->initOldIndikatorKinerjas();
            $this->collOldIndikatorKinerjasPartial = true;
        }

        if (!$this->collOldIndikatorKinerjas->contains($l)) {
            $this->doAddOldIndikatorKinerja($l);

            if ($this->oldIndikatorKinerjasScheduledForDeletion and $this->oldIndikatorKinerjasScheduledForDeletion->contains($l)) {
                $this->oldIndikatorKinerjasScheduledForDeletion->remove($this->oldIndikatorKinerjasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildOldIndikatorKinerja $oldIndikatorKinerja The ChildOldIndikatorKinerja object to add.
     */
    protected function doAddOldIndikatorKinerja(ChildOldIndikatorKinerja $oldIndikatorKinerja)
    {
        $this->collOldIndikatorKinerjas[]= $oldIndikatorKinerja;
        $oldIndikatorKinerja->setOldPegawai($this);
    }

    /**
     * @param  ChildOldIndikatorKinerja $oldIndikatorKinerja The ChildOldIndikatorKinerja object to remove.
     * @return $this|ChildOldPegawai The current object (for fluent API support)
     */
    public function removeOldIndikatorKinerja(ChildOldIndikatorKinerja $oldIndikatorKinerja)
    {
        if ($this->getOldIndikatorKinerjas()->contains($oldIndikatorKinerja)) {
            $pos = $this->collOldIndikatorKinerjas->search($oldIndikatorKinerja);
            $this->collOldIndikatorKinerjas->remove($pos);
            if (null === $this->oldIndikatorKinerjasScheduledForDeletion) {
                $this->oldIndikatorKinerjasScheduledForDeletion = clone $this->collOldIndikatorKinerjas;
                $this->oldIndikatorKinerjasScheduledForDeletion->clear();
            }
            $this->oldIndikatorKinerjasScheduledForDeletion[]= $oldIndikatorKinerja;
            $oldIndikatorKinerja->setOldPegawai(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this OldPegawai is new, it will return
     * an empty collection; or if this OldPegawai has previously
     * been saved, it will retrieve related OldIndikatorKinerjas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in OldPegawai.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOldIndikatorKinerja[] List of ChildOldIndikatorKinerja objects
     */
    public function getOldIndikatorKinerjasJoinOldMasterSkpd(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOldIndikatorKinerjaQuery::create(null, $criteria);
        $query->joinWith('OldMasterSkpd', $joinBehavior);

        return $this->getOldIndikatorKinerjas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this OldPegawai is new, it will return
     * an empty collection; or if this OldPegawai has previously
     * been saved, it will retrieve related OldIndikatorKinerjas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in OldPegawai.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOldIndikatorKinerja[] List of ChildOldIndikatorKinerja objects
     */
    public function getOldIndikatorKinerjasJoinOldIndikatorKinerja(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOldIndikatorKinerjaQuery::create(null, $criteria);
        $query->joinWith('OldIndikatorKinerja', $joinBehavior);

        return $this->getOldIndikatorKinerjas($query, $con);
    }

    /**
     * Clears out the collOldKegiatanPegawais collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addOldKegiatanPegawais()
     */
    public function clearOldKegiatanPegawais()
    {
        $this->collOldKegiatanPegawais = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collOldKegiatanPegawais collection loaded partially.
     */
    public function resetPartialOldKegiatanPegawais($v = true)
    {
        $this->collOldKegiatanPegawaisPartial = $v;
    }

    /**
     * Initializes the collOldKegiatanPegawais collection.
     *
     * By default this just sets the collOldKegiatanPegawais collection to an empty array (like clearcollOldKegiatanPegawais());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initOldKegiatanPegawais($overrideExisting = true)
    {
        if (null !== $this->collOldKegiatanPegawais && !$overrideExisting) {
            return;
        }

        $collectionClassName = OldKegiatanPegawaiTableMap::getTableMap()->getCollectionClassName();

        $this->collOldKegiatanPegawais = new $collectionClassName;
        $this->collOldKegiatanPegawais->setModel('\CoreBundle\Model\OldEperformance\OldKegiatanPegawai');
    }

    /**
     * Gets an array of ChildOldKegiatanPegawai objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildOldPegawai is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildOldKegiatanPegawai[] List of ChildOldKegiatanPegawai objects
     * @throws PropelException
     */
    public function getOldKegiatanPegawais(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collOldKegiatanPegawaisPartial && !$this->isNew();
        if (null === $this->collOldKegiatanPegawais || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collOldKegiatanPegawais) {
                // return empty collection
                $this->initOldKegiatanPegawais();
            } else {
                $collOldKegiatanPegawais = ChildOldKegiatanPegawaiQuery::create(null, $criteria)
                    ->filterByOldPegawai($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collOldKegiatanPegawaisPartial && count($collOldKegiatanPegawais)) {
                        $this->initOldKegiatanPegawais(false);

                        foreach ($collOldKegiatanPegawais as $obj) {
                            if (false == $this->collOldKegiatanPegawais->contains($obj)) {
                                $this->collOldKegiatanPegawais->append($obj);
                            }
                        }

                        $this->collOldKegiatanPegawaisPartial = true;
                    }

                    return $collOldKegiatanPegawais;
                }

                if ($partial && $this->collOldKegiatanPegawais) {
                    foreach ($this->collOldKegiatanPegawais as $obj) {
                        if ($obj->isNew()) {
                            $collOldKegiatanPegawais[] = $obj;
                        }
                    }
                }

                $this->collOldKegiatanPegawais = $collOldKegiatanPegawais;
                $this->collOldKegiatanPegawaisPartial = false;
            }
        }

        return $this->collOldKegiatanPegawais;
    }

    /**
     * Sets a collection of ChildOldKegiatanPegawai objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $oldKegiatanPegawais A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildOldPegawai The current object (for fluent API support)
     */
    public function setOldKegiatanPegawais(Collection $oldKegiatanPegawais, ConnectionInterface $con = null)
    {
        /** @var ChildOldKegiatanPegawai[] $oldKegiatanPegawaisToDelete */
        $oldKegiatanPegawaisToDelete = $this->getOldKegiatanPegawais(new Criteria(), $con)->diff($oldKegiatanPegawais);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->oldKegiatanPegawaisScheduledForDeletion = clone $oldKegiatanPegawaisToDelete;

        foreach ($oldKegiatanPegawaisToDelete as $oldKegiatanPegawaiRemoved) {
            $oldKegiatanPegawaiRemoved->setOldPegawai(null);
        }

        $this->collOldKegiatanPegawais = null;
        foreach ($oldKegiatanPegawais as $oldKegiatanPegawai) {
            $this->addOldKegiatanPegawai($oldKegiatanPegawai);
        }

        $this->collOldKegiatanPegawais = $oldKegiatanPegawais;
        $this->collOldKegiatanPegawaisPartial = false;

        return $this;
    }

    /**
     * Returns the number of related OldKegiatanPegawai objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related OldKegiatanPegawai objects.
     * @throws PropelException
     */
    public function countOldKegiatanPegawais(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collOldKegiatanPegawaisPartial && !$this->isNew();
        if (null === $this->collOldKegiatanPegawais || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collOldKegiatanPegawais) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getOldKegiatanPegawais());
            }

            $query = ChildOldKegiatanPegawaiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByOldPegawai($this)
                ->count($con);
        }

        return count($this->collOldKegiatanPegawais);
    }

    /**
     * Method called to associate a ChildOldKegiatanPegawai object to this object
     * through the ChildOldKegiatanPegawai foreign key attribute.
     *
     * @param  ChildOldKegiatanPegawai $l ChildOldKegiatanPegawai
     * @return $this|\CoreBundle\Model\OldEperformance\OldPegawai The current object (for fluent API support)
     */
    public function addOldKegiatanPegawai(ChildOldKegiatanPegawai $l)
    {
        if ($this->collOldKegiatanPegawais === null) {
            $this->initOldKegiatanPegawais();
            $this->collOldKegiatanPegawaisPartial = true;
        }

        if (!$this->collOldKegiatanPegawais->contains($l)) {
            $this->doAddOldKegiatanPegawai($l);

            if ($this->oldKegiatanPegawaisScheduledForDeletion and $this->oldKegiatanPegawaisScheduledForDeletion->contains($l)) {
                $this->oldKegiatanPegawaisScheduledForDeletion->remove($this->oldKegiatanPegawaisScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildOldKegiatanPegawai $oldKegiatanPegawai The ChildOldKegiatanPegawai object to add.
     */
    protected function doAddOldKegiatanPegawai(ChildOldKegiatanPegawai $oldKegiatanPegawai)
    {
        $this->collOldKegiatanPegawais[]= $oldKegiatanPegawai;
        $oldKegiatanPegawai->setOldPegawai($this);
    }

    /**
     * @param  ChildOldKegiatanPegawai $oldKegiatanPegawai The ChildOldKegiatanPegawai object to remove.
     * @return $this|ChildOldPegawai The current object (for fluent API support)
     */
    public function removeOldKegiatanPegawai(ChildOldKegiatanPegawai $oldKegiatanPegawai)
    {
        if ($this->getOldKegiatanPegawais()->contains($oldKegiatanPegawai)) {
            $pos = $this->collOldKegiatanPegawais->search($oldKegiatanPegawai);
            $this->collOldKegiatanPegawais->remove($pos);
            if (null === $this->oldKegiatanPegawaisScheduledForDeletion) {
                $this->oldKegiatanPegawaisScheduledForDeletion = clone $this->collOldKegiatanPegawais;
                $this->oldKegiatanPegawaisScheduledForDeletion->clear();
            }
            $this->oldKegiatanPegawaisScheduledForDeletion[]= clone $oldKegiatanPegawai;
            $oldKegiatanPegawai->setOldPegawai(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this OldPegawai is new, it will return
     * an empty collection; or if this OldPegawai has previously
     * been saved, it will retrieve related OldKegiatanPegawais from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in OldPegawai.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOldKegiatanPegawai[] List of ChildOldKegiatanPegawai objects
     */
    public function getOldKegiatanPegawaisJoinOldMonevTspKegiatan(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOldKegiatanPegawaiQuery::create(null, $criteria);
        $query->joinWith('OldMonevTspKegiatan', $joinBehavior);

        return $this->getOldKegiatanPegawais($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aOldMasterSkpd) {
            $this->aOldMasterSkpd->removeOldPegawai($this);
        }
        if (null !== $this->aOldPegawai) {
            $this->aOldPegawai->removeOldPegawaiRelatedById($this);
        }
        if (null !== $this->aOldJabatanStruktural) {
            $this->aOldJabatanStruktural->removeOldPegawai($this);
        }
        $this->id = null;
        $this->nip = null;
        $this->nama = null;
        $this->golongan = null;
        $this->tmt = null;
        $this->pendidikan_terakhir = null;
        $this->skpd_id = null;
        $this->atasan_id = null;
        $this->is_atasan = null;
        $this->level_struktural = null;
        $this->level_penilaian = null;
        $this->password = null;
        $this->user_id = null;
        $this->username_eproject = null;
        $this->tgl_masuk = null;
        $this->tgl_keluar = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->deleted_at = null;
        $this->is_out = null;
        $this->is_ka_uptd = null;
        $this->is_struktural = null;
        $this->is_kunci_aktifitas = null;
        $this->is_only_skp = null;
        $this->jabatan_struktural_id = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collOldPerilakuHasils) {
                foreach ($this->collOldPerilakuHasils as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collOldPegawaisRelatedById) {
                foreach ($this->collOldPegawaisRelatedById as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collOldIndikatorKinerjas) {
                foreach ($this->collOldIndikatorKinerjas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collOldKegiatanPegawais) {
                foreach ($this->collOldKegiatanPegawais as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collOldPerilakuHasils = null;
        $this->collOldPegawaisRelatedById = null;
        $this->collOldIndikatorKinerjas = null;
        $this->collOldKegiatanPegawais = null;
        $this->aOldMasterSkpd = null;
        $this->aOldPegawai = null;
        $this->aOldJabatanStruktural = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(OldPegawaiTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}

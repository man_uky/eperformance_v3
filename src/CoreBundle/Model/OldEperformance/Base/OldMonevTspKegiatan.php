<?php

namespace CoreBundle\Model\OldEperformance\Base;

use \DateTime;
use \Exception;
use \PDO;
use CoreBundle\Model\OldEperformance\OldKegiatanPegawai as ChildOldKegiatanPegawai;
use CoreBundle\Model\OldEperformance\OldKegiatanPegawaiQuery as ChildOldKegiatanPegawaiQuery;
use CoreBundle\Model\OldEperformance\OldMasterSkpd as ChildOldMasterSkpd;
use CoreBundle\Model\OldEperformance\OldMasterSkpdQuery as ChildOldMasterSkpdQuery;
use CoreBundle\Model\OldEperformance\OldMonevTspKegiatan as ChildOldMonevTspKegiatan;
use CoreBundle\Model\OldEperformance\OldMonevTspKegiatanQuery as ChildOldMonevTspKegiatanQuery;
use CoreBundle\Model\OldEperformance\Map\OldKegiatanPegawaiTableMap;
use CoreBundle\Model\OldEperformance\Map\OldMonevTspKegiatanTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'eperformance.monev_tsp_kegiatan' table.
 *
 *
 *
 * @package    propel.generator.src.CoreBundle.Model.OldEperformance.Base
 */
abstract class OldMonevTspKegiatan implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\CoreBundle\\Model\\OldEperformance\\Map\\OldMonevTspKegiatanTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the kegiatan_id field.
     *
     * @var        int
     */
    protected $kegiatan_id;

    /**
     * The value for the kegiatan_kode field.
     *
     * @var        string
     */
    protected $kegiatan_kode;

    /**
     * The value for the kegiatan_nama field.
     *
     * @var        string
     */
    protected $kegiatan_nama;

    /**
     * The value for the unit_id field.
     *
     * @var        int
     */
    protected $unit_id;

    /**
     * The value for the unit_kode field.
     *
     * @var        string
     */
    protected $unit_kode;

    /**
     * The value for the unit_nama field.
     *
     * @var        string
     */
    protected $unit_nama;

    /**
     * The value for the tahun field.
     *
     * @var        int
     */
    protected $tahun;

    /**
     * The value for the target field.
     *
     * @var        string
     */
    protected $target;

    /**
     * The value for the realisasi field.
     *
     * @var        string
     */
    protected $realisasi;

    /**
     * The value for the satuan field.
     *
     * @var        string
     */
    protected $satuan;

    /**
     * The value for the capaian field.
     *
     * @var        string
     */
    protected $capaian;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildOldMasterSkpd
     */
    protected $aOldMasterSkpd;

    /**
     * @var        ObjectCollection|ChildOldKegiatanPegawai[] Collection to store aggregation of ChildOldKegiatanPegawai objects.
     */
    protected $collOldKegiatanPegawais;
    protected $collOldKegiatanPegawaisPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildOldKegiatanPegawai[]
     */
    protected $oldKegiatanPegawaisScheduledForDeletion = null;

    /**
     * Initializes internal state of CoreBundle\Model\OldEperformance\Base\OldMonevTspKegiatan object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>OldMonevTspKegiatan</code> instance.  If
     * <code>obj</code> is an instance of <code>OldMonevTspKegiatan</code>, delegates to
     * <code>equals(OldMonevTspKegiatan)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|OldMonevTspKegiatan The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [kegiatan_id] column value.
     *
     * @return int
     */
    public function getKegiatanId()
    {
        return $this->kegiatan_id;
    }

    /**
     * Get the [kegiatan_kode] column value.
     *
     * @return string
     */
    public function getKegiatanKode()
    {
        return $this->kegiatan_kode;
    }

    /**
     * Get the [kegiatan_nama] column value.
     *
     * @return string
     */
    public function getKegiatanNama()
    {
        return $this->kegiatan_nama;
    }

    /**
     * Get the [unit_id] column value.
     *
     * @return int
     */
    public function getUnitId()
    {
        return $this->unit_id;
    }

    /**
     * Get the [unit_kode] column value.
     *
     * @return string
     */
    public function getUnitKode()
    {
        return $this->unit_kode;
    }

    /**
     * Get the [unit_nama] column value.
     *
     * @return string
     */
    public function getUnitNama()
    {
        return $this->unit_nama;
    }

    /**
     * Get the [tahun] column value.
     *
     * @return int
     */
    public function getTahun()
    {
        return $this->tahun;
    }

    /**
     * Get the [target] column value.
     *
     * @return string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Get the [realisasi] column value.
     *
     * @return string
     */
    public function getRealisasi()
    {
        return $this->realisasi;
    }

    /**
     * Get the [satuan] column value.
     *
     * @return string
     */
    public function getSatuan()
    {
        return $this->satuan;
    }

    /**
     * Get the [capaian] column value.
     *
     * @return string
     */
    public function getCapaian()
    {
        return $this->capaian;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTspKegiatan The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[OldMonevTspKegiatanTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [kegiatan_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTspKegiatan The current object (for fluent API support)
     */
    public function setKegiatanId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->kegiatan_id !== $v) {
            $this->kegiatan_id = $v;
            $this->modifiedColumns[OldMonevTspKegiatanTableMap::COL_KEGIATAN_ID] = true;
        }

        return $this;
    } // setKegiatanId()

    /**
     * Set the value of [kegiatan_kode] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTspKegiatan The current object (for fluent API support)
     */
    public function setKegiatanKode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->kegiatan_kode !== $v) {
            $this->kegiatan_kode = $v;
            $this->modifiedColumns[OldMonevTspKegiatanTableMap::COL_KEGIATAN_KODE] = true;
        }

        return $this;
    } // setKegiatanKode()

    /**
     * Set the value of [kegiatan_nama] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTspKegiatan The current object (for fluent API support)
     */
    public function setKegiatanNama($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->kegiatan_nama !== $v) {
            $this->kegiatan_nama = $v;
            $this->modifiedColumns[OldMonevTspKegiatanTableMap::COL_KEGIATAN_NAMA] = true;
        }

        return $this;
    } // setKegiatanNama()

    /**
     * Set the value of [unit_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTspKegiatan The current object (for fluent API support)
     */
    public function setUnitId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->unit_id !== $v) {
            $this->unit_id = $v;
            $this->modifiedColumns[OldMonevTspKegiatanTableMap::COL_UNIT_ID] = true;
        }

        if ($this->aOldMasterSkpd !== null && $this->aOldMasterSkpd->getSkpdId() !== $v) {
            $this->aOldMasterSkpd = null;
        }

        return $this;
    } // setUnitId()

    /**
     * Set the value of [unit_kode] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTspKegiatan The current object (for fluent API support)
     */
    public function setUnitKode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->unit_kode !== $v) {
            $this->unit_kode = $v;
            $this->modifiedColumns[OldMonevTspKegiatanTableMap::COL_UNIT_KODE] = true;
        }

        return $this;
    } // setUnitKode()

    /**
     * Set the value of [unit_nama] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTspKegiatan The current object (for fluent API support)
     */
    public function setUnitNama($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->unit_nama !== $v) {
            $this->unit_nama = $v;
            $this->modifiedColumns[OldMonevTspKegiatanTableMap::COL_UNIT_NAMA] = true;
        }

        return $this;
    } // setUnitNama()

    /**
     * Set the value of [tahun] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTspKegiatan The current object (for fluent API support)
     */
    public function setTahun($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->tahun !== $v) {
            $this->tahun = $v;
            $this->modifiedColumns[OldMonevTspKegiatanTableMap::COL_TAHUN] = true;
        }

        return $this;
    } // setTahun()

    /**
     * Set the value of [target] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTspKegiatan The current object (for fluent API support)
     */
    public function setTarget($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->target !== $v) {
            $this->target = $v;
            $this->modifiedColumns[OldMonevTspKegiatanTableMap::COL_TARGET] = true;
        }

        return $this;
    } // setTarget()

    /**
     * Set the value of [realisasi] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTspKegiatan The current object (for fluent API support)
     */
    public function setRealisasi($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->realisasi !== $v) {
            $this->realisasi = $v;
            $this->modifiedColumns[OldMonevTspKegiatanTableMap::COL_REALISASI] = true;
        }

        return $this;
    } // setRealisasi()

    /**
     * Set the value of [satuan] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTspKegiatan The current object (for fluent API support)
     */
    public function setSatuan($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->satuan !== $v) {
            $this->satuan = $v;
            $this->modifiedColumns[OldMonevTspKegiatanTableMap::COL_SATUAN] = true;
        }

        return $this;
    } // setSatuan()

    /**
     * Set the value of [capaian] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTspKegiatan The current object (for fluent API support)
     */
    public function setCapaian($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->capaian !== $v) {
            $this->capaian = $v;
            $this->modifiedColumns[OldMonevTspKegiatanTableMap::COL_CAPAIAN] = true;
        }

        return $this;
    } // setCapaian()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTspKegiatan The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[OldMonevTspKegiatanTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTspKegiatan The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[OldMonevTspKegiatanTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : OldMonevTspKegiatanTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : OldMonevTspKegiatanTableMap::translateFieldName('KegiatanId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->kegiatan_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : OldMonevTspKegiatanTableMap::translateFieldName('KegiatanKode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->kegiatan_kode = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : OldMonevTspKegiatanTableMap::translateFieldName('KegiatanNama', TableMap::TYPE_PHPNAME, $indexType)];
            $this->kegiatan_nama = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : OldMonevTspKegiatanTableMap::translateFieldName('UnitId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->unit_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : OldMonevTspKegiatanTableMap::translateFieldName('UnitKode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->unit_kode = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : OldMonevTspKegiatanTableMap::translateFieldName('UnitNama', TableMap::TYPE_PHPNAME, $indexType)];
            $this->unit_nama = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : OldMonevTspKegiatanTableMap::translateFieldName('Tahun', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tahun = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : OldMonevTspKegiatanTableMap::translateFieldName('Target', TableMap::TYPE_PHPNAME, $indexType)];
            $this->target = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : OldMonevTspKegiatanTableMap::translateFieldName('Realisasi', TableMap::TYPE_PHPNAME, $indexType)];
            $this->realisasi = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : OldMonevTspKegiatanTableMap::translateFieldName('Satuan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->satuan = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : OldMonevTspKegiatanTableMap::translateFieldName('Capaian', TableMap::TYPE_PHPNAME, $indexType)];
            $this->capaian = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : OldMonevTspKegiatanTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : OldMonevTspKegiatanTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 14; // 14 = OldMonevTspKegiatanTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\CoreBundle\\Model\\OldEperformance\\OldMonevTspKegiatan'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aOldMasterSkpd !== null && $this->unit_id !== $this->aOldMasterSkpd->getSkpdId()) {
            $this->aOldMasterSkpd = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(OldMonevTspKegiatanTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildOldMonevTspKegiatanQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aOldMasterSkpd = null;
            $this->collOldKegiatanPegawais = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see OldMonevTspKegiatan::setDeleted()
     * @see OldMonevTspKegiatan::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldMonevTspKegiatanTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildOldMonevTspKegiatanQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldMonevTspKegiatanTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                OldMonevTspKegiatanTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aOldMasterSkpd !== null) {
                if ($this->aOldMasterSkpd->isModified() || $this->aOldMasterSkpd->isNew()) {
                    $affectedRows += $this->aOldMasterSkpd->save($con);
                }
                $this->setOldMasterSkpd($this->aOldMasterSkpd);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->oldKegiatanPegawaisScheduledForDeletion !== null) {
                if (!$this->oldKegiatanPegawaisScheduledForDeletion->isEmpty()) {
                    \CoreBundle\Model\OldEperformance\OldKegiatanPegawaiQuery::create()
                        ->filterByPrimaryKeys($this->oldKegiatanPegawaisScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->oldKegiatanPegawaisScheduledForDeletion = null;
                }
            }

            if ($this->collOldKegiatanPegawais !== null) {
                foreach ($this->collOldKegiatanPegawais as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_KEGIATAN_ID)) {
            $modifiedColumns[':p' . $index++]  = 'kegiatan_id';
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_KEGIATAN_KODE)) {
            $modifiedColumns[':p' . $index++]  = 'kegiatan_kode';
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_KEGIATAN_NAMA)) {
            $modifiedColumns[':p' . $index++]  = 'kegiatan_nama';
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_UNIT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'unit_id';
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_UNIT_KODE)) {
            $modifiedColumns[':p' . $index++]  = 'unit_kode';
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_UNIT_NAMA)) {
            $modifiedColumns[':p' . $index++]  = 'unit_nama';
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_TAHUN)) {
            $modifiedColumns[':p' . $index++]  = 'tahun';
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_TARGET)) {
            $modifiedColumns[':p' . $index++]  = 'target';
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_REALISASI)) {
            $modifiedColumns[':p' . $index++]  = 'realisasi';
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_SATUAN)) {
            $modifiedColumns[':p' . $index++]  = 'satuan';
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_CAPAIAN)) {
            $modifiedColumns[':p' . $index++]  = 'capaian';
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO eperformance.monev_tsp_kegiatan (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'kegiatan_id':
                        $stmt->bindValue($identifier, $this->kegiatan_id, PDO::PARAM_INT);
                        break;
                    case 'kegiatan_kode':
                        $stmt->bindValue($identifier, $this->kegiatan_kode, PDO::PARAM_STR);
                        break;
                    case 'kegiatan_nama':
                        $stmt->bindValue($identifier, $this->kegiatan_nama, PDO::PARAM_STR);
                        break;
                    case 'unit_id':
                        $stmt->bindValue($identifier, $this->unit_id, PDO::PARAM_INT);
                        break;
                    case 'unit_kode':
                        $stmt->bindValue($identifier, $this->unit_kode, PDO::PARAM_STR);
                        break;
                    case 'unit_nama':
                        $stmt->bindValue($identifier, $this->unit_nama, PDO::PARAM_STR);
                        break;
                    case 'tahun':
                        $stmt->bindValue($identifier, $this->tahun, PDO::PARAM_INT);
                        break;
                    case 'target':
                        $stmt->bindValue($identifier, $this->target, PDO::PARAM_INT);
                        break;
                    case 'realisasi':
                        $stmt->bindValue($identifier, $this->realisasi, PDO::PARAM_INT);
                        break;
                    case 'satuan':
                        $stmt->bindValue($identifier, $this->satuan, PDO::PARAM_STR);
                        break;
                    case 'capaian':
                        $stmt->bindValue($identifier, $this->capaian, PDO::PARAM_INT);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = OldMonevTspKegiatanTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getKegiatanId();
                break;
            case 2:
                return $this->getKegiatanKode();
                break;
            case 3:
                return $this->getKegiatanNama();
                break;
            case 4:
                return $this->getUnitId();
                break;
            case 5:
                return $this->getUnitKode();
                break;
            case 6:
                return $this->getUnitNama();
                break;
            case 7:
                return $this->getTahun();
                break;
            case 8:
                return $this->getTarget();
                break;
            case 9:
                return $this->getRealisasi();
                break;
            case 10:
                return $this->getSatuan();
                break;
            case 11:
                return $this->getCapaian();
                break;
            case 12:
                return $this->getCreatedAt();
                break;
            case 13:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['OldMonevTspKegiatan'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['OldMonevTspKegiatan'][$this->hashCode()] = true;
        $keys = OldMonevTspKegiatanTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getKegiatanId(),
            $keys[2] => $this->getKegiatanKode(),
            $keys[3] => $this->getKegiatanNama(),
            $keys[4] => $this->getUnitId(),
            $keys[5] => $this->getUnitKode(),
            $keys[6] => $this->getUnitNama(),
            $keys[7] => $this->getTahun(),
            $keys[8] => $this->getTarget(),
            $keys[9] => $this->getRealisasi(),
            $keys[10] => $this->getSatuan(),
            $keys[11] => $this->getCapaian(),
            $keys[12] => $this->getCreatedAt(),
            $keys[13] => $this->getUpdatedAt(),
        );
        if ($result[$keys[12]] instanceof \DateTime) {
            $result[$keys[12]] = $result[$keys[12]]->format('c');
        }

        if ($result[$keys[13]] instanceof \DateTime) {
            $result[$keys[13]] = $result[$keys[13]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aOldMasterSkpd) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldMasterSkpd';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.master_skpd';
                        break;
                    default:
                        $key = 'OldMasterSkpd';
                }

                $result[$key] = $this->aOldMasterSkpd->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collOldKegiatanPegawais) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldKegiatanPegawais';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.kegiatan_pegawais';
                        break;
                    default:
                        $key = 'OldKegiatanPegawais';
                }

                $result[$key] = $this->collOldKegiatanPegawais->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTspKegiatan
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = OldMonevTspKegiatanTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTspKegiatan
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setKegiatanId($value);
                break;
            case 2:
                $this->setKegiatanKode($value);
                break;
            case 3:
                $this->setKegiatanNama($value);
                break;
            case 4:
                $this->setUnitId($value);
                break;
            case 5:
                $this->setUnitKode($value);
                break;
            case 6:
                $this->setUnitNama($value);
                break;
            case 7:
                $this->setTahun($value);
                break;
            case 8:
                $this->setTarget($value);
                break;
            case 9:
                $this->setRealisasi($value);
                break;
            case 10:
                $this->setSatuan($value);
                break;
            case 11:
                $this->setCapaian($value);
                break;
            case 12:
                $this->setCreatedAt($value);
                break;
            case 13:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = OldMonevTspKegiatanTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setKegiatanId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setKegiatanKode($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setKegiatanNama($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setUnitId($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setUnitKode($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setUnitNama($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setTahun($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setTarget($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setRealisasi($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setSatuan($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setCapaian($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setCreatedAt($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setUpdatedAt($arr[$keys[13]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTspKegiatan The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(OldMonevTspKegiatanTableMap::DATABASE_NAME);

        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_ID)) {
            $criteria->add(OldMonevTspKegiatanTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_KEGIATAN_ID)) {
            $criteria->add(OldMonevTspKegiatanTableMap::COL_KEGIATAN_ID, $this->kegiatan_id);
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_KEGIATAN_KODE)) {
            $criteria->add(OldMonevTspKegiatanTableMap::COL_KEGIATAN_KODE, $this->kegiatan_kode);
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_KEGIATAN_NAMA)) {
            $criteria->add(OldMonevTspKegiatanTableMap::COL_KEGIATAN_NAMA, $this->kegiatan_nama);
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_UNIT_ID)) {
            $criteria->add(OldMonevTspKegiatanTableMap::COL_UNIT_ID, $this->unit_id);
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_UNIT_KODE)) {
            $criteria->add(OldMonevTspKegiatanTableMap::COL_UNIT_KODE, $this->unit_kode);
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_UNIT_NAMA)) {
            $criteria->add(OldMonevTspKegiatanTableMap::COL_UNIT_NAMA, $this->unit_nama);
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_TAHUN)) {
            $criteria->add(OldMonevTspKegiatanTableMap::COL_TAHUN, $this->tahun);
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_TARGET)) {
            $criteria->add(OldMonevTspKegiatanTableMap::COL_TARGET, $this->target);
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_REALISASI)) {
            $criteria->add(OldMonevTspKegiatanTableMap::COL_REALISASI, $this->realisasi);
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_SATUAN)) {
            $criteria->add(OldMonevTspKegiatanTableMap::COL_SATUAN, $this->satuan);
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_CAPAIAN)) {
            $criteria->add(OldMonevTspKegiatanTableMap::COL_CAPAIAN, $this->capaian);
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_CREATED_AT)) {
            $criteria->add(OldMonevTspKegiatanTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(OldMonevTspKegiatanTableMap::COL_UPDATED_AT)) {
            $criteria->add(OldMonevTspKegiatanTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildOldMonevTspKegiatanQuery::create();
        $criteria->add(OldMonevTspKegiatanTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \CoreBundle\Model\OldEperformance\OldMonevTspKegiatan (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setKegiatanId($this->getKegiatanId());
        $copyObj->setKegiatanKode($this->getKegiatanKode());
        $copyObj->setKegiatanNama($this->getKegiatanNama());
        $copyObj->setUnitId($this->getUnitId());
        $copyObj->setUnitKode($this->getUnitKode());
        $copyObj->setUnitNama($this->getUnitNama());
        $copyObj->setTahun($this->getTahun());
        $copyObj->setTarget($this->getTarget());
        $copyObj->setRealisasi($this->getRealisasi());
        $copyObj->setSatuan($this->getSatuan());
        $copyObj->setCapaian($this->getCapaian());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getOldKegiatanPegawais() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addOldKegiatanPegawai($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \CoreBundle\Model\OldEperformance\OldMonevTspKegiatan Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildOldMasterSkpd object.
     *
     * @param  ChildOldMasterSkpd $v
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTspKegiatan The current object (for fluent API support)
     * @throws PropelException
     */
    public function setOldMasterSkpd(ChildOldMasterSkpd $v = null)
    {
        if ($v === null) {
            $this->setUnitId(NULL);
        } else {
            $this->setUnitId($v->getSkpdId());
        }

        $this->aOldMasterSkpd = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildOldMasterSkpd object, it will not be re-added.
        if ($v !== null) {
            $v->addOldMonevTspKegiatan($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildOldMasterSkpd object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildOldMasterSkpd The associated ChildOldMasterSkpd object.
     * @throws PropelException
     */
    public function getOldMasterSkpd(ConnectionInterface $con = null)
    {
        if ($this->aOldMasterSkpd === null && ($this->unit_id !== null)) {
            $this->aOldMasterSkpd = ChildOldMasterSkpdQuery::create()->findPk($this->unit_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aOldMasterSkpd->addOldMonevTspKegiatans($this);
             */
        }

        return $this->aOldMasterSkpd;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('OldKegiatanPegawai' == $relationName) {
            return $this->initOldKegiatanPegawais();
        }
    }

    /**
     * Clears out the collOldKegiatanPegawais collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addOldKegiatanPegawais()
     */
    public function clearOldKegiatanPegawais()
    {
        $this->collOldKegiatanPegawais = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collOldKegiatanPegawais collection loaded partially.
     */
    public function resetPartialOldKegiatanPegawais($v = true)
    {
        $this->collOldKegiatanPegawaisPartial = $v;
    }

    /**
     * Initializes the collOldKegiatanPegawais collection.
     *
     * By default this just sets the collOldKegiatanPegawais collection to an empty array (like clearcollOldKegiatanPegawais());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initOldKegiatanPegawais($overrideExisting = true)
    {
        if (null !== $this->collOldKegiatanPegawais && !$overrideExisting) {
            return;
        }

        $collectionClassName = OldKegiatanPegawaiTableMap::getTableMap()->getCollectionClassName();

        $this->collOldKegiatanPegawais = new $collectionClassName;
        $this->collOldKegiatanPegawais->setModel('\CoreBundle\Model\OldEperformance\OldKegiatanPegawai');
    }

    /**
     * Gets an array of ChildOldKegiatanPegawai objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildOldMonevTspKegiatan is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildOldKegiatanPegawai[] List of ChildOldKegiatanPegawai objects
     * @throws PropelException
     */
    public function getOldKegiatanPegawais(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collOldKegiatanPegawaisPartial && !$this->isNew();
        if (null === $this->collOldKegiatanPegawais || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collOldKegiatanPegawais) {
                // return empty collection
                $this->initOldKegiatanPegawais();
            } else {
                $collOldKegiatanPegawais = ChildOldKegiatanPegawaiQuery::create(null, $criteria)
                    ->filterByOldMonevTspKegiatan($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collOldKegiatanPegawaisPartial && count($collOldKegiatanPegawais)) {
                        $this->initOldKegiatanPegawais(false);

                        foreach ($collOldKegiatanPegawais as $obj) {
                            if (false == $this->collOldKegiatanPegawais->contains($obj)) {
                                $this->collOldKegiatanPegawais->append($obj);
                            }
                        }

                        $this->collOldKegiatanPegawaisPartial = true;
                    }

                    return $collOldKegiatanPegawais;
                }

                if ($partial && $this->collOldKegiatanPegawais) {
                    foreach ($this->collOldKegiatanPegawais as $obj) {
                        if ($obj->isNew()) {
                            $collOldKegiatanPegawais[] = $obj;
                        }
                    }
                }

                $this->collOldKegiatanPegawais = $collOldKegiatanPegawais;
                $this->collOldKegiatanPegawaisPartial = false;
            }
        }

        return $this->collOldKegiatanPegawais;
    }

    /**
     * Sets a collection of ChildOldKegiatanPegawai objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $oldKegiatanPegawais A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildOldMonevTspKegiatan The current object (for fluent API support)
     */
    public function setOldKegiatanPegawais(Collection $oldKegiatanPegawais, ConnectionInterface $con = null)
    {
        /** @var ChildOldKegiatanPegawai[] $oldKegiatanPegawaisToDelete */
        $oldKegiatanPegawaisToDelete = $this->getOldKegiatanPegawais(new Criteria(), $con)->diff($oldKegiatanPegawais);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->oldKegiatanPegawaisScheduledForDeletion = clone $oldKegiatanPegawaisToDelete;

        foreach ($oldKegiatanPegawaisToDelete as $oldKegiatanPegawaiRemoved) {
            $oldKegiatanPegawaiRemoved->setOldMonevTspKegiatan(null);
        }

        $this->collOldKegiatanPegawais = null;
        foreach ($oldKegiatanPegawais as $oldKegiatanPegawai) {
            $this->addOldKegiatanPegawai($oldKegiatanPegawai);
        }

        $this->collOldKegiatanPegawais = $oldKegiatanPegawais;
        $this->collOldKegiatanPegawaisPartial = false;

        return $this;
    }

    /**
     * Returns the number of related OldKegiatanPegawai objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related OldKegiatanPegawai objects.
     * @throws PropelException
     */
    public function countOldKegiatanPegawais(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collOldKegiatanPegawaisPartial && !$this->isNew();
        if (null === $this->collOldKegiatanPegawais || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collOldKegiatanPegawais) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getOldKegiatanPegawais());
            }

            $query = ChildOldKegiatanPegawaiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByOldMonevTspKegiatan($this)
                ->count($con);
        }

        return count($this->collOldKegiatanPegawais);
    }

    /**
     * Method called to associate a ChildOldKegiatanPegawai object to this object
     * through the ChildOldKegiatanPegawai foreign key attribute.
     *
     * @param  ChildOldKegiatanPegawai $l ChildOldKegiatanPegawai
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTspKegiatan The current object (for fluent API support)
     */
    public function addOldKegiatanPegawai(ChildOldKegiatanPegawai $l)
    {
        if ($this->collOldKegiatanPegawais === null) {
            $this->initOldKegiatanPegawais();
            $this->collOldKegiatanPegawaisPartial = true;
        }

        if (!$this->collOldKegiatanPegawais->contains($l)) {
            $this->doAddOldKegiatanPegawai($l);

            if ($this->oldKegiatanPegawaisScheduledForDeletion and $this->oldKegiatanPegawaisScheduledForDeletion->contains($l)) {
                $this->oldKegiatanPegawaisScheduledForDeletion->remove($this->oldKegiatanPegawaisScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildOldKegiatanPegawai $oldKegiatanPegawai The ChildOldKegiatanPegawai object to add.
     */
    protected function doAddOldKegiatanPegawai(ChildOldKegiatanPegawai $oldKegiatanPegawai)
    {
        $this->collOldKegiatanPegawais[]= $oldKegiatanPegawai;
        $oldKegiatanPegawai->setOldMonevTspKegiatan($this);
    }

    /**
     * @param  ChildOldKegiatanPegawai $oldKegiatanPegawai The ChildOldKegiatanPegawai object to remove.
     * @return $this|ChildOldMonevTspKegiatan The current object (for fluent API support)
     */
    public function removeOldKegiatanPegawai(ChildOldKegiatanPegawai $oldKegiatanPegawai)
    {
        if ($this->getOldKegiatanPegawais()->contains($oldKegiatanPegawai)) {
            $pos = $this->collOldKegiatanPegawais->search($oldKegiatanPegawai);
            $this->collOldKegiatanPegawais->remove($pos);
            if (null === $this->oldKegiatanPegawaisScheduledForDeletion) {
                $this->oldKegiatanPegawaisScheduledForDeletion = clone $this->collOldKegiatanPegawais;
                $this->oldKegiatanPegawaisScheduledForDeletion->clear();
            }
            $this->oldKegiatanPegawaisScheduledForDeletion[]= clone $oldKegiatanPegawai;
            $oldKegiatanPegawai->setOldMonevTspKegiatan(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this OldMonevTspKegiatan is new, it will return
     * an empty collection; or if this OldMonevTspKegiatan has previously
     * been saved, it will retrieve related OldKegiatanPegawais from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in OldMonevTspKegiatan.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOldKegiatanPegawai[] List of ChildOldKegiatanPegawai objects
     */
    public function getOldKegiatanPegawaisJoinOldPegawai(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOldKegiatanPegawaiQuery::create(null, $criteria);
        $query->joinWith('OldPegawai', $joinBehavior);

        return $this->getOldKegiatanPegawais($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aOldMasterSkpd) {
            $this->aOldMasterSkpd->removeOldMonevTspKegiatan($this);
        }
        $this->id = null;
        $this->kegiatan_id = null;
        $this->kegiatan_kode = null;
        $this->kegiatan_nama = null;
        $this->unit_id = null;
        $this->unit_kode = null;
        $this->unit_nama = null;
        $this->tahun = null;
        $this->target = null;
        $this->realisasi = null;
        $this->satuan = null;
        $this->capaian = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collOldKegiatanPegawais) {
                foreach ($this->collOldKegiatanPegawais as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collOldKegiatanPegawais = null;
        $this->aOldMasterSkpd = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(OldMonevTspKegiatanTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}

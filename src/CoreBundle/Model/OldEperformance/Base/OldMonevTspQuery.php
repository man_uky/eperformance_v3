<?php

namespace CoreBundle\Model\OldEperformance\Base;

use \Exception;
use \PDO;
use CoreBundle\Model\OldEperformance\OldMonevTsp as ChildOldMonevTsp;
use CoreBundle\Model\OldEperformance\OldMonevTspQuery as ChildOldMonevTspQuery;
use CoreBundle\Model\OldEperformance\Map\OldMonevTspTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'eperformance.monev_tsp' table.
 *
 *
 *
 * @method     ChildOldMonevTspQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildOldMonevTspQuery orderByPegawaiId($order = Criteria::ASC) Order by the pegawai_id column
 * @method     ChildOldMonevTspQuery orderByPegawaiNip($order = Criteria::ASC) Order by the pegawai_nip column
 * @method     ChildOldMonevTspQuery orderByPegawaiNama($order = Criteria::ASC) Order by the pegawai_nama column
 * @method     ChildOldMonevTspQuery orderByUnitId($order = Criteria::ASC) Order by the unit_id column
 * @method     ChildOldMonevTspQuery orderByUnitKode($order = Criteria::ASC) Order by the unit_kode column
 * @method     ChildOldMonevTspQuery orderByTspId($order = Criteria::ASC) Order by the tsp_id column
 * @method     ChildOldMonevTspQuery orderByIndikatorId($order = Criteria::ASC) Order by the indikator_id column
 * @method     ChildOldMonevTspQuery orderByIndikatorLevel($order = Criteria::ASC) Order by the indikator_level column
 * @method     ChildOldMonevTspQuery orderByIndikatorNama($order = Criteria::ASC) Order by the indikator_nama column
 * @method     ChildOldMonevTspQuery orderByIndikatorFormulasi($order = Criteria::ASC) Order by the indikator_formulasi column
 * @method     ChildOldMonevTspQuery orderByIndikatorTarget($order = Criteria::ASC) Order by the indikator_target column
 * @method     ChildOldMonevTspQuery orderByIndikatorRealisasi($order = Criteria::ASC) Order by the indikator_realisasi column
 * @method     ChildOldMonevTspQuery orderByIndikatorSatuan($order = Criteria::ASC) Order by the indikator_satuan column
 * @method     ChildOldMonevTspQuery orderByIndikatorBobot($order = Criteria::ASC) Order by the indikator_bobot column
 * @method     ChildOldMonevTspQuery orderByIndikatorCapaian($order = Criteria::ASC) Order by the indikator_capaian column
 * @method     ChildOldMonevTspQuery orderByKinerjaCapaian($order = Criteria::ASC) Order by the kinerja_capaian column
 * @method     ChildOldMonevTspQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildOldMonevTspQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildOldMonevTspQuery groupById() Group by the id column
 * @method     ChildOldMonevTspQuery groupByPegawaiId() Group by the pegawai_id column
 * @method     ChildOldMonevTspQuery groupByPegawaiNip() Group by the pegawai_nip column
 * @method     ChildOldMonevTspQuery groupByPegawaiNama() Group by the pegawai_nama column
 * @method     ChildOldMonevTspQuery groupByUnitId() Group by the unit_id column
 * @method     ChildOldMonevTspQuery groupByUnitKode() Group by the unit_kode column
 * @method     ChildOldMonevTspQuery groupByTspId() Group by the tsp_id column
 * @method     ChildOldMonevTspQuery groupByIndikatorId() Group by the indikator_id column
 * @method     ChildOldMonevTspQuery groupByIndikatorLevel() Group by the indikator_level column
 * @method     ChildOldMonevTspQuery groupByIndikatorNama() Group by the indikator_nama column
 * @method     ChildOldMonevTspQuery groupByIndikatorFormulasi() Group by the indikator_formulasi column
 * @method     ChildOldMonevTspQuery groupByIndikatorTarget() Group by the indikator_target column
 * @method     ChildOldMonevTspQuery groupByIndikatorRealisasi() Group by the indikator_realisasi column
 * @method     ChildOldMonevTspQuery groupByIndikatorSatuan() Group by the indikator_satuan column
 * @method     ChildOldMonevTspQuery groupByIndikatorBobot() Group by the indikator_bobot column
 * @method     ChildOldMonevTspQuery groupByIndikatorCapaian() Group by the indikator_capaian column
 * @method     ChildOldMonevTspQuery groupByKinerjaCapaian() Group by the kinerja_capaian column
 * @method     ChildOldMonevTspQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildOldMonevTspQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildOldMonevTspQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildOldMonevTspQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildOldMonevTspQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildOldMonevTspQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildOldMonevTspQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildOldMonevTspQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildOldMonevTspQuery leftJoinOldMasterSkpd($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldMasterSkpd relation
 * @method     ChildOldMonevTspQuery rightJoinOldMasterSkpd($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldMasterSkpd relation
 * @method     ChildOldMonevTspQuery innerJoinOldMasterSkpd($relationAlias = null) Adds a INNER JOIN clause to the query using the OldMasterSkpd relation
 *
 * @method     ChildOldMonevTspQuery joinWithOldMasterSkpd($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldMasterSkpd relation
 *
 * @method     ChildOldMonevTspQuery leftJoinWithOldMasterSkpd() Adds a LEFT JOIN clause and with to the query using the OldMasterSkpd relation
 * @method     ChildOldMonevTspQuery rightJoinWithOldMasterSkpd() Adds a RIGHT JOIN clause and with to the query using the OldMasterSkpd relation
 * @method     ChildOldMonevTspQuery innerJoinWithOldMasterSkpd() Adds a INNER JOIN clause and with to the query using the OldMasterSkpd relation
 *
 * @method     \CoreBundle\Model\OldEperformance\OldMasterSkpdQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildOldMonevTsp findOne(ConnectionInterface $con = null) Return the first ChildOldMonevTsp matching the query
 * @method     ChildOldMonevTsp findOneOrCreate(ConnectionInterface $con = null) Return the first ChildOldMonevTsp matching the query, or a new ChildOldMonevTsp object populated from the query conditions when no match is found
 *
 * @method     ChildOldMonevTsp findOneById(int $id) Return the first ChildOldMonevTsp filtered by the id column
 * @method     ChildOldMonevTsp findOneByPegawaiId(int $pegawai_id) Return the first ChildOldMonevTsp filtered by the pegawai_id column
 * @method     ChildOldMonevTsp findOneByPegawaiNip(string $pegawai_nip) Return the first ChildOldMonevTsp filtered by the pegawai_nip column
 * @method     ChildOldMonevTsp findOneByPegawaiNama(string $pegawai_nama) Return the first ChildOldMonevTsp filtered by the pegawai_nama column
 * @method     ChildOldMonevTsp findOneByUnitId(int $unit_id) Return the first ChildOldMonevTsp filtered by the unit_id column
 * @method     ChildOldMonevTsp findOneByUnitKode(string $unit_kode) Return the first ChildOldMonevTsp filtered by the unit_kode column
 * @method     ChildOldMonevTsp findOneByTspId(int $tsp_id) Return the first ChildOldMonevTsp filtered by the tsp_id column
 * @method     ChildOldMonevTsp findOneByIndikatorId(string $indikator_id) Return the first ChildOldMonevTsp filtered by the indikator_id column
 * @method     ChildOldMonevTsp findOneByIndikatorLevel(int $indikator_level) Return the first ChildOldMonevTsp filtered by the indikator_level column
 * @method     ChildOldMonevTsp findOneByIndikatorNama(string $indikator_nama) Return the first ChildOldMonevTsp filtered by the indikator_nama column
 * @method     ChildOldMonevTsp findOneByIndikatorFormulasi(string $indikator_formulasi) Return the first ChildOldMonevTsp filtered by the indikator_formulasi column
 * @method     ChildOldMonevTsp findOneByIndikatorTarget(string $indikator_target) Return the first ChildOldMonevTsp filtered by the indikator_target column
 * @method     ChildOldMonevTsp findOneByIndikatorRealisasi(string $indikator_realisasi) Return the first ChildOldMonevTsp filtered by the indikator_realisasi column
 * @method     ChildOldMonevTsp findOneByIndikatorSatuan(string $indikator_satuan) Return the first ChildOldMonevTsp filtered by the indikator_satuan column
 * @method     ChildOldMonevTsp findOneByIndikatorBobot(string $indikator_bobot) Return the first ChildOldMonevTsp filtered by the indikator_bobot column
 * @method     ChildOldMonevTsp findOneByIndikatorCapaian(string $indikator_capaian) Return the first ChildOldMonevTsp filtered by the indikator_capaian column
 * @method     ChildOldMonevTsp findOneByKinerjaCapaian(string $kinerja_capaian) Return the first ChildOldMonevTsp filtered by the kinerja_capaian column
 * @method     ChildOldMonevTsp findOneByCreatedAt(string $created_at) Return the first ChildOldMonevTsp filtered by the created_at column
 * @method     ChildOldMonevTsp findOneByUpdatedAt(string $updated_at) Return the first ChildOldMonevTsp filtered by the updated_at column *

 * @method     ChildOldMonevTsp requirePk($key, ConnectionInterface $con = null) Return the ChildOldMonevTsp by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTsp requireOne(ConnectionInterface $con = null) Return the first ChildOldMonevTsp matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOldMonevTsp requireOneById(int $id) Return the first ChildOldMonevTsp filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTsp requireOneByPegawaiId(int $pegawai_id) Return the first ChildOldMonevTsp filtered by the pegawai_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTsp requireOneByPegawaiNip(string $pegawai_nip) Return the first ChildOldMonevTsp filtered by the pegawai_nip column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTsp requireOneByPegawaiNama(string $pegawai_nama) Return the first ChildOldMonevTsp filtered by the pegawai_nama column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTsp requireOneByUnitId(int $unit_id) Return the first ChildOldMonevTsp filtered by the unit_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTsp requireOneByUnitKode(string $unit_kode) Return the first ChildOldMonevTsp filtered by the unit_kode column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTsp requireOneByTspId(int $tsp_id) Return the first ChildOldMonevTsp filtered by the tsp_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTsp requireOneByIndikatorId(string $indikator_id) Return the first ChildOldMonevTsp filtered by the indikator_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTsp requireOneByIndikatorLevel(int $indikator_level) Return the first ChildOldMonevTsp filtered by the indikator_level column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTsp requireOneByIndikatorNama(string $indikator_nama) Return the first ChildOldMonevTsp filtered by the indikator_nama column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTsp requireOneByIndikatorFormulasi(string $indikator_formulasi) Return the first ChildOldMonevTsp filtered by the indikator_formulasi column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTsp requireOneByIndikatorTarget(string $indikator_target) Return the first ChildOldMonevTsp filtered by the indikator_target column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTsp requireOneByIndikatorRealisasi(string $indikator_realisasi) Return the first ChildOldMonevTsp filtered by the indikator_realisasi column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTsp requireOneByIndikatorSatuan(string $indikator_satuan) Return the first ChildOldMonevTsp filtered by the indikator_satuan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTsp requireOneByIndikatorBobot(string $indikator_bobot) Return the first ChildOldMonevTsp filtered by the indikator_bobot column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTsp requireOneByIndikatorCapaian(string $indikator_capaian) Return the first ChildOldMonevTsp filtered by the indikator_capaian column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTsp requireOneByKinerjaCapaian(string $kinerja_capaian) Return the first ChildOldMonevTsp filtered by the kinerja_capaian column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTsp requireOneByCreatedAt(string $created_at) Return the first ChildOldMonevTsp filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTsp requireOneByUpdatedAt(string $updated_at) Return the first ChildOldMonevTsp filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOldMonevTsp[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildOldMonevTsp objects based on current ModelCriteria
 * @method     ChildOldMonevTsp[]|ObjectCollection findById(int $id) Return ChildOldMonevTsp objects filtered by the id column
 * @method     ChildOldMonevTsp[]|ObjectCollection findByPegawaiId(int $pegawai_id) Return ChildOldMonevTsp objects filtered by the pegawai_id column
 * @method     ChildOldMonevTsp[]|ObjectCollection findByPegawaiNip(string $pegawai_nip) Return ChildOldMonevTsp objects filtered by the pegawai_nip column
 * @method     ChildOldMonevTsp[]|ObjectCollection findByPegawaiNama(string $pegawai_nama) Return ChildOldMonevTsp objects filtered by the pegawai_nama column
 * @method     ChildOldMonevTsp[]|ObjectCollection findByUnitId(int $unit_id) Return ChildOldMonevTsp objects filtered by the unit_id column
 * @method     ChildOldMonevTsp[]|ObjectCollection findByUnitKode(string $unit_kode) Return ChildOldMonevTsp objects filtered by the unit_kode column
 * @method     ChildOldMonevTsp[]|ObjectCollection findByTspId(int $tsp_id) Return ChildOldMonevTsp objects filtered by the tsp_id column
 * @method     ChildOldMonevTsp[]|ObjectCollection findByIndikatorId(string $indikator_id) Return ChildOldMonevTsp objects filtered by the indikator_id column
 * @method     ChildOldMonevTsp[]|ObjectCollection findByIndikatorLevel(int $indikator_level) Return ChildOldMonevTsp objects filtered by the indikator_level column
 * @method     ChildOldMonevTsp[]|ObjectCollection findByIndikatorNama(string $indikator_nama) Return ChildOldMonevTsp objects filtered by the indikator_nama column
 * @method     ChildOldMonevTsp[]|ObjectCollection findByIndikatorFormulasi(string $indikator_formulasi) Return ChildOldMonevTsp objects filtered by the indikator_formulasi column
 * @method     ChildOldMonevTsp[]|ObjectCollection findByIndikatorTarget(string $indikator_target) Return ChildOldMonevTsp objects filtered by the indikator_target column
 * @method     ChildOldMonevTsp[]|ObjectCollection findByIndikatorRealisasi(string $indikator_realisasi) Return ChildOldMonevTsp objects filtered by the indikator_realisasi column
 * @method     ChildOldMonevTsp[]|ObjectCollection findByIndikatorSatuan(string $indikator_satuan) Return ChildOldMonevTsp objects filtered by the indikator_satuan column
 * @method     ChildOldMonevTsp[]|ObjectCollection findByIndikatorBobot(string $indikator_bobot) Return ChildOldMonevTsp objects filtered by the indikator_bobot column
 * @method     ChildOldMonevTsp[]|ObjectCollection findByIndikatorCapaian(string $indikator_capaian) Return ChildOldMonevTsp objects filtered by the indikator_capaian column
 * @method     ChildOldMonevTsp[]|ObjectCollection findByKinerjaCapaian(string $kinerja_capaian) Return ChildOldMonevTsp objects filtered by the kinerja_capaian column
 * @method     ChildOldMonevTsp[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildOldMonevTsp objects filtered by the created_at column
 * @method     ChildOldMonevTsp[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildOldMonevTsp objects filtered by the updated_at column
 * @method     ChildOldMonevTsp[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class OldMonevTspQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \CoreBundle\Model\OldEperformance\Base\OldMonevTspQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'old_eperformance', $modelName = '\\CoreBundle\\Model\\OldEperformance\\OldMonevTsp', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildOldMonevTspQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildOldMonevTspQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildOldMonevTspQuery) {
            return $criteria;
        }
        $query = new ChildOldMonevTspQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildOldMonevTsp|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(OldMonevTspTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = OldMonevTspTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldMonevTsp A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, pegawai_id, pegawai_nip, pegawai_nama, unit_id, unit_kode, tsp_id, indikator_id, indikator_level, indikator_nama, indikator_formulasi, indikator_target, indikator_realisasi, indikator_satuan, indikator_bobot, indikator_capaian, kinerja_capaian, created_at, updated_at FROM eperformance.monev_tsp WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildOldMonevTsp $obj */
            $obj = new ChildOldMonevTsp();
            $obj->hydrate($row);
            OldMonevTspTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildOldMonevTsp|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(OldMonevTspTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(OldMonevTspTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the pegawai_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPegawaiId(1234); // WHERE pegawai_id = 1234
     * $query->filterByPegawaiId(array(12, 34)); // WHERE pegawai_id IN (12, 34)
     * $query->filterByPegawaiId(array('min' => 12)); // WHERE pegawai_id > 12
     * </code>
     *
     * @param     mixed $pegawaiId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByPegawaiId($pegawaiId = null, $comparison = null)
    {
        if (is_array($pegawaiId)) {
            $useMinMax = false;
            if (isset($pegawaiId['min'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_PEGAWAI_ID, $pegawaiId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pegawaiId['max'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_PEGAWAI_ID, $pegawaiId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspTableMap::COL_PEGAWAI_ID, $pegawaiId, $comparison);
    }

    /**
     * Filter the query on the pegawai_nip column
     *
     * Example usage:
     * <code>
     * $query->filterByPegawaiNip('fooValue');   // WHERE pegawai_nip = 'fooValue'
     * $query->filterByPegawaiNip('%fooValue%', Criteria::LIKE); // WHERE pegawai_nip LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pegawaiNip The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByPegawaiNip($pegawaiNip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pegawaiNip)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspTableMap::COL_PEGAWAI_NIP, $pegawaiNip, $comparison);
    }

    /**
     * Filter the query on the pegawai_nama column
     *
     * Example usage:
     * <code>
     * $query->filterByPegawaiNama('fooValue');   // WHERE pegawai_nama = 'fooValue'
     * $query->filterByPegawaiNama('%fooValue%', Criteria::LIKE); // WHERE pegawai_nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pegawaiNama The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByPegawaiNama($pegawaiNama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pegawaiNama)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspTableMap::COL_PEGAWAI_NAMA, $pegawaiNama, $comparison);
    }

    /**
     * Filter the query on the unit_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitId(1234); // WHERE unit_id = 1234
     * $query->filterByUnitId(array(12, 34)); // WHERE unit_id IN (12, 34)
     * $query->filterByUnitId(array('min' => 12)); // WHERE unit_id > 12
     * </code>
     *
     * @see       filterByOldMasterSkpd()
     *
     * @param     mixed $unitId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByUnitId($unitId = null, $comparison = null)
    {
        if (is_array($unitId)) {
            $useMinMax = false;
            if (isset($unitId['min'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_UNIT_ID, $unitId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($unitId['max'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_UNIT_ID, $unitId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspTableMap::COL_UNIT_ID, $unitId, $comparison);
    }

    /**
     * Filter the query on the unit_kode column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitKode('fooValue');   // WHERE unit_kode = 'fooValue'
     * $query->filterByUnitKode('%fooValue%', Criteria::LIKE); // WHERE unit_kode LIKE '%fooValue%'
     * </code>
     *
     * @param     string $unitKode The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByUnitKode($unitKode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($unitKode)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspTableMap::COL_UNIT_KODE, $unitKode, $comparison);
    }

    /**
     * Filter the query on the tsp_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTspId(1234); // WHERE tsp_id = 1234
     * $query->filterByTspId(array(12, 34)); // WHERE tsp_id IN (12, 34)
     * $query->filterByTspId(array('min' => 12)); // WHERE tsp_id > 12
     * </code>
     *
     * @param     mixed $tspId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByTspId($tspId = null, $comparison = null)
    {
        if (is_array($tspId)) {
            $useMinMax = false;
            if (isset($tspId['min'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_TSP_ID, $tspId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tspId['max'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_TSP_ID, $tspId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspTableMap::COL_TSP_ID, $tspId, $comparison);
    }

    /**
     * Filter the query on the indikator_id column
     *
     * Example usage:
     * <code>
     * $query->filterByIndikatorId('fooValue');   // WHERE indikator_id = 'fooValue'
     * $query->filterByIndikatorId('%fooValue%', Criteria::LIKE); // WHERE indikator_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $indikatorId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByIndikatorId($indikatorId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($indikatorId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspTableMap::COL_INDIKATOR_ID, $indikatorId, $comparison);
    }

    /**
     * Filter the query on the indikator_level column
     *
     * Example usage:
     * <code>
     * $query->filterByIndikatorLevel(1234); // WHERE indikator_level = 1234
     * $query->filterByIndikatorLevel(array(12, 34)); // WHERE indikator_level IN (12, 34)
     * $query->filterByIndikatorLevel(array('min' => 12)); // WHERE indikator_level > 12
     * </code>
     *
     * @param     mixed $indikatorLevel The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByIndikatorLevel($indikatorLevel = null, $comparison = null)
    {
        if (is_array($indikatorLevel)) {
            $useMinMax = false;
            if (isset($indikatorLevel['min'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_INDIKATOR_LEVEL, $indikatorLevel['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($indikatorLevel['max'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_INDIKATOR_LEVEL, $indikatorLevel['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspTableMap::COL_INDIKATOR_LEVEL, $indikatorLevel, $comparison);
    }

    /**
     * Filter the query on the indikator_nama column
     *
     * Example usage:
     * <code>
     * $query->filterByIndikatorNama('fooValue');   // WHERE indikator_nama = 'fooValue'
     * $query->filterByIndikatorNama('%fooValue%', Criteria::LIKE); // WHERE indikator_nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $indikatorNama The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByIndikatorNama($indikatorNama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($indikatorNama)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspTableMap::COL_INDIKATOR_NAMA, $indikatorNama, $comparison);
    }

    /**
     * Filter the query on the indikator_formulasi column
     *
     * Example usage:
     * <code>
     * $query->filterByIndikatorFormulasi('fooValue');   // WHERE indikator_formulasi = 'fooValue'
     * $query->filterByIndikatorFormulasi('%fooValue%', Criteria::LIKE); // WHERE indikator_formulasi LIKE '%fooValue%'
     * </code>
     *
     * @param     string $indikatorFormulasi The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByIndikatorFormulasi($indikatorFormulasi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($indikatorFormulasi)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspTableMap::COL_INDIKATOR_FORMULASI, $indikatorFormulasi, $comparison);
    }

    /**
     * Filter the query on the indikator_target column
     *
     * Example usage:
     * <code>
     * $query->filterByIndikatorTarget(1234); // WHERE indikator_target = 1234
     * $query->filterByIndikatorTarget(array(12, 34)); // WHERE indikator_target IN (12, 34)
     * $query->filterByIndikatorTarget(array('min' => 12)); // WHERE indikator_target > 12
     * </code>
     *
     * @param     mixed $indikatorTarget The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByIndikatorTarget($indikatorTarget = null, $comparison = null)
    {
        if (is_array($indikatorTarget)) {
            $useMinMax = false;
            if (isset($indikatorTarget['min'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_INDIKATOR_TARGET, $indikatorTarget['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($indikatorTarget['max'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_INDIKATOR_TARGET, $indikatorTarget['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspTableMap::COL_INDIKATOR_TARGET, $indikatorTarget, $comparison);
    }

    /**
     * Filter the query on the indikator_realisasi column
     *
     * Example usage:
     * <code>
     * $query->filterByIndikatorRealisasi(1234); // WHERE indikator_realisasi = 1234
     * $query->filterByIndikatorRealisasi(array(12, 34)); // WHERE indikator_realisasi IN (12, 34)
     * $query->filterByIndikatorRealisasi(array('min' => 12)); // WHERE indikator_realisasi > 12
     * </code>
     *
     * @param     mixed $indikatorRealisasi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByIndikatorRealisasi($indikatorRealisasi = null, $comparison = null)
    {
        if (is_array($indikatorRealisasi)) {
            $useMinMax = false;
            if (isset($indikatorRealisasi['min'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_INDIKATOR_REALISASI, $indikatorRealisasi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($indikatorRealisasi['max'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_INDIKATOR_REALISASI, $indikatorRealisasi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspTableMap::COL_INDIKATOR_REALISASI, $indikatorRealisasi, $comparison);
    }

    /**
     * Filter the query on the indikator_satuan column
     *
     * Example usage:
     * <code>
     * $query->filterByIndikatorSatuan('fooValue');   // WHERE indikator_satuan = 'fooValue'
     * $query->filterByIndikatorSatuan('%fooValue%', Criteria::LIKE); // WHERE indikator_satuan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $indikatorSatuan The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByIndikatorSatuan($indikatorSatuan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($indikatorSatuan)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspTableMap::COL_INDIKATOR_SATUAN, $indikatorSatuan, $comparison);
    }

    /**
     * Filter the query on the indikator_bobot column
     *
     * Example usage:
     * <code>
     * $query->filterByIndikatorBobot(1234); // WHERE indikator_bobot = 1234
     * $query->filterByIndikatorBobot(array(12, 34)); // WHERE indikator_bobot IN (12, 34)
     * $query->filterByIndikatorBobot(array('min' => 12)); // WHERE indikator_bobot > 12
     * </code>
     *
     * @param     mixed $indikatorBobot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByIndikatorBobot($indikatorBobot = null, $comparison = null)
    {
        if (is_array($indikatorBobot)) {
            $useMinMax = false;
            if (isset($indikatorBobot['min'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_INDIKATOR_BOBOT, $indikatorBobot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($indikatorBobot['max'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_INDIKATOR_BOBOT, $indikatorBobot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspTableMap::COL_INDIKATOR_BOBOT, $indikatorBobot, $comparison);
    }

    /**
     * Filter the query on the indikator_capaian column
     *
     * Example usage:
     * <code>
     * $query->filterByIndikatorCapaian(1234); // WHERE indikator_capaian = 1234
     * $query->filterByIndikatorCapaian(array(12, 34)); // WHERE indikator_capaian IN (12, 34)
     * $query->filterByIndikatorCapaian(array('min' => 12)); // WHERE indikator_capaian > 12
     * </code>
     *
     * @param     mixed $indikatorCapaian The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByIndikatorCapaian($indikatorCapaian = null, $comparison = null)
    {
        if (is_array($indikatorCapaian)) {
            $useMinMax = false;
            if (isset($indikatorCapaian['min'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_INDIKATOR_CAPAIAN, $indikatorCapaian['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($indikatorCapaian['max'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_INDIKATOR_CAPAIAN, $indikatorCapaian['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspTableMap::COL_INDIKATOR_CAPAIAN, $indikatorCapaian, $comparison);
    }

    /**
     * Filter the query on the kinerja_capaian column
     *
     * Example usage:
     * <code>
     * $query->filterByKinerjaCapaian(1234); // WHERE kinerja_capaian = 1234
     * $query->filterByKinerjaCapaian(array(12, 34)); // WHERE kinerja_capaian IN (12, 34)
     * $query->filterByKinerjaCapaian(array('min' => 12)); // WHERE kinerja_capaian > 12
     * </code>
     *
     * @param     mixed $kinerjaCapaian The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByKinerjaCapaian($kinerjaCapaian = null, $comparison = null)
    {
        if (is_array($kinerjaCapaian)) {
            $useMinMax = false;
            if (isset($kinerjaCapaian['min'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_KINERJA_CAPAIAN, $kinerjaCapaian['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($kinerjaCapaian['max'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_KINERJA_CAPAIAN, $kinerjaCapaian['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspTableMap::COL_KINERJA_CAPAIAN, $kinerjaCapaian, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(OldMonevTspTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldMasterSkpd object
     *
     * @param \CoreBundle\Model\OldEperformance\OldMasterSkpd|ObjectCollection $oldMasterSkpd The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function filterByOldMasterSkpd($oldMasterSkpd, $comparison = null)
    {
        if ($oldMasterSkpd instanceof \CoreBundle\Model\OldEperformance\OldMasterSkpd) {
            return $this
                ->addUsingAlias(OldMonevTspTableMap::COL_UNIT_ID, $oldMasterSkpd->getSkpdId(), $comparison);
        } elseif ($oldMasterSkpd instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OldMonevTspTableMap::COL_UNIT_ID, $oldMasterSkpd->toKeyValue('PrimaryKey', 'SkpdId'), $comparison);
        } else {
            throw new PropelException('filterByOldMasterSkpd() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldMasterSkpd or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldMasterSkpd relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function joinOldMasterSkpd($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldMasterSkpd');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldMasterSkpd');
        }

        return $this;
    }

    /**
     * Use the OldMasterSkpd relation OldMasterSkpd object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldMasterSkpdQuery A secondary query class using the current class as primary query
     */
    public function useOldMasterSkpdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinOldMasterSkpd($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldMasterSkpd', '\CoreBundle\Model\OldEperformance\OldMasterSkpdQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildOldMonevTsp $oldMonevTsp Object to remove from the list of results
     *
     * @return $this|ChildOldMonevTspQuery The current query, for fluid interface
     */
    public function prune($oldMonevTsp = null)
    {
        if ($oldMonevTsp) {
            $this->addUsingAlias(OldMonevTspTableMap::COL_ID, $oldMonevTsp->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the eperformance.monev_tsp table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldMonevTspTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            OldMonevTspTableMap::clearInstancePool();
            OldMonevTspTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldMonevTspTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(OldMonevTspTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            OldMonevTspTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            OldMonevTspTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 7200)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(OldMonevTspTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(OldMonevTspTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }


        return $con->getDataFetcher($stmt);
    }

} // OldMonevTspQuery

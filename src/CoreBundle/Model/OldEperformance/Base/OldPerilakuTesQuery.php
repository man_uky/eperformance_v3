<?php

namespace CoreBundle\Model\OldEperformance\Base;

use \Exception;
use \PDO;
use CoreBundle\Model\OldEperformance\OldPerilakuTes as ChildOldPerilakuTes;
use CoreBundle\Model\OldEperformance\OldPerilakuTesQuery as ChildOldPerilakuTesQuery;
use CoreBundle\Model\OldEperformance\Map\OldPerilakuTesTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'eperformance.skp_perilaku_kerja_tes' table.
 *
 *
 *
 * @method     ChildOldPerilakuTesQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildOldPerilakuTesQuery orderByTahun($order = Criteria::ASC) Order by the tahun column
 * @method     ChildOldPerilakuTesQuery orderByTriwulan($order = Criteria::ASC) Order by the triwulan column
 * @method     ChildOldPerilakuTesQuery orderByBulan($order = Criteria::ASC) Order by the bulan column
 * @method     ChildOldPerilakuTesQuery orderBySkpdId($order = Criteria::ASC) Order by the skpd_id column
 * @method     ChildOldPerilakuTesQuery orderByStatus($order = Criteria::ASC) Order by the status column
 *
 * @method     ChildOldPerilakuTesQuery groupById() Group by the id column
 * @method     ChildOldPerilakuTesQuery groupByTahun() Group by the tahun column
 * @method     ChildOldPerilakuTesQuery groupByTriwulan() Group by the triwulan column
 * @method     ChildOldPerilakuTesQuery groupByBulan() Group by the bulan column
 * @method     ChildOldPerilakuTesQuery groupBySkpdId() Group by the skpd_id column
 * @method     ChildOldPerilakuTesQuery groupByStatus() Group by the status column
 *
 * @method     ChildOldPerilakuTesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildOldPerilakuTesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildOldPerilakuTesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildOldPerilakuTesQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildOldPerilakuTesQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildOldPerilakuTesQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildOldPerilakuTesQuery leftJoinOldMasterSkpd($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldMasterSkpd relation
 * @method     ChildOldPerilakuTesQuery rightJoinOldMasterSkpd($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldMasterSkpd relation
 * @method     ChildOldPerilakuTesQuery innerJoinOldMasterSkpd($relationAlias = null) Adds a INNER JOIN clause to the query using the OldMasterSkpd relation
 *
 * @method     ChildOldPerilakuTesQuery joinWithOldMasterSkpd($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldMasterSkpd relation
 *
 * @method     ChildOldPerilakuTesQuery leftJoinWithOldMasterSkpd() Adds a LEFT JOIN clause and with to the query using the OldMasterSkpd relation
 * @method     ChildOldPerilakuTesQuery rightJoinWithOldMasterSkpd() Adds a RIGHT JOIN clause and with to the query using the OldMasterSkpd relation
 * @method     ChildOldPerilakuTesQuery innerJoinWithOldMasterSkpd() Adds a INNER JOIN clause and with to the query using the OldMasterSkpd relation
 *
 * @method     ChildOldPerilakuTesQuery leftJoinOldPerilakuHasil($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldPerilakuHasil relation
 * @method     ChildOldPerilakuTesQuery rightJoinOldPerilakuHasil($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldPerilakuHasil relation
 * @method     ChildOldPerilakuTesQuery innerJoinOldPerilakuHasil($relationAlias = null) Adds a INNER JOIN clause to the query using the OldPerilakuHasil relation
 *
 * @method     ChildOldPerilakuTesQuery joinWithOldPerilakuHasil($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldPerilakuHasil relation
 *
 * @method     ChildOldPerilakuTesQuery leftJoinWithOldPerilakuHasil() Adds a LEFT JOIN clause and with to the query using the OldPerilakuHasil relation
 * @method     ChildOldPerilakuTesQuery rightJoinWithOldPerilakuHasil() Adds a RIGHT JOIN clause and with to the query using the OldPerilakuHasil relation
 * @method     ChildOldPerilakuTesQuery innerJoinWithOldPerilakuHasil() Adds a INNER JOIN clause and with to the query using the OldPerilakuHasil relation
 *
 * @method     \CoreBundle\Model\OldEperformance\OldMasterSkpdQuery|\CoreBundle\Model\OldEperformance\OldPerilakuHasilQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildOldPerilakuTes findOne(ConnectionInterface $con = null) Return the first ChildOldPerilakuTes matching the query
 * @method     ChildOldPerilakuTes findOneOrCreate(ConnectionInterface $con = null) Return the first ChildOldPerilakuTes matching the query, or a new ChildOldPerilakuTes object populated from the query conditions when no match is found
 *
 * @method     ChildOldPerilakuTes findOneById(int $id) Return the first ChildOldPerilakuTes filtered by the id column
 * @method     ChildOldPerilakuTes findOneByTahun(int $tahun) Return the first ChildOldPerilakuTes filtered by the tahun column
 * @method     ChildOldPerilakuTes findOneByTriwulan(int $triwulan) Return the first ChildOldPerilakuTes filtered by the triwulan column
 * @method     ChildOldPerilakuTes findOneByBulan(int $bulan) Return the first ChildOldPerilakuTes filtered by the bulan column
 * @method     ChildOldPerilakuTes findOneBySkpdId(int $skpd_id) Return the first ChildOldPerilakuTes filtered by the skpd_id column
 * @method     ChildOldPerilakuTes findOneByStatus(int $status) Return the first ChildOldPerilakuTes filtered by the status column *

 * @method     ChildOldPerilakuTes requirePk($key, ConnectionInterface $con = null) Return the ChildOldPerilakuTes by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPerilakuTes requireOne(ConnectionInterface $con = null) Return the first ChildOldPerilakuTes matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOldPerilakuTes requireOneById(int $id) Return the first ChildOldPerilakuTes filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPerilakuTes requireOneByTahun(int $tahun) Return the first ChildOldPerilakuTes filtered by the tahun column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPerilakuTes requireOneByTriwulan(int $triwulan) Return the first ChildOldPerilakuTes filtered by the triwulan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPerilakuTes requireOneByBulan(int $bulan) Return the first ChildOldPerilakuTes filtered by the bulan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPerilakuTes requireOneBySkpdId(int $skpd_id) Return the first ChildOldPerilakuTes filtered by the skpd_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPerilakuTes requireOneByStatus(int $status) Return the first ChildOldPerilakuTes filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOldPerilakuTes[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildOldPerilakuTes objects based on current ModelCriteria
 * @method     ChildOldPerilakuTes[]|ObjectCollection findById(int $id) Return ChildOldPerilakuTes objects filtered by the id column
 * @method     ChildOldPerilakuTes[]|ObjectCollection findByTahun(int $tahun) Return ChildOldPerilakuTes objects filtered by the tahun column
 * @method     ChildOldPerilakuTes[]|ObjectCollection findByTriwulan(int $triwulan) Return ChildOldPerilakuTes objects filtered by the triwulan column
 * @method     ChildOldPerilakuTes[]|ObjectCollection findByBulan(int $bulan) Return ChildOldPerilakuTes objects filtered by the bulan column
 * @method     ChildOldPerilakuTes[]|ObjectCollection findBySkpdId(int $skpd_id) Return ChildOldPerilakuTes objects filtered by the skpd_id column
 * @method     ChildOldPerilakuTes[]|ObjectCollection findByStatus(int $status) Return ChildOldPerilakuTes objects filtered by the status column
 * @method     ChildOldPerilakuTes[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class OldPerilakuTesQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \CoreBundle\Model\OldEperformance\Base\OldPerilakuTesQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'old_eperformance', $modelName = '\\CoreBundle\\Model\\OldEperformance\\OldPerilakuTes', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildOldPerilakuTesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildOldPerilakuTesQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildOldPerilakuTesQuery) {
            return $criteria;
        }
        $query = new ChildOldPerilakuTesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildOldPerilakuTes|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(OldPerilakuTesTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = OldPerilakuTesTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldPerilakuTes A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, tahun, triwulan, bulan, skpd_id, status FROM eperformance.skp_perilaku_kerja_tes WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildOldPerilakuTes $obj */
            $obj = new ChildOldPerilakuTes();
            $obj->hydrate($row);
            OldPerilakuTesTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildOldPerilakuTes|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildOldPerilakuTesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(OldPerilakuTesTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildOldPerilakuTesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(OldPerilakuTesTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPerilakuTesQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(OldPerilakuTesTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(OldPerilakuTesTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPerilakuTesTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the tahun column
     *
     * Example usage:
     * <code>
     * $query->filterByTahun(1234); // WHERE tahun = 1234
     * $query->filterByTahun(array(12, 34)); // WHERE tahun IN (12, 34)
     * $query->filterByTahun(array('min' => 12)); // WHERE tahun > 12
     * </code>
     *
     * @param     mixed $tahun The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPerilakuTesQuery The current query, for fluid interface
     */
    public function filterByTahun($tahun = null, $comparison = null)
    {
        if (is_array($tahun)) {
            $useMinMax = false;
            if (isset($tahun['min'])) {
                $this->addUsingAlias(OldPerilakuTesTableMap::COL_TAHUN, $tahun['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tahun['max'])) {
                $this->addUsingAlias(OldPerilakuTesTableMap::COL_TAHUN, $tahun['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPerilakuTesTableMap::COL_TAHUN, $tahun, $comparison);
    }

    /**
     * Filter the query on the triwulan column
     *
     * Example usage:
     * <code>
     * $query->filterByTriwulan(1234); // WHERE triwulan = 1234
     * $query->filterByTriwulan(array(12, 34)); // WHERE triwulan IN (12, 34)
     * $query->filterByTriwulan(array('min' => 12)); // WHERE triwulan > 12
     * </code>
     *
     * @param     mixed $triwulan The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPerilakuTesQuery The current query, for fluid interface
     */
    public function filterByTriwulan($triwulan = null, $comparison = null)
    {
        if (is_array($triwulan)) {
            $useMinMax = false;
            if (isset($triwulan['min'])) {
                $this->addUsingAlias(OldPerilakuTesTableMap::COL_TRIWULAN, $triwulan['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($triwulan['max'])) {
                $this->addUsingAlias(OldPerilakuTesTableMap::COL_TRIWULAN, $triwulan['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPerilakuTesTableMap::COL_TRIWULAN, $triwulan, $comparison);
    }

    /**
     * Filter the query on the bulan column
     *
     * Example usage:
     * <code>
     * $query->filterByBulan(1234); // WHERE bulan = 1234
     * $query->filterByBulan(array(12, 34)); // WHERE bulan IN (12, 34)
     * $query->filterByBulan(array('min' => 12)); // WHERE bulan > 12
     * </code>
     *
     * @param     mixed $bulan The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPerilakuTesQuery The current query, for fluid interface
     */
    public function filterByBulan($bulan = null, $comparison = null)
    {
        if (is_array($bulan)) {
            $useMinMax = false;
            if (isset($bulan['min'])) {
                $this->addUsingAlias(OldPerilakuTesTableMap::COL_BULAN, $bulan['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bulan['max'])) {
                $this->addUsingAlias(OldPerilakuTesTableMap::COL_BULAN, $bulan['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPerilakuTesTableMap::COL_BULAN, $bulan, $comparison);
    }

    /**
     * Filter the query on the skpd_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySkpdId(1234); // WHERE skpd_id = 1234
     * $query->filterBySkpdId(array(12, 34)); // WHERE skpd_id IN (12, 34)
     * $query->filterBySkpdId(array('min' => 12)); // WHERE skpd_id > 12
     * </code>
     *
     * @see       filterByOldMasterSkpd()
     *
     * @param     mixed $skpdId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPerilakuTesQuery The current query, for fluid interface
     */
    public function filterBySkpdId($skpdId = null, $comparison = null)
    {
        if (is_array($skpdId)) {
            $useMinMax = false;
            if (isset($skpdId['min'])) {
                $this->addUsingAlias(OldPerilakuTesTableMap::COL_SKPD_ID, $skpdId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($skpdId['max'])) {
                $this->addUsingAlias(OldPerilakuTesTableMap::COL_SKPD_ID, $skpdId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPerilakuTesTableMap::COL_SKPD_ID, $skpdId, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(1234); // WHERE status = 1234
     * $query->filterByStatus(array(12, 34)); // WHERE status IN (12, 34)
     * $query->filterByStatus(array('min' => 12)); // WHERE status > 12
     * </code>
     *
     * @param     mixed $status The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPerilakuTesQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_array($status)) {
            $useMinMax = false;
            if (isset($status['min'])) {
                $this->addUsingAlias(OldPerilakuTesTableMap::COL_STATUS, $status['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($status['max'])) {
                $this->addUsingAlias(OldPerilakuTesTableMap::COL_STATUS, $status['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPerilakuTesTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldMasterSkpd object
     *
     * @param \CoreBundle\Model\OldEperformance\OldMasterSkpd|ObjectCollection $oldMasterSkpd The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldPerilakuTesQuery The current query, for fluid interface
     */
    public function filterByOldMasterSkpd($oldMasterSkpd, $comparison = null)
    {
        if ($oldMasterSkpd instanceof \CoreBundle\Model\OldEperformance\OldMasterSkpd) {
            return $this
                ->addUsingAlias(OldPerilakuTesTableMap::COL_SKPD_ID, $oldMasterSkpd->getSkpdId(), $comparison);
        } elseif ($oldMasterSkpd instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OldPerilakuTesTableMap::COL_SKPD_ID, $oldMasterSkpd->toKeyValue('PrimaryKey', 'SkpdId'), $comparison);
        } else {
            throw new PropelException('filterByOldMasterSkpd() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldMasterSkpd or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldMasterSkpd relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldPerilakuTesQuery The current query, for fluid interface
     */
    public function joinOldMasterSkpd($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldMasterSkpd');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldMasterSkpd');
        }

        return $this;
    }

    /**
     * Use the OldMasterSkpd relation OldMasterSkpd object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldMasterSkpdQuery A secondary query class using the current class as primary query
     */
    public function useOldMasterSkpdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldMasterSkpd($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldMasterSkpd', '\CoreBundle\Model\OldEperformance\OldMasterSkpdQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldPerilakuHasil object
     *
     * @param \CoreBundle\Model\OldEperformance\OldPerilakuHasil|ObjectCollection $oldPerilakuHasil the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildOldPerilakuTesQuery The current query, for fluid interface
     */
    public function filterByOldPerilakuHasil($oldPerilakuHasil, $comparison = null)
    {
        if ($oldPerilakuHasil instanceof \CoreBundle\Model\OldEperformance\OldPerilakuHasil) {
            return $this
                ->addUsingAlias(OldPerilakuTesTableMap::COL_ID, $oldPerilakuHasil->getPerilakuTesId(), $comparison);
        } elseif ($oldPerilakuHasil instanceof ObjectCollection) {
            return $this
                ->useOldPerilakuHasilQuery()
                ->filterByPrimaryKeys($oldPerilakuHasil->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOldPerilakuHasil() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldPerilakuHasil or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldPerilakuHasil relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldPerilakuTesQuery The current query, for fluid interface
     */
    public function joinOldPerilakuHasil($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldPerilakuHasil');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldPerilakuHasil');
        }

        return $this;
    }

    /**
     * Use the OldPerilakuHasil relation OldPerilakuHasil object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldPerilakuHasilQuery A secondary query class using the current class as primary query
     */
    public function useOldPerilakuHasilQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldPerilakuHasil($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldPerilakuHasil', '\CoreBundle\Model\OldEperformance\OldPerilakuHasilQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildOldPerilakuTes $oldPerilakuTes Object to remove from the list of results
     *
     * @return $this|ChildOldPerilakuTesQuery The current query, for fluid interface
     */
    public function prune($oldPerilakuTes = null)
    {
        if ($oldPerilakuTes) {
            $this->addUsingAlias(OldPerilakuTesTableMap::COL_ID, $oldPerilakuTes->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the eperformance.skp_perilaku_kerja_tes table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldPerilakuTesTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            OldPerilakuTesTableMap::clearInstancePool();
            OldPerilakuTesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldPerilakuTesTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(OldPerilakuTesTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            OldPerilakuTesTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            OldPerilakuTesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 7200)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(OldPerilakuTesTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(OldPerilakuTesTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }


        return $con->getDataFetcher($stmt);
    }

} // OldPerilakuTesQuery

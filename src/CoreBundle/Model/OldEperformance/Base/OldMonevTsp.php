<?php

namespace CoreBundle\Model\OldEperformance\Base;

use \DateTime;
use \Exception;
use \PDO;
use CoreBundle\Model\OldEperformance\OldMasterSkpd as ChildOldMasterSkpd;
use CoreBundle\Model\OldEperformance\OldMasterSkpdQuery as ChildOldMasterSkpdQuery;
use CoreBundle\Model\OldEperformance\OldMonevTspQuery as ChildOldMonevTspQuery;
use CoreBundle\Model\OldEperformance\Map\OldMonevTspTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'eperformance.monev_tsp' table.
 *
 *
 *
 * @package    propel.generator.src.CoreBundle.Model.OldEperformance.Base
 */
abstract class OldMonevTsp implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\CoreBundle\\Model\\OldEperformance\\Map\\OldMonevTspTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the pegawai_id field.
     *
     * @var        int
     */
    protected $pegawai_id;

    /**
     * The value for the pegawai_nip field.
     *
     * @var        string
     */
    protected $pegawai_nip;

    /**
     * The value for the pegawai_nama field.
     *
     * @var        string
     */
    protected $pegawai_nama;

    /**
     * The value for the unit_id field.
     *
     * @var        int
     */
    protected $unit_id;

    /**
     * The value for the unit_kode field.
     *
     * @var        string
     */
    protected $unit_kode;

    /**
     * The value for the tsp_id field.
     *
     * @var        int
     */
    protected $tsp_id;

    /**
     * The value for the indikator_id field.
     *
     * @var        string
     */
    protected $indikator_id;

    /**
     * The value for the indikator_level field.
     *
     * @var        int
     */
    protected $indikator_level;

    /**
     * The value for the indikator_nama field.
     *
     * @var        string
     */
    protected $indikator_nama;

    /**
     * The value for the indikator_formulasi field.
     *
     * @var        string
     */
    protected $indikator_formulasi;

    /**
     * The value for the indikator_target field.
     *
     * @var        string
     */
    protected $indikator_target;

    /**
     * The value for the indikator_realisasi field.
     *
     * @var        string
     */
    protected $indikator_realisasi;

    /**
     * The value for the indikator_satuan field.
     *
     * @var        string
     */
    protected $indikator_satuan;

    /**
     * The value for the indikator_bobot field.
     *
     * @var        string
     */
    protected $indikator_bobot;

    /**
     * The value for the indikator_capaian field.
     *
     * @var        string
     */
    protected $indikator_capaian;

    /**
     * The value for the kinerja_capaian field.
     *
     * @var        string
     */
    protected $kinerja_capaian;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildOldMasterSkpd
     */
    protected $aOldMasterSkpd;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of CoreBundle\Model\OldEperformance\Base\OldMonevTsp object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>OldMonevTsp</code> instance.  If
     * <code>obj</code> is an instance of <code>OldMonevTsp</code>, delegates to
     * <code>equals(OldMonevTsp)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|OldMonevTsp The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [pegawai_id] column value.
     *
     * @return int
     */
    public function getPegawaiId()
    {
        return $this->pegawai_id;
    }

    /**
     * Get the [pegawai_nip] column value.
     *
     * @return string
     */
    public function getPegawaiNip()
    {
        return $this->pegawai_nip;
    }

    /**
     * Get the [pegawai_nama] column value.
     *
     * @return string
     */
    public function getPegawaiNama()
    {
        return $this->pegawai_nama;
    }

    /**
     * Get the [unit_id] column value.
     *
     * @return int
     */
    public function getUnitId()
    {
        return $this->unit_id;
    }

    /**
     * Get the [unit_kode] column value.
     *
     * @return string
     */
    public function getUnitKode()
    {
        return $this->unit_kode;
    }

    /**
     * Get the [tsp_id] column value.
     *
     * @return int
     */
    public function getTspId()
    {
        return $this->tsp_id;
    }

    /**
     * Get the [indikator_id] column value.
     *
     * @return string
     */
    public function getIndikatorId()
    {
        return $this->indikator_id;
    }

    /**
     * Get the [indikator_level] column value.
     *
     * @return int
     */
    public function getIndikatorLevel()
    {
        return $this->indikator_level;
    }

    /**
     * Get the [indikator_nama] column value.
     *
     * @return string
     */
    public function getIndikatorNama()
    {
        return $this->indikator_nama;
    }

    /**
     * Get the [indikator_formulasi] column value.
     *
     * @return string
     */
    public function getIndikatorFormulasi()
    {
        return $this->indikator_formulasi;
    }

    /**
     * Get the [indikator_target] column value.
     *
     * @return string
     */
    public function getIndikatorTarget()
    {
        return $this->indikator_target;
    }

    /**
     * Get the [indikator_realisasi] column value.
     *
     * @return string
     */
    public function getIndikatorRealisasi()
    {
        return $this->indikator_realisasi;
    }

    /**
     * Get the [indikator_satuan] column value.
     *
     * @return string
     */
    public function getIndikatorSatuan()
    {
        return $this->indikator_satuan;
    }

    /**
     * Get the [indikator_bobot] column value.
     *
     * @return string
     */
    public function getIndikatorBobot()
    {
        return $this->indikator_bobot;
    }

    /**
     * Get the [indikator_capaian] column value.
     *
     * @return string
     */
    public function getIndikatorCapaian()
    {
        return $this->indikator_capaian;
    }

    /**
     * Get the [kinerja_capaian] column value.
     *
     * @return string
     */
    public function getKinerjaCapaian()
    {
        return $this->kinerja_capaian;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[OldMonevTspTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [pegawai_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object (for fluent API support)
     */
    public function setPegawaiId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->pegawai_id !== $v) {
            $this->pegawai_id = $v;
            $this->modifiedColumns[OldMonevTspTableMap::COL_PEGAWAI_ID] = true;
        }

        return $this;
    } // setPegawaiId()

    /**
     * Set the value of [pegawai_nip] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object (for fluent API support)
     */
    public function setPegawaiNip($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->pegawai_nip !== $v) {
            $this->pegawai_nip = $v;
            $this->modifiedColumns[OldMonevTspTableMap::COL_PEGAWAI_NIP] = true;
        }

        return $this;
    } // setPegawaiNip()

    /**
     * Set the value of [pegawai_nama] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object (for fluent API support)
     */
    public function setPegawaiNama($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->pegawai_nama !== $v) {
            $this->pegawai_nama = $v;
            $this->modifiedColumns[OldMonevTspTableMap::COL_PEGAWAI_NAMA] = true;
        }

        return $this;
    } // setPegawaiNama()

    /**
     * Set the value of [unit_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object (for fluent API support)
     */
    public function setUnitId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->unit_id !== $v) {
            $this->unit_id = $v;
            $this->modifiedColumns[OldMonevTspTableMap::COL_UNIT_ID] = true;
        }

        if ($this->aOldMasterSkpd !== null && $this->aOldMasterSkpd->getSkpdId() !== $v) {
            $this->aOldMasterSkpd = null;
        }

        return $this;
    } // setUnitId()

    /**
     * Set the value of [unit_kode] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object (for fluent API support)
     */
    public function setUnitKode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->unit_kode !== $v) {
            $this->unit_kode = $v;
            $this->modifiedColumns[OldMonevTspTableMap::COL_UNIT_KODE] = true;
        }

        return $this;
    } // setUnitKode()

    /**
     * Set the value of [tsp_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object (for fluent API support)
     */
    public function setTspId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->tsp_id !== $v) {
            $this->tsp_id = $v;
            $this->modifiedColumns[OldMonevTspTableMap::COL_TSP_ID] = true;
        }

        return $this;
    } // setTspId()

    /**
     * Set the value of [indikator_id] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object (for fluent API support)
     */
    public function setIndikatorId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->indikator_id !== $v) {
            $this->indikator_id = $v;
            $this->modifiedColumns[OldMonevTspTableMap::COL_INDIKATOR_ID] = true;
        }

        return $this;
    } // setIndikatorId()

    /**
     * Set the value of [indikator_level] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object (for fluent API support)
     */
    public function setIndikatorLevel($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->indikator_level !== $v) {
            $this->indikator_level = $v;
            $this->modifiedColumns[OldMonevTspTableMap::COL_INDIKATOR_LEVEL] = true;
        }

        return $this;
    } // setIndikatorLevel()

    /**
     * Set the value of [indikator_nama] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object (for fluent API support)
     */
    public function setIndikatorNama($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->indikator_nama !== $v) {
            $this->indikator_nama = $v;
            $this->modifiedColumns[OldMonevTspTableMap::COL_INDIKATOR_NAMA] = true;
        }

        return $this;
    } // setIndikatorNama()

    /**
     * Set the value of [indikator_formulasi] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object (for fluent API support)
     */
    public function setIndikatorFormulasi($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->indikator_formulasi !== $v) {
            $this->indikator_formulasi = $v;
            $this->modifiedColumns[OldMonevTspTableMap::COL_INDIKATOR_FORMULASI] = true;
        }

        return $this;
    } // setIndikatorFormulasi()

    /**
     * Set the value of [indikator_target] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object (for fluent API support)
     */
    public function setIndikatorTarget($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->indikator_target !== $v) {
            $this->indikator_target = $v;
            $this->modifiedColumns[OldMonevTspTableMap::COL_INDIKATOR_TARGET] = true;
        }

        return $this;
    } // setIndikatorTarget()

    /**
     * Set the value of [indikator_realisasi] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object (for fluent API support)
     */
    public function setIndikatorRealisasi($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->indikator_realisasi !== $v) {
            $this->indikator_realisasi = $v;
            $this->modifiedColumns[OldMonevTspTableMap::COL_INDIKATOR_REALISASI] = true;
        }

        return $this;
    } // setIndikatorRealisasi()

    /**
     * Set the value of [indikator_satuan] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object (for fluent API support)
     */
    public function setIndikatorSatuan($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->indikator_satuan !== $v) {
            $this->indikator_satuan = $v;
            $this->modifiedColumns[OldMonevTspTableMap::COL_INDIKATOR_SATUAN] = true;
        }

        return $this;
    } // setIndikatorSatuan()

    /**
     * Set the value of [indikator_bobot] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object (for fluent API support)
     */
    public function setIndikatorBobot($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->indikator_bobot !== $v) {
            $this->indikator_bobot = $v;
            $this->modifiedColumns[OldMonevTspTableMap::COL_INDIKATOR_BOBOT] = true;
        }

        return $this;
    } // setIndikatorBobot()

    /**
     * Set the value of [indikator_capaian] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object (for fluent API support)
     */
    public function setIndikatorCapaian($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->indikator_capaian !== $v) {
            $this->indikator_capaian = $v;
            $this->modifiedColumns[OldMonevTspTableMap::COL_INDIKATOR_CAPAIAN] = true;
        }

        return $this;
    } // setIndikatorCapaian()

    /**
     * Set the value of [kinerja_capaian] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object (for fluent API support)
     */
    public function setKinerjaCapaian($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->kinerja_capaian !== $v) {
            $this->kinerja_capaian = $v;
            $this->modifiedColumns[OldMonevTspTableMap::COL_KINERJA_CAPAIAN] = true;
        }

        return $this;
    } // setKinerjaCapaian()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[OldMonevTspTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[OldMonevTspTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : OldMonevTspTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : OldMonevTspTableMap::translateFieldName('PegawaiId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pegawai_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : OldMonevTspTableMap::translateFieldName('PegawaiNip', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pegawai_nip = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : OldMonevTspTableMap::translateFieldName('PegawaiNama', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pegawai_nama = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : OldMonevTspTableMap::translateFieldName('UnitId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->unit_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : OldMonevTspTableMap::translateFieldName('UnitKode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->unit_kode = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : OldMonevTspTableMap::translateFieldName('TspId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tsp_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : OldMonevTspTableMap::translateFieldName('IndikatorId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->indikator_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : OldMonevTspTableMap::translateFieldName('IndikatorLevel', TableMap::TYPE_PHPNAME, $indexType)];
            $this->indikator_level = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : OldMonevTspTableMap::translateFieldName('IndikatorNama', TableMap::TYPE_PHPNAME, $indexType)];
            $this->indikator_nama = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : OldMonevTspTableMap::translateFieldName('IndikatorFormulasi', TableMap::TYPE_PHPNAME, $indexType)];
            $this->indikator_formulasi = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : OldMonevTspTableMap::translateFieldName('IndikatorTarget', TableMap::TYPE_PHPNAME, $indexType)];
            $this->indikator_target = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : OldMonevTspTableMap::translateFieldName('IndikatorRealisasi', TableMap::TYPE_PHPNAME, $indexType)];
            $this->indikator_realisasi = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : OldMonevTspTableMap::translateFieldName('IndikatorSatuan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->indikator_satuan = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : OldMonevTspTableMap::translateFieldName('IndikatorBobot', TableMap::TYPE_PHPNAME, $indexType)];
            $this->indikator_bobot = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : OldMonevTspTableMap::translateFieldName('IndikatorCapaian', TableMap::TYPE_PHPNAME, $indexType)];
            $this->indikator_capaian = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : OldMonevTspTableMap::translateFieldName('KinerjaCapaian', TableMap::TYPE_PHPNAME, $indexType)];
            $this->kinerja_capaian = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : OldMonevTspTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : OldMonevTspTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 19; // 19 = OldMonevTspTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\CoreBundle\\Model\\OldEperformance\\OldMonevTsp'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aOldMasterSkpd !== null && $this->unit_id !== $this->aOldMasterSkpd->getSkpdId()) {
            $this->aOldMasterSkpd = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(OldMonevTspTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildOldMonevTspQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aOldMasterSkpd = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see OldMonevTsp::setDeleted()
     * @see OldMonevTsp::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldMonevTspTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildOldMonevTspQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldMonevTspTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                OldMonevTspTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aOldMasterSkpd !== null) {
                if ($this->aOldMasterSkpd->isModified() || $this->aOldMasterSkpd->isNew()) {
                    $affectedRows += $this->aOldMasterSkpd->save($con);
                }
                $this->setOldMasterSkpd($this->aOldMasterSkpd);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(OldMonevTspTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_PEGAWAI_ID)) {
            $modifiedColumns[':p' . $index++]  = 'pegawai_id';
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_PEGAWAI_NIP)) {
            $modifiedColumns[':p' . $index++]  = 'pegawai_nip';
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_PEGAWAI_NAMA)) {
            $modifiedColumns[':p' . $index++]  = 'pegawai_nama';
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_UNIT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'unit_id';
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_UNIT_KODE)) {
            $modifiedColumns[':p' . $index++]  = 'unit_kode';
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_TSP_ID)) {
            $modifiedColumns[':p' . $index++]  = 'tsp_id';
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_INDIKATOR_ID)) {
            $modifiedColumns[':p' . $index++]  = 'indikator_id';
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_INDIKATOR_LEVEL)) {
            $modifiedColumns[':p' . $index++]  = 'indikator_level';
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_INDIKATOR_NAMA)) {
            $modifiedColumns[':p' . $index++]  = 'indikator_nama';
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_INDIKATOR_FORMULASI)) {
            $modifiedColumns[':p' . $index++]  = 'indikator_formulasi';
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_INDIKATOR_TARGET)) {
            $modifiedColumns[':p' . $index++]  = 'indikator_target';
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_INDIKATOR_REALISASI)) {
            $modifiedColumns[':p' . $index++]  = 'indikator_realisasi';
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_INDIKATOR_SATUAN)) {
            $modifiedColumns[':p' . $index++]  = 'indikator_satuan';
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_INDIKATOR_BOBOT)) {
            $modifiedColumns[':p' . $index++]  = 'indikator_bobot';
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_INDIKATOR_CAPAIAN)) {
            $modifiedColumns[':p' . $index++]  = 'indikator_capaian';
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_KINERJA_CAPAIAN)) {
            $modifiedColumns[':p' . $index++]  = 'kinerja_capaian';
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO eperformance.monev_tsp (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'pegawai_id':
                        $stmt->bindValue($identifier, $this->pegawai_id, PDO::PARAM_INT);
                        break;
                    case 'pegawai_nip':
                        $stmt->bindValue($identifier, $this->pegawai_nip, PDO::PARAM_STR);
                        break;
                    case 'pegawai_nama':
                        $stmt->bindValue($identifier, $this->pegawai_nama, PDO::PARAM_STR);
                        break;
                    case 'unit_id':
                        $stmt->bindValue($identifier, $this->unit_id, PDO::PARAM_INT);
                        break;
                    case 'unit_kode':
                        $stmt->bindValue($identifier, $this->unit_kode, PDO::PARAM_STR);
                        break;
                    case 'tsp_id':
                        $stmt->bindValue($identifier, $this->tsp_id, PDO::PARAM_INT);
                        break;
                    case 'indikator_id':
                        $stmt->bindValue($identifier, $this->indikator_id, PDO::PARAM_STR);
                        break;
                    case 'indikator_level':
                        $stmt->bindValue($identifier, $this->indikator_level, PDO::PARAM_INT);
                        break;
                    case 'indikator_nama':
                        $stmt->bindValue($identifier, $this->indikator_nama, PDO::PARAM_STR);
                        break;
                    case 'indikator_formulasi':
                        $stmt->bindValue($identifier, $this->indikator_formulasi, PDO::PARAM_STR);
                        break;
                    case 'indikator_target':
                        $stmt->bindValue($identifier, $this->indikator_target, PDO::PARAM_INT);
                        break;
                    case 'indikator_realisasi':
                        $stmt->bindValue($identifier, $this->indikator_realisasi, PDO::PARAM_INT);
                        break;
                    case 'indikator_satuan':
                        $stmt->bindValue($identifier, $this->indikator_satuan, PDO::PARAM_STR);
                        break;
                    case 'indikator_bobot':
                        $stmt->bindValue($identifier, $this->indikator_bobot, PDO::PARAM_INT);
                        break;
                    case 'indikator_capaian':
                        $stmt->bindValue($identifier, $this->indikator_capaian, PDO::PARAM_INT);
                        break;
                    case 'kinerja_capaian':
                        $stmt->bindValue($identifier, $this->kinerja_capaian, PDO::PARAM_INT);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = OldMonevTspTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getPegawaiId();
                break;
            case 2:
                return $this->getPegawaiNip();
                break;
            case 3:
                return $this->getPegawaiNama();
                break;
            case 4:
                return $this->getUnitId();
                break;
            case 5:
                return $this->getUnitKode();
                break;
            case 6:
                return $this->getTspId();
                break;
            case 7:
                return $this->getIndikatorId();
                break;
            case 8:
                return $this->getIndikatorLevel();
                break;
            case 9:
                return $this->getIndikatorNama();
                break;
            case 10:
                return $this->getIndikatorFormulasi();
                break;
            case 11:
                return $this->getIndikatorTarget();
                break;
            case 12:
                return $this->getIndikatorRealisasi();
                break;
            case 13:
                return $this->getIndikatorSatuan();
                break;
            case 14:
                return $this->getIndikatorBobot();
                break;
            case 15:
                return $this->getIndikatorCapaian();
                break;
            case 16:
                return $this->getKinerjaCapaian();
                break;
            case 17:
                return $this->getCreatedAt();
                break;
            case 18:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['OldMonevTsp'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['OldMonevTsp'][$this->hashCode()] = true;
        $keys = OldMonevTspTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getPegawaiId(),
            $keys[2] => $this->getPegawaiNip(),
            $keys[3] => $this->getPegawaiNama(),
            $keys[4] => $this->getUnitId(),
            $keys[5] => $this->getUnitKode(),
            $keys[6] => $this->getTspId(),
            $keys[7] => $this->getIndikatorId(),
            $keys[8] => $this->getIndikatorLevel(),
            $keys[9] => $this->getIndikatorNama(),
            $keys[10] => $this->getIndikatorFormulasi(),
            $keys[11] => $this->getIndikatorTarget(),
            $keys[12] => $this->getIndikatorRealisasi(),
            $keys[13] => $this->getIndikatorSatuan(),
            $keys[14] => $this->getIndikatorBobot(),
            $keys[15] => $this->getIndikatorCapaian(),
            $keys[16] => $this->getKinerjaCapaian(),
            $keys[17] => $this->getCreatedAt(),
            $keys[18] => $this->getUpdatedAt(),
        );
        if ($result[$keys[17]] instanceof \DateTime) {
            $result[$keys[17]] = $result[$keys[17]]->format('c');
        }

        if ($result[$keys[18]] instanceof \DateTime) {
            $result[$keys[18]] = $result[$keys[18]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aOldMasterSkpd) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldMasterSkpd';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.master_skpd';
                        break;
                    default:
                        $key = 'OldMasterSkpd';
                }

                $result[$key] = $this->aOldMasterSkpd->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = OldMonevTspTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setPegawaiId($value);
                break;
            case 2:
                $this->setPegawaiNip($value);
                break;
            case 3:
                $this->setPegawaiNama($value);
                break;
            case 4:
                $this->setUnitId($value);
                break;
            case 5:
                $this->setUnitKode($value);
                break;
            case 6:
                $this->setTspId($value);
                break;
            case 7:
                $this->setIndikatorId($value);
                break;
            case 8:
                $this->setIndikatorLevel($value);
                break;
            case 9:
                $this->setIndikatorNama($value);
                break;
            case 10:
                $this->setIndikatorFormulasi($value);
                break;
            case 11:
                $this->setIndikatorTarget($value);
                break;
            case 12:
                $this->setIndikatorRealisasi($value);
                break;
            case 13:
                $this->setIndikatorSatuan($value);
                break;
            case 14:
                $this->setIndikatorBobot($value);
                break;
            case 15:
                $this->setIndikatorCapaian($value);
                break;
            case 16:
                $this->setKinerjaCapaian($value);
                break;
            case 17:
                $this->setCreatedAt($value);
                break;
            case 18:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = OldMonevTspTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setPegawaiId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setPegawaiNip($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setPegawaiNama($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setUnitId($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setUnitKode($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setTspId($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setIndikatorId($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setIndikatorLevel($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setIndikatorNama($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setIndikatorFormulasi($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setIndikatorTarget($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setIndikatorRealisasi($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setIndikatorSatuan($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setIndikatorBobot($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setIndikatorCapaian($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setKinerjaCapaian($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setCreatedAt($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setUpdatedAt($arr[$keys[18]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(OldMonevTspTableMap::DATABASE_NAME);

        if ($this->isColumnModified(OldMonevTspTableMap::COL_ID)) {
            $criteria->add(OldMonevTspTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_PEGAWAI_ID)) {
            $criteria->add(OldMonevTspTableMap::COL_PEGAWAI_ID, $this->pegawai_id);
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_PEGAWAI_NIP)) {
            $criteria->add(OldMonevTspTableMap::COL_PEGAWAI_NIP, $this->pegawai_nip);
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_PEGAWAI_NAMA)) {
            $criteria->add(OldMonevTspTableMap::COL_PEGAWAI_NAMA, $this->pegawai_nama);
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_UNIT_ID)) {
            $criteria->add(OldMonevTspTableMap::COL_UNIT_ID, $this->unit_id);
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_UNIT_KODE)) {
            $criteria->add(OldMonevTspTableMap::COL_UNIT_KODE, $this->unit_kode);
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_TSP_ID)) {
            $criteria->add(OldMonevTspTableMap::COL_TSP_ID, $this->tsp_id);
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_INDIKATOR_ID)) {
            $criteria->add(OldMonevTspTableMap::COL_INDIKATOR_ID, $this->indikator_id);
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_INDIKATOR_LEVEL)) {
            $criteria->add(OldMonevTspTableMap::COL_INDIKATOR_LEVEL, $this->indikator_level);
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_INDIKATOR_NAMA)) {
            $criteria->add(OldMonevTspTableMap::COL_INDIKATOR_NAMA, $this->indikator_nama);
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_INDIKATOR_FORMULASI)) {
            $criteria->add(OldMonevTspTableMap::COL_INDIKATOR_FORMULASI, $this->indikator_formulasi);
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_INDIKATOR_TARGET)) {
            $criteria->add(OldMonevTspTableMap::COL_INDIKATOR_TARGET, $this->indikator_target);
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_INDIKATOR_REALISASI)) {
            $criteria->add(OldMonevTspTableMap::COL_INDIKATOR_REALISASI, $this->indikator_realisasi);
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_INDIKATOR_SATUAN)) {
            $criteria->add(OldMonevTspTableMap::COL_INDIKATOR_SATUAN, $this->indikator_satuan);
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_INDIKATOR_BOBOT)) {
            $criteria->add(OldMonevTspTableMap::COL_INDIKATOR_BOBOT, $this->indikator_bobot);
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_INDIKATOR_CAPAIAN)) {
            $criteria->add(OldMonevTspTableMap::COL_INDIKATOR_CAPAIAN, $this->indikator_capaian);
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_KINERJA_CAPAIAN)) {
            $criteria->add(OldMonevTspTableMap::COL_KINERJA_CAPAIAN, $this->kinerja_capaian);
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_CREATED_AT)) {
            $criteria->add(OldMonevTspTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(OldMonevTspTableMap::COL_UPDATED_AT)) {
            $criteria->add(OldMonevTspTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildOldMonevTspQuery::create();
        $criteria->add(OldMonevTspTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \CoreBundle\Model\OldEperformance\OldMonevTsp (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setPegawaiId($this->getPegawaiId());
        $copyObj->setPegawaiNip($this->getPegawaiNip());
        $copyObj->setPegawaiNama($this->getPegawaiNama());
        $copyObj->setUnitId($this->getUnitId());
        $copyObj->setUnitKode($this->getUnitKode());
        $copyObj->setTspId($this->getTspId());
        $copyObj->setIndikatorId($this->getIndikatorId());
        $copyObj->setIndikatorLevel($this->getIndikatorLevel());
        $copyObj->setIndikatorNama($this->getIndikatorNama());
        $copyObj->setIndikatorFormulasi($this->getIndikatorFormulasi());
        $copyObj->setIndikatorTarget($this->getIndikatorTarget());
        $copyObj->setIndikatorRealisasi($this->getIndikatorRealisasi());
        $copyObj->setIndikatorSatuan($this->getIndikatorSatuan());
        $copyObj->setIndikatorBobot($this->getIndikatorBobot());
        $copyObj->setIndikatorCapaian($this->getIndikatorCapaian());
        $copyObj->setKinerjaCapaian($this->getKinerjaCapaian());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \CoreBundle\Model\OldEperformance\OldMonevTsp Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildOldMasterSkpd object.
     *
     * @param  ChildOldMasterSkpd $v
     * @return $this|\CoreBundle\Model\OldEperformance\OldMonevTsp The current object (for fluent API support)
     * @throws PropelException
     */
    public function setOldMasterSkpd(ChildOldMasterSkpd $v = null)
    {
        if ($v === null) {
            $this->setUnitId(NULL);
        } else {
            $this->setUnitId($v->getSkpdId());
        }

        $this->aOldMasterSkpd = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildOldMasterSkpd object, it will not be re-added.
        if ($v !== null) {
            $v->addOldMonevTsp($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildOldMasterSkpd object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildOldMasterSkpd The associated ChildOldMasterSkpd object.
     * @throws PropelException
     */
    public function getOldMasterSkpd(ConnectionInterface $con = null)
    {
        if ($this->aOldMasterSkpd === null && ($this->unit_id !== null)) {
            $this->aOldMasterSkpd = ChildOldMasterSkpdQuery::create()->findPk($this->unit_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aOldMasterSkpd->addOldMonevTsps($this);
             */
        }

        return $this->aOldMasterSkpd;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aOldMasterSkpd) {
            $this->aOldMasterSkpd->removeOldMonevTsp($this);
        }
        $this->id = null;
        $this->pegawai_id = null;
        $this->pegawai_nip = null;
        $this->pegawai_nama = null;
        $this->unit_id = null;
        $this->unit_kode = null;
        $this->tsp_id = null;
        $this->indikator_id = null;
        $this->indikator_level = null;
        $this->indikator_nama = null;
        $this->indikator_formulasi = null;
        $this->indikator_target = null;
        $this->indikator_realisasi = null;
        $this->indikator_satuan = null;
        $this->indikator_bobot = null;
        $this->indikator_capaian = null;
        $this->kinerja_capaian = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aOldMasterSkpd = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(OldMonevTspTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}

<?php

namespace CoreBundle\Model\OldEperformance\Base;

use \DateTime;
use \Exception;
use \PDO;
use CoreBundle\Model\OldEperformance\OldIndikatorKinerja as ChildOldIndikatorKinerja;
use CoreBundle\Model\OldEperformance\OldIndikatorKinerjaQuery as ChildOldIndikatorKinerjaQuery;
use CoreBundle\Model\OldEperformance\OldJabatanStruktural as ChildOldJabatanStruktural;
use CoreBundle\Model\OldEperformance\OldJabatanStrukturalQuery as ChildOldJabatanStrukturalQuery;
use CoreBundle\Model\OldEperformance\OldMasterSkpd as ChildOldMasterSkpd;
use CoreBundle\Model\OldEperformance\OldMasterSkpdQuery as ChildOldMasterSkpdQuery;
use CoreBundle\Model\OldEperformance\OldMonevTsp as ChildOldMonevTsp;
use CoreBundle\Model\OldEperformance\OldMonevTspKegiatan as ChildOldMonevTspKegiatan;
use CoreBundle\Model\OldEperformance\OldMonevTspKegiatanQuery as ChildOldMonevTspKegiatanQuery;
use CoreBundle\Model\OldEperformance\OldMonevTspQuery as ChildOldMonevTspQuery;
use CoreBundle\Model\OldEperformance\OldPegawai as ChildOldPegawai;
use CoreBundle\Model\OldEperformance\OldPegawaiQuery as ChildOldPegawaiQuery;
use CoreBundle\Model\OldEperformance\OldPerilakuTes as ChildOldPerilakuTes;
use CoreBundle\Model\OldEperformance\OldPerilakuTesQuery as ChildOldPerilakuTesQuery;
use CoreBundle\Model\OldEperformance\Map\OldIndikatorKinerjaTableMap;
use CoreBundle\Model\OldEperformance\Map\OldJabatanStrukturalTableMap;
use CoreBundle\Model\OldEperformance\Map\OldMasterSkpdTableMap;
use CoreBundle\Model\OldEperformance\Map\OldMonevTspKegiatanTableMap;
use CoreBundle\Model\OldEperformance\Map\OldMonevTspTableMap;
use CoreBundle\Model\OldEperformance\Map\OldPegawaiTableMap;
use CoreBundle\Model\OldEperformance\Map\OldPerilakuTesTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'eperformance.master_skpd' table.
 *
 *
 *
 * @package    propel.generator.src.CoreBundle.Model.OldEperformance.Base
 */
abstract class OldMasterSkpd implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\CoreBundle\\Model\\OldEperformance\\Map\\OldMasterSkpdTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the skpd_id field.
     *
     * @var        int
     */
    protected $skpd_id;

    /**
     * The value for the skpd_kode field.
     *
     * @var        string
     */
    protected $skpd_kode;

    /**
     * The value for the skpd_nama field.
     *
     * @var        string
     */
    protected $skpd_nama;

    /**
     * The value for the deleted_at field.
     *
     * @var        DateTime
     */
    protected $deleted_at;

    /**
     * @var        ObjectCollection|ChildOldPerilakuTes[] Collection to store aggregation of ChildOldPerilakuTes objects.
     */
    protected $collOldPerilakuTess;
    protected $collOldPerilakuTessPartial;

    /**
     * @var        ObjectCollection|ChildOldPegawai[] Collection to store aggregation of ChildOldPegawai objects.
     */
    protected $collOldPegawais;
    protected $collOldPegawaisPartial;

    /**
     * @var        ObjectCollection|ChildOldIndikatorKinerja[] Collection to store aggregation of ChildOldIndikatorKinerja objects.
     */
    protected $collOldIndikatorKinerjas;
    protected $collOldIndikatorKinerjasPartial;

    /**
     * @var        ObjectCollection|ChildOldJabatanStruktural[] Collection to store aggregation of ChildOldJabatanStruktural objects.
     */
    protected $collOldJabatanStrukturals;
    protected $collOldJabatanStrukturalsPartial;

    /**
     * @var        ObjectCollection|ChildOldMonevTsp[] Collection to store aggregation of ChildOldMonevTsp objects.
     */
    protected $collOldMonevTsps;
    protected $collOldMonevTspsPartial;

    /**
     * @var        ObjectCollection|ChildOldMonevTspKegiatan[] Collection to store aggregation of ChildOldMonevTspKegiatan objects.
     */
    protected $collOldMonevTspKegiatans;
    protected $collOldMonevTspKegiatansPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildOldPerilakuTes[]
     */
    protected $oldPerilakuTessScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildOldPegawai[]
     */
    protected $oldPegawaisScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildOldIndikatorKinerja[]
     */
    protected $oldIndikatorKinerjasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildOldJabatanStruktural[]
     */
    protected $oldJabatanStrukturalsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildOldMonevTsp[]
     */
    protected $oldMonevTspsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildOldMonevTspKegiatan[]
     */
    protected $oldMonevTspKegiatansScheduledForDeletion = null;

    /**
     * Initializes internal state of CoreBundle\Model\OldEperformance\Base\OldMasterSkpd object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>OldMasterSkpd</code> instance.  If
     * <code>obj</code> is an instance of <code>OldMasterSkpd</code>, delegates to
     * <code>equals(OldMasterSkpd)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|OldMasterSkpd The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [skpd_id] column value.
     *
     * @return int
     */
    public function getSkpdId()
    {
        return $this->skpd_id;
    }

    /**
     * Get the [skpd_kode] column value.
     *
     * @return string
     */
    public function getSkpdKode()
    {
        return $this->skpd_kode;
    }

    /**
     * Get the [skpd_nama] column value.
     *
     * @return string
     */
    public function getSkpdNama()
    {
        return $this->skpd_nama;
    }

    /**
     * Get the [optionally formatted] temporal [deleted_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDeletedAt($format = NULL)
    {
        if ($format === null) {
            return $this->deleted_at;
        } else {
            return $this->deleted_at instanceof \DateTimeInterface ? $this->deleted_at->format($format) : null;
        }
    }

    /**
     * Set the value of [skpd_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMasterSkpd The current object (for fluent API support)
     */
    public function setSkpdId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->skpd_id !== $v) {
            $this->skpd_id = $v;
            $this->modifiedColumns[OldMasterSkpdTableMap::COL_SKPD_ID] = true;
        }

        return $this;
    } // setSkpdId()

    /**
     * Set the value of [skpd_kode] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMasterSkpd The current object (for fluent API support)
     */
    public function setSkpdKode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->skpd_kode !== $v) {
            $this->skpd_kode = $v;
            $this->modifiedColumns[OldMasterSkpdTableMap::COL_SKPD_KODE] = true;
        }

        return $this;
    } // setSkpdKode()

    /**
     * Set the value of [skpd_nama] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMasterSkpd The current object (for fluent API support)
     */
    public function setSkpdNama($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->skpd_nama !== $v) {
            $this->skpd_nama = $v;
            $this->modifiedColumns[OldMasterSkpdTableMap::COL_SKPD_NAMA] = true;
        }

        return $this;
    } // setSkpdNama()

    /**
     * Sets the value of [deleted_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\OldEperformance\OldMasterSkpd The current object (for fluent API support)
     */
    public function setDeletedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->deleted_at !== null || $dt !== null) {
            if ($this->deleted_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->deleted_at->format("Y-m-d H:i:s.u")) {
                $this->deleted_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[OldMasterSkpdTableMap::COL_DELETED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setDeletedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : OldMasterSkpdTableMap::translateFieldName('SkpdId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->skpd_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : OldMasterSkpdTableMap::translateFieldName('SkpdKode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->skpd_kode = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : OldMasterSkpdTableMap::translateFieldName('SkpdNama', TableMap::TYPE_PHPNAME, $indexType)];
            $this->skpd_nama = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : OldMasterSkpdTableMap::translateFieldName('DeletedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->deleted_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 4; // 4 = OldMasterSkpdTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\CoreBundle\\Model\\OldEperformance\\OldMasterSkpd'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(OldMasterSkpdTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildOldMasterSkpdQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collOldPerilakuTess = null;

            $this->collOldPegawais = null;

            $this->collOldIndikatorKinerjas = null;

            $this->collOldJabatanStrukturals = null;

            $this->collOldMonevTsps = null;

            $this->collOldMonevTspKegiatans = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see OldMasterSkpd::setDeleted()
     * @see OldMasterSkpd::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldMasterSkpdTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildOldMasterSkpdQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldMasterSkpdTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                OldMasterSkpdTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->oldPerilakuTessScheduledForDeletion !== null) {
                if (!$this->oldPerilakuTessScheduledForDeletion->isEmpty()) {
                    foreach ($this->oldPerilakuTessScheduledForDeletion as $oldPerilakuTes) {
                        // need to save related object because we set the relation to null
                        $oldPerilakuTes->save($con);
                    }
                    $this->oldPerilakuTessScheduledForDeletion = null;
                }
            }

            if ($this->collOldPerilakuTess !== null) {
                foreach ($this->collOldPerilakuTess as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->oldPegawaisScheduledForDeletion !== null) {
                if (!$this->oldPegawaisScheduledForDeletion->isEmpty()) {
                    foreach ($this->oldPegawaisScheduledForDeletion as $oldPegawai) {
                        // need to save related object because we set the relation to null
                        $oldPegawai->save($con);
                    }
                    $this->oldPegawaisScheduledForDeletion = null;
                }
            }

            if ($this->collOldPegawais !== null) {
                foreach ($this->collOldPegawais as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->oldIndikatorKinerjasScheduledForDeletion !== null) {
                if (!$this->oldIndikatorKinerjasScheduledForDeletion->isEmpty()) {
                    foreach ($this->oldIndikatorKinerjasScheduledForDeletion as $oldIndikatorKinerja) {
                        // need to save related object because we set the relation to null
                        $oldIndikatorKinerja->save($con);
                    }
                    $this->oldIndikatorKinerjasScheduledForDeletion = null;
                }
            }

            if ($this->collOldIndikatorKinerjas !== null) {
                foreach ($this->collOldIndikatorKinerjas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->oldJabatanStrukturalsScheduledForDeletion !== null) {
                if (!$this->oldJabatanStrukturalsScheduledForDeletion->isEmpty()) {
                    foreach ($this->oldJabatanStrukturalsScheduledForDeletion as $oldJabatanStruktural) {
                        // need to save related object because we set the relation to null
                        $oldJabatanStruktural->save($con);
                    }
                    $this->oldJabatanStrukturalsScheduledForDeletion = null;
                }
            }

            if ($this->collOldJabatanStrukturals !== null) {
                foreach ($this->collOldJabatanStrukturals as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->oldMonevTspsScheduledForDeletion !== null) {
                if (!$this->oldMonevTspsScheduledForDeletion->isEmpty()) {
                    \CoreBundle\Model\OldEperformance\OldMonevTspQuery::create()
                        ->filterByPrimaryKeys($this->oldMonevTspsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->oldMonevTspsScheduledForDeletion = null;
                }
            }

            if ($this->collOldMonevTsps !== null) {
                foreach ($this->collOldMonevTsps as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->oldMonevTspKegiatansScheduledForDeletion !== null) {
                if (!$this->oldMonevTspKegiatansScheduledForDeletion->isEmpty()) {
                    \CoreBundle\Model\OldEperformance\OldMonevTspKegiatanQuery::create()
                        ->filterByPrimaryKeys($this->oldMonevTspKegiatansScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->oldMonevTspKegiatansScheduledForDeletion = null;
                }
            }

            if ($this->collOldMonevTspKegiatans !== null) {
                foreach ($this->collOldMonevTspKegiatans as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[OldMasterSkpdTableMap::COL_SKPD_ID] = true;
        if (null !== $this->skpd_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . OldMasterSkpdTableMap::COL_SKPD_ID . ')');
        }
        if (null === $this->skpd_id) {
            try {
                $dataFetcher = $con->query("SELECT nextval('eperformance.master_skpd_skpd_id_seq')");
                $this->skpd_id = (int) $dataFetcher->fetchColumn();
            } catch (Exception $e) {
                throw new PropelException('Unable to get sequence id.', 0, $e);
            }
        }


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(OldMasterSkpdTableMap::COL_SKPD_ID)) {
            $modifiedColumns[':p' . $index++]  = 'skpd_id';
        }
        if ($this->isColumnModified(OldMasterSkpdTableMap::COL_SKPD_KODE)) {
            $modifiedColumns[':p' . $index++]  = 'skpd_kode';
        }
        if ($this->isColumnModified(OldMasterSkpdTableMap::COL_SKPD_NAMA)) {
            $modifiedColumns[':p' . $index++]  = 'skpd_nama';
        }
        if ($this->isColumnModified(OldMasterSkpdTableMap::COL_DELETED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'deleted_at';
        }

        $sql = sprintf(
            'INSERT INTO eperformance.master_skpd (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'skpd_id':
                        $stmt->bindValue($identifier, $this->skpd_id, PDO::PARAM_INT);
                        break;
                    case 'skpd_kode':
                        $stmt->bindValue($identifier, $this->skpd_kode, PDO::PARAM_STR);
                        break;
                    case 'skpd_nama':
                        $stmt->bindValue($identifier, $this->skpd_nama, PDO::PARAM_STR);
                        break;
                    case 'deleted_at':
                        $stmt->bindValue($identifier, $this->deleted_at ? $this->deleted_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = OldMasterSkpdTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getSkpdId();
                break;
            case 1:
                return $this->getSkpdKode();
                break;
            case 2:
                return $this->getSkpdNama();
                break;
            case 3:
                return $this->getDeletedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['OldMasterSkpd'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['OldMasterSkpd'][$this->hashCode()] = true;
        $keys = OldMasterSkpdTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getSkpdId(),
            $keys[1] => $this->getSkpdKode(),
            $keys[2] => $this->getSkpdNama(),
            $keys[3] => $this->getDeletedAt(),
        );
        if ($result[$keys[3]] instanceof \DateTime) {
            $result[$keys[3]] = $result[$keys[3]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collOldPerilakuTess) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldPerilakuTess';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.skp_perilaku_kerja_tess';
                        break;
                    default:
                        $key = 'OldPerilakuTess';
                }

                $result[$key] = $this->collOldPerilakuTess->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collOldPegawais) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldPegawais';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.pegawais';
                        break;
                    default:
                        $key = 'OldPegawais';
                }

                $result[$key] = $this->collOldPegawais->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collOldIndikatorKinerjas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldIndikatorKinerjas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.indikator_kinerjas';
                        break;
                    default:
                        $key = 'OldIndikatorKinerjas';
                }

                $result[$key] = $this->collOldIndikatorKinerjas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collOldJabatanStrukturals) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldJabatanStrukturals';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.jabatan_strukturals';
                        break;
                    default:
                        $key = 'OldJabatanStrukturals';
                }

                $result[$key] = $this->collOldJabatanStrukturals->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collOldMonevTsps) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldMonevTsps';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.monev_tsps';
                        break;
                    default:
                        $key = 'OldMonevTsps';
                }

                $result[$key] = $this->collOldMonevTsps->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collOldMonevTspKegiatans) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldMonevTspKegiatans';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.monev_tsp_kegiatans';
                        break;
                    default:
                        $key = 'OldMonevTspKegiatans';
                }

                $result[$key] = $this->collOldMonevTspKegiatans->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\CoreBundle\Model\OldEperformance\OldMasterSkpd
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = OldMasterSkpdTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\CoreBundle\Model\OldEperformance\OldMasterSkpd
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setSkpdId($value);
                break;
            case 1:
                $this->setSkpdKode($value);
                break;
            case 2:
                $this->setSkpdNama($value);
                break;
            case 3:
                $this->setDeletedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = OldMasterSkpdTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setSkpdId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setSkpdKode($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setSkpdNama($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDeletedAt($arr[$keys[3]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\CoreBundle\Model\OldEperformance\OldMasterSkpd The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(OldMasterSkpdTableMap::DATABASE_NAME);

        if ($this->isColumnModified(OldMasterSkpdTableMap::COL_SKPD_ID)) {
            $criteria->add(OldMasterSkpdTableMap::COL_SKPD_ID, $this->skpd_id);
        }
        if ($this->isColumnModified(OldMasterSkpdTableMap::COL_SKPD_KODE)) {
            $criteria->add(OldMasterSkpdTableMap::COL_SKPD_KODE, $this->skpd_kode);
        }
        if ($this->isColumnModified(OldMasterSkpdTableMap::COL_SKPD_NAMA)) {
            $criteria->add(OldMasterSkpdTableMap::COL_SKPD_NAMA, $this->skpd_nama);
        }
        if ($this->isColumnModified(OldMasterSkpdTableMap::COL_DELETED_AT)) {
            $criteria->add(OldMasterSkpdTableMap::COL_DELETED_AT, $this->deleted_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildOldMasterSkpdQuery::create();
        $criteria->add(OldMasterSkpdTableMap::COL_SKPD_ID, $this->skpd_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getSkpdId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getSkpdId();
    }

    /**
     * Generic method to set the primary key (skpd_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setSkpdId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getSkpdId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \CoreBundle\Model\OldEperformance\OldMasterSkpd (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSkpdKode($this->getSkpdKode());
        $copyObj->setSkpdNama($this->getSkpdNama());
        $copyObj->setDeletedAt($this->getDeletedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getOldPerilakuTess() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addOldPerilakuTes($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getOldPegawais() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addOldPegawai($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getOldIndikatorKinerjas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addOldIndikatorKinerja($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getOldJabatanStrukturals() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addOldJabatanStruktural($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getOldMonevTsps() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addOldMonevTsp($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getOldMonevTspKegiatans() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addOldMonevTspKegiatan($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setSkpdId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \CoreBundle\Model\OldEperformance\OldMasterSkpd Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('OldPerilakuTes' == $relationName) {
            return $this->initOldPerilakuTess();
        }
        if ('OldPegawai' == $relationName) {
            return $this->initOldPegawais();
        }
        if ('OldIndikatorKinerja' == $relationName) {
            return $this->initOldIndikatorKinerjas();
        }
        if ('OldJabatanStruktural' == $relationName) {
            return $this->initOldJabatanStrukturals();
        }
        if ('OldMonevTsp' == $relationName) {
            return $this->initOldMonevTsps();
        }
        if ('OldMonevTspKegiatan' == $relationName) {
            return $this->initOldMonevTspKegiatans();
        }
    }

    /**
     * Clears out the collOldPerilakuTess collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addOldPerilakuTess()
     */
    public function clearOldPerilakuTess()
    {
        $this->collOldPerilakuTess = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collOldPerilakuTess collection loaded partially.
     */
    public function resetPartialOldPerilakuTess($v = true)
    {
        $this->collOldPerilakuTessPartial = $v;
    }

    /**
     * Initializes the collOldPerilakuTess collection.
     *
     * By default this just sets the collOldPerilakuTess collection to an empty array (like clearcollOldPerilakuTess());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initOldPerilakuTess($overrideExisting = true)
    {
        if (null !== $this->collOldPerilakuTess && !$overrideExisting) {
            return;
        }

        $collectionClassName = OldPerilakuTesTableMap::getTableMap()->getCollectionClassName();

        $this->collOldPerilakuTess = new $collectionClassName;
        $this->collOldPerilakuTess->setModel('\CoreBundle\Model\OldEperformance\OldPerilakuTes');
    }

    /**
     * Gets an array of ChildOldPerilakuTes objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildOldMasterSkpd is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildOldPerilakuTes[] List of ChildOldPerilakuTes objects
     * @throws PropelException
     */
    public function getOldPerilakuTess(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collOldPerilakuTessPartial && !$this->isNew();
        if (null === $this->collOldPerilakuTess || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collOldPerilakuTess) {
                // return empty collection
                $this->initOldPerilakuTess();
            } else {
                $collOldPerilakuTess = ChildOldPerilakuTesQuery::create(null, $criteria)
                    ->filterByOldMasterSkpd($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collOldPerilakuTessPartial && count($collOldPerilakuTess)) {
                        $this->initOldPerilakuTess(false);

                        foreach ($collOldPerilakuTess as $obj) {
                            if (false == $this->collOldPerilakuTess->contains($obj)) {
                                $this->collOldPerilakuTess->append($obj);
                            }
                        }

                        $this->collOldPerilakuTessPartial = true;
                    }

                    return $collOldPerilakuTess;
                }

                if ($partial && $this->collOldPerilakuTess) {
                    foreach ($this->collOldPerilakuTess as $obj) {
                        if ($obj->isNew()) {
                            $collOldPerilakuTess[] = $obj;
                        }
                    }
                }

                $this->collOldPerilakuTess = $collOldPerilakuTess;
                $this->collOldPerilakuTessPartial = false;
            }
        }

        return $this->collOldPerilakuTess;
    }

    /**
     * Sets a collection of ChildOldPerilakuTes objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $oldPerilakuTess A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildOldMasterSkpd The current object (for fluent API support)
     */
    public function setOldPerilakuTess(Collection $oldPerilakuTess, ConnectionInterface $con = null)
    {
        /** @var ChildOldPerilakuTes[] $oldPerilakuTessToDelete */
        $oldPerilakuTessToDelete = $this->getOldPerilakuTess(new Criteria(), $con)->diff($oldPerilakuTess);


        $this->oldPerilakuTessScheduledForDeletion = $oldPerilakuTessToDelete;

        foreach ($oldPerilakuTessToDelete as $oldPerilakuTesRemoved) {
            $oldPerilakuTesRemoved->setOldMasterSkpd(null);
        }

        $this->collOldPerilakuTess = null;
        foreach ($oldPerilakuTess as $oldPerilakuTes) {
            $this->addOldPerilakuTes($oldPerilakuTes);
        }

        $this->collOldPerilakuTess = $oldPerilakuTess;
        $this->collOldPerilakuTessPartial = false;

        return $this;
    }

    /**
     * Returns the number of related OldPerilakuTes objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related OldPerilakuTes objects.
     * @throws PropelException
     */
    public function countOldPerilakuTess(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collOldPerilakuTessPartial && !$this->isNew();
        if (null === $this->collOldPerilakuTess || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collOldPerilakuTess) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getOldPerilakuTess());
            }

            $query = ChildOldPerilakuTesQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByOldMasterSkpd($this)
                ->count($con);
        }

        return count($this->collOldPerilakuTess);
    }

    /**
     * Method called to associate a ChildOldPerilakuTes object to this object
     * through the ChildOldPerilakuTes foreign key attribute.
     *
     * @param  ChildOldPerilakuTes $l ChildOldPerilakuTes
     * @return $this|\CoreBundle\Model\OldEperformance\OldMasterSkpd The current object (for fluent API support)
     */
    public function addOldPerilakuTes(ChildOldPerilakuTes $l)
    {
        if ($this->collOldPerilakuTess === null) {
            $this->initOldPerilakuTess();
            $this->collOldPerilakuTessPartial = true;
        }

        if (!$this->collOldPerilakuTess->contains($l)) {
            $this->doAddOldPerilakuTes($l);

            if ($this->oldPerilakuTessScheduledForDeletion and $this->oldPerilakuTessScheduledForDeletion->contains($l)) {
                $this->oldPerilakuTessScheduledForDeletion->remove($this->oldPerilakuTessScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildOldPerilakuTes $oldPerilakuTes The ChildOldPerilakuTes object to add.
     */
    protected function doAddOldPerilakuTes(ChildOldPerilakuTes $oldPerilakuTes)
    {
        $this->collOldPerilakuTess[]= $oldPerilakuTes;
        $oldPerilakuTes->setOldMasterSkpd($this);
    }

    /**
     * @param  ChildOldPerilakuTes $oldPerilakuTes The ChildOldPerilakuTes object to remove.
     * @return $this|ChildOldMasterSkpd The current object (for fluent API support)
     */
    public function removeOldPerilakuTes(ChildOldPerilakuTes $oldPerilakuTes)
    {
        if ($this->getOldPerilakuTess()->contains($oldPerilakuTes)) {
            $pos = $this->collOldPerilakuTess->search($oldPerilakuTes);
            $this->collOldPerilakuTess->remove($pos);
            if (null === $this->oldPerilakuTessScheduledForDeletion) {
                $this->oldPerilakuTessScheduledForDeletion = clone $this->collOldPerilakuTess;
                $this->oldPerilakuTessScheduledForDeletion->clear();
            }
            $this->oldPerilakuTessScheduledForDeletion[]= $oldPerilakuTes;
            $oldPerilakuTes->setOldMasterSkpd(null);
        }

        return $this;
    }

    /**
     * Clears out the collOldPegawais collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addOldPegawais()
     */
    public function clearOldPegawais()
    {
        $this->collOldPegawais = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collOldPegawais collection loaded partially.
     */
    public function resetPartialOldPegawais($v = true)
    {
        $this->collOldPegawaisPartial = $v;
    }

    /**
     * Initializes the collOldPegawais collection.
     *
     * By default this just sets the collOldPegawais collection to an empty array (like clearcollOldPegawais());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initOldPegawais($overrideExisting = true)
    {
        if (null !== $this->collOldPegawais && !$overrideExisting) {
            return;
        }

        $collectionClassName = OldPegawaiTableMap::getTableMap()->getCollectionClassName();

        $this->collOldPegawais = new $collectionClassName;
        $this->collOldPegawais->setModel('\CoreBundle\Model\OldEperformance\OldPegawai');
    }

    /**
     * Gets an array of ChildOldPegawai objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildOldMasterSkpd is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildOldPegawai[] List of ChildOldPegawai objects
     * @throws PropelException
     */
    public function getOldPegawais(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collOldPegawaisPartial && !$this->isNew();
        if (null === $this->collOldPegawais || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collOldPegawais) {
                // return empty collection
                $this->initOldPegawais();
            } else {
                $collOldPegawais = ChildOldPegawaiQuery::create(null, $criteria)
                    ->filterByOldMasterSkpd($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collOldPegawaisPartial && count($collOldPegawais)) {
                        $this->initOldPegawais(false);

                        foreach ($collOldPegawais as $obj) {
                            if (false == $this->collOldPegawais->contains($obj)) {
                                $this->collOldPegawais->append($obj);
                            }
                        }

                        $this->collOldPegawaisPartial = true;
                    }

                    return $collOldPegawais;
                }

                if ($partial && $this->collOldPegawais) {
                    foreach ($this->collOldPegawais as $obj) {
                        if ($obj->isNew()) {
                            $collOldPegawais[] = $obj;
                        }
                    }
                }

                $this->collOldPegawais = $collOldPegawais;
                $this->collOldPegawaisPartial = false;
            }
        }

        return $this->collOldPegawais;
    }

    /**
     * Sets a collection of ChildOldPegawai objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $oldPegawais A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildOldMasterSkpd The current object (for fluent API support)
     */
    public function setOldPegawais(Collection $oldPegawais, ConnectionInterface $con = null)
    {
        /** @var ChildOldPegawai[] $oldPegawaisToDelete */
        $oldPegawaisToDelete = $this->getOldPegawais(new Criteria(), $con)->diff($oldPegawais);


        $this->oldPegawaisScheduledForDeletion = $oldPegawaisToDelete;

        foreach ($oldPegawaisToDelete as $oldPegawaiRemoved) {
            $oldPegawaiRemoved->setOldMasterSkpd(null);
        }

        $this->collOldPegawais = null;
        foreach ($oldPegawais as $oldPegawai) {
            $this->addOldPegawai($oldPegawai);
        }

        $this->collOldPegawais = $oldPegawais;
        $this->collOldPegawaisPartial = false;

        return $this;
    }

    /**
     * Returns the number of related OldPegawai objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related OldPegawai objects.
     * @throws PropelException
     */
    public function countOldPegawais(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collOldPegawaisPartial && !$this->isNew();
        if (null === $this->collOldPegawais || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collOldPegawais) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getOldPegawais());
            }

            $query = ChildOldPegawaiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByOldMasterSkpd($this)
                ->count($con);
        }

        return count($this->collOldPegawais);
    }

    /**
     * Method called to associate a ChildOldPegawai object to this object
     * through the ChildOldPegawai foreign key attribute.
     *
     * @param  ChildOldPegawai $l ChildOldPegawai
     * @return $this|\CoreBundle\Model\OldEperformance\OldMasterSkpd The current object (for fluent API support)
     */
    public function addOldPegawai(ChildOldPegawai $l)
    {
        if ($this->collOldPegawais === null) {
            $this->initOldPegawais();
            $this->collOldPegawaisPartial = true;
        }

        if (!$this->collOldPegawais->contains($l)) {
            $this->doAddOldPegawai($l);

            if ($this->oldPegawaisScheduledForDeletion and $this->oldPegawaisScheduledForDeletion->contains($l)) {
                $this->oldPegawaisScheduledForDeletion->remove($this->oldPegawaisScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildOldPegawai $oldPegawai The ChildOldPegawai object to add.
     */
    protected function doAddOldPegawai(ChildOldPegawai $oldPegawai)
    {
        $this->collOldPegawais[]= $oldPegawai;
        $oldPegawai->setOldMasterSkpd($this);
    }

    /**
     * @param  ChildOldPegawai $oldPegawai The ChildOldPegawai object to remove.
     * @return $this|ChildOldMasterSkpd The current object (for fluent API support)
     */
    public function removeOldPegawai(ChildOldPegawai $oldPegawai)
    {
        if ($this->getOldPegawais()->contains($oldPegawai)) {
            $pos = $this->collOldPegawais->search($oldPegawai);
            $this->collOldPegawais->remove($pos);
            if (null === $this->oldPegawaisScheduledForDeletion) {
                $this->oldPegawaisScheduledForDeletion = clone $this->collOldPegawais;
                $this->oldPegawaisScheduledForDeletion->clear();
            }
            $this->oldPegawaisScheduledForDeletion[]= $oldPegawai;
            $oldPegawai->setOldMasterSkpd(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this OldMasterSkpd is new, it will return
     * an empty collection; or if this OldMasterSkpd has previously
     * been saved, it will retrieve related OldPegawais from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in OldMasterSkpd.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOldPegawai[] List of ChildOldPegawai objects
     */
    public function getOldPegawaisJoinOldPegawai(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOldPegawaiQuery::create(null, $criteria);
        $query->joinWith('OldPegawai', $joinBehavior);

        return $this->getOldPegawais($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this OldMasterSkpd is new, it will return
     * an empty collection; or if this OldMasterSkpd has previously
     * been saved, it will retrieve related OldPegawais from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in OldMasterSkpd.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOldPegawai[] List of ChildOldPegawai objects
     */
    public function getOldPegawaisJoinOldJabatanStruktural(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOldPegawaiQuery::create(null, $criteria);
        $query->joinWith('OldJabatanStruktural', $joinBehavior);

        return $this->getOldPegawais($query, $con);
    }

    /**
     * Clears out the collOldIndikatorKinerjas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addOldIndikatorKinerjas()
     */
    public function clearOldIndikatorKinerjas()
    {
        $this->collOldIndikatorKinerjas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collOldIndikatorKinerjas collection loaded partially.
     */
    public function resetPartialOldIndikatorKinerjas($v = true)
    {
        $this->collOldIndikatorKinerjasPartial = $v;
    }

    /**
     * Initializes the collOldIndikatorKinerjas collection.
     *
     * By default this just sets the collOldIndikatorKinerjas collection to an empty array (like clearcollOldIndikatorKinerjas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initOldIndikatorKinerjas($overrideExisting = true)
    {
        if (null !== $this->collOldIndikatorKinerjas && !$overrideExisting) {
            return;
        }

        $collectionClassName = OldIndikatorKinerjaTableMap::getTableMap()->getCollectionClassName();

        $this->collOldIndikatorKinerjas = new $collectionClassName;
        $this->collOldIndikatorKinerjas->setModel('\CoreBundle\Model\OldEperformance\OldIndikatorKinerja');
    }

    /**
     * Gets an array of ChildOldIndikatorKinerja objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildOldMasterSkpd is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildOldIndikatorKinerja[] List of ChildOldIndikatorKinerja objects
     * @throws PropelException
     */
    public function getOldIndikatorKinerjas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collOldIndikatorKinerjasPartial && !$this->isNew();
        if (null === $this->collOldIndikatorKinerjas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collOldIndikatorKinerjas) {
                // return empty collection
                $this->initOldIndikatorKinerjas();
            } else {
                $collOldIndikatorKinerjas = ChildOldIndikatorKinerjaQuery::create(null, $criteria)
                    ->filterByOldMasterSkpd($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collOldIndikatorKinerjasPartial && count($collOldIndikatorKinerjas)) {
                        $this->initOldIndikatorKinerjas(false);

                        foreach ($collOldIndikatorKinerjas as $obj) {
                            if (false == $this->collOldIndikatorKinerjas->contains($obj)) {
                                $this->collOldIndikatorKinerjas->append($obj);
                            }
                        }

                        $this->collOldIndikatorKinerjasPartial = true;
                    }

                    return $collOldIndikatorKinerjas;
                }

                if ($partial && $this->collOldIndikatorKinerjas) {
                    foreach ($this->collOldIndikatorKinerjas as $obj) {
                        if ($obj->isNew()) {
                            $collOldIndikatorKinerjas[] = $obj;
                        }
                    }
                }

                $this->collOldIndikatorKinerjas = $collOldIndikatorKinerjas;
                $this->collOldIndikatorKinerjasPartial = false;
            }
        }

        return $this->collOldIndikatorKinerjas;
    }

    /**
     * Sets a collection of ChildOldIndikatorKinerja objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $oldIndikatorKinerjas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildOldMasterSkpd The current object (for fluent API support)
     */
    public function setOldIndikatorKinerjas(Collection $oldIndikatorKinerjas, ConnectionInterface $con = null)
    {
        /** @var ChildOldIndikatorKinerja[] $oldIndikatorKinerjasToDelete */
        $oldIndikatorKinerjasToDelete = $this->getOldIndikatorKinerjas(new Criteria(), $con)->diff($oldIndikatorKinerjas);


        $this->oldIndikatorKinerjasScheduledForDeletion = $oldIndikatorKinerjasToDelete;

        foreach ($oldIndikatorKinerjasToDelete as $oldIndikatorKinerjaRemoved) {
            $oldIndikatorKinerjaRemoved->setOldMasterSkpd(null);
        }

        $this->collOldIndikatorKinerjas = null;
        foreach ($oldIndikatorKinerjas as $oldIndikatorKinerja) {
            $this->addOldIndikatorKinerja($oldIndikatorKinerja);
        }

        $this->collOldIndikatorKinerjas = $oldIndikatorKinerjas;
        $this->collOldIndikatorKinerjasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related OldIndikatorKinerja objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related OldIndikatorKinerja objects.
     * @throws PropelException
     */
    public function countOldIndikatorKinerjas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collOldIndikatorKinerjasPartial && !$this->isNew();
        if (null === $this->collOldIndikatorKinerjas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collOldIndikatorKinerjas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getOldIndikatorKinerjas());
            }

            $query = ChildOldIndikatorKinerjaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByOldMasterSkpd($this)
                ->count($con);
        }

        return count($this->collOldIndikatorKinerjas);
    }

    /**
     * Method called to associate a ChildOldIndikatorKinerja object to this object
     * through the ChildOldIndikatorKinerja foreign key attribute.
     *
     * @param  ChildOldIndikatorKinerja $l ChildOldIndikatorKinerja
     * @return $this|\CoreBundle\Model\OldEperformance\OldMasterSkpd The current object (for fluent API support)
     */
    public function addOldIndikatorKinerja(ChildOldIndikatorKinerja $l)
    {
        if ($this->collOldIndikatorKinerjas === null) {
            $this->initOldIndikatorKinerjas();
            $this->collOldIndikatorKinerjasPartial = true;
        }

        if (!$this->collOldIndikatorKinerjas->contains($l)) {
            $this->doAddOldIndikatorKinerja($l);

            if ($this->oldIndikatorKinerjasScheduledForDeletion and $this->oldIndikatorKinerjasScheduledForDeletion->contains($l)) {
                $this->oldIndikatorKinerjasScheduledForDeletion->remove($this->oldIndikatorKinerjasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildOldIndikatorKinerja $oldIndikatorKinerja The ChildOldIndikatorKinerja object to add.
     */
    protected function doAddOldIndikatorKinerja(ChildOldIndikatorKinerja $oldIndikatorKinerja)
    {
        $this->collOldIndikatorKinerjas[]= $oldIndikatorKinerja;
        $oldIndikatorKinerja->setOldMasterSkpd($this);
    }

    /**
     * @param  ChildOldIndikatorKinerja $oldIndikatorKinerja The ChildOldIndikatorKinerja object to remove.
     * @return $this|ChildOldMasterSkpd The current object (for fluent API support)
     */
    public function removeOldIndikatorKinerja(ChildOldIndikatorKinerja $oldIndikatorKinerja)
    {
        if ($this->getOldIndikatorKinerjas()->contains($oldIndikatorKinerja)) {
            $pos = $this->collOldIndikatorKinerjas->search($oldIndikatorKinerja);
            $this->collOldIndikatorKinerjas->remove($pos);
            if (null === $this->oldIndikatorKinerjasScheduledForDeletion) {
                $this->oldIndikatorKinerjasScheduledForDeletion = clone $this->collOldIndikatorKinerjas;
                $this->oldIndikatorKinerjasScheduledForDeletion->clear();
            }
            $this->oldIndikatorKinerjasScheduledForDeletion[]= $oldIndikatorKinerja;
            $oldIndikatorKinerja->setOldMasterSkpd(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this OldMasterSkpd is new, it will return
     * an empty collection; or if this OldMasterSkpd has previously
     * been saved, it will retrieve related OldIndikatorKinerjas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in OldMasterSkpd.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOldIndikatorKinerja[] List of ChildOldIndikatorKinerja objects
     */
    public function getOldIndikatorKinerjasJoinOldPegawai(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOldIndikatorKinerjaQuery::create(null, $criteria);
        $query->joinWith('OldPegawai', $joinBehavior);

        return $this->getOldIndikatorKinerjas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this OldMasterSkpd is new, it will return
     * an empty collection; or if this OldMasterSkpd has previously
     * been saved, it will retrieve related OldIndikatorKinerjas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in OldMasterSkpd.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOldIndikatorKinerja[] List of ChildOldIndikatorKinerja objects
     */
    public function getOldIndikatorKinerjasJoinOldIndikatorKinerja(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOldIndikatorKinerjaQuery::create(null, $criteria);
        $query->joinWith('OldIndikatorKinerja', $joinBehavior);

        return $this->getOldIndikatorKinerjas($query, $con);
    }

    /**
     * Clears out the collOldJabatanStrukturals collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addOldJabatanStrukturals()
     */
    public function clearOldJabatanStrukturals()
    {
        $this->collOldJabatanStrukturals = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collOldJabatanStrukturals collection loaded partially.
     */
    public function resetPartialOldJabatanStrukturals($v = true)
    {
        $this->collOldJabatanStrukturalsPartial = $v;
    }

    /**
     * Initializes the collOldJabatanStrukturals collection.
     *
     * By default this just sets the collOldJabatanStrukturals collection to an empty array (like clearcollOldJabatanStrukturals());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initOldJabatanStrukturals($overrideExisting = true)
    {
        if (null !== $this->collOldJabatanStrukturals && !$overrideExisting) {
            return;
        }

        $collectionClassName = OldJabatanStrukturalTableMap::getTableMap()->getCollectionClassName();

        $this->collOldJabatanStrukturals = new $collectionClassName;
        $this->collOldJabatanStrukturals->setModel('\CoreBundle\Model\OldEperformance\OldJabatanStruktural');
    }

    /**
     * Gets an array of ChildOldJabatanStruktural objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildOldMasterSkpd is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildOldJabatanStruktural[] List of ChildOldJabatanStruktural objects
     * @throws PropelException
     */
    public function getOldJabatanStrukturals(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collOldJabatanStrukturalsPartial && !$this->isNew();
        if (null === $this->collOldJabatanStrukturals || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collOldJabatanStrukturals) {
                // return empty collection
                $this->initOldJabatanStrukturals();
            } else {
                $collOldJabatanStrukturals = ChildOldJabatanStrukturalQuery::create(null, $criteria)
                    ->filterByOldMasterSkpd($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collOldJabatanStrukturalsPartial && count($collOldJabatanStrukturals)) {
                        $this->initOldJabatanStrukturals(false);

                        foreach ($collOldJabatanStrukturals as $obj) {
                            if (false == $this->collOldJabatanStrukturals->contains($obj)) {
                                $this->collOldJabatanStrukturals->append($obj);
                            }
                        }

                        $this->collOldJabatanStrukturalsPartial = true;
                    }

                    return $collOldJabatanStrukturals;
                }

                if ($partial && $this->collOldJabatanStrukturals) {
                    foreach ($this->collOldJabatanStrukturals as $obj) {
                        if ($obj->isNew()) {
                            $collOldJabatanStrukturals[] = $obj;
                        }
                    }
                }

                $this->collOldJabatanStrukturals = $collOldJabatanStrukturals;
                $this->collOldJabatanStrukturalsPartial = false;
            }
        }

        return $this->collOldJabatanStrukturals;
    }

    /**
     * Sets a collection of ChildOldJabatanStruktural objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $oldJabatanStrukturals A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildOldMasterSkpd The current object (for fluent API support)
     */
    public function setOldJabatanStrukturals(Collection $oldJabatanStrukturals, ConnectionInterface $con = null)
    {
        /** @var ChildOldJabatanStruktural[] $oldJabatanStrukturalsToDelete */
        $oldJabatanStrukturalsToDelete = $this->getOldJabatanStrukturals(new Criteria(), $con)->diff($oldJabatanStrukturals);


        $this->oldJabatanStrukturalsScheduledForDeletion = $oldJabatanStrukturalsToDelete;

        foreach ($oldJabatanStrukturalsToDelete as $oldJabatanStrukturalRemoved) {
            $oldJabatanStrukturalRemoved->setOldMasterSkpd(null);
        }

        $this->collOldJabatanStrukturals = null;
        foreach ($oldJabatanStrukturals as $oldJabatanStruktural) {
            $this->addOldJabatanStruktural($oldJabatanStruktural);
        }

        $this->collOldJabatanStrukturals = $oldJabatanStrukturals;
        $this->collOldJabatanStrukturalsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related OldJabatanStruktural objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related OldJabatanStruktural objects.
     * @throws PropelException
     */
    public function countOldJabatanStrukturals(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collOldJabatanStrukturalsPartial && !$this->isNew();
        if (null === $this->collOldJabatanStrukturals || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collOldJabatanStrukturals) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getOldJabatanStrukturals());
            }

            $query = ChildOldJabatanStrukturalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByOldMasterSkpd($this)
                ->count($con);
        }

        return count($this->collOldJabatanStrukturals);
    }

    /**
     * Method called to associate a ChildOldJabatanStruktural object to this object
     * through the ChildOldJabatanStruktural foreign key attribute.
     *
     * @param  ChildOldJabatanStruktural $l ChildOldJabatanStruktural
     * @return $this|\CoreBundle\Model\OldEperformance\OldMasterSkpd The current object (for fluent API support)
     */
    public function addOldJabatanStruktural(ChildOldJabatanStruktural $l)
    {
        if ($this->collOldJabatanStrukturals === null) {
            $this->initOldJabatanStrukturals();
            $this->collOldJabatanStrukturalsPartial = true;
        }

        if (!$this->collOldJabatanStrukturals->contains($l)) {
            $this->doAddOldJabatanStruktural($l);

            if ($this->oldJabatanStrukturalsScheduledForDeletion and $this->oldJabatanStrukturalsScheduledForDeletion->contains($l)) {
                $this->oldJabatanStrukturalsScheduledForDeletion->remove($this->oldJabatanStrukturalsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildOldJabatanStruktural $oldJabatanStruktural The ChildOldJabatanStruktural object to add.
     */
    protected function doAddOldJabatanStruktural(ChildOldJabatanStruktural $oldJabatanStruktural)
    {
        $this->collOldJabatanStrukturals[]= $oldJabatanStruktural;
        $oldJabatanStruktural->setOldMasterSkpd($this);
    }

    /**
     * @param  ChildOldJabatanStruktural $oldJabatanStruktural The ChildOldJabatanStruktural object to remove.
     * @return $this|ChildOldMasterSkpd The current object (for fluent API support)
     */
    public function removeOldJabatanStruktural(ChildOldJabatanStruktural $oldJabatanStruktural)
    {
        if ($this->getOldJabatanStrukturals()->contains($oldJabatanStruktural)) {
            $pos = $this->collOldJabatanStrukturals->search($oldJabatanStruktural);
            $this->collOldJabatanStrukturals->remove($pos);
            if (null === $this->oldJabatanStrukturalsScheduledForDeletion) {
                $this->oldJabatanStrukturalsScheduledForDeletion = clone $this->collOldJabatanStrukturals;
                $this->oldJabatanStrukturalsScheduledForDeletion->clear();
            }
            $this->oldJabatanStrukturalsScheduledForDeletion[]= $oldJabatanStruktural;
            $oldJabatanStruktural->setOldMasterSkpd(null);
        }

        return $this;
    }

    /**
     * Clears out the collOldMonevTsps collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addOldMonevTsps()
     */
    public function clearOldMonevTsps()
    {
        $this->collOldMonevTsps = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collOldMonevTsps collection loaded partially.
     */
    public function resetPartialOldMonevTsps($v = true)
    {
        $this->collOldMonevTspsPartial = $v;
    }

    /**
     * Initializes the collOldMonevTsps collection.
     *
     * By default this just sets the collOldMonevTsps collection to an empty array (like clearcollOldMonevTsps());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initOldMonevTsps($overrideExisting = true)
    {
        if (null !== $this->collOldMonevTsps && !$overrideExisting) {
            return;
        }

        $collectionClassName = OldMonevTspTableMap::getTableMap()->getCollectionClassName();

        $this->collOldMonevTsps = new $collectionClassName;
        $this->collOldMonevTsps->setModel('\CoreBundle\Model\OldEperformance\OldMonevTsp');
    }

    /**
     * Gets an array of ChildOldMonevTsp objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildOldMasterSkpd is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildOldMonevTsp[] List of ChildOldMonevTsp objects
     * @throws PropelException
     */
    public function getOldMonevTsps(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collOldMonevTspsPartial && !$this->isNew();
        if (null === $this->collOldMonevTsps || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collOldMonevTsps) {
                // return empty collection
                $this->initOldMonevTsps();
            } else {
                $collOldMonevTsps = ChildOldMonevTspQuery::create(null, $criteria)
                    ->filterByOldMasterSkpd($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collOldMonevTspsPartial && count($collOldMonevTsps)) {
                        $this->initOldMonevTsps(false);

                        foreach ($collOldMonevTsps as $obj) {
                            if (false == $this->collOldMonevTsps->contains($obj)) {
                                $this->collOldMonevTsps->append($obj);
                            }
                        }

                        $this->collOldMonevTspsPartial = true;
                    }

                    return $collOldMonevTsps;
                }

                if ($partial && $this->collOldMonevTsps) {
                    foreach ($this->collOldMonevTsps as $obj) {
                        if ($obj->isNew()) {
                            $collOldMonevTsps[] = $obj;
                        }
                    }
                }

                $this->collOldMonevTsps = $collOldMonevTsps;
                $this->collOldMonevTspsPartial = false;
            }
        }

        return $this->collOldMonevTsps;
    }

    /**
     * Sets a collection of ChildOldMonevTsp objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $oldMonevTsps A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildOldMasterSkpd The current object (for fluent API support)
     */
    public function setOldMonevTsps(Collection $oldMonevTsps, ConnectionInterface $con = null)
    {
        /** @var ChildOldMonevTsp[] $oldMonevTspsToDelete */
        $oldMonevTspsToDelete = $this->getOldMonevTsps(new Criteria(), $con)->diff($oldMonevTsps);


        $this->oldMonevTspsScheduledForDeletion = $oldMonevTspsToDelete;

        foreach ($oldMonevTspsToDelete as $oldMonevTspRemoved) {
            $oldMonevTspRemoved->setOldMasterSkpd(null);
        }

        $this->collOldMonevTsps = null;
        foreach ($oldMonevTsps as $oldMonevTsp) {
            $this->addOldMonevTsp($oldMonevTsp);
        }

        $this->collOldMonevTsps = $oldMonevTsps;
        $this->collOldMonevTspsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related OldMonevTsp objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related OldMonevTsp objects.
     * @throws PropelException
     */
    public function countOldMonevTsps(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collOldMonevTspsPartial && !$this->isNew();
        if (null === $this->collOldMonevTsps || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collOldMonevTsps) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getOldMonevTsps());
            }

            $query = ChildOldMonevTspQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByOldMasterSkpd($this)
                ->count($con);
        }

        return count($this->collOldMonevTsps);
    }

    /**
     * Method called to associate a ChildOldMonevTsp object to this object
     * through the ChildOldMonevTsp foreign key attribute.
     *
     * @param  ChildOldMonevTsp $l ChildOldMonevTsp
     * @return $this|\CoreBundle\Model\OldEperformance\OldMasterSkpd The current object (for fluent API support)
     */
    public function addOldMonevTsp(ChildOldMonevTsp $l)
    {
        if ($this->collOldMonevTsps === null) {
            $this->initOldMonevTsps();
            $this->collOldMonevTspsPartial = true;
        }

        if (!$this->collOldMonevTsps->contains($l)) {
            $this->doAddOldMonevTsp($l);

            if ($this->oldMonevTspsScheduledForDeletion and $this->oldMonevTspsScheduledForDeletion->contains($l)) {
                $this->oldMonevTspsScheduledForDeletion->remove($this->oldMonevTspsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildOldMonevTsp $oldMonevTsp The ChildOldMonevTsp object to add.
     */
    protected function doAddOldMonevTsp(ChildOldMonevTsp $oldMonevTsp)
    {
        $this->collOldMonevTsps[]= $oldMonevTsp;
        $oldMonevTsp->setOldMasterSkpd($this);
    }

    /**
     * @param  ChildOldMonevTsp $oldMonevTsp The ChildOldMonevTsp object to remove.
     * @return $this|ChildOldMasterSkpd The current object (for fluent API support)
     */
    public function removeOldMonevTsp(ChildOldMonevTsp $oldMonevTsp)
    {
        if ($this->getOldMonevTsps()->contains($oldMonevTsp)) {
            $pos = $this->collOldMonevTsps->search($oldMonevTsp);
            $this->collOldMonevTsps->remove($pos);
            if (null === $this->oldMonevTspsScheduledForDeletion) {
                $this->oldMonevTspsScheduledForDeletion = clone $this->collOldMonevTsps;
                $this->oldMonevTspsScheduledForDeletion->clear();
            }
            $this->oldMonevTspsScheduledForDeletion[]= clone $oldMonevTsp;
            $oldMonevTsp->setOldMasterSkpd(null);
        }

        return $this;
    }

    /**
     * Clears out the collOldMonevTspKegiatans collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addOldMonevTspKegiatans()
     */
    public function clearOldMonevTspKegiatans()
    {
        $this->collOldMonevTspKegiatans = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collOldMonevTspKegiatans collection loaded partially.
     */
    public function resetPartialOldMonevTspKegiatans($v = true)
    {
        $this->collOldMonevTspKegiatansPartial = $v;
    }

    /**
     * Initializes the collOldMonevTspKegiatans collection.
     *
     * By default this just sets the collOldMonevTspKegiatans collection to an empty array (like clearcollOldMonevTspKegiatans());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initOldMonevTspKegiatans($overrideExisting = true)
    {
        if (null !== $this->collOldMonevTspKegiatans && !$overrideExisting) {
            return;
        }

        $collectionClassName = OldMonevTspKegiatanTableMap::getTableMap()->getCollectionClassName();

        $this->collOldMonevTspKegiatans = new $collectionClassName;
        $this->collOldMonevTspKegiatans->setModel('\CoreBundle\Model\OldEperformance\OldMonevTspKegiatan');
    }

    /**
     * Gets an array of ChildOldMonevTspKegiatan objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildOldMasterSkpd is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildOldMonevTspKegiatan[] List of ChildOldMonevTspKegiatan objects
     * @throws PropelException
     */
    public function getOldMonevTspKegiatans(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collOldMonevTspKegiatansPartial && !$this->isNew();
        if (null === $this->collOldMonevTspKegiatans || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collOldMonevTspKegiatans) {
                // return empty collection
                $this->initOldMonevTspKegiatans();
            } else {
                $collOldMonevTspKegiatans = ChildOldMonevTspKegiatanQuery::create(null, $criteria)
                    ->filterByOldMasterSkpd($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collOldMonevTspKegiatansPartial && count($collOldMonevTspKegiatans)) {
                        $this->initOldMonevTspKegiatans(false);

                        foreach ($collOldMonevTspKegiatans as $obj) {
                            if (false == $this->collOldMonevTspKegiatans->contains($obj)) {
                                $this->collOldMonevTspKegiatans->append($obj);
                            }
                        }

                        $this->collOldMonevTspKegiatansPartial = true;
                    }

                    return $collOldMonevTspKegiatans;
                }

                if ($partial && $this->collOldMonevTspKegiatans) {
                    foreach ($this->collOldMonevTspKegiatans as $obj) {
                        if ($obj->isNew()) {
                            $collOldMonevTspKegiatans[] = $obj;
                        }
                    }
                }

                $this->collOldMonevTspKegiatans = $collOldMonevTspKegiatans;
                $this->collOldMonevTspKegiatansPartial = false;
            }
        }

        return $this->collOldMonevTspKegiatans;
    }

    /**
     * Sets a collection of ChildOldMonevTspKegiatan objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $oldMonevTspKegiatans A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildOldMasterSkpd The current object (for fluent API support)
     */
    public function setOldMonevTspKegiatans(Collection $oldMonevTspKegiatans, ConnectionInterface $con = null)
    {
        /** @var ChildOldMonevTspKegiatan[] $oldMonevTspKegiatansToDelete */
        $oldMonevTspKegiatansToDelete = $this->getOldMonevTspKegiatans(new Criteria(), $con)->diff($oldMonevTspKegiatans);


        $this->oldMonevTspKegiatansScheduledForDeletion = $oldMonevTspKegiatansToDelete;

        foreach ($oldMonevTspKegiatansToDelete as $oldMonevTspKegiatanRemoved) {
            $oldMonevTspKegiatanRemoved->setOldMasterSkpd(null);
        }

        $this->collOldMonevTspKegiatans = null;
        foreach ($oldMonevTspKegiatans as $oldMonevTspKegiatan) {
            $this->addOldMonevTspKegiatan($oldMonevTspKegiatan);
        }

        $this->collOldMonevTspKegiatans = $oldMonevTspKegiatans;
        $this->collOldMonevTspKegiatansPartial = false;

        return $this;
    }

    /**
     * Returns the number of related OldMonevTspKegiatan objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related OldMonevTspKegiatan objects.
     * @throws PropelException
     */
    public function countOldMonevTspKegiatans(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collOldMonevTspKegiatansPartial && !$this->isNew();
        if (null === $this->collOldMonevTspKegiatans || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collOldMonevTspKegiatans) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getOldMonevTspKegiatans());
            }

            $query = ChildOldMonevTspKegiatanQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByOldMasterSkpd($this)
                ->count($con);
        }

        return count($this->collOldMonevTspKegiatans);
    }

    /**
     * Method called to associate a ChildOldMonevTspKegiatan object to this object
     * through the ChildOldMonevTspKegiatan foreign key attribute.
     *
     * @param  ChildOldMonevTspKegiatan $l ChildOldMonevTspKegiatan
     * @return $this|\CoreBundle\Model\OldEperformance\OldMasterSkpd The current object (for fluent API support)
     */
    public function addOldMonevTspKegiatan(ChildOldMonevTspKegiatan $l)
    {
        if ($this->collOldMonevTspKegiatans === null) {
            $this->initOldMonevTspKegiatans();
            $this->collOldMonevTspKegiatansPartial = true;
        }

        if (!$this->collOldMonevTspKegiatans->contains($l)) {
            $this->doAddOldMonevTspKegiatan($l);

            if ($this->oldMonevTspKegiatansScheduledForDeletion and $this->oldMonevTspKegiatansScheduledForDeletion->contains($l)) {
                $this->oldMonevTspKegiatansScheduledForDeletion->remove($this->oldMonevTspKegiatansScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildOldMonevTspKegiatan $oldMonevTspKegiatan The ChildOldMonevTspKegiatan object to add.
     */
    protected function doAddOldMonevTspKegiatan(ChildOldMonevTspKegiatan $oldMonevTspKegiatan)
    {
        $this->collOldMonevTspKegiatans[]= $oldMonevTspKegiatan;
        $oldMonevTspKegiatan->setOldMasterSkpd($this);
    }

    /**
     * @param  ChildOldMonevTspKegiatan $oldMonevTspKegiatan The ChildOldMonevTspKegiatan object to remove.
     * @return $this|ChildOldMasterSkpd The current object (for fluent API support)
     */
    public function removeOldMonevTspKegiatan(ChildOldMonevTspKegiatan $oldMonevTspKegiatan)
    {
        if ($this->getOldMonevTspKegiatans()->contains($oldMonevTspKegiatan)) {
            $pos = $this->collOldMonevTspKegiatans->search($oldMonevTspKegiatan);
            $this->collOldMonevTspKegiatans->remove($pos);
            if (null === $this->oldMonevTspKegiatansScheduledForDeletion) {
                $this->oldMonevTspKegiatansScheduledForDeletion = clone $this->collOldMonevTspKegiatans;
                $this->oldMonevTspKegiatansScheduledForDeletion->clear();
            }
            $this->oldMonevTspKegiatansScheduledForDeletion[]= clone $oldMonevTspKegiatan;
            $oldMonevTspKegiatan->setOldMasterSkpd(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->skpd_id = null;
        $this->skpd_kode = null;
        $this->skpd_nama = null;
        $this->deleted_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collOldPerilakuTess) {
                foreach ($this->collOldPerilakuTess as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collOldPegawais) {
                foreach ($this->collOldPegawais as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collOldIndikatorKinerjas) {
                foreach ($this->collOldIndikatorKinerjas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collOldJabatanStrukturals) {
                foreach ($this->collOldJabatanStrukturals as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collOldMonevTsps) {
                foreach ($this->collOldMonevTsps as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collOldMonevTspKegiatans) {
                foreach ($this->collOldMonevTspKegiatans as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collOldPerilakuTess = null;
        $this->collOldPegawais = null;
        $this->collOldIndikatorKinerjas = null;
        $this->collOldJabatanStrukturals = null;
        $this->collOldMonevTsps = null;
        $this->collOldMonevTspKegiatans = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(OldMasterSkpdTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}

<?php

namespace CoreBundle\Model\OldEperformance\Base;

use \Exception;
use \PDO;
use CoreBundle\Model\OldEperformance\OldJabatanStruktural as ChildOldJabatanStruktural;
use CoreBundle\Model\OldEperformance\OldJabatanStrukturalQuery as ChildOldJabatanStrukturalQuery;
use CoreBundle\Model\OldEperformance\OldMasterSkpd as ChildOldMasterSkpd;
use CoreBundle\Model\OldEperformance\OldMasterSkpdQuery as ChildOldMasterSkpdQuery;
use CoreBundle\Model\OldEperformance\OldPegawai as ChildOldPegawai;
use CoreBundle\Model\OldEperformance\OldPegawaiQuery as ChildOldPegawaiQuery;
use CoreBundle\Model\OldEperformance\Map\OldJabatanStrukturalTableMap;
use CoreBundle\Model\OldEperformance\Map\OldPegawaiTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'eperformance.jabatan_struktural' table.
 *
 *
 *
 * @package    propel.generator.src.CoreBundle.Model.OldEperformance.Base
 */
abstract class OldJabatanStruktural implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\CoreBundle\\Model\\OldEperformance\\Map\\OldJabatanStrukturalTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the nama field.
     *
     * @var        string
     */
    protected $nama;

    /**
     * The value for the eselon field.
     *
     * @var        string
     */
    protected $eselon;

    /**
     * The value for the jenis_indikator_kinerja field.
     *
     * @var        int
     */
    protected $jenis_indikator_kinerja;

    /**
     * The value for the skpd_id field.
     *
     * @var        int
     */
    protected $skpd_id;

    /**
     * The value for the nomer field.
     *
     * @var        int
     */
    protected $nomer;

    /**
     * The value for the is_asisten field.
     *
     * @var        boolean
     */
    protected $is_asisten;

    /**
     * The value for the nomer_urut field.
     *
     * @var        int
     */
    protected $nomer_urut;

    /**
     * @var        ChildOldMasterSkpd
     */
    protected $aOldMasterSkpd;

    /**
     * @var        ObjectCollection|ChildOldPegawai[] Collection to store aggregation of ChildOldPegawai objects.
     */
    protected $collOldPegawais;
    protected $collOldPegawaisPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildOldPegawai[]
     */
    protected $oldPegawaisScheduledForDeletion = null;

    /**
     * Initializes internal state of CoreBundle\Model\OldEperformance\Base\OldJabatanStruktural object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>OldJabatanStruktural</code> instance.  If
     * <code>obj</code> is an instance of <code>OldJabatanStruktural</code>, delegates to
     * <code>equals(OldJabatanStruktural)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|OldJabatanStruktural The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [nama] column value.
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [eselon] column value.
     *
     * @return string
     */
    public function getEselon()
    {
        return $this->eselon;
    }

    /**
     * Get the [jenis_indikator_kinerja] column value.
     *
     * @return int
     */
    public function getJenisIndikatorKinerja()
    {
        return $this->jenis_indikator_kinerja;
    }

    /**
     * Get the [skpd_id] column value.
     *
     * @return int
     */
    public function getSkpdId()
    {
        return $this->skpd_id;
    }

    /**
     * Get the [nomer] column value.
     *
     * @return int
     */
    public function getNomer()
    {
        return $this->nomer;
    }

    /**
     * Get the [is_asisten] column value.
     *
     * @return boolean
     */
    public function getIsAsisten()
    {
        return $this->is_asisten;
    }

    /**
     * Get the [is_asisten] column value.
     *
     * @return boolean
     */
    public function isAsisten()
    {
        return $this->getIsAsisten();
    }

    /**
     * Get the [nomer_urut] column value.
     *
     * @return int
     */
    public function getNomerUrut()
    {
        return $this->nomer_urut;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldJabatanStruktural The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[OldJabatanStrukturalTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [nama] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldJabatanStruktural The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[OldJabatanStrukturalTableMap::COL_NAMA] = true;
        }

        return $this;
    } // setNama()

    /**
     * Set the value of [eselon] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldJabatanStruktural The current object (for fluent API support)
     */
    public function setEselon($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->eselon !== $v) {
            $this->eselon = $v;
            $this->modifiedColumns[OldJabatanStrukturalTableMap::COL_ESELON] = true;
        }

        return $this;
    } // setEselon()

    /**
     * Set the value of [jenis_indikator_kinerja] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldJabatanStruktural The current object (for fluent API support)
     */
    public function setJenisIndikatorKinerja($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->jenis_indikator_kinerja !== $v) {
            $this->jenis_indikator_kinerja = $v;
            $this->modifiedColumns[OldJabatanStrukturalTableMap::COL_JENIS_INDIKATOR_KINERJA] = true;
        }

        return $this;
    } // setJenisIndikatorKinerja()

    /**
     * Set the value of [skpd_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldJabatanStruktural The current object (for fluent API support)
     */
    public function setSkpdId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->skpd_id !== $v) {
            $this->skpd_id = $v;
            $this->modifiedColumns[OldJabatanStrukturalTableMap::COL_SKPD_ID] = true;
        }

        if ($this->aOldMasterSkpd !== null && $this->aOldMasterSkpd->getSkpdId() !== $v) {
            $this->aOldMasterSkpd = null;
        }

        return $this;
    } // setSkpdId()

    /**
     * Set the value of [nomer] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldJabatanStruktural The current object (for fluent API support)
     */
    public function setNomer($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->nomer !== $v) {
            $this->nomer = $v;
            $this->modifiedColumns[OldJabatanStrukturalTableMap::COL_NOMER] = true;
        }

        return $this;
    } // setNomer()

    /**
     * Sets the value of the [is_asisten] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldJabatanStruktural The current object (for fluent API support)
     */
    public function setIsAsisten($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_asisten !== $v) {
            $this->is_asisten = $v;
            $this->modifiedColumns[OldJabatanStrukturalTableMap::COL_IS_ASISTEN] = true;
        }

        return $this;
    } // setIsAsisten()

    /**
     * Set the value of [nomer_urut] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldJabatanStruktural The current object (for fluent API support)
     */
    public function setNomerUrut($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->nomer_urut !== $v) {
            $this->nomer_urut = $v;
            $this->modifiedColumns[OldJabatanStrukturalTableMap::COL_NOMER_URUT] = true;
        }

        return $this;
    } // setNomerUrut()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : OldJabatanStrukturalTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : OldJabatanStrukturalTableMap::translateFieldName('Nama', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nama = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : OldJabatanStrukturalTableMap::translateFieldName('Eselon', TableMap::TYPE_PHPNAME, $indexType)];
            $this->eselon = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : OldJabatanStrukturalTableMap::translateFieldName('JenisIndikatorKinerja', TableMap::TYPE_PHPNAME, $indexType)];
            $this->jenis_indikator_kinerja = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : OldJabatanStrukturalTableMap::translateFieldName('SkpdId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->skpd_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : OldJabatanStrukturalTableMap::translateFieldName('Nomer', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nomer = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : OldJabatanStrukturalTableMap::translateFieldName('IsAsisten', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_asisten = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : OldJabatanStrukturalTableMap::translateFieldName('NomerUrut', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nomer_urut = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 8; // 8 = OldJabatanStrukturalTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\CoreBundle\\Model\\OldEperformance\\OldJabatanStruktural'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aOldMasterSkpd !== null && $this->skpd_id !== $this->aOldMasterSkpd->getSkpdId()) {
            $this->aOldMasterSkpd = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(OldJabatanStrukturalTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildOldJabatanStrukturalQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aOldMasterSkpd = null;
            $this->collOldPegawais = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see OldJabatanStruktural::setDeleted()
     * @see OldJabatanStruktural::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldJabatanStrukturalTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildOldJabatanStrukturalQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldJabatanStrukturalTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                OldJabatanStrukturalTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aOldMasterSkpd !== null) {
                if ($this->aOldMasterSkpd->isModified() || $this->aOldMasterSkpd->isNew()) {
                    $affectedRows += $this->aOldMasterSkpd->save($con);
                }
                $this->setOldMasterSkpd($this->aOldMasterSkpd);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->oldPegawaisScheduledForDeletion !== null) {
                if (!$this->oldPegawaisScheduledForDeletion->isEmpty()) {
                    foreach ($this->oldPegawaisScheduledForDeletion as $oldPegawai) {
                        // need to save related object because we set the relation to null
                        $oldPegawai->save($con);
                    }
                    $this->oldPegawaisScheduledForDeletion = null;
                }
            }

            if ($this->collOldPegawais !== null) {
                foreach ($this->collOldPegawais as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(OldJabatanStrukturalTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(OldJabatanStrukturalTableMap::COL_NAMA)) {
            $modifiedColumns[':p' . $index++]  = 'nama';
        }
        if ($this->isColumnModified(OldJabatanStrukturalTableMap::COL_ESELON)) {
            $modifiedColumns[':p' . $index++]  = 'eselon';
        }
        if ($this->isColumnModified(OldJabatanStrukturalTableMap::COL_JENIS_INDIKATOR_KINERJA)) {
            $modifiedColumns[':p' . $index++]  = 'jenis_indikator_kinerja';
        }
        if ($this->isColumnModified(OldJabatanStrukturalTableMap::COL_SKPD_ID)) {
            $modifiedColumns[':p' . $index++]  = 'skpd_id';
        }
        if ($this->isColumnModified(OldJabatanStrukturalTableMap::COL_NOMER)) {
            $modifiedColumns[':p' . $index++]  = 'nomer';
        }
        if ($this->isColumnModified(OldJabatanStrukturalTableMap::COL_IS_ASISTEN)) {
            $modifiedColumns[':p' . $index++]  = 'is_asisten';
        }
        if ($this->isColumnModified(OldJabatanStrukturalTableMap::COL_NOMER_URUT)) {
            $modifiedColumns[':p' . $index++]  = 'nomer_urut';
        }

        $sql = sprintf(
            'INSERT INTO eperformance.jabatan_struktural (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'nama':
                        $stmt->bindValue($identifier, $this->nama, PDO::PARAM_STR);
                        break;
                    case 'eselon':
                        $stmt->bindValue($identifier, $this->eselon, PDO::PARAM_STR);
                        break;
                    case 'jenis_indikator_kinerja':
                        $stmt->bindValue($identifier, $this->jenis_indikator_kinerja, PDO::PARAM_INT);
                        break;
                    case 'skpd_id':
                        $stmt->bindValue($identifier, $this->skpd_id, PDO::PARAM_INT);
                        break;
                    case 'nomer':
                        $stmt->bindValue($identifier, $this->nomer, PDO::PARAM_INT);
                        break;
                    case 'is_asisten':
                        $stmt->bindValue($identifier, $this->is_asisten, PDO::PARAM_BOOL);
                        break;
                    case 'nomer_urut':
                        $stmt->bindValue($identifier, $this->nomer_urut, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = OldJabatanStrukturalTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getNama();
                break;
            case 2:
                return $this->getEselon();
                break;
            case 3:
                return $this->getJenisIndikatorKinerja();
                break;
            case 4:
                return $this->getSkpdId();
                break;
            case 5:
                return $this->getNomer();
                break;
            case 6:
                return $this->getIsAsisten();
                break;
            case 7:
                return $this->getNomerUrut();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['OldJabatanStruktural'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['OldJabatanStruktural'][$this->hashCode()] = true;
        $keys = OldJabatanStrukturalTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getNama(),
            $keys[2] => $this->getEselon(),
            $keys[3] => $this->getJenisIndikatorKinerja(),
            $keys[4] => $this->getSkpdId(),
            $keys[5] => $this->getNomer(),
            $keys[6] => $this->getIsAsisten(),
            $keys[7] => $this->getNomerUrut(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aOldMasterSkpd) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldMasterSkpd';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.master_skpd';
                        break;
                    default:
                        $key = 'OldMasterSkpd';
                }

                $result[$key] = $this->aOldMasterSkpd->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collOldPegawais) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldPegawais';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.pegawais';
                        break;
                    default:
                        $key = 'OldPegawais';
                }

                $result[$key] = $this->collOldPegawais->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\CoreBundle\Model\OldEperformance\OldJabatanStruktural
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = OldJabatanStrukturalTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\CoreBundle\Model\OldEperformance\OldJabatanStruktural
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setNama($value);
                break;
            case 2:
                $this->setEselon($value);
                break;
            case 3:
                $this->setJenisIndikatorKinerja($value);
                break;
            case 4:
                $this->setSkpdId($value);
                break;
            case 5:
                $this->setNomer($value);
                break;
            case 6:
                $this->setIsAsisten($value);
                break;
            case 7:
                $this->setNomerUrut($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = OldJabatanStrukturalTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setNama($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setEselon($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setJenisIndikatorKinerja($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setSkpdId($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setNomer($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setIsAsisten($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setNomerUrut($arr[$keys[7]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\CoreBundle\Model\OldEperformance\OldJabatanStruktural The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(OldJabatanStrukturalTableMap::DATABASE_NAME);

        if ($this->isColumnModified(OldJabatanStrukturalTableMap::COL_ID)) {
            $criteria->add(OldJabatanStrukturalTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(OldJabatanStrukturalTableMap::COL_NAMA)) {
            $criteria->add(OldJabatanStrukturalTableMap::COL_NAMA, $this->nama);
        }
        if ($this->isColumnModified(OldJabatanStrukturalTableMap::COL_ESELON)) {
            $criteria->add(OldJabatanStrukturalTableMap::COL_ESELON, $this->eselon);
        }
        if ($this->isColumnModified(OldJabatanStrukturalTableMap::COL_JENIS_INDIKATOR_KINERJA)) {
            $criteria->add(OldJabatanStrukturalTableMap::COL_JENIS_INDIKATOR_KINERJA, $this->jenis_indikator_kinerja);
        }
        if ($this->isColumnModified(OldJabatanStrukturalTableMap::COL_SKPD_ID)) {
            $criteria->add(OldJabatanStrukturalTableMap::COL_SKPD_ID, $this->skpd_id);
        }
        if ($this->isColumnModified(OldJabatanStrukturalTableMap::COL_NOMER)) {
            $criteria->add(OldJabatanStrukturalTableMap::COL_NOMER, $this->nomer);
        }
        if ($this->isColumnModified(OldJabatanStrukturalTableMap::COL_IS_ASISTEN)) {
            $criteria->add(OldJabatanStrukturalTableMap::COL_IS_ASISTEN, $this->is_asisten);
        }
        if ($this->isColumnModified(OldJabatanStrukturalTableMap::COL_NOMER_URUT)) {
            $criteria->add(OldJabatanStrukturalTableMap::COL_NOMER_URUT, $this->nomer_urut);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildOldJabatanStrukturalQuery::create();
        $criteria->add(OldJabatanStrukturalTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \CoreBundle\Model\OldEperformance\OldJabatanStruktural (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setNama($this->getNama());
        $copyObj->setEselon($this->getEselon());
        $copyObj->setJenisIndikatorKinerja($this->getJenisIndikatorKinerja());
        $copyObj->setSkpdId($this->getSkpdId());
        $copyObj->setNomer($this->getNomer());
        $copyObj->setIsAsisten($this->getIsAsisten());
        $copyObj->setNomerUrut($this->getNomerUrut());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getOldPegawais() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addOldPegawai($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \CoreBundle\Model\OldEperformance\OldJabatanStruktural Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildOldMasterSkpd object.
     *
     * @param  ChildOldMasterSkpd $v
     * @return $this|\CoreBundle\Model\OldEperformance\OldJabatanStruktural The current object (for fluent API support)
     * @throws PropelException
     */
    public function setOldMasterSkpd(ChildOldMasterSkpd $v = null)
    {
        if ($v === null) {
            $this->setSkpdId(NULL);
        } else {
            $this->setSkpdId($v->getSkpdId());
        }

        $this->aOldMasterSkpd = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildOldMasterSkpd object, it will not be re-added.
        if ($v !== null) {
            $v->addOldJabatanStruktural($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildOldMasterSkpd object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildOldMasterSkpd The associated ChildOldMasterSkpd object.
     * @throws PropelException
     */
    public function getOldMasterSkpd(ConnectionInterface $con = null)
    {
        if ($this->aOldMasterSkpd === null && ($this->skpd_id !== null)) {
            $this->aOldMasterSkpd = ChildOldMasterSkpdQuery::create()->findPk($this->skpd_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aOldMasterSkpd->addOldJabatanStrukturals($this);
             */
        }

        return $this->aOldMasterSkpd;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('OldPegawai' == $relationName) {
            return $this->initOldPegawais();
        }
    }

    /**
     * Clears out the collOldPegawais collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addOldPegawais()
     */
    public function clearOldPegawais()
    {
        $this->collOldPegawais = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collOldPegawais collection loaded partially.
     */
    public function resetPartialOldPegawais($v = true)
    {
        $this->collOldPegawaisPartial = $v;
    }

    /**
     * Initializes the collOldPegawais collection.
     *
     * By default this just sets the collOldPegawais collection to an empty array (like clearcollOldPegawais());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initOldPegawais($overrideExisting = true)
    {
        if (null !== $this->collOldPegawais && !$overrideExisting) {
            return;
        }

        $collectionClassName = OldPegawaiTableMap::getTableMap()->getCollectionClassName();

        $this->collOldPegawais = new $collectionClassName;
        $this->collOldPegawais->setModel('\CoreBundle\Model\OldEperformance\OldPegawai');
    }

    /**
     * Gets an array of ChildOldPegawai objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildOldJabatanStruktural is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildOldPegawai[] List of ChildOldPegawai objects
     * @throws PropelException
     */
    public function getOldPegawais(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collOldPegawaisPartial && !$this->isNew();
        if (null === $this->collOldPegawais || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collOldPegawais) {
                // return empty collection
                $this->initOldPegawais();
            } else {
                $collOldPegawais = ChildOldPegawaiQuery::create(null, $criteria)
                    ->filterByOldJabatanStruktural($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collOldPegawaisPartial && count($collOldPegawais)) {
                        $this->initOldPegawais(false);

                        foreach ($collOldPegawais as $obj) {
                            if (false == $this->collOldPegawais->contains($obj)) {
                                $this->collOldPegawais->append($obj);
                            }
                        }

                        $this->collOldPegawaisPartial = true;
                    }

                    return $collOldPegawais;
                }

                if ($partial && $this->collOldPegawais) {
                    foreach ($this->collOldPegawais as $obj) {
                        if ($obj->isNew()) {
                            $collOldPegawais[] = $obj;
                        }
                    }
                }

                $this->collOldPegawais = $collOldPegawais;
                $this->collOldPegawaisPartial = false;
            }
        }

        return $this->collOldPegawais;
    }

    /**
     * Sets a collection of ChildOldPegawai objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $oldPegawais A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildOldJabatanStruktural The current object (for fluent API support)
     */
    public function setOldPegawais(Collection $oldPegawais, ConnectionInterface $con = null)
    {
        /** @var ChildOldPegawai[] $oldPegawaisToDelete */
        $oldPegawaisToDelete = $this->getOldPegawais(new Criteria(), $con)->diff($oldPegawais);


        $this->oldPegawaisScheduledForDeletion = $oldPegawaisToDelete;

        foreach ($oldPegawaisToDelete as $oldPegawaiRemoved) {
            $oldPegawaiRemoved->setOldJabatanStruktural(null);
        }

        $this->collOldPegawais = null;
        foreach ($oldPegawais as $oldPegawai) {
            $this->addOldPegawai($oldPegawai);
        }

        $this->collOldPegawais = $oldPegawais;
        $this->collOldPegawaisPartial = false;

        return $this;
    }

    /**
     * Returns the number of related OldPegawai objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related OldPegawai objects.
     * @throws PropelException
     */
    public function countOldPegawais(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collOldPegawaisPartial && !$this->isNew();
        if (null === $this->collOldPegawais || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collOldPegawais) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getOldPegawais());
            }

            $query = ChildOldPegawaiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByOldJabatanStruktural($this)
                ->count($con);
        }

        return count($this->collOldPegawais);
    }

    /**
     * Method called to associate a ChildOldPegawai object to this object
     * through the ChildOldPegawai foreign key attribute.
     *
     * @param  ChildOldPegawai $l ChildOldPegawai
     * @return $this|\CoreBundle\Model\OldEperformance\OldJabatanStruktural The current object (for fluent API support)
     */
    public function addOldPegawai(ChildOldPegawai $l)
    {
        if ($this->collOldPegawais === null) {
            $this->initOldPegawais();
            $this->collOldPegawaisPartial = true;
        }

        if (!$this->collOldPegawais->contains($l)) {
            $this->doAddOldPegawai($l);

            if ($this->oldPegawaisScheduledForDeletion and $this->oldPegawaisScheduledForDeletion->contains($l)) {
                $this->oldPegawaisScheduledForDeletion->remove($this->oldPegawaisScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildOldPegawai $oldPegawai The ChildOldPegawai object to add.
     */
    protected function doAddOldPegawai(ChildOldPegawai $oldPegawai)
    {
        $this->collOldPegawais[]= $oldPegawai;
        $oldPegawai->setOldJabatanStruktural($this);
    }

    /**
     * @param  ChildOldPegawai $oldPegawai The ChildOldPegawai object to remove.
     * @return $this|ChildOldJabatanStruktural The current object (for fluent API support)
     */
    public function removeOldPegawai(ChildOldPegawai $oldPegawai)
    {
        if ($this->getOldPegawais()->contains($oldPegawai)) {
            $pos = $this->collOldPegawais->search($oldPegawai);
            $this->collOldPegawais->remove($pos);
            if (null === $this->oldPegawaisScheduledForDeletion) {
                $this->oldPegawaisScheduledForDeletion = clone $this->collOldPegawais;
                $this->oldPegawaisScheduledForDeletion->clear();
            }
            $this->oldPegawaisScheduledForDeletion[]= $oldPegawai;
            $oldPegawai->setOldJabatanStruktural(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this OldJabatanStruktural is new, it will return
     * an empty collection; or if this OldJabatanStruktural has previously
     * been saved, it will retrieve related OldPegawais from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in OldJabatanStruktural.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOldPegawai[] List of ChildOldPegawai objects
     */
    public function getOldPegawaisJoinOldMasterSkpd(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOldPegawaiQuery::create(null, $criteria);
        $query->joinWith('OldMasterSkpd', $joinBehavior);

        return $this->getOldPegawais($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this OldJabatanStruktural is new, it will return
     * an empty collection; or if this OldJabatanStruktural has previously
     * been saved, it will retrieve related OldPegawais from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in OldJabatanStruktural.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOldPegawai[] List of ChildOldPegawai objects
     */
    public function getOldPegawaisJoinOldPegawai(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOldPegawaiQuery::create(null, $criteria);
        $query->joinWith('OldPegawai', $joinBehavior);

        return $this->getOldPegawais($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aOldMasterSkpd) {
            $this->aOldMasterSkpd->removeOldJabatanStruktural($this);
        }
        $this->id = null;
        $this->nama = null;
        $this->eselon = null;
        $this->jenis_indikator_kinerja = null;
        $this->skpd_id = null;
        $this->nomer = null;
        $this->is_asisten = null;
        $this->nomer_urut = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collOldPegawais) {
                foreach ($this->collOldPegawais as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collOldPegawais = null;
        $this->aOldMasterSkpd = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(OldJabatanStrukturalTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}

<?php

namespace CoreBundle\Model\OldEperformance\Base;

use \Exception;
use \PDO;
use CoreBundle\Model\OldEperformance\OldJabatanStruktural as ChildOldJabatanStruktural;
use CoreBundle\Model\OldEperformance\OldJabatanStrukturalQuery as ChildOldJabatanStrukturalQuery;
use CoreBundle\Model\OldEperformance\Map\OldJabatanStrukturalTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'eperformance.jabatan_struktural' table.
 *
 *
 *
 * @method     ChildOldJabatanStrukturalQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildOldJabatanStrukturalQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method     ChildOldJabatanStrukturalQuery orderByEselon($order = Criteria::ASC) Order by the eselon column
 * @method     ChildOldJabatanStrukturalQuery orderByJenisIndikatorKinerja($order = Criteria::ASC) Order by the jenis_indikator_kinerja column
 * @method     ChildOldJabatanStrukturalQuery orderBySkpdId($order = Criteria::ASC) Order by the skpd_id column
 * @method     ChildOldJabatanStrukturalQuery orderByNomer($order = Criteria::ASC) Order by the nomer column
 * @method     ChildOldJabatanStrukturalQuery orderByIsAsisten($order = Criteria::ASC) Order by the is_asisten column
 * @method     ChildOldJabatanStrukturalQuery orderByNomerUrut($order = Criteria::ASC) Order by the nomer_urut column
 *
 * @method     ChildOldJabatanStrukturalQuery groupById() Group by the id column
 * @method     ChildOldJabatanStrukturalQuery groupByNama() Group by the nama column
 * @method     ChildOldJabatanStrukturalQuery groupByEselon() Group by the eselon column
 * @method     ChildOldJabatanStrukturalQuery groupByJenisIndikatorKinerja() Group by the jenis_indikator_kinerja column
 * @method     ChildOldJabatanStrukturalQuery groupBySkpdId() Group by the skpd_id column
 * @method     ChildOldJabatanStrukturalQuery groupByNomer() Group by the nomer column
 * @method     ChildOldJabatanStrukturalQuery groupByIsAsisten() Group by the is_asisten column
 * @method     ChildOldJabatanStrukturalQuery groupByNomerUrut() Group by the nomer_urut column
 *
 * @method     ChildOldJabatanStrukturalQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildOldJabatanStrukturalQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildOldJabatanStrukturalQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildOldJabatanStrukturalQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildOldJabatanStrukturalQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildOldJabatanStrukturalQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildOldJabatanStrukturalQuery leftJoinOldMasterSkpd($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldMasterSkpd relation
 * @method     ChildOldJabatanStrukturalQuery rightJoinOldMasterSkpd($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldMasterSkpd relation
 * @method     ChildOldJabatanStrukturalQuery innerJoinOldMasterSkpd($relationAlias = null) Adds a INNER JOIN clause to the query using the OldMasterSkpd relation
 *
 * @method     ChildOldJabatanStrukturalQuery joinWithOldMasterSkpd($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldMasterSkpd relation
 *
 * @method     ChildOldJabatanStrukturalQuery leftJoinWithOldMasterSkpd() Adds a LEFT JOIN clause and with to the query using the OldMasterSkpd relation
 * @method     ChildOldJabatanStrukturalQuery rightJoinWithOldMasterSkpd() Adds a RIGHT JOIN clause and with to the query using the OldMasterSkpd relation
 * @method     ChildOldJabatanStrukturalQuery innerJoinWithOldMasterSkpd() Adds a INNER JOIN clause and with to the query using the OldMasterSkpd relation
 *
 * @method     ChildOldJabatanStrukturalQuery leftJoinOldPegawai($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldPegawai relation
 * @method     ChildOldJabatanStrukturalQuery rightJoinOldPegawai($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldPegawai relation
 * @method     ChildOldJabatanStrukturalQuery innerJoinOldPegawai($relationAlias = null) Adds a INNER JOIN clause to the query using the OldPegawai relation
 *
 * @method     ChildOldJabatanStrukturalQuery joinWithOldPegawai($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldPegawai relation
 *
 * @method     ChildOldJabatanStrukturalQuery leftJoinWithOldPegawai() Adds a LEFT JOIN clause and with to the query using the OldPegawai relation
 * @method     ChildOldJabatanStrukturalQuery rightJoinWithOldPegawai() Adds a RIGHT JOIN clause and with to the query using the OldPegawai relation
 * @method     ChildOldJabatanStrukturalQuery innerJoinWithOldPegawai() Adds a INNER JOIN clause and with to the query using the OldPegawai relation
 *
 * @method     \CoreBundle\Model\OldEperformance\OldMasterSkpdQuery|\CoreBundle\Model\OldEperformance\OldPegawaiQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildOldJabatanStruktural findOne(ConnectionInterface $con = null) Return the first ChildOldJabatanStruktural matching the query
 * @method     ChildOldJabatanStruktural findOneOrCreate(ConnectionInterface $con = null) Return the first ChildOldJabatanStruktural matching the query, or a new ChildOldJabatanStruktural object populated from the query conditions when no match is found
 *
 * @method     ChildOldJabatanStruktural findOneById(int $id) Return the first ChildOldJabatanStruktural filtered by the id column
 * @method     ChildOldJabatanStruktural findOneByNama(string $nama) Return the first ChildOldJabatanStruktural filtered by the nama column
 * @method     ChildOldJabatanStruktural findOneByEselon(string $eselon) Return the first ChildOldJabatanStruktural filtered by the eselon column
 * @method     ChildOldJabatanStruktural findOneByJenisIndikatorKinerja(int $jenis_indikator_kinerja) Return the first ChildOldJabatanStruktural filtered by the jenis_indikator_kinerja column
 * @method     ChildOldJabatanStruktural findOneBySkpdId(int $skpd_id) Return the first ChildOldJabatanStruktural filtered by the skpd_id column
 * @method     ChildOldJabatanStruktural findOneByNomer(int $nomer) Return the first ChildOldJabatanStruktural filtered by the nomer column
 * @method     ChildOldJabatanStruktural findOneByIsAsisten(boolean $is_asisten) Return the first ChildOldJabatanStruktural filtered by the is_asisten column
 * @method     ChildOldJabatanStruktural findOneByNomerUrut(int $nomer_urut) Return the first ChildOldJabatanStruktural filtered by the nomer_urut column *

 * @method     ChildOldJabatanStruktural requirePk($key, ConnectionInterface $con = null) Return the ChildOldJabatanStruktural by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldJabatanStruktural requireOne(ConnectionInterface $con = null) Return the first ChildOldJabatanStruktural matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOldJabatanStruktural requireOneById(int $id) Return the first ChildOldJabatanStruktural filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldJabatanStruktural requireOneByNama(string $nama) Return the first ChildOldJabatanStruktural filtered by the nama column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldJabatanStruktural requireOneByEselon(string $eselon) Return the first ChildOldJabatanStruktural filtered by the eselon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldJabatanStruktural requireOneByJenisIndikatorKinerja(int $jenis_indikator_kinerja) Return the first ChildOldJabatanStruktural filtered by the jenis_indikator_kinerja column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldJabatanStruktural requireOneBySkpdId(int $skpd_id) Return the first ChildOldJabatanStruktural filtered by the skpd_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldJabatanStruktural requireOneByNomer(int $nomer) Return the first ChildOldJabatanStruktural filtered by the nomer column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldJabatanStruktural requireOneByIsAsisten(boolean $is_asisten) Return the first ChildOldJabatanStruktural filtered by the is_asisten column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldJabatanStruktural requireOneByNomerUrut(int $nomer_urut) Return the first ChildOldJabatanStruktural filtered by the nomer_urut column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOldJabatanStruktural[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildOldJabatanStruktural objects based on current ModelCriteria
 * @method     ChildOldJabatanStruktural[]|ObjectCollection findById(int $id) Return ChildOldJabatanStruktural objects filtered by the id column
 * @method     ChildOldJabatanStruktural[]|ObjectCollection findByNama(string $nama) Return ChildOldJabatanStruktural objects filtered by the nama column
 * @method     ChildOldJabatanStruktural[]|ObjectCollection findByEselon(string $eselon) Return ChildOldJabatanStruktural objects filtered by the eselon column
 * @method     ChildOldJabatanStruktural[]|ObjectCollection findByJenisIndikatorKinerja(int $jenis_indikator_kinerja) Return ChildOldJabatanStruktural objects filtered by the jenis_indikator_kinerja column
 * @method     ChildOldJabatanStruktural[]|ObjectCollection findBySkpdId(int $skpd_id) Return ChildOldJabatanStruktural objects filtered by the skpd_id column
 * @method     ChildOldJabatanStruktural[]|ObjectCollection findByNomer(int $nomer) Return ChildOldJabatanStruktural objects filtered by the nomer column
 * @method     ChildOldJabatanStruktural[]|ObjectCollection findByIsAsisten(boolean $is_asisten) Return ChildOldJabatanStruktural objects filtered by the is_asisten column
 * @method     ChildOldJabatanStruktural[]|ObjectCollection findByNomerUrut(int $nomer_urut) Return ChildOldJabatanStruktural objects filtered by the nomer_urut column
 * @method     ChildOldJabatanStruktural[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class OldJabatanStrukturalQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \CoreBundle\Model\OldEperformance\Base\OldJabatanStrukturalQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'old_eperformance', $modelName = '\\CoreBundle\\Model\\OldEperformance\\OldJabatanStruktural', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildOldJabatanStrukturalQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildOldJabatanStrukturalQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildOldJabatanStrukturalQuery) {
            return $criteria;
        }
        $query = new ChildOldJabatanStrukturalQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildOldJabatanStruktural|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(OldJabatanStrukturalTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = OldJabatanStrukturalTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldJabatanStruktural A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, nama, eselon, jenis_indikator_kinerja, skpd_id, nomer, is_asisten, nomer_urut FROM eperformance.jabatan_struktural WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildOldJabatanStruktural $obj */
            $obj = new ChildOldJabatanStruktural();
            $obj->hydrate($row);
            OldJabatanStrukturalTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildOldJabatanStruktural|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildOldJabatanStrukturalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildOldJabatanStrukturalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldJabatanStrukturalQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%', Criteria::LIKE); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldJabatanStrukturalQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the eselon column
     *
     * Example usage:
     * <code>
     * $query->filterByEselon('fooValue');   // WHERE eselon = 'fooValue'
     * $query->filterByEselon('%fooValue%', Criteria::LIKE); // WHERE eselon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $eselon The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldJabatanStrukturalQuery The current query, for fluid interface
     */
    public function filterByEselon($eselon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($eselon)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_ESELON, $eselon, $comparison);
    }

    /**
     * Filter the query on the jenis_indikator_kinerja column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisIndikatorKinerja(1234); // WHERE jenis_indikator_kinerja = 1234
     * $query->filterByJenisIndikatorKinerja(array(12, 34)); // WHERE jenis_indikator_kinerja IN (12, 34)
     * $query->filterByJenisIndikatorKinerja(array('min' => 12)); // WHERE jenis_indikator_kinerja > 12
     * </code>
     *
     * @param     mixed $jenisIndikatorKinerja The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldJabatanStrukturalQuery The current query, for fluid interface
     */
    public function filterByJenisIndikatorKinerja($jenisIndikatorKinerja = null, $comparison = null)
    {
        if (is_array($jenisIndikatorKinerja)) {
            $useMinMax = false;
            if (isset($jenisIndikatorKinerja['min'])) {
                $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_JENIS_INDIKATOR_KINERJA, $jenisIndikatorKinerja['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisIndikatorKinerja['max'])) {
                $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_JENIS_INDIKATOR_KINERJA, $jenisIndikatorKinerja['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_JENIS_INDIKATOR_KINERJA, $jenisIndikatorKinerja, $comparison);
    }

    /**
     * Filter the query on the skpd_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySkpdId(1234); // WHERE skpd_id = 1234
     * $query->filterBySkpdId(array(12, 34)); // WHERE skpd_id IN (12, 34)
     * $query->filterBySkpdId(array('min' => 12)); // WHERE skpd_id > 12
     * </code>
     *
     * @see       filterByOldMasterSkpd()
     *
     * @param     mixed $skpdId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldJabatanStrukturalQuery The current query, for fluid interface
     */
    public function filterBySkpdId($skpdId = null, $comparison = null)
    {
        if (is_array($skpdId)) {
            $useMinMax = false;
            if (isset($skpdId['min'])) {
                $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_SKPD_ID, $skpdId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($skpdId['max'])) {
                $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_SKPD_ID, $skpdId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_SKPD_ID, $skpdId, $comparison);
    }

    /**
     * Filter the query on the nomer column
     *
     * Example usage:
     * <code>
     * $query->filterByNomer(1234); // WHERE nomer = 1234
     * $query->filterByNomer(array(12, 34)); // WHERE nomer IN (12, 34)
     * $query->filterByNomer(array('min' => 12)); // WHERE nomer > 12
     * </code>
     *
     * @param     mixed $nomer The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldJabatanStrukturalQuery The current query, for fluid interface
     */
    public function filterByNomer($nomer = null, $comparison = null)
    {
        if (is_array($nomer)) {
            $useMinMax = false;
            if (isset($nomer['min'])) {
                $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_NOMER, $nomer['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nomer['max'])) {
                $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_NOMER, $nomer['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_NOMER, $nomer, $comparison);
    }

    /**
     * Filter the query on the is_asisten column
     *
     * Example usage:
     * <code>
     * $query->filterByIsAsisten(true); // WHERE is_asisten = true
     * $query->filterByIsAsisten('yes'); // WHERE is_asisten = true
     * </code>
     *
     * @param     boolean|string $isAsisten The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldJabatanStrukturalQuery The current query, for fluid interface
     */
    public function filterByIsAsisten($isAsisten = null, $comparison = null)
    {
        if (is_string($isAsisten)) {
            $isAsisten = in_array(strtolower($isAsisten), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_IS_ASISTEN, $isAsisten, $comparison);
    }

    /**
     * Filter the query on the nomer_urut column
     *
     * Example usage:
     * <code>
     * $query->filterByNomerUrut(1234); // WHERE nomer_urut = 1234
     * $query->filterByNomerUrut(array(12, 34)); // WHERE nomer_urut IN (12, 34)
     * $query->filterByNomerUrut(array('min' => 12)); // WHERE nomer_urut > 12
     * </code>
     *
     * @param     mixed $nomerUrut The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldJabatanStrukturalQuery The current query, for fluid interface
     */
    public function filterByNomerUrut($nomerUrut = null, $comparison = null)
    {
        if (is_array($nomerUrut)) {
            $useMinMax = false;
            if (isset($nomerUrut['min'])) {
                $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_NOMER_URUT, $nomerUrut['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nomerUrut['max'])) {
                $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_NOMER_URUT, $nomerUrut['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_NOMER_URUT, $nomerUrut, $comparison);
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldMasterSkpd object
     *
     * @param \CoreBundle\Model\OldEperformance\OldMasterSkpd|ObjectCollection $oldMasterSkpd The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldJabatanStrukturalQuery The current query, for fluid interface
     */
    public function filterByOldMasterSkpd($oldMasterSkpd, $comparison = null)
    {
        if ($oldMasterSkpd instanceof \CoreBundle\Model\OldEperformance\OldMasterSkpd) {
            return $this
                ->addUsingAlias(OldJabatanStrukturalTableMap::COL_SKPD_ID, $oldMasterSkpd->getSkpdId(), $comparison);
        } elseif ($oldMasterSkpd instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OldJabatanStrukturalTableMap::COL_SKPD_ID, $oldMasterSkpd->toKeyValue('PrimaryKey', 'SkpdId'), $comparison);
        } else {
            throw new PropelException('filterByOldMasterSkpd() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldMasterSkpd or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldMasterSkpd relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldJabatanStrukturalQuery The current query, for fluid interface
     */
    public function joinOldMasterSkpd($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldMasterSkpd');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldMasterSkpd');
        }

        return $this;
    }

    /**
     * Use the OldMasterSkpd relation OldMasterSkpd object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldMasterSkpdQuery A secondary query class using the current class as primary query
     */
    public function useOldMasterSkpdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldMasterSkpd($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldMasterSkpd', '\CoreBundle\Model\OldEperformance\OldMasterSkpdQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldPegawai object
     *
     * @param \CoreBundle\Model\OldEperformance\OldPegawai|ObjectCollection $oldPegawai the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildOldJabatanStrukturalQuery The current query, for fluid interface
     */
    public function filterByOldPegawai($oldPegawai, $comparison = null)
    {
        if ($oldPegawai instanceof \CoreBundle\Model\OldEperformance\OldPegawai) {
            return $this
                ->addUsingAlias(OldJabatanStrukturalTableMap::COL_ID, $oldPegawai->getJabatanStrukturalId(), $comparison);
        } elseif ($oldPegawai instanceof ObjectCollection) {
            return $this
                ->useOldPegawaiQuery()
                ->filterByPrimaryKeys($oldPegawai->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOldPegawai() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldPegawai or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldPegawai relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldJabatanStrukturalQuery The current query, for fluid interface
     */
    public function joinOldPegawai($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldPegawai');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldPegawai');
        }

        return $this;
    }

    /**
     * Use the OldPegawai relation OldPegawai object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldPegawaiQuery A secondary query class using the current class as primary query
     */
    public function useOldPegawaiQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldPegawai($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldPegawai', '\CoreBundle\Model\OldEperformance\OldPegawaiQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildOldJabatanStruktural $oldJabatanStruktural Object to remove from the list of results
     *
     * @return $this|ChildOldJabatanStrukturalQuery The current query, for fluid interface
     */
    public function prune($oldJabatanStruktural = null)
    {
        if ($oldJabatanStruktural) {
            $this->addUsingAlias(OldJabatanStrukturalTableMap::COL_ID, $oldJabatanStruktural->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the eperformance.jabatan_struktural table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldJabatanStrukturalTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            OldJabatanStrukturalTableMap::clearInstancePool();
            OldJabatanStrukturalTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldJabatanStrukturalTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(OldJabatanStrukturalTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            OldJabatanStrukturalTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            OldJabatanStrukturalTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 7200)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(OldJabatanStrukturalTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(OldJabatanStrukturalTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }


        return $con->getDataFetcher($stmt);
    }

} // OldJabatanStrukturalQuery

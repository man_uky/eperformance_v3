<?php

namespace CoreBundle\Model\OldEperformance\Base;

use \Exception;
use \PDO;
use CoreBundle\Model\OldEperformance\OldIndikatorKinerja as ChildOldIndikatorKinerja;
use CoreBundle\Model\OldEperformance\OldIndikatorKinerjaQuery as ChildOldIndikatorKinerjaQuery;
use CoreBundle\Model\OldEperformance\Map\OldIndikatorKinerjaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'eperformance.indikator_kinerja' table.
 *
 *
 *
 * @method     ChildOldIndikatorKinerjaQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildOldIndikatorKinerjaQuery orderByPegawaiId($order = Criteria::ASC) Order by the pegawai_id column
 * @method     ChildOldIndikatorKinerjaQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method     ChildOldIndikatorKinerjaQuery orderByDeskripsi($order = Criteria::ASC) Order by the deskripsi column
 * @method     ChildOldIndikatorKinerjaQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildOldIndikatorKinerjaQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildOldIndikatorKinerjaQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildOldIndikatorKinerjaQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildOldIndikatorKinerjaQuery orderBySkpdId($order = Criteria::ASC) Order by the skpd_id column
 * @method     ChildOldIndikatorKinerjaQuery orderByTarget($order = Criteria::ASC) Order by the target column
 * @method     ChildOldIndikatorKinerjaQuery orderBySatuan($order = Criteria::ASC) Order by the satuan column
 * @method     ChildOldIndikatorKinerjaQuery orderByTargetKemarin($order = Criteria::ASC) Order by the target_kemarin column
 * @method     ChildOldIndikatorKinerjaQuery orderByCaraPerhitungan($order = Criteria::ASC) Order by the cara_perhitungan column
 * @method     ChildOldIndikatorKinerjaQuery orderByCapaianAkhir($order = Criteria::ASC) Order by the capaian_akhir column
 * @method     ChildOldIndikatorKinerjaQuery orderByJenis($order = Criteria::ASC) Order by the jenis column
 * @method     ChildOldIndikatorKinerjaQuery orderByIndikatorIdAtasan($order = Criteria::ASC) Order by the indikator_id_atasan column
 * @method     ChildOldIndikatorKinerjaQuery orderByIsTambahan($order = Criteria::ASC) Order by the is_tambahan column
 * @method     ChildOldIndikatorKinerjaQuery orderByIsTahunLalu($order = Criteria::ASC) Order by the is_tahun_lalu column
 * @method     ChildOldIndikatorKinerjaQuery orderByIsTarikEbudgeting($order = Criteria::ASC) Order by the is_tarik_ebudgeting column
 * @method     ChildOldIndikatorKinerjaQuery orderByIsPerhitunganTerbalik($order = Criteria::ASC) Order by the is_perhitungan_terbalik column
 * @method     ChildOldIndikatorKinerjaQuery orderByIsRealisasiAngkaPrediksi($order = Criteria::ASC) Order by the is_realisasi_angka_prediksi column
 *
 * @method     ChildOldIndikatorKinerjaQuery groupById() Group by the id column
 * @method     ChildOldIndikatorKinerjaQuery groupByPegawaiId() Group by the pegawai_id column
 * @method     ChildOldIndikatorKinerjaQuery groupByNama() Group by the nama column
 * @method     ChildOldIndikatorKinerjaQuery groupByDeskripsi() Group by the deskripsi column
 * @method     ChildOldIndikatorKinerjaQuery groupByStatus() Group by the status column
 * @method     ChildOldIndikatorKinerjaQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildOldIndikatorKinerjaQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildOldIndikatorKinerjaQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildOldIndikatorKinerjaQuery groupBySkpdId() Group by the skpd_id column
 * @method     ChildOldIndikatorKinerjaQuery groupByTarget() Group by the target column
 * @method     ChildOldIndikatorKinerjaQuery groupBySatuan() Group by the satuan column
 * @method     ChildOldIndikatorKinerjaQuery groupByTargetKemarin() Group by the target_kemarin column
 * @method     ChildOldIndikatorKinerjaQuery groupByCaraPerhitungan() Group by the cara_perhitungan column
 * @method     ChildOldIndikatorKinerjaQuery groupByCapaianAkhir() Group by the capaian_akhir column
 * @method     ChildOldIndikatorKinerjaQuery groupByJenis() Group by the jenis column
 * @method     ChildOldIndikatorKinerjaQuery groupByIndikatorIdAtasan() Group by the indikator_id_atasan column
 * @method     ChildOldIndikatorKinerjaQuery groupByIsTambahan() Group by the is_tambahan column
 * @method     ChildOldIndikatorKinerjaQuery groupByIsTahunLalu() Group by the is_tahun_lalu column
 * @method     ChildOldIndikatorKinerjaQuery groupByIsTarikEbudgeting() Group by the is_tarik_ebudgeting column
 * @method     ChildOldIndikatorKinerjaQuery groupByIsPerhitunganTerbalik() Group by the is_perhitungan_terbalik column
 * @method     ChildOldIndikatorKinerjaQuery groupByIsRealisasiAngkaPrediksi() Group by the is_realisasi_angka_prediksi column
 *
 * @method     ChildOldIndikatorKinerjaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildOldIndikatorKinerjaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildOldIndikatorKinerjaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildOldIndikatorKinerjaQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildOldIndikatorKinerjaQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildOldIndikatorKinerjaQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildOldIndikatorKinerjaQuery leftJoinOldPegawai($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldPegawai relation
 * @method     ChildOldIndikatorKinerjaQuery rightJoinOldPegawai($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldPegawai relation
 * @method     ChildOldIndikatorKinerjaQuery innerJoinOldPegawai($relationAlias = null) Adds a INNER JOIN clause to the query using the OldPegawai relation
 *
 * @method     ChildOldIndikatorKinerjaQuery joinWithOldPegawai($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldPegawai relation
 *
 * @method     ChildOldIndikatorKinerjaQuery leftJoinWithOldPegawai() Adds a LEFT JOIN clause and with to the query using the OldPegawai relation
 * @method     ChildOldIndikatorKinerjaQuery rightJoinWithOldPegawai() Adds a RIGHT JOIN clause and with to the query using the OldPegawai relation
 * @method     ChildOldIndikatorKinerjaQuery innerJoinWithOldPegawai() Adds a INNER JOIN clause and with to the query using the OldPegawai relation
 *
 * @method     ChildOldIndikatorKinerjaQuery leftJoinOldMasterSkpd($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldMasterSkpd relation
 * @method     ChildOldIndikatorKinerjaQuery rightJoinOldMasterSkpd($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldMasterSkpd relation
 * @method     ChildOldIndikatorKinerjaQuery innerJoinOldMasterSkpd($relationAlias = null) Adds a INNER JOIN clause to the query using the OldMasterSkpd relation
 *
 * @method     ChildOldIndikatorKinerjaQuery joinWithOldMasterSkpd($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldMasterSkpd relation
 *
 * @method     ChildOldIndikatorKinerjaQuery leftJoinWithOldMasterSkpd() Adds a LEFT JOIN clause and with to the query using the OldMasterSkpd relation
 * @method     ChildOldIndikatorKinerjaQuery rightJoinWithOldMasterSkpd() Adds a RIGHT JOIN clause and with to the query using the OldMasterSkpd relation
 * @method     ChildOldIndikatorKinerjaQuery innerJoinWithOldMasterSkpd() Adds a INNER JOIN clause and with to the query using the OldMasterSkpd relation
 *
 * @method     ChildOldIndikatorKinerjaQuery leftJoinOldIndikatorKinerja($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldIndikatorKinerja relation
 * @method     ChildOldIndikatorKinerjaQuery rightJoinOldIndikatorKinerja($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldIndikatorKinerja relation
 * @method     ChildOldIndikatorKinerjaQuery innerJoinOldIndikatorKinerja($relationAlias = null) Adds a INNER JOIN clause to the query using the OldIndikatorKinerja relation
 *
 * @method     ChildOldIndikatorKinerjaQuery joinWithOldIndikatorKinerja($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldIndikatorKinerja relation
 *
 * @method     ChildOldIndikatorKinerjaQuery leftJoinWithOldIndikatorKinerja() Adds a LEFT JOIN clause and with to the query using the OldIndikatorKinerja relation
 * @method     ChildOldIndikatorKinerjaQuery rightJoinWithOldIndikatorKinerja() Adds a RIGHT JOIN clause and with to the query using the OldIndikatorKinerja relation
 * @method     ChildOldIndikatorKinerjaQuery innerJoinWithOldIndikatorKinerja() Adds a INNER JOIN clause and with to the query using the OldIndikatorKinerja relation
 *
 * @method     ChildOldIndikatorKinerjaQuery leftJoinOldIndikatorKinerjaRelatedById($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldIndikatorKinerjaRelatedById relation
 * @method     ChildOldIndikatorKinerjaQuery rightJoinOldIndikatorKinerjaRelatedById($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldIndikatorKinerjaRelatedById relation
 * @method     ChildOldIndikatorKinerjaQuery innerJoinOldIndikatorKinerjaRelatedById($relationAlias = null) Adds a INNER JOIN clause to the query using the OldIndikatorKinerjaRelatedById relation
 *
 * @method     ChildOldIndikatorKinerjaQuery joinWithOldIndikatorKinerjaRelatedById($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldIndikatorKinerjaRelatedById relation
 *
 * @method     ChildOldIndikatorKinerjaQuery leftJoinWithOldIndikatorKinerjaRelatedById() Adds a LEFT JOIN clause and with to the query using the OldIndikatorKinerjaRelatedById relation
 * @method     ChildOldIndikatorKinerjaQuery rightJoinWithOldIndikatorKinerjaRelatedById() Adds a RIGHT JOIN clause and with to the query using the OldIndikatorKinerjaRelatedById relation
 * @method     ChildOldIndikatorKinerjaQuery innerJoinWithOldIndikatorKinerjaRelatedById() Adds a INNER JOIN clause and with to the query using the OldIndikatorKinerjaRelatedById relation
 *
 * @method     \CoreBundle\Model\OldEperformance\OldPegawaiQuery|\CoreBundle\Model\OldEperformance\OldMasterSkpdQuery|\CoreBundle\Model\OldEperformance\OldIndikatorKinerjaQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildOldIndikatorKinerja findOne(ConnectionInterface $con = null) Return the first ChildOldIndikatorKinerja matching the query
 * @method     ChildOldIndikatorKinerja findOneOrCreate(ConnectionInterface $con = null) Return the first ChildOldIndikatorKinerja matching the query, or a new ChildOldIndikatorKinerja object populated from the query conditions when no match is found
 *
 * @method     ChildOldIndikatorKinerja findOneById(int $id) Return the first ChildOldIndikatorKinerja filtered by the id column
 * @method     ChildOldIndikatorKinerja findOneByPegawaiId(int $pegawai_id) Return the first ChildOldIndikatorKinerja filtered by the pegawai_id column
 * @method     ChildOldIndikatorKinerja findOneByNama(string $nama) Return the first ChildOldIndikatorKinerja filtered by the nama column
 * @method     ChildOldIndikatorKinerja findOneByDeskripsi(string $deskripsi) Return the first ChildOldIndikatorKinerja filtered by the deskripsi column
 * @method     ChildOldIndikatorKinerja findOneByStatus(int $status) Return the first ChildOldIndikatorKinerja filtered by the status column
 * @method     ChildOldIndikatorKinerja findOneByCreatedAt(string $created_at) Return the first ChildOldIndikatorKinerja filtered by the created_at column
 * @method     ChildOldIndikatorKinerja findOneByUpdatedAt(string $updated_at) Return the first ChildOldIndikatorKinerja filtered by the updated_at column
 * @method     ChildOldIndikatorKinerja findOneByDeletedAt(string $deleted_at) Return the first ChildOldIndikatorKinerja filtered by the deleted_at column
 * @method     ChildOldIndikatorKinerja findOneBySkpdId(int $skpd_id) Return the first ChildOldIndikatorKinerja filtered by the skpd_id column
 * @method     ChildOldIndikatorKinerja findOneByTarget(string $target) Return the first ChildOldIndikatorKinerja filtered by the target column
 * @method     ChildOldIndikatorKinerja findOneBySatuan(string $satuan) Return the first ChildOldIndikatorKinerja filtered by the satuan column
 * @method     ChildOldIndikatorKinerja findOneByTargetKemarin(string $target_kemarin) Return the first ChildOldIndikatorKinerja filtered by the target_kemarin column
 * @method     ChildOldIndikatorKinerja findOneByCaraPerhitungan(string $cara_perhitungan) Return the first ChildOldIndikatorKinerja filtered by the cara_perhitungan column
 * @method     ChildOldIndikatorKinerja findOneByCapaianAkhir(string $capaian_akhir) Return the first ChildOldIndikatorKinerja filtered by the capaian_akhir column
 * @method     ChildOldIndikatorKinerja findOneByJenis(int $jenis) Return the first ChildOldIndikatorKinerja filtered by the jenis column
 * @method     ChildOldIndikatorKinerja findOneByIndikatorIdAtasan(int $indikator_id_atasan) Return the first ChildOldIndikatorKinerja filtered by the indikator_id_atasan column
 * @method     ChildOldIndikatorKinerja findOneByIsTambahan(boolean $is_tambahan) Return the first ChildOldIndikatorKinerja filtered by the is_tambahan column
 * @method     ChildOldIndikatorKinerja findOneByIsTahunLalu(boolean $is_tahun_lalu) Return the first ChildOldIndikatorKinerja filtered by the is_tahun_lalu column
 * @method     ChildOldIndikatorKinerja findOneByIsTarikEbudgeting(boolean $is_tarik_ebudgeting) Return the first ChildOldIndikatorKinerja filtered by the is_tarik_ebudgeting column
 * @method     ChildOldIndikatorKinerja findOneByIsPerhitunganTerbalik(boolean $is_perhitungan_terbalik) Return the first ChildOldIndikatorKinerja filtered by the is_perhitungan_terbalik column
 * @method     ChildOldIndikatorKinerja findOneByIsRealisasiAngkaPrediksi(boolean $is_realisasi_angka_prediksi) Return the first ChildOldIndikatorKinerja filtered by the is_realisasi_angka_prediksi column *

 * @method     ChildOldIndikatorKinerja requirePk($key, ConnectionInterface $con = null) Return the ChildOldIndikatorKinerja by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOne(ConnectionInterface $con = null) Return the first ChildOldIndikatorKinerja matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOldIndikatorKinerja requireOneById(int $id) Return the first ChildOldIndikatorKinerja filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOneByPegawaiId(int $pegawai_id) Return the first ChildOldIndikatorKinerja filtered by the pegawai_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOneByNama(string $nama) Return the first ChildOldIndikatorKinerja filtered by the nama column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOneByDeskripsi(string $deskripsi) Return the first ChildOldIndikatorKinerja filtered by the deskripsi column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOneByStatus(int $status) Return the first ChildOldIndikatorKinerja filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOneByCreatedAt(string $created_at) Return the first ChildOldIndikatorKinerja filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOneByUpdatedAt(string $updated_at) Return the first ChildOldIndikatorKinerja filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOneByDeletedAt(string $deleted_at) Return the first ChildOldIndikatorKinerja filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOneBySkpdId(int $skpd_id) Return the first ChildOldIndikatorKinerja filtered by the skpd_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOneByTarget(string $target) Return the first ChildOldIndikatorKinerja filtered by the target column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOneBySatuan(string $satuan) Return the first ChildOldIndikatorKinerja filtered by the satuan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOneByTargetKemarin(string $target_kemarin) Return the first ChildOldIndikatorKinerja filtered by the target_kemarin column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOneByCaraPerhitungan(string $cara_perhitungan) Return the first ChildOldIndikatorKinerja filtered by the cara_perhitungan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOneByCapaianAkhir(string $capaian_akhir) Return the first ChildOldIndikatorKinerja filtered by the capaian_akhir column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOneByJenis(int $jenis) Return the first ChildOldIndikatorKinerja filtered by the jenis column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOneByIndikatorIdAtasan(int $indikator_id_atasan) Return the first ChildOldIndikatorKinerja filtered by the indikator_id_atasan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOneByIsTambahan(boolean $is_tambahan) Return the first ChildOldIndikatorKinerja filtered by the is_tambahan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOneByIsTahunLalu(boolean $is_tahun_lalu) Return the first ChildOldIndikatorKinerja filtered by the is_tahun_lalu column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOneByIsTarikEbudgeting(boolean $is_tarik_ebudgeting) Return the first ChildOldIndikatorKinerja filtered by the is_tarik_ebudgeting column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOneByIsPerhitunganTerbalik(boolean $is_perhitungan_terbalik) Return the first ChildOldIndikatorKinerja filtered by the is_perhitungan_terbalik column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldIndikatorKinerja requireOneByIsRealisasiAngkaPrediksi(boolean $is_realisasi_angka_prediksi) Return the first ChildOldIndikatorKinerja filtered by the is_realisasi_angka_prediksi column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildOldIndikatorKinerja objects based on current ModelCriteria
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findById(int $id) Return ChildOldIndikatorKinerja objects filtered by the id column
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findByPegawaiId(int $pegawai_id) Return ChildOldIndikatorKinerja objects filtered by the pegawai_id column
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findByNama(string $nama) Return ChildOldIndikatorKinerja objects filtered by the nama column
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findByDeskripsi(string $deskripsi) Return ChildOldIndikatorKinerja objects filtered by the deskripsi column
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findByStatus(int $status) Return ChildOldIndikatorKinerja objects filtered by the status column
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildOldIndikatorKinerja objects filtered by the created_at column
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildOldIndikatorKinerja objects filtered by the updated_at column
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildOldIndikatorKinerja objects filtered by the deleted_at column
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findBySkpdId(int $skpd_id) Return ChildOldIndikatorKinerja objects filtered by the skpd_id column
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findByTarget(string $target) Return ChildOldIndikatorKinerja objects filtered by the target column
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findBySatuan(string $satuan) Return ChildOldIndikatorKinerja objects filtered by the satuan column
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findByTargetKemarin(string $target_kemarin) Return ChildOldIndikatorKinerja objects filtered by the target_kemarin column
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findByCaraPerhitungan(string $cara_perhitungan) Return ChildOldIndikatorKinerja objects filtered by the cara_perhitungan column
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findByCapaianAkhir(string $capaian_akhir) Return ChildOldIndikatorKinerja objects filtered by the capaian_akhir column
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findByJenis(int $jenis) Return ChildOldIndikatorKinerja objects filtered by the jenis column
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findByIndikatorIdAtasan(int $indikator_id_atasan) Return ChildOldIndikatorKinerja objects filtered by the indikator_id_atasan column
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findByIsTambahan(boolean $is_tambahan) Return ChildOldIndikatorKinerja objects filtered by the is_tambahan column
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findByIsTahunLalu(boolean $is_tahun_lalu) Return ChildOldIndikatorKinerja objects filtered by the is_tahun_lalu column
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findByIsTarikEbudgeting(boolean $is_tarik_ebudgeting) Return ChildOldIndikatorKinerja objects filtered by the is_tarik_ebudgeting column
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findByIsPerhitunganTerbalik(boolean $is_perhitungan_terbalik) Return ChildOldIndikatorKinerja objects filtered by the is_perhitungan_terbalik column
 * @method     ChildOldIndikatorKinerja[]|ObjectCollection findByIsRealisasiAngkaPrediksi(boolean $is_realisasi_angka_prediksi) Return ChildOldIndikatorKinerja objects filtered by the is_realisasi_angka_prediksi column
 * @method     ChildOldIndikatorKinerja[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class OldIndikatorKinerjaQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \CoreBundle\Model\OldEperformance\Base\OldIndikatorKinerjaQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'old_eperformance', $modelName = '\\CoreBundle\\Model\\OldEperformance\\OldIndikatorKinerja', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildOldIndikatorKinerjaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildOldIndikatorKinerjaQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildOldIndikatorKinerjaQuery) {
            return $criteria;
        }
        $query = new ChildOldIndikatorKinerjaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildOldIndikatorKinerja|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(OldIndikatorKinerjaTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = OldIndikatorKinerjaTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldIndikatorKinerja A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, pegawai_id, nama, deskripsi, status, created_at, updated_at, deleted_at, skpd_id, target, satuan, target_kemarin, cara_perhitungan, capaian_akhir, jenis, indikator_id_atasan, is_tambahan, is_tahun_lalu, is_tarik_ebudgeting, is_perhitungan_terbalik, is_realisasi_angka_prediksi FROM eperformance.indikator_kinerja WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildOldIndikatorKinerja $obj */
            $obj = new ChildOldIndikatorKinerja();
            $obj->hydrate($row);
            OldIndikatorKinerjaTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildOldIndikatorKinerja|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the pegawai_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPegawaiId(1234); // WHERE pegawai_id = 1234
     * $query->filterByPegawaiId(array(12, 34)); // WHERE pegawai_id IN (12, 34)
     * $query->filterByPegawaiId(array('min' => 12)); // WHERE pegawai_id > 12
     * </code>
     *
     * @see       filterByOldPegawai()
     *
     * @param     mixed $pegawaiId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByPegawaiId($pegawaiId = null, $comparison = null)
    {
        if (is_array($pegawaiId)) {
            $useMinMax = false;
            if (isset($pegawaiId['min'])) {
                $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_PEGAWAI_ID, $pegawaiId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pegawaiId['max'])) {
                $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_PEGAWAI_ID, $pegawaiId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_PEGAWAI_ID, $pegawaiId, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%', Criteria::LIKE); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the deskripsi column
     *
     * Example usage:
     * <code>
     * $query->filterByDeskripsi('fooValue');   // WHERE deskripsi = 'fooValue'
     * $query->filterByDeskripsi('%fooValue%', Criteria::LIKE); // WHERE deskripsi LIKE '%fooValue%'
     * </code>
     *
     * @param     string $deskripsi The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByDeskripsi($deskripsi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($deskripsi)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_DESKRIPSI, $deskripsi, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(1234); // WHERE status = 1234
     * $query->filterByStatus(array(12, 34)); // WHERE status IN (12, 34)
     * $query->filterByStatus(array('min' => 12)); // WHERE status > 12
     * </code>
     *
     * @param     mixed $status The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_array($status)) {
            $useMinMax = false;
            if (isset($status['min'])) {
                $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_STATUS, $status['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($status['max'])) {
                $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_STATUS, $status['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the skpd_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySkpdId(1234); // WHERE skpd_id = 1234
     * $query->filterBySkpdId(array(12, 34)); // WHERE skpd_id IN (12, 34)
     * $query->filterBySkpdId(array('min' => 12)); // WHERE skpd_id > 12
     * </code>
     *
     * @see       filterByOldMasterSkpd()
     *
     * @param     mixed $skpdId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterBySkpdId($skpdId = null, $comparison = null)
    {
        if (is_array($skpdId)) {
            $useMinMax = false;
            if (isset($skpdId['min'])) {
                $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_SKPD_ID, $skpdId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($skpdId['max'])) {
                $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_SKPD_ID, $skpdId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_SKPD_ID, $skpdId, $comparison);
    }

    /**
     * Filter the query on the target column
     *
     * Example usage:
     * <code>
     * $query->filterByTarget('fooValue');   // WHERE target = 'fooValue'
     * $query->filterByTarget('%fooValue%', Criteria::LIKE); // WHERE target LIKE '%fooValue%'
     * </code>
     *
     * @param     string $target The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByTarget($target = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($target)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_TARGET, $target, $comparison);
    }

    /**
     * Filter the query on the satuan column
     *
     * Example usage:
     * <code>
     * $query->filterBySatuan('fooValue');   // WHERE satuan = 'fooValue'
     * $query->filterBySatuan('%fooValue%', Criteria::LIKE); // WHERE satuan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $satuan The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterBySatuan($satuan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($satuan)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_SATUAN, $satuan, $comparison);
    }

    /**
     * Filter the query on the target_kemarin column
     *
     * Example usage:
     * <code>
     * $query->filterByTargetKemarin(1234); // WHERE target_kemarin = 1234
     * $query->filterByTargetKemarin(array(12, 34)); // WHERE target_kemarin IN (12, 34)
     * $query->filterByTargetKemarin(array('min' => 12)); // WHERE target_kemarin > 12
     * </code>
     *
     * @param     mixed $targetKemarin The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByTargetKemarin($targetKemarin = null, $comparison = null)
    {
        if (is_array($targetKemarin)) {
            $useMinMax = false;
            if (isset($targetKemarin['min'])) {
                $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_TARGET_KEMARIN, $targetKemarin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($targetKemarin['max'])) {
                $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_TARGET_KEMARIN, $targetKemarin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_TARGET_KEMARIN, $targetKemarin, $comparison);
    }

    /**
     * Filter the query on the cara_perhitungan column
     *
     * Example usage:
     * <code>
     * $query->filterByCaraPerhitungan('fooValue');   // WHERE cara_perhitungan = 'fooValue'
     * $query->filterByCaraPerhitungan('%fooValue%', Criteria::LIKE); // WHERE cara_perhitungan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $caraPerhitungan The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByCaraPerhitungan($caraPerhitungan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($caraPerhitungan)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_CARA_PERHITUNGAN, $caraPerhitungan, $comparison);
    }

    /**
     * Filter the query on the capaian_akhir column
     *
     * Example usage:
     * <code>
     * $query->filterByCapaianAkhir('fooValue');   // WHERE capaian_akhir = 'fooValue'
     * $query->filterByCapaianAkhir('%fooValue%', Criteria::LIKE); // WHERE capaian_akhir LIKE '%fooValue%'
     * </code>
     *
     * @param     string $capaianAkhir The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByCapaianAkhir($capaianAkhir = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($capaianAkhir)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_CAPAIAN_AKHIR, $capaianAkhir, $comparison);
    }

    /**
     * Filter the query on the jenis column
     *
     * Example usage:
     * <code>
     * $query->filterByJenis(1234); // WHERE jenis = 1234
     * $query->filterByJenis(array(12, 34)); // WHERE jenis IN (12, 34)
     * $query->filterByJenis(array('min' => 12)); // WHERE jenis > 12
     * </code>
     *
     * @param     mixed $jenis The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByJenis($jenis = null, $comparison = null)
    {
        if (is_array($jenis)) {
            $useMinMax = false;
            if (isset($jenis['min'])) {
                $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_JENIS, $jenis['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenis['max'])) {
                $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_JENIS, $jenis['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_JENIS, $jenis, $comparison);
    }

    /**
     * Filter the query on the indikator_id_atasan column
     *
     * Example usage:
     * <code>
     * $query->filterByIndikatorIdAtasan(1234); // WHERE indikator_id_atasan = 1234
     * $query->filterByIndikatorIdAtasan(array(12, 34)); // WHERE indikator_id_atasan IN (12, 34)
     * $query->filterByIndikatorIdAtasan(array('min' => 12)); // WHERE indikator_id_atasan > 12
     * </code>
     *
     * @see       filterByOldIndikatorKinerja()
     *
     * @param     mixed $indikatorIdAtasan The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByIndikatorIdAtasan($indikatorIdAtasan = null, $comparison = null)
    {
        if (is_array($indikatorIdAtasan)) {
            $useMinMax = false;
            if (isset($indikatorIdAtasan['min'])) {
                $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_INDIKATOR_ID_ATASAN, $indikatorIdAtasan['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($indikatorIdAtasan['max'])) {
                $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_INDIKATOR_ID_ATASAN, $indikatorIdAtasan['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_INDIKATOR_ID_ATASAN, $indikatorIdAtasan, $comparison);
    }

    /**
     * Filter the query on the is_tambahan column
     *
     * Example usage:
     * <code>
     * $query->filterByIsTambahan(true); // WHERE is_tambahan = true
     * $query->filterByIsTambahan('yes'); // WHERE is_tambahan = true
     * </code>
     *
     * @param     boolean|string $isTambahan The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByIsTambahan($isTambahan = null, $comparison = null)
    {
        if (is_string($isTambahan)) {
            $isTambahan = in_array(strtolower($isTambahan), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_IS_TAMBAHAN, $isTambahan, $comparison);
    }

    /**
     * Filter the query on the is_tahun_lalu column
     *
     * Example usage:
     * <code>
     * $query->filterByIsTahunLalu(true); // WHERE is_tahun_lalu = true
     * $query->filterByIsTahunLalu('yes'); // WHERE is_tahun_lalu = true
     * </code>
     *
     * @param     boolean|string $isTahunLalu The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByIsTahunLalu($isTahunLalu = null, $comparison = null)
    {
        if (is_string($isTahunLalu)) {
            $isTahunLalu = in_array(strtolower($isTahunLalu), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_IS_TAHUN_LALU, $isTahunLalu, $comparison);
    }

    /**
     * Filter the query on the is_tarik_ebudgeting column
     *
     * Example usage:
     * <code>
     * $query->filterByIsTarikEbudgeting(true); // WHERE is_tarik_ebudgeting = true
     * $query->filterByIsTarikEbudgeting('yes'); // WHERE is_tarik_ebudgeting = true
     * </code>
     *
     * @param     boolean|string $isTarikEbudgeting The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByIsTarikEbudgeting($isTarikEbudgeting = null, $comparison = null)
    {
        if (is_string($isTarikEbudgeting)) {
            $isTarikEbudgeting = in_array(strtolower($isTarikEbudgeting), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_IS_TARIK_EBUDGETING, $isTarikEbudgeting, $comparison);
    }

    /**
     * Filter the query on the is_perhitungan_terbalik column
     *
     * Example usage:
     * <code>
     * $query->filterByIsPerhitunganTerbalik(true); // WHERE is_perhitungan_terbalik = true
     * $query->filterByIsPerhitunganTerbalik('yes'); // WHERE is_perhitungan_terbalik = true
     * </code>
     *
     * @param     boolean|string $isPerhitunganTerbalik The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByIsPerhitunganTerbalik($isPerhitunganTerbalik = null, $comparison = null)
    {
        if (is_string($isPerhitunganTerbalik)) {
            $isPerhitunganTerbalik = in_array(strtolower($isPerhitunganTerbalik), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_IS_PERHITUNGAN_TERBALIK, $isPerhitunganTerbalik, $comparison);
    }

    /**
     * Filter the query on the is_realisasi_angka_prediksi column
     *
     * Example usage:
     * <code>
     * $query->filterByIsRealisasiAngkaPrediksi(true); // WHERE is_realisasi_angka_prediksi = true
     * $query->filterByIsRealisasiAngkaPrediksi('yes'); // WHERE is_realisasi_angka_prediksi = true
     * </code>
     *
     * @param     boolean|string $isRealisasiAngkaPrediksi The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByIsRealisasiAngkaPrediksi($isRealisasiAngkaPrediksi = null, $comparison = null)
    {
        if (is_string($isRealisasiAngkaPrediksi)) {
            $isRealisasiAngkaPrediksi = in_array(strtolower($isRealisasiAngkaPrediksi), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_IS_REALISASI_ANGKA_PREDIKSI, $isRealisasiAngkaPrediksi, $comparison);
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldPegawai object
     *
     * @param \CoreBundle\Model\OldEperformance\OldPegawai|ObjectCollection $oldPegawai The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByOldPegawai($oldPegawai, $comparison = null)
    {
        if ($oldPegawai instanceof \CoreBundle\Model\OldEperformance\OldPegawai) {
            return $this
                ->addUsingAlias(OldIndikatorKinerjaTableMap::COL_PEGAWAI_ID, $oldPegawai->getId(), $comparison);
        } elseif ($oldPegawai instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OldIndikatorKinerjaTableMap::COL_PEGAWAI_ID, $oldPegawai->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByOldPegawai() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldPegawai or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldPegawai relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function joinOldPegawai($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldPegawai');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldPegawai');
        }

        return $this;
    }

    /**
     * Use the OldPegawai relation OldPegawai object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldPegawaiQuery A secondary query class using the current class as primary query
     */
    public function useOldPegawaiQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldPegawai($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldPegawai', '\CoreBundle\Model\OldEperformance\OldPegawaiQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldMasterSkpd object
     *
     * @param \CoreBundle\Model\OldEperformance\OldMasterSkpd|ObjectCollection $oldMasterSkpd The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByOldMasterSkpd($oldMasterSkpd, $comparison = null)
    {
        if ($oldMasterSkpd instanceof \CoreBundle\Model\OldEperformance\OldMasterSkpd) {
            return $this
                ->addUsingAlias(OldIndikatorKinerjaTableMap::COL_SKPD_ID, $oldMasterSkpd->getSkpdId(), $comparison);
        } elseif ($oldMasterSkpd instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OldIndikatorKinerjaTableMap::COL_SKPD_ID, $oldMasterSkpd->toKeyValue('PrimaryKey', 'SkpdId'), $comparison);
        } else {
            throw new PropelException('filterByOldMasterSkpd() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldMasterSkpd or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldMasterSkpd relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function joinOldMasterSkpd($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldMasterSkpd');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldMasterSkpd');
        }

        return $this;
    }

    /**
     * Use the OldMasterSkpd relation OldMasterSkpd object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldMasterSkpdQuery A secondary query class using the current class as primary query
     */
    public function useOldMasterSkpdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldMasterSkpd($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldMasterSkpd', '\CoreBundle\Model\OldEperformance\OldMasterSkpdQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldIndikatorKinerja object
     *
     * @param \CoreBundle\Model\OldEperformance\OldIndikatorKinerja|ObjectCollection $oldIndikatorKinerja The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByOldIndikatorKinerja($oldIndikatorKinerja, $comparison = null)
    {
        if ($oldIndikatorKinerja instanceof \CoreBundle\Model\OldEperformance\OldIndikatorKinerja) {
            return $this
                ->addUsingAlias(OldIndikatorKinerjaTableMap::COL_INDIKATOR_ID_ATASAN, $oldIndikatorKinerja->getId(), $comparison);
        } elseif ($oldIndikatorKinerja instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OldIndikatorKinerjaTableMap::COL_INDIKATOR_ID_ATASAN, $oldIndikatorKinerja->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByOldIndikatorKinerja() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldIndikatorKinerja or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldIndikatorKinerja relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function joinOldIndikatorKinerja($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldIndikatorKinerja');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldIndikatorKinerja');
        }

        return $this;
    }

    /**
     * Use the OldIndikatorKinerja relation OldIndikatorKinerja object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldIndikatorKinerjaQuery A secondary query class using the current class as primary query
     */
    public function useOldIndikatorKinerjaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldIndikatorKinerja($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldIndikatorKinerja', '\CoreBundle\Model\OldEperformance\OldIndikatorKinerjaQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldIndikatorKinerja object
     *
     * @param \CoreBundle\Model\OldEperformance\OldIndikatorKinerja|ObjectCollection $oldIndikatorKinerja the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function filterByOldIndikatorKinerjaRelatedById($oldIndikatorKinerja, $comparison = null)
    {
        if ($oldIndikatorKinerja instanceof \CoreBundle\Model\OldEperformance\OldIndikatorKinerja) {
            return $this
                ->addUsingAlias(OldIndikatorKinerjaTableMap::COL_ID, $oldIndikatorKinerja->getIndikatorIdAtasan(), $comparison);
        } elseif ($oldIndikatorKinerja instanceof ObjectCollection) {
            return $this
                ->useOldIndikatorKinerjaRelatedByIdQuery()
                ->filterByPrimaryKeys($oldIndikatorKinerja->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOldIndikatorKinerjaRelatedById() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldIndikatorKinerja or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldIndikatorKinerjaRelatedById relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function joinOldIndikatorKinerjaRelatedById($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldIndikatorKinerjaRelatedById');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldIndikatorKinerjaRelatedById');
        }

        return $this;
    }

    /**
     * Use the OldIndikatorKinerjaRelatedById relation OldIndikatorKinerja object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldIndikatorKinerjaQuery A secondary query class using the current class as primary query
     */
    public function useOldIndikatorKinerjaRelatedByIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldIndikatorKinerjaRelatedById($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldIndikatorKinerjaRelatedById', '\CoreBundle\Model\OldEperformance\OldIndikatorKinerjaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildOldIndikatorKinerja $oldIndikatorKinerja Object to remove from the list of results
     *
     * @return $this|ChildOldIndikatorKinerjaQuery The current query, for fluid interface
     */
    public function prune($oldIndikatorKinerja = null)
    {
        if ($oldIndikatorKinerja) {
            $this->addUsingAlias(OldIndikatorKinerjaTableMap::COL_ID, $oldIndikatorKinerja->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the eperformance.indikator_kinerja table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldIndikatorKinerjaTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            OldIndikatorKinerjaTableMap::clearInstancePool();
            OldIndikatorKinerjaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldIndikatorKinerjaTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(OldIndikatorKinerjaTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            OldIndikatorKinerjaTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            OldIndikatorKinerjaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 7200)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(OldIndikatorKinerjaTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(OldIndikatorKinerjaTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }


        return $con->getDataFetcher($stmt);
    }

} // OldIndikatorKinerjaQuery

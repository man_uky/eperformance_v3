<?php

namespace CoreBundle\Model\OldEperformance\Base;

use \Exception;
use \PDO;
use CoreBundle\Model\OldEperformance\OldPerilakuHasil as ChildOldPerilakuHasil;
use CoreBundle\Model\OldEperformance\OldPerilakuHasilQuery as ChildOldPerilakuHasilQuery;
use CoreBundle\Model\OldEperformance\Map\OldPerilakuHasilTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'eperformance.skp_perilaku_kerja_hasil' table.
 *
 *
 *
 * @method     ChildOldPerilakuHasilQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildOldPerilakuHasilQuery orderByPegawaiId($order = Criteria::ASC) Order by the pegawai_id column
 * @method     ChildOldPerilakuHasilQuery orderByPerilakuTesId($order = Criteria::ASC) Order by the skp_perilaku_kerja_tes_id column
 * @method     ChildOldPerilakuHasilQuery orderByPerilakuAspekId($order = Criteria::ASC) Order by the skp_perilaku_kerja_aspek_id column
 * @method     ChildOldPerilakuHasilQuery orderByMean($order = Criteria::ASC) Order by the mean column
 * @method     ChildOldPerilakuHasilQuery orderByStandarDeviasi($order = Criteria::ASC) Order by the standar_deviasi column
 * @method     ChildOldPerilakuHasilQuery orderByIcc($order = Criteria::ASC) Order by the icc column
 * @method     ChildOldPerilakuHasilQuery orderByRsp($order = Criteria::ASC) Order by the rsp column
 * @method     ChildOldPerilakuHasilQuery orderByRsrk($order = Criteria::ASC) Order by the rsrk column
 * @method     ChildOldPerilakuHasilQuery orderBySpk($order = Criteria::ASC) Order by the spk column
 *
 * @method     ChildOldPerilakuHasilQuery groupById() Group by the id column
 * @method     ChildOldPerilakuHasilQuery groupByPegawaiId() Group by the pegawai_id column
 * @method     ChildOldPerilakuHasilQuery groupByPerilakuTesId() Group by the skp_perilaku_kerja_tes_id column
 * @method     ChildOldPerilakuHasilQuery groupByPerilakuAspekId() Group by the skp_perilaku_kerja_aspek_id column
 * @method     ChildOldPerilakuHasilQuery groupByMean() Group by the mean column
 * @method     ChildOldPerilakuHasilQuery groupByStandarDeviasi() Group by the standar_deviasi column
 * @method     ChildOldPerilakuHasilQuery groupByIcc() Group by the icc column
 * @method     ChildOldPerilakuHasilQuery groupByRsp() Group by the rsp column
 * @method     ChildOldPerilakuHasilQuery groupByRsrk() Group by the rsrk column
 * @method     ChildOldPerilakuHasilQuery groupBySpk() Group by the spk column
 *
 * @method     ChildOldPerilakuHasilQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildOldPerilakuHasilQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildOldPerilakuHasilQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildOldPerilakuHasilQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildOldPerilakuHasilQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildOldPerilakuHasilQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildOldPerilakuHasilQuery leftJoinOldPegawai($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldPegawai relation
 * @method     ChildOldPerilakuHasilQuery rightJoinOldPegawai($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldPegawai relation
 * @method     ChildOldPerilakuHasilQuery innerJoinOldPegawai($relationAlias = null) Adds a INNER JOIN clause to the query using the OldPegawai relation
 *
 * @method     ChildOldPerilakuHasilQuery joinWithOldPegawai($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldPegawai relation
 *
 * @method     ChildOldPerilakuHasilQuery leftJoinWithOldPegawai() Adds a LEFT JOIN clause and with to the query using the OldPegawai relation
 * @method     ChildOldPerilakuHasilQuery rightJoinWithOldPegawai() Adds a RIGHT JOIN clause and with to the query using the OldPegawai relation
 * @method     ChildOldPerilakuHasilQuery innerJoinWithOldPegawai() Adds a INNER JOIN clause and with to the query using the OldPegawai relation
 *
 * @method     ChildOldPerilakuHasilQuery leftJoinOldPerilakuTes($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldPerilakuTes relation
 * @method     ChildOldPerilakuHasilQuery rightJoinOldPerilakuTes($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldPerilakuTes relation
 * @method     ChildOldPerilakuHasilQuery innerJoinOldPerilakuTes($relationAlias = null) Adds a INNER JOIN clause to the query using the OldPerilakuTes relation
 *
 * @method     ChildOldPerilakuHasilQuery joinWithOldPerilakuTes($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldPerilakuTes relation
 *
 * @method     ChildOldPerilakuHasilQuery leftJoinWithOldPerilakuTes() Adds a LEFT JOIN clause and with to the query using the OldPerilakuTes relation
 * @method     ChildOldPerilakuHasilQuery rightJoinWithOldPerilakuTes() Adds a RIGHT JOIN clause and with to the query using the OldPerilakuTes relation
 * @method     ChildOldPerilakuHasilQuery innerJoinWithOldPerilakuTes() Adds a INNER JOIN clause and with to the query using the OldPerilakuTes relation
 *
 * @method     ChildOldPerilakuHasilQuery leftJoinOldPerilakuAspek($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldPerilakuAspek relation
 * @method     ChildOldPerilakuHasilQuery rightJoinOldPerilakuAspek($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldPerilakuAspek relation
 * @method     ChildOldPerilakuHasilQuery innerJoinOldPerilakuAspek($relationAlias = null) Adds a INNER JOIN clause to the query using the OldPerilakuAspek relation
 *
 * @method     ChildOldPerilakuHasilQuery joinWithOldPerilakuAspek($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldPerilakuAspek relation
 *
 * @method     ChildOldPerilakuHasilQuery leftJoinWithOldPerilakuAspek() Adds a LEFT JOIN clause and with to the query using the OldPerilakuAspek relation
 * @method     ChildOldPerilakuHasilQuery rightJoinWithOldPerilakuAspek() Adds a RIGHT JOIN clause and with to the query using the OldPerilakuAspek relation
 * @method     ChildOldPerilakuHasilQuery innerJoinWithOldPerilakuAspek() Adds a INNER JOIN clause and with to the query using the OldPerilakuAspek relation
 *
 * @method     \CoreBundle\Model\OldEperformance\OldPegawaiQuery|\CoreBundle\Model\OldEperformance\OldPerilakuTesQuery|\CoreBundle\Model\OldEperformance\OldPerilakuAspekQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildOldPerilakuHasil findOne(ConnectionInterface $con = null) Return the first ChildOldPerilakuHasil matching the query
 * @method     ChildOldPerilakuHasil findOneOrCreate(ConnectionInterface $con = null) Return the first ChildOldPerilakuHasil matching the query, or a new ChildOldPerilakuHasil object populated from the query conditions when no match is found
 *
 * @method     ChildOldPerilakuHasil findOneById(int $id) Return the first ChildOldPerilakuHasil filtered by the id column
 * @method     ChildOldPerilakuHasil findOneByPegawaiId(int $pegawai_id) Return the first ChildOldPerilakuHasil filtered by the pegawai_id column
 * @method     ChildOldPerilakuHasil findOneByPerilakuTesId(int $skp_perilaku_kerja_tes_id) Return the first ChildOldPerilakuHasil filtered by the skp_perilaku_kerja_tes_id column
 * @method     ChildOldPerilakuHasil findOneByPerilakuAspekId(int $skp_perilaku_kerja_aspek_id) Return the first ChildOldPerilakuHasil filtered by the skp_perilaku_kerja_aspek_id column
 * @method     ChildOldPerilakuHasil findOneByMean(string $mean) Return the first ChildOldPerilakuHasil filtered by the mean column
 * @method     ChildOldPerilakuHasil findOneByStandarDeviasi(string $standar_deviasi) Return the first ChildOldPerilakuHasil filtered by the standar_deviasi column
 * @method     ChildOldPerilakuHasil findOneByIcc(string $icc) Return the first ChildOldPerilakuHasil filtered by the icc column
 * @method     ChildOldPerilakuHasil findOneByRsp(string $rsp) Return the first ChildOldPerilakuHasil filtered by the rsp column
 * @method     ChildOldPerilakuHasil findOneByRsrk(string $rsrk) Return the first ChildOldPerilakuHasil filtered by the rsrk column
 * @method     ChildOldPerilakuHasil findOneBySpk(string $spk) Return the first ChildOldPerilakuHasil filtered by the spk column *

 * @method     ChildOldPerilakuHasil requirePk($key, ConnectionInterface $con = null) Return the ChildOldPerilakuHasil by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPerilakuHasil requireOne(ConnectionInterface $con = null) Return the first ChildOldPerilakuHasil matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOldPerilakuHasil requireOneById(int $id) Return the first ChildOldPerilakuHasil filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPerilakuHasil requireOneByPegawaiId(int $pegawai_id) Return the first ChildOldPerilakuHasil filtered by the pegawai_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPerilakuHasil requireOneByPerilakuTesId(int $skp_perilaku_kerja_tes_id) Return the first ChildOldPerilakuHasil filtered by the skp_perilaku_kerja_tes_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPerilakuHasil requireOneByPerilakuAspekId(int $skp_perilaku_kerja_aspek_id) Return the first ChildOldPerilakuHasil filtered by the skp_perilaku_kerja_aspek_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPerilakuHasil requireOneByMean(string $mean) Return the first ChildOldPerilakuHasil filtered by the mean column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPerilakuHasil requireOneByStandarDeviasi(string $standar_deviasi) Return the first ChildOldPerilakuHasil filtered by the standar_deviasi column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPerilakuHasil requireOneByIcc(string $icc) Return the first ChildOldPerilakuHasil filtered by the icc column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPerilakuHasil requireOneByRsp(string $rsp) Return the first ChildOldPerilakuHasil filtered by the rsp column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPerilakuHasil requireOneByRsrk(string $rsrk) Return the first ChildOldPerilakuHasil filtered by the rsrk column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPerilakuHasil requireOneBySpk(string $spk) Return the first ChildOldPerilakuHasil filtered by the spk column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOldPerilakuHasil[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildOldPerilakuHasil objects based on current ModelCriteria
 * @method     ChildOldPerilakuHasil[]|ObjectCollection findById(int $id) Return ChildOldPerilakuHasil objects filtered by the id column
 * @method     ChildOldPerilakuHasil[]|ObjectCollection findByPegawaiId(int $pegawai_id) Return ChildOldPerilakuHasil objects filtered by the pegawai_id column
 * @method     ChildOldPerilakuHasil[]|ObjectCollection findByPerilakuTesId(int $skp_perilaku_kerja_tes_id) Return ChildOldPerilakuHasil objects filtered by the skp_perilaku_kerja_tes_id column
 * @method     ChildOldPerilakuHasil[]|ObjectCollection findByPerilakuAspekId(int $skp_perilaku_kerja_aspek_id) Return ChildOldPerilakuHasil objects filtered by the skp_perilaku_kerja_aspek_id column
 * @method     ChildOldPerilakuHasil[]|ObjectCollection findByMean(string $mean) Return ChildOldPerilakuHasil objects filtered by the mean column
 * @method     ChildOldPerilakuHasil[]|ObjectCollection findByStandarDeviasi(string $standar_deviasi) Return ChildOldPerilakuHasil objects filtered by the standar_deviasi column
 * @method     ChildOldPerilakuHasil[]|ObjectCollection findByIcc(string $icc) Return ChildOldPerilakuHasil objects filtered by the icc column
 * @method     ChildOldPerilakuHasil[]|ObjectCollection findByRsp(string $rsp) Return ChildOldPerilakuHasil objects filtered by the rsp column
 * @method     ChildOldPerilakuHasil[]|ObjectCollection findByRsrk(string $rsrk) Return ChildOldPerilakuHasil objects filtered by the rsrk column
 * @method     ChildOldPerilakuHasil[]|ObjectCollection findBySpk(string $spk) Return ChildOldPerilakuHasil objects filtered by the spk column
 * @method     ChildOldPerilakuHasil[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class OldPerilakuHasilQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \CoreBundle\Model\OldEperformance\Base\OldPerilakuHasilQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'old_eperformance', $modelName = '\\CoreBundle\\Model\\OldEperformance\\OldPerilakuHasil', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildOldPerilakuHasilQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildOldPerilakuHasilQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildOldPerilakuHasilQuery) {
            return $criteria;
        }
        $query = new ChildOldPerilakuHasilQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildOldPerilakuHasil|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(OldPerilakuHasilTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = OldPerilakuHasilTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldPerilakuHasil A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, pegawai_id, skp_perilaku_kerja_tes_id, skp_perilaku_kerja_aspek_id, mean, standar_deviasi, icc, rsp, rsrk, spk FROM eperformance.skp_perilaku_kerja_hasil WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildOldPerilakuHasil $obj */
            $obj = new ChildOldPerilakuHasil();
            $obj->hydrate($row);
            OldPerilakuHasilTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildOldPerilakuHasil|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildOldPerilakuHasilQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(OldPerilakuHasilTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildOldPerilakuHasilQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(OldPerilakuHasilTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPerilakuHasilQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(OldPerilakuHasilTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(OldPerilakuHasilTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPerilakuHasilTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the pegawai_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPegawaiId(1234); // WHERE pegawai_id = 1234
     * $query->filterByPegawaiId(array(12, 34)); // WHERE pegawai_id IN (12, 34)
     * $query->filterByPegawaiId(array('min' => 12)); // WHERE pegawai_id > 12
     * </code>
     *
     * @see       filterByOldPegawai()
     *
     * @param     mixed $pegawaiId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPerilakuHasilQuery The current query, for fluid interface
     */
    public function filterByPegawaiId($pegawaiId = null, $comparison = null)
    {
        if (is_array($pegawaiId)) {
            $useMinMax = false;
            if (isset($pegawaiId['min'])) {
                $this->addUsingAlias(OldPerilakuHasilTableMap::COL_PEGAWAI_ID, $pegawaiId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pegawaiId['max'])) {
                $this->addUsingAlias(OldPerilakuHasilTableMap::COL_PEGAWAI_ID, $pegawaiId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPerilakuHasilTableMap::COL_PEGAWAI_ID, $pegawaiId, $comparison);
    }

    /**
     * Filter the query on the skp_perilaku_kerja_tes_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPerilakuTesId(1234); // WHERE skp_perilaku_kerja_tes_id = 1234
     * $query->filterByPerilakuTesId(array(12, 34)); // WHERE skp_perilaku_kerja_tes_id IN (12, 34)
     * $query->filterByPerilakuTesId(array('min' => 12)); // WHERE skp_perilaku_kerja_tes_id > 12
     * </code>
     *
     * @see       filterByOldPerilakuTes()
     *
     * @param     mixed $perilakuTesId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPerilakuHasilQuery The current query, for fluid interface
     */
    public function filterByPerilakuTesId($perilakuTesId = null, $comparison = null)
    {
        if (is_array($perilakuTesId)) {
            $useMinMax = false;
            if (isset($perilakuTesId['min'])) {
                $this->addUsingAlias(OldPerilakuHasilTableMap::COL_SKP_PERILAKU_KERJA_TES_ID, $perilakuTesId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($perilakuTesId['max'])) {
                $this->addUsingAlias(OldPerilakuHasilTableMap::COL_SKP_PERILAKU_KERJA_TES_ID, $perilakuTesId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPerilakuHasilTableMap::COL_SKP_PERILAKU_KERJA_TES_ID, $perilakuTesId, $comparison);
    }

    /**
     * Filter the query on the skp_perilaku_kerja_aspek_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPerilakuAspekId(1234); // WHERE skp_perilaku_kerja_aspek_id = 1234
     * $query->filterByPerilakuAspekId(array(12, 34)); // WHERE skp_perilaku_kerja_aspek_id IN (12, 34)
     * $query->filterByPerilakuAspekId(array('min' => 12)); // WHERE skp_perilaku_kerja_aspek_id > 12
     * </code>
     *
     * @see       filterByOldPerilakuAspek()
     *
     * @param     mixed $perilakuAspekId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPerilakuHasilQuery The current query, for fluid interface
     */
    public function filterByPerilakuAspekId($perilakuAspekId = null, $comparison = null)
    {
        if (is_array($perilakuAspekId)) {
            $useMinMax = false;
            if (isset($perilakuAspekId['min'])) {
                $this->addUsingAlias(OldPerilakuHasilTableMap::COL_SKP_PERILAKU_KERJA_ASPEK_ID, $perilakuAspekId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($perilakuAspekId['max'])) {
                $this->addUsingAlias(OldPerilakuHasilTableMap::COL_SKP_PERILAKU_KERJA_ASPEK_ID, $perilakuAspekId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPerilakuHasilTableMap::COL_SKP_PERILAKU_KERJA_ASPEK_ID, $perilakuAspekId, $comparison);
    }

    /**
     * Filter the query on the mean column
     *
     * Example usage:
     * <code>
     * $query->filterByMean(1234); // WHERE mean = 1234
     * $query->filterByMean(array(12, 34)); // WHERE mean IN (12, 34)
     * $query->filterByMean(array('min' => 12)); // WHERE mean > 12
     * </code>
     *
     * @param     mixed $mean The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPerilakuHasilQuery The current query, for fluid interface
     */
    public function filterByMean($mean = null, $comparison = null)
    {
        if (is_array($mean)) {
            $useMinMax = false;
            if (isset($mean['min'])) {
                $this->addUsingAlias(OldPerilakuHasilTableMap::COL_MEAN, $mean['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mean['max'])) {
                $this->addUsingAlias(OldPerilakuHasilTableMap::COL_MEAN, $mean['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPerilakuHasilTableMap::COL_MEAN, $mean, $comparison);
    }

    /**
     * Filter the query on the standar_deviasi column
     *
     * Example usage:
     * <code>
     * $query->filterByStandarDeviasi(1234); // WHERE standar_deviasi = 1234
     * $query->filterByStandarDeviasi(array(12, 34)); // WHERE standar_deviasi IN (12, 34)
     * $query->filterByStandarDeviasi(array('min' => 12)); // WHERE standar_deviasi > 12
     * </code>
     *
     * @param     mixed $standarDeviasi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPerilakuHasilQuery The current query, for fluid interface
     */
    public function filterByStandarDeviasi($standarDeviasi = null, $comparison = null)
    {
        if (is_array($standarDeviasi)) {
            $useMinMax = false;
            if (isset($standarDeviasi['min'])) {
                $this->addUsingAlias(OldPerilakuHasilTableMap::COL_STANDAR_DEVIASI, $standarDeviasi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($standarDeviasi['max'])) {
                $this->addUsingAlias(OldPerilakuHasilTableMap::COL_STANDAR_DEVIASI, $standarDeviasi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPerilakuHasilTableMap::COL_STANDAR_DEVIASI, $standarDeviasi, $comparison);
    }

    /**
     * Filter the query on the icc column
     *
     * Example usage:
     * <code>
     * $query->filterByIcc(1234); // WHERE icc = 1234
     * $query->filterByIcc(array(12, 34)); // WHERE icc IN (12, 34)
     * $query->filterByIcc(array('min' => 12)); // WHERE icc > 12
     * </code>
     *
     * @param     mixed $icc The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPerilakuHasilQuery The current query, for fluid interface
     */
    public function filterByIcc($icc = null, $comparison = null)
    {
        if (is_array($icc)) {
            $useMinMax = false;
            if (isset($icc['min'])) {
                $this->addUsingAlias(OldPerilakuHasilTableMap::COL_ICC, $icc['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($icc['max'])) {
                $this->addUsingAlias(OldPerilakuHasilTableMap::COL_ICC, $icc['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPerilakuHasilTableMap::COL_ICC, $icc, $comparison);
    }

    /**
     * Filter the query on the rsp column
     *
     * Example usage:
     * <code>
     * $query->filterByRsp(1234); // WHERE rsp = 1234
     * $query->filterByRsp(array(12, 34)); // WHERE rsp IN (12, 34)
     * $query->filterByRsp(array('min' => 12)); // WHERE rsp > 12
     * </code>
     *
     * @param     mixed $rsp The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPerilakuHasilQuery The current query, for fluid interface
     */
    public function filterByRsp($rsp = null, $comparison = null)
    {
        if (is_array($rsp)) {
            $useMinMax = false;
            if (isset($rsp['min'])) {
                $this->addUsingAlias(OldPerilakuHasilTableMap::COL_RSP, $rsp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rsp['max'])) {
                $this->addUsingAlias(OldPerilakuHasilTableMap::COL_RSP, $rsp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPerilakuHasilTableMap::COL_RSP, $rsp, $comparison);
    }

    /**
     * Filter the query on the rsrk column
     *
     * Example usage:
     * <code>
     * $query->filterByRsrk(1234); // WHERE rsrk = 1234
     * $query->filterByRsrk(array(12, 34)); // WHERE rsrk IN (12, 34)
     * $query->filterByRsrk(array('min' => 12)); // WHERE rsrk > 12
     * </code>
     *
     * @param     mixed $rsrk The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPerilakuHasilQuery The current query, for fluid interface
     */
    public function filterByRsrk($rsrk = null, $comparison = null)
    {
        if (is_array($rsrk)) {
            $useMinMax = false;
            if (isset($rsrk['min'])) {
                $this->addUsingAlias(OldPerilakuHasilTableMap::COL_RSRK, $rsrk['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rsrk['max'])) {
                $this->addUsingAlias(OldPerilakuHasilTableMap::COL_RSRK, $rsrk['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPerilakuHasilTableMap::COL_RSRK, $rsrk, $comparison);
    }

    /**
     * Filter the query on the spk column
     *
     * Example usage:
     * <code>
     * $query->filterBySpk(1234); // WHERE spk = 1234
     * $query->filterBySpk(array(12, 34)); // WHERE spk IN (12, 34)
     * $query->filterBySpk(array('min' => 12)); // WHERE spk > 12
     * </code>
     *
     * @param     mixed $spk The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPerilakuHasilQuery The current query, for fluid interface
     */
    public function filterBySpk($spk = null, $comparison = null)
    {
        if (is_array($spk)) {
            $useMinMax = false;
            if (isset($spk['min'])) {
                $this->addUsingAlias(OldPerilakuHasilTableMap::COL_SPK, $spk['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($spk['max'])) {
                $this->addUsingAlias(OldPerilakuHasilTableMap::COL_SPK, $spk['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPerilakuHasilTableMap::COL_SPK, $spk, $comparison);
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldPegawai object
     *
     * @param \CoreBundle\Model\OldEperformance\OldPegawai|ObjectCollection $oldPegawai The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldPerilakuHasilQuery The current query, for fluid interface
     */
    public function filterByOldPegawai($oldPegawai, $comparison = null)
    {
        if ($oldPegawai instanceof \CoreBundle\Model\OldEperformance\OldPegawai) {
            return $this
                ->addUsingAlias(OldPerilakuHasilTableMap::COL_PEGAWAI_ID, $oldPegawai->getId(), $comparison);
        } elseif ($oldPegawai instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OldPerilakuHasilTableMap::COL_PEGAWAI_ID, $oldPegawai->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByOldPegawai() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldPegawai or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldPegawai relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldPerilakuHasilQuery The current query, for fluid interface
     */
    public function joinOldPegawai($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldPegawai');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldPegawai');
        }

        return $this;
    }

    /**
     * Use the OldPegawai relation OldPegawai object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldPegawaiQuery A secondary query class using the current class as primary query
     */
    public function useOldPegawaiQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldPegawai($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldPegawai', '\CoreBundle\Model\OldEperformance\OldPegawaiQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldPerilakuTes object
     *
     * @param \CoreBundle\Model\OldEperformance\OldPerilakuTes|ObjectCollection $oldPerilakuTes The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldPerilakuHasilQuery The current query, for fluid interface
     */
    public function filterByOldPerilakuTes($oldPerilakuTes, $comparison = null)
    {
        if ($oldPerilakuTes instanceof \CoreBundle\Model\OldEperformance\OldPerilakuTes) {
            return $this
                ->addUsingAlias(OldPerilakuHasilTableMap::COL_SKP_PERILAKU_KERJA_TES_ID, $oldPerilakuTes->getId(), $comparison);
        } elseif ($oldPerilakuTes instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OldPerilakuHasilTableMap::COL_SKP_PERILAKU_KERJA_TES_ID, $oldPerilakuTes->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByOldPerilakuTes() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldPerilakuTes or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldPerilakuTes relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldPerilakuHasilQuery The current query, for fluid interface
     */
    public function joinOldPerilakuTes($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldPerilakuTes');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldPerilakuTes');
        }

        return $this;
    }

    /**
     * Use the OldPerilakuTes relation OldPerilakuTes object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldPerilakuTesQuery A secondary query class using the current class as primary query
     */
    public function useOldPerilakuTesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldPerilakuTes($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldPerilakuTes', '\CoreBundle\Model\OldEperformance\OldPerilakuTesQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldPerilakuAspek object
     *
     * @param \CoreBundle\Model\OldEperformance\OldPerilakuAspek|ObjectCollection $oldPerilakuAspek The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldPerilakuHasilQuery The current query, for fluid interface
     */
    public function filterByOldPerilakuAspek($oldPerilakuAspek, $comparison = null)
    {
        if ($oldPerilakuAspek instanceof \CoreBundle\Model\OldEperformance\OldPerilakuAspek) {
            return $this
                ->addUsingAlias(OldPerilakuHasilTableMap::COL_SKP_PERILAKU_KERJA_ASPEK_ID, $oldPerilakuAspek->getId(), $comparison);
        } elseif ($oldPerilakuAspek instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OldPerilakuHasilTableMap::COL_SKP_PERILAKU_KERJA_ASPEK_ID, $oldPerilakuAspek->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByOldPerilakuAspek() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldPerilakuAspek or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldPerilakuAspek relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldPerilakuHasilQuery The current query, for fluid interface
     */
    public function joinOldPerilakuAspek($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldPerilakuAspek');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldPerilakuAspek');
        }

        return $this;
    }

    /**
     * Use the OldPerilakuAspek relation OldPerilakuAspek object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldPerilakuAspekQuery A secondary query class using the current class as primary query
     */
    public function useOldPerilakuAspekQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldPerilakuAspek($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldPerilakuAspek', '\CoreBundle\Model\OldEperformance\OldPerilakuAspekQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildOldPerilakuHasil $oldPerilakuHasil Object to remove from the list of results
     *
     * @return $this|ChildOldPerilakuHasilQuery The current query, for fluid interface
     */
    public function prune($oldPerilakuHasil = null)
    {
        if ($oldPerilakuHasil) {
            $this->addUsingAlias(OldPerilakuHasilTableMap::COL_ID, $oldPerilakuHasil->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the eperformance.skp_perilaku_kerja_hasil table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldPerilakuHasilTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            OldPerilakuHasilTableMap::clearInstancePool();
            OldPerilakuHasilTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldPerilakuHasilTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(OldPerilakuHasilTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            OldPerilakuHasilTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            OldPerilakuHasilTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 7200)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(OldPerilakuHasilTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(OldPerilakuHasilTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }


        return $con->getDataFetcher($stmt);
    }

} // OldPerilakuHasilQuery

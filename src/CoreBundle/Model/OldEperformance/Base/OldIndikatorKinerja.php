<?php

namespace CoreBundle\Model\OldEperformance\Base;

use \DateTime;
use \Exception;
use \PDO;
use CoreBundle\Model\OldEperformance\OldIndikatorKinerja as ChildOldIndikatorKinerja;
use CoreBundle\Model\OldEperformance\OldIndikatorKinerjaQuery as ChildOldIndikatorKinerjaQuery;
use CoreBundle\Model\OldEperformance\OldMasterSkpd as ChildOldMasterSkpd;
use CoreBundle\Model\OldEperformance\OldMasterSkpdQuery as ChildOldMasterSkpdQuery;
use CoreBundle\Model\OldEperformance\OldPegawai as ChildOldPegawai;
use CoreBundle\Model\OldEperformance\OldPegawaiQuery as ChildOldPegawaiQuery;
use CoreBundle\Model\OldEperformance\Map\OldIndikatorKinerjaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'eperformance.indikator_kinerja' table.
 *
 *
 *
 * @package    propel.generator.src.CoreBundle.Model.OldEperformance.Base
 */
abstract class OldIndikatorKinerja implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\CoreBundle\\Model\\OldEperformance\\Map\\OldIndikatorKinerjaTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the pegawai_id field.
     *
     * @var        int
     */
    protected $pegawai_id;

    /**
     * The value for the nama field.
     *
     * @var        string
     */
    protected $nama;

    /**
     * The value for the deskripsi field.
     *
     * @var        string
     */
    protected $deskripsi;

    /**
     * The value for the status field.
     * 0: belum disetujui
     * @var        int
     */
    protected $status;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * The value for the deleted_at field.
     *
     * @var        DateTime
     */
    protected $deleted_at;

    /**
     * The value for the skpd_id field.
     *
     * @var        int
     */
    protected $skpd_id;

    /**
     * The value for the target field.
     *
     * @var        string
     */
    protected $target;

    /**
     * The value for the satuan field.
     *
     * @var        string
     */
    protected $satuan;

    /**
     * The value for the target_kemarin field.
     *
     * @var        string
     */
    protected $target_kemarin;

    /**
     * The value for the cara_perhitungan field.
     *
     * @var        string
     */
    protected $cara_perhitungan;

    /**
     * The value for the capaian_akhir field.
     *
     * @var        string
     */
    protected $capaian_akhir;

    /**
     * The value for the jenis field.
     * 1=IKU, 2=IKK, 3=IKT, 4=IKS
     * @var        int
     */
    protected $jenis;

    /**
     * The value for the indikator_id_atasan field.
     *
     * @var        int
     */
    protected $indikator_id_atasan;

    /**
     * The value for the is_tambahan field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_tambahan;

    /**
     * The value for the is_tahun_lalu field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_tahun_lalu;

    /**
     * The value for the is_tarik_ebudgeting field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_tarik_ebudgeting;

    /**
     * The value for the is_perhitungan_terbalik field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_perhitungan_terbalik;

    /**
     * The value for the is_realisasi_angka_prediksi field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_realisasi_angka_prediksi;

    /**
     * @var        ChildOldPegawai
     */
    protected $aOldPegawai;

    /**
     * @var        ChildOldMasterSkpd
     */
    protected $aOldMasterSkpd;

    /**
     * @var        ChildOldIndikatorKinerja
     */
    protected $aOldIndikatorKinerja;

    /**
     * @var        ObjectCollection|ChildOldIndikatorKinerja[] Collection to store aggregation of ChildOldIndikatorKinerja objects.
     */
    protected $collOldIndikatorKinerjasRelatedById;
    protected $collOldIndikatorKinerjasRelatedByIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildOldIndikatorKinerja[]
     */
    protected $oldIndikatorKinerjasRelatedByIdScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->is_tambahan = false;
        $this->is_tahun_lalu = false;
        $this->is_tarik_ebudgeting = false;
        $this->is_perhitungan_terbalik = false;
        $this->is_realisasi_angka_prediksi = false;
    }

    /**
     * Initializes internal state of CoreBundle\Model\OldEperformance\Base\OldIndikatorKinerja object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>OldIndikatorKinerja</code> instance.  If
     * <code>obj</code> is an instance of <code>OldIndikatorKinerja</code>, delegates to
     * <code>equals(OldIndikatorKinerja)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|OldIndikatorKinerja The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [pegawai_id] column value.
     *
     * @return int
     */
    public function getPegawaiId()
    {
        return $this->pegawai_id;
    }

    /**
     * Get the [nama] column value.
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [deskripsi] column value.
     *
     * @return string
     */
    public function getDeskripsi()
    {
        return $this->deskripsi;
    }

    /**
     * Get the [status] column value.
     * 0: belum disetujui
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [deleted_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDeletedAt($format = NULL)
    {
        if ($format === null) {
            return $this->deleted_at;
        } else {
            return $this->deleted_at instanceof \DateTimeInterface ? $this->deleted_at->format($format) : null;
        }
    }

    /**
     * Get the [skpd_id] column value.
     *
     * @return int
     */
    public function getSkpdId()
    {
        return $this->skpd_id;
    }

    /**
     * Get the [target] column value.
     *
     * @return string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Get the [satuan] column value.
     *
     * @return string
     */
    public function getSatuan()
    {
        return $this->satuan;
    }

    /**
     * Get the [target_kemarin] column value.
     *
     * @return string
     */
    public function getTargetKemarin()
    {
        return $this->target_kemarin;
    }

    /**
     * Get the [cara_perhitungan] column value.
     *
     * @return string
     */
    public function getCaraPerhitungan()
    {
        return $this->cara_perhitungan;
    }

    /**
     * Get the [capaian_akhir] column value.
     *
     * @return string
     */
    public function getCapaianAkhir()
    {
        return $this->capaian_akhir;
    }

    /**
     * Get the [jenis] column value.
     * 1=IKU, 2=IKK, 3=IKT, 4=IKS
     * @return int
     */
    public function getJenis()
    {
        return $this->jenis;
    }

    /**
     * Get the [indikator_id_atasan] column value.
     *
     * @return int
     */
    public function getIndikatorIdAtasan()
    {
        return $this->indikator_id_atasan;
    }

    /**
     * Get the [is_tambahan] column value.
     *
     * @return boolean
     */
    public function getIsTambahan()
    {
        return $this->is_tambahan;
    }

    /**
     * Get the [is_tambahan] column value.
     *
     * @return boolean
     */
    public function isTambahan()
    {
        return $this->getIsTambahan();
    }

    /**
     * Get the [is_tahun_lalu] column value.
     *
     * @return boolean
     */
    public function getIsTahunLalu()
    {
        return $this->is_tahun_lalu;
    }

    /**
     * Get the [is_tahun_lalu] column value.
     *
     * @return boolean
     */
    public function isTahunLalu()
    {
        return $this->getIsTahunLalu();
    }

    /**
     * Get the [is_tarik_ebudgeting] column value.
     *
     * @return boolean
     */
    public function getIsTarikEbudgeting()
    {
        return $this->is_tarik_ebudgeting;
    }

    /**
     * Get the [is_tarik_ebudgeting] column value.
     *
     * @return boolean
     */
    public function isTarikEbudgeting()
    {
        return $this->getIsTarikEbudgeting();
    }

    /**
     * Get the [is_perhitungan_terbalik] column value.
     *
     * @return boolean
     */
    public function getIsPerhitunganTerbalik()
    {
        return $this->is_perhitungan_terbalik;
    }

    /**
     * Get the [is_perhitungan_terbalik] column value.
     *
     * @return boolean
     */
    public function isPerhitunganTerbalik()
    {
        return $this->getIsPerhitunganTerbalik();
    }

    /**
     * Get the [is_realisasi_angka_prediksi] column value.
     *
     * @return boolean
     */
    public function getIsRealisasiAngkaPrediksi()
    {
        return $this->is_realisasi_angka_prediksi;
    }

    /**
     * Get the [is_realisasi_angka_prediksi] column value.
     *
     * @return boolean
     */
    public function isRealisasiAngkaPrediksi()
    {
        return $this->getIsRealisasiAngkaPrediksi();
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [pegawai_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setPegawaiId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->pegawai_id !== $v) {
            $this->pegawai_id = $v;
            $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_PEGAWAI_ID] = true;
        }

        if ($this->aOldPegawai !== null && $this->aOldPegawai->getId() !== $v) {
            $this->aOldPegawai = null;
        }

        return $this;
    } // setPegawaiId()

    /**
     * Set the value of [nama] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_NAMA] = true;
        }

        return $this;
    } // setNama()

    /**
     * Set the value of [deskripsi] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setDeskripsi($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->deskripsi !== $v) {
            $this->deskripsi = $v;
            $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_DESKRIPSI] = true;
        }

        return $this;
    } // setDeskripsi()

    /**
     * Set the value of [status] column.
     * 0: belum disetujui
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_STATUS] = true;
        }

        return $this;
    } // setStatus()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Sets the value of [deleted_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setDeletedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->deleted_at !== null || $dt !== null) {
            if ($this->deleted_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->deleted_at->format("Y-m-d H:i:s.u")) {
                $this->deleted_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_DELETED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setDeletedAt()

    /**
     * Set the value of [skpd_id] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setSkpdId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->skpd_id !== $v) {
            $this->skpd_id = $v;
            $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_SKPD_ID] = true;
        }

        if ($this->aOldMasterSkpd !== null && $this->aOldMasterSkpd->getSkpdId() !== $v) {
            $this->aOldMasterSkpd = null;
        }

        return $this;
    } // setSkpdId()

    /**
     * Set the value of [target] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setTarget($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->target !== $v) {
            $this->target = $v;
            $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_TARGET] = true;
        }

        return $this;
    } // setTarget()

    /**
     * Set the value of [satuan] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setSatuan($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->satuan !== $v) {
            $this->satuan = $v;
            $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_SATUAN] = true;
        }

        return $this;
    } // setSatuan()

    /**
     * Set the value of [target_kemarin] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setTargetKemarin($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->target_kemarin !== $v) {
            $this->target_kemarin = $v;
            $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_TARGET_KEMARIN] = true;
        }

        return $this;
    } // setTargetKemarin()

    /**
     * Set the value of [cara_perhitungan] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setCaraPerhitungan($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cara_perhitungan !== $v) {
            $this->cara_perhitungan = $v;
            $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_CARA_PERHITUNGAN] = true;
        }

        return $this;
    } // setCaraPerhitungan()

    /**
     * Set the value of [capaian_akhir] column.
     *
     * @param string $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setCapaianAkhir($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->capaian_akhir !== $v) {
            $this->capaian_akhir = $v;
            $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_CAPAIAN_AKHIR] = true;
        }

        return $this;
    } // setCapaianAkhir()

    /**
     * Set the value of [jenis] column.
     * 1=IKU, 2=IKK, 3=IKT, 4=IKS
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setJenis($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->jenis !== $v) {
            $this->jenis = $v;
            $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_JENIS] = true;
        }

        return $this;
    } // setJenis()

    /**
     * Set the value of [indikator_id_atasan] column.
     *
     * @param int $v new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setIndikatorIdAtasan($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->indikator_id_atasan !== $v) {
            $this->indikator_id_atasan = $v;
            $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_INDIKATOR_ID_ATASAN] = true;
        }

        if ($this->aOldIndikatorKinerja !== null && $this->aOldIndikatorKinerja->getId() !== $v) {
            $this->aOldIndikatorKinerja = null;
        }

        return $this;
    } // setIndikatorIdAtasan()

    /**
     * Sets the value of the [is_tambahan] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setIsTambahan($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_tambahan !== $v) {
            $this->is_tambahan = $v;
            $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_IS_TAMBAHAN] = true;
        }

        return $this;
    } // setIsTambahan()

    /**
     * Sets the value of the [is_tahun_lalu] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setIsTahunLalu($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_tahun_lalu !== $v) {
            $this->is_tahun_lalu = $v;
            $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_IS_TAHUN_LALU] = true;
        }

        return $this;
    } // setIsTahunLalu()

    /**
     * Sets the value of the [is_tarik_ebudgeting] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setIsTarikEbudgeting($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_tarik_ebudgeting !== $v) {
            $this->is_tarik_ebudgeting = $v;
            $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_IS_TARIK_EBUDGETING] = true;
        }

        return $this;
    } // setIsTarikEbudgeting()

    /**
     * Sets the value of the [is_perhitungan_terbalik] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setIsPerhitunganTerbalik($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_perhitungan_terbalik !== $v) {
            $this->is_perhitungan_terbalik = $v;
            $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_IS_PERHITUNGAN_TERBALIK] = true;
        }

        return $this;
    } // setIsPerhitunganTerbalik()

    /**
     * Sets the value of the [is_realisasi_angka_prediksi] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function setIsRealisasiAngkaPrediksi($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_realisasi_angka_prediksi !== $v) {
            $this->is_realisasi_angka_prediksi = $v;
            $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_IS_REALISASI_ANGKA_PREDIKSI] = true;
        }

        return $this;
    } // setIsRealisasiAngkaPrediksi()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->is_tambahan !== false) {
                return false;
            }

            if ($this->is_tahun_lalu !== false) {
                return false;
            }

            if ($this->is_tarik_ebudgeting !== false) {
                return false;
            }

            if ($this->is_perhitungan_terbalik !== false) {
                return false;
            }

            if ($this->is_realisasi_angka_prediksi !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('PegawaiId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pegawai_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('Nama', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nama = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('Deskripsi', TableMap::TYPE_PHPNAME, $indexType)];
            $this->deskripsi = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('Status', TableMap::TYPE_PHPNAME, $indexType)];
            $this->status = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('DeletedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->deleted_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('SkpdId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->skpd_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('Target', TableMap::TYPE_PHPNAME, $indexType)];
            $this->target = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('Satuan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->satuan = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('TargetKemarin', TableMap::TYPE_PHPNAME, $indexType)];
            $this->target_kemarin = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('CaraPerhitungan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cara_perhitungan = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('CapaianAkhir', TableMap::TYPE_PHPNAME, $indexType)];
            $this->capaian_akhir = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('Jenis', TableMap::TYPE_PHPNAME, $indexType)];
            $this->jenis = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('IndikatorIdAtasan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->indikator_id_atasan = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('IsTambahan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_tambahan = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('IsTahunLalu', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_tahun_lalu = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('IsTarikEbudgeting', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_tarik_ebudgeting = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('IsPerhitunganTerbalik', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_perhitungan_terbalik = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : OldIndikatorKinerjaTableMap::translateFieldName('IsRealisasiAngkaPrediksi', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_realisasi_angka_prediksi = (null !== $col) ? (boolean) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 21; // 21 = OldIndikatorKinerjaTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\CoreBundle\\Model\\OldEperformance\\OldIndikatorKinerja'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aOldPegawai !== null && $this->pegawai_id !== $this->aOldPegawai->getId()) {
            $this->aOldPegawai = null;
        }
        if ($this->aOldMasterSkpd !== null && $this->skpd_id !== $this->aOldMasterSkpd->getSkpdId()) {
            $this->aOldMasterSkpd = null;
        }
        if ($this->aOldIndikatorKinerja !== null && $this->indikator_id_atasan !== $this->aOldIndikatorKinerja->getId()) {
            $this->aOldIndikatorKinerja = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(OldIndikatorKinerjaTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildOldIndikatorKinerjaQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aOldPegawai = null;
            $this->aOldMasterSkpd = null;
            $this->aOldIndikatorKinerja = null;
            $this->collOldIndikatorKinerjasRelatedById = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see OldIndikatorKinerja::setDeleted()
     * @see OldIndikatorKinerja::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldIndikatorKinerjaTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildOldIndikatorKinerjaQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldIndikatorKinerjaTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                OldIndikatorKinerjaTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aOldPegawai !== null) {
                if ($this->aOldPegawai->isModified() || $this->aOldPegawai->isNew()) {
                    $affectedRows += $this->aOldPegawai->save($con);
                }
                $this->setOldPegawai($this->aOldPegawai);
            }

            if ($this->aOldMasterSkpd !== null) {
                if ($this->aOldMasterSkpd->isModified() || $this->aOldMasterSkpd->isNew()) {
                    $affectedRows += $this->aOldMasterSkpd->save($con);
                }
                $this->setOldMasterSkpd($this->aOldMasterSkpd);
            }

            if ($this->aOldIndikatorKinerja !== null) {
                if ($this->aOldIndikatorKinerja->isModified() || $this->aOldIndikatorKinerja->isNew()) {
                    $affectedRows += $this->aOldIndikatorKinerja->save($con);
                }
                $this->setOldIndikatorKinerja($this->aOldIndikatorKinerja);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->oldIndikatorKinerjasRelatedByIdScheduledForDeletion !== null) {
                if (!$this->oldIndikatorKinerjasRelatedByIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->oldIndikatorKinerjasRelatedByIdScheduledForDeletion as $oldIndikatorKinerjaRelatedById) {
                        // need to save related object because we set the relation to null
                        $oldIndikatorKinerjaRelatedById->save($con);
                    }
                    $this->oldIndikatorKinerjasRelatedByIdScheduledForDeletion = null;
                }
            }

            if ($this->collOldIndikatorKinerjasRelatedById !== null) {
                foreach ($this->collOldIndikatorKinerjasRelatedById as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[OldIndikatorKinerjaTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . OldIndikatorKinerjaTableMap::COL_ID . ')');
        }
        if (null === $this->id) {
            try {
                $dataFetcher = $con->query("SELECT nextval('eperformance.indikator_kinerja_id_seq')");
                $this->id = (int) $dataFetcher->fetchColumn();
            } catch (Exception $e) {
                throw new PropelException('Unable to get sequence id.', 0, $e);
            }
        }


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_PEGAWAI_ID)) {
            $modifiedColumns[':p' . $index++]  = 'pegawai_id';
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_NAMA)) {
            $modifiedColumns[':p' . $index++]  = 'nama';
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_DESKRIPSI)) {
            $modifiedColumns[':p' . $index++]  = 'deskripsi';
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_STATUS)) {
            $modifiedColumns[':p' . $index++]  = 'status';
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_DELETED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'deleted_at';
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_SKPD_ID)) {
            $modifiedColumns[':p' . $index++]  = 'skpd_id';
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_TARGET)) {
            $modifiedColumns[':p' . $index++]  = 'target';
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_SATUAN)) {
            $modifiedColumns[':p' . $index++]  = 'satuan';
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_TARGET_KEMARIN)) {
            $modifiedColumns[':p' . $index++]  = 'target_kemarin';
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_CARA_PERHITUNGAN)) {
            $modifiedColumns[':p' . $index++]  = 'cara_perhitungan';
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_CAPAIAN_AKHIR)) {
            $modifiedColumns[':p' . $index++]  = 'capaian_akhir';
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_JENIS)) {
            $modifiedColumns[':p' . $index++]  = 'jenis';
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_INDIKATOR_ID_ATASAN)) {
            $modifiedColumns[':p' . $index++]  = 'indikator_id_atasan';
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_IS_TAMBAHAN)) {
            $modifiedColumns[':p' . $index++]  = 'is_tambahan';
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_IS_TAHUN_LALU)) {
            $modifiedColumns[':p' . $index++]  = 'is_tahun_lalu';
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_IS_TARIK_EBUDGETING)) {
            $modifiedColumns[':p' . $index++]  = 'is_tarik_ebudgeting';
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_IS_PERHITUNGAN_TERBALIK)) {
            $modifiedColumns[':p' . $index++]  = 'is_perhitungan_terbalik';
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_IS_REALISASI_ANGKA_PREDIKSI)) {
            $modifiedColumns[':p' . $index++]  = 'is_realisasi_angka_prediksi';
        }

        $sql = sprintf(
            'INSERT INTO eperformance.indikator_kinerja (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'pegawai_id':
                        $stmt->bindValue($identifier, $this->pegawai_id, PDO::PARAM_INT);
                        break;
                    case 'nama':
                        $stmt->bindValue($identifier, $this->nama, PDO::PARAM_STR);
                        break;
                    case 'deskripsi':
                        $stmt->bindValue($identifier, $this->deskripsi, PDO::PARAM_STR);
                        break;
                    case 'status':
                        $stmt->bindValue($identifier, $this->status, PDO::PARAM_INT);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'deleted_at':
                        $stmt->bindValue($identifier, $this->deleted_at ? $this->deleted_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'skpd_id':
                        $stmt->bindValue($identifier, $this->skpd_id, PDO::PARAM_INT);
                        break;
                    case 'target':
                        $stmt->bindValue($identifier, $this->target, PDO::PARAM_STR);
                        break;
                    case 'satuan':
                        $stmt->bindValue($identifier, $this->satuan, PDO::PARAM_STR);
                        break;
                    case 'target_kemarin':
                        $stmt->bindValue($identifier, $this->target_kemarin, PDO::PARAM_INT);
                        break;
                    case 'cara_perhitungan':
                        $stmt->bindValue($identifier, $this->cara_perhitungan, PDO::PARAM_STR);
                        break;
                    case 'capaian_akhir':
                        $stmt->bindValue($identifier, $this->capaian_akhir, PDO::PARAM_STR);
                        break;
                    case 'jenis':
                        $stmt->bindValue($identifier, $this->jenis, PDO::PARAM_INT);
                        break;
                    case 'indikator_id_atasan':
                        $stmt->bindValue($identifier, $this->indikator_id_atasan, PDO::PARAM_INT);
                        break;
                    case 'is_tambahan':
                        $stmt->bindValue($identifier, $this->is_tambahan, PDO::PARAM_BOOL);
                        break;
                    case 'is_tahun_lalu':
                        $stmt->bindValue($identifier, $this->is_tahun_lalu, PDO::PARAM_BOOL);
                        break;
                    case 'is_tarik_ebudgeting':
                        $stmt->bindValue($identifier, $this->is_tarik_ebudgeting, PDO::PARAM_BOOL);
                        break;
                    case 'is_perhitungan_terbalik':
                        $stmt->bindValue($identifier, $this->is_perhitungan_terbalik, PDO::PARAM_BOOL);
                        break;
                    case 'is_realisasi_angka_prediksi':
                        $stmt->bindValue($identifier, $this->is_realisasi_angka_prediksi, PDO::PARAM_BOOL);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = OldIndikatorKinerjaTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getPegawaiId();
                break;
            case 2:
                return $this->getNama();
                break;
            case 3:
                return $this->getDeskripsi();
                break;
            case 4:
                return $this->getStatus();
                break;
            case 5:
                return $this->getCreatedAt();
                break;
            case 6:
                return $this->getUpdatedAt();
                break;
            case 7:
                return $this->getDeletedAt();
                break;
            case 8:
                return $this->getSkpdId();
                break;
            case 9:
                return $this->getTarget();
                break;
            case 10:
                return $this->getSatuan();
                break;
            case 11:
                return $this->getTargetKemarin();
                break;
            case 12:
                return $this->getCaraPerhitungan();
                break;
            case 13:
                return $this->getCapaianAkhir();
                break;
            case 14:
                return $this->getJenis();
                break;
            case 15:
                return $this->getIndikatorIdAtasan();
                break;
            case 16:
                return $this->getIsTambahan();
                break;
            case 17:
                return $this->getIsTahunLalu();
                break;
            case 18:
                return $this->getIsTarikEbudgeting();
                break;
            case 19:
                return $this->getIsPerhitunganTerbalik();
                break;
            case 20:
                return $this->getIsRealisasiAngkaPrediksi();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['OldIndikatorKinerja'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['OldIndikatorKinerja'][$this->hashCode()] = true;
        $keys = OldIndikatorKinerjaTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getPegawaiId(),
            $keys[2] => $this->getNama(),
            $keys[3] => $this->getDeskripsi(),
            $keys[4] => $this->getStatus(),
            $keys[5] => $this->getCreatedAt(),
            $keys[6] => $this->getUpdatedAt(),
            $keys[7] => $this->getDeletedAt(),
            $keys[8] => $this->getSkpdId(),
            $keys[9] => $this->getTarget(),
            $keys[10] => $this->getSatuan(),
            $keys[11] => $this->getTargetKemarin(),
            $keys[12] => $this->getCaraPerhitungan(),
            $keys[13] => $this->getCapaianAkhir(),
            $keys[14] => $this->getJenis(),
            $keys[15] => $this->getIndikatorIdAtasan(),
            $keys[16] => $this->getIsTambahan(),
            $keys[17] => $this->getIsTahunLalu(),
            $keys[18] => $this->getIsTarikEbudgeting(),
            $keys[19] => $this->getIsPerhitunganTerbalik(),
            $keys[20] => $this->getIsRealisasiAngkaPrediksi(),
        );
        if ($result[$keys[5]] instanceof \DateTime) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        if ($result[$keys[6]] instanceof \DateTime) {
            $result[$keys[6]] = $result[$keys[6]]->format('c');
        }

        if ($result[$keys[7]] instanceof \DateTime) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aOldPegawai) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldPegawai';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.pegawai';
                        break;
                    default:
                        $key = 'OldPegawai';
                }

                $result[$key] = $this->aOldPegawai->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aOldMasterSkpd) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldMasterSkpd';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.master_skpd';
                        break;
                    default:
                        $key = 'OldMasterSkpd';
                }

                $result[$key] = $this->aOldMasterSkpd->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aOldIndikatorKinerja) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldIndikatorKinerja';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.indikator_kinerja';
                        break;
                    default:
                        $key = 'OldIndikatorKinerja';
                }

                $result[$key] = $this->aOldIndikatorKinerja->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collOldIndikatorKinerjasRelatedById) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'oldIndikatorKinerjas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'eperformance.indikator_kinerjas';
                        break;
                    default:
                        $key = 'OldIndikatorKinerjas';
                }

                $result[$key] = $this->collOldIndikatorKinerjasRelatedById->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = OldIndikatorKinerjaTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setPegawaiId($value);
                break;
            case 2:
                $this->setNama($value);
                break;
            case 3:
                $this->setDeskripsi($value);
                break;
            case 4:
                $this->setStatus($value);
                break;
            case 5:
                $this->setCreatedAt($value);
                break;
            case 6:
                $this->setUpdatedAt($value);
                break;
            case 7:
                $this->setDeletedAt($value);
                break;
            case 8:
                $this->setSkpdId($value);
                break;
            case 9:
                $this->setTarget($value);
                break;
            case 10:
                $this->setSatuan($value);
                break;
            case 11:
                $this->setTargetKemarin($value);
                break;
            case 12:
                $this->setCaraPerhitungan($value);
                break;
            case 13:
                $this->setCapaianAkhir($value);
                break;
            case 14:
                $this->setJenis($value);
                break;
            case 15:
                $this->setIndikatorIdAtasan($value);
                break;
            case 16:
                $this->setIsTambahan($value);
                break;
            case 17:
                $this->setIsTahunLalu($value);
                break;
            case 18:
                $this->setIsTarikEbudgeting($value);
                break;
            case 19:
                $this->setIsPerhitunganTerbalik($value);
                break;
            case 20:
                $this->setIsRealisasiAngkaPrediksi($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = OldIndikatorKinerjaTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setPegawaiId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setNama($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDeskripsi($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setStatus($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setCreatedAt($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setUpdatedAt($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDeletedAt($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setSkpdId($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setTarget($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setSatuan($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setTargetKemarin($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setCaraPerhitungan($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setCapaianAkhir($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setJenis($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setIndikatorIdAtasan($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setIsTambahan($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setIsTahunLalu($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setIsTarikEbudgeting($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setIsPerhitunganTerbalik($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setIsRealisasiAngkaPrediksi($arr[$keys[20]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(OldIndikatorKinerjaTableMap::DATABASE_NAME);

        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_ID)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_PEGAWAI_ID)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_PEGAWAI_ID, $this->pegawai_id);
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_NAMA)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_NAMA, $this->nama);
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_DESKRIPSI)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_DESKRIPSI, $this->deskripsi);
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_STATUS)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_STATUS, $this->status);
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_CREATED_AT)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_UPDATED_AT)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_UPDATED_AT, $this->updated_at);
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_DELETED_AT)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_DELETED_AT, $this->deleted_at);
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_SKPD_ID)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_SKPD_ID, $this->skpd_id);
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_TARGET)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_TARGET, $this->target);
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_SATUAN)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_SATUAN, $this->satuan);
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_TARGET_KEMARIN)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_TARGET_KEMARIN, $this->target_kemarin);
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_CARA_PERHITUNGAN)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_CARA_PERHITUNGAN, $this->cara_perhitungan);
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_CAPAIAN_AKHIR)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_CAPAIAN_AKHIR, $this->capaian_akhir);
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_JENIS)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_JENIS, $this->jenis);
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_INDIKATOR_ID_ATASAN)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_INDIKATOR_ID_ATASAN, $this->indikator_id_atasan);
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_IS_TAMBAHAN)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_IS_TAMBAHAN, $this->is_tambahan);
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_IS_TAHUN_LALU)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_IS_TAHUN_LALU, $this->is_tahun_lalu);
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_IS_TARIK_EBUDGETING)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_IS_TARIK_EBUDGETING, $this->is_tarik_ebudgeting);
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_IS_PERHITUNGAN_TERBALIK)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_IS_PERHITUNGAN_TERBALIK, $this->is_perhitungan_terbalik);
        }
        if ($this->isColumnModified(OldIndikatorKinerjaTableMap::COL_IS_REALISASI_ANGKA_PREDIKSI)) {
            $criteria->add(OldIndikatorKinerjaTableMap::COL_IS_REALISASI_ANGKA_PREDIKSI, $this->is_realisasi_angka_prediksi);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildOldIndikatorKinerjaQuery::create();
        $criteria->add(OldIndikatorKinerjaTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \CoreBundle\Model\OldEperformance\OldIndikatorKinerja (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setPegawaiId($this->getPegawaiId());
        $copyObj->setNama($this->getNama());
        $copyObj->setDeskripsi($this->getDeskripsi());
        $copyObj->setStatus($this->getStatus());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        $copyObj->setDeletedAt($this->getDeletedAt());
        $copyObj->setSkpdId($this->getSkpdId());
        $copyObj->setTarget($this->getTarget());
        $copyObj->setSatuan($this->getSatuan());
        $copyObj->setTargetKemarin($this->getTargetKemarin());
        $copyObj->setCaraPerhitungan($this->getCaraPerhitungan());
        $copyObj->setCapaianAkhir($this->getCapaianAkhir());
        $copyObj->setJenis($this->getJenis());
        $copyObj->setIndikatorIdAtasan($this->getIndikatorIdAtasan());
        $copyObj->setIsTambahan($this->getIsTambahan());
        $copyObj->setIsTahunLalu($this->getIsTahunLalu());
        $copyObj->setIsTarikEbudgeting($this->getIsTarikEbudgeting());
        $copyObj->setIsPerhitunganTerbalik($this->getIsPerhitunganTerbalik());
        $copyObj->setIsRealisasiAngkaPrediksi($this->getIsRealisasiAngkaPrediksi());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getOldIndikatorKinerjasRelatedById() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addOldIndikatorKinerjaRelatedById($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \CoreBundle\Model\OldEperformance\OldIndikatorKinerja Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildOldPegawai object.
     *
     * @param  ChildOldPegawai $v
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     * @throws PropelException
     */
    public function setOldPegawai(ChildOldPegawai $v = null)
    {
        if ($v === null) {
            $this->setPegawaiId(NULL);
        } else {
            $this->setPegawaiId($v->getId());
        }

        $this->aOldPegawai = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildOldPegawai object, it will not be re-added.
        if ($v !== null) {
            $v->addOldIndikatorKinerja($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildOldPegawai object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildOldPegawai The associated ChildOldPegawai object.
     * @throws PropelException
     */
    public function getOldPegawai(ConnectionInterface $con = null)
    {
        if ($this->aOldPegawai === null && ($this->pegawai_id !== null)) {
            $this->aOldPegawai = ChildOldPegawaiQuery::create()->findPk($this->pegawai_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aOldPegawai->addOldIndikatorKinerjas($this);
             */
        }

        return $this->aOldPegawai;
    }

    /**
     * Declares an association between this object and a ChildOldMasterSkpd object.
     *
     * @param  ChildOldMasterSkpd $v
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     * @throws PropelException
     */
    public function setOldMasterSkpd(ChildOldMasterSkpd $v = null)
    {
        if ($v === null) {
            $this->setSkpdId(NULL);
        } else {
            $this->setSkpdId($v->getSkpdId());
        }

        $this->aOldMasterSkpd = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildOldMasterSkpd object, it will not be re-added.
        if ($v !== null) {
            $v->addOldIndikatorKinerja($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildOldMasterSkpd object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildOldMasterSkpd The associated ChildOldMasterSkpd object.
     * @throws PropelException
     */
    public function getOldMasterSkpd(ConnectionInterface $con = null)
    {
        if ($this->aOldMasterSkpd === null && ($this->skpd_id !== null)) {
            $this->aOldMasterSkpd = ChildOldMasterSkpdQuery::create()->findPk($this->skpd_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aOldMasterSkpd->addOldIndikatorKinerjas($this);
             */
        }

        return $this->aOldMasterSkpd;
    }

    /**
     * Declares an association between this object and a ChildOldIndikatorKinerja object.
     *
     * @param  ChildOldIndikatorKinerja $v
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     * @throws PropelException
     */
    public function setOldIndikatorKinerja(ChildOldIndikatorKinerja $v = null)
    {
        if ($v === null) {
            $this->setIndikatorIdAtasan(NULL);
        } else {
            $this->setIndikatorIdAtasan($v->getId());
        }

        $this->aOldIndikatorKinerja = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildOldIndikatorKinerja object, it will not be re-added.
        if ($v !== null) {
            $v->addOldIndikatorKinerjaRelatedById($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildOldIndikatorKinerja object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildOldIndikatorKinerja The associated ChildOldIndikatorKinerja object.
     * @throws PropelException
     */
    public function getOldIndikatorKinerja(ConnectionInterface $con = null)
    {
        if ($this->aOldIndikatorKinerja === null && ($this->indikator_id_atasan !== null)) {
            $this->aOldIndikatorKinerja = ChildOldIndikatorKinerjaQuery::create()->findPk($this->indikator_id_atasan, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aOldIndikatorKinerja->addOldIndikatorKinerjasRelatedById($this);
             */
        }

        return $this->aOldIndikatorKinerja;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('OldIndikatorKinerjaRelatedById' == $relationName) {
            return $this->initOldIndikatorKinerjasRelatedById();
        }
    }

    /**
     * Clears out the collOldIndikatorKinerjasRelatedById collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addOldIndikatorKinerjasRelatedById()
     */
    public function clearOldIndikatorKinerjasRelatedById()
    {
        $this->collOldIndikatorKinerjasRelatedById = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collOldIndikatorKinerjasRelatedById collection loaded partially.
     */
    public function resetPartialOldIndikatorKinerjasRelatedById($v = true)
    {
        $this->collOldIndikatorKinerjasRelatedByIdPartial = $v;
    }

    /**
     * Initializes the collOldIndikatorKinerjasRelatedById collection.
     *
     * By default this just sets the collOldIndikatorKinerjasRelatedById collection to an empty array (like clearcollOldIndikatorKinerjasRelatedById());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initOldIndikatorKinerjasRelatedById($overrideExisting = true)
    {
        if (null !== $this->collOldIndikatorKinerjasRelatedById && !$overrideExisting) {
            return;
        }

        $collectionClassName = OldIndikatorKinerjaTableMap::getTableMap()->getCollectionClassName();

        $this->collOldIndikatorKinerjasRelatedById = new $collectionClassName;
        $this->collOldIndikatorKinerjasRelatedById->setModel('\CoreBundle\Model\OldEperformance\OldIndikatorKinerja');
    }

    /**
     * Gets an array of ChildOldIndikatorKinerja objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildOldIndikatorKinerja is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildOldIndikatorKinerja[] List of ChildOldIndikatorKinerja objects
     * @throws PropelException
     */
    public function getOldIndikatorKinerjasRelatedById(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collOldIndikatorKinerjasRelatedByIdPartial && !$this->isNew();
        if (null === $this->collOldIndikatorKinerjasRelatedById || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collOldIndikatorKinerjasRelatedById) {
                // return empty collection
                $this->initOldIndikatorKinerjasRelatedById();
            } else {
                $collOldIndikatorKinerjasRelatedById = ChildOldIndikatorKinerjaQuery::create(null, $criteria)
                    ->filterByOldIndikatorKinerja($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collOldIndikatorKinerjasRelatedByIdPartial && count($collOldIndikatorKinerjasRelatedById)) {
                        $this->initOldIndikatorKinerjasRelatedById(false);

                        foreach ($collOldIndikatorKinerjasRelatedById as $obj) {
                            if (false == $this->collOldIndikatorKinerjasRelatedById->contains($obj)) {
                                $this->collOldIndikatorKinerjasRelatedById->append($obj);
                            }
                        }

                        $this->collOldIndikatorKinerjasRelatedByIdPartial = true;
                    }

                    return $collOldIndikatorKinerjasRelatedById;
                }

                if ($partial && $this->collOldIndikatorKinerjasRelatedById) {
                    foreach ($this->collOldIndikatorKinerjasRelatedById as $obj) {
                        if ($obj->isNew()) {
                            $collOldIndikatorKinerjasRelatedById[] = $obj;
                        }
                    }
                }

                $this->collOldIndikatorKinerjasRelatedById = $collOldIndikatorKinerjasRelatedById;
                $this->collOldIndikatorKinerjasRelatedByIdPartial = false;
            }
        }

        return $this->collOldIndikatorKinerjasRelatedById;
    }

    /**
     * Sets a collection of ChildOldIndikatorKinerja objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $oldIndikatorKinerjasRelatedById A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildOldIndikatorKinerja The current object (for fluent API support)
     */
    public function setOldIndikatorKinerjasRelatedById(Collection $oldIndikatorKinerjasRelatedById, ConnectionInterface $con = null)
    {
        /** @var ChildOldIndikatorKinerja[] $oldIndikatorKinerjasRelatedByIdToDelete */
        $oldIndikatorKinerjasRelatedByIdToDelete = $this->getOldIndikatorKinerjasRelatedById(new Criteria(), $con)->diff($oldIndikatorKinerjasRelatedById);


        $this->oldIndikatorKinerjasRelatedByIdScheduledForDeletion = $oldIndikatorKinerjasRelatedByIdToDelete;

        foreach ($oldIndikatorKinerjasRelatedByIdToDelete as $oldIndikatorKinerjaRelatedByIdRemoved) {
            $oldIndikatorKinerjaRelatedByIdRemoved->setOldIndikatorKinerja(null);
        }

        $this->collOldIndikatorKinerjasRelatedById = null;
        foreach ($oldIndikatorKinerjasRelatedById as $oldIndikatorKinerjaRelatedById) {
            $this->addOldIndikatorKinerjaRelatedById($oldIndikatorKinerjaRelatedById);
        }

        $this->collOldIndikatorKinerjasRelatedById = $oldIndikatorKinerjasRelatedById;
        $this->collOldIndikatorKinerjasRelatedByIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related OldIndikatorKinerja objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related OldIndikatorKinerja objects.
     * @throws PropelException
     */
    public function countOldIndikatorKinerjasRelatedById(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collOldIndikatorKinerjasRelatedByIdPartial && !$this->isNew();
        if (null === $this->collOldIndikatorKinerjasRelatedById || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collOldIndikatorKinerjasRelatedById) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getOldIndikatorKinerjasRelatedById());
            }

            $query = ChildOldIndikatorKinerjaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByOldIndikatorKinerja($this)
                ->count($con);
        }

        return count($this->collOldIndikatorKinerjasRelatedById);
    }

    /**
     * Method called to associate a ChildOldIndikatorKinerja object to this object
     * through the ChildOldIndikatorKinerja foreign key attribute.
     *
     * @param  ChildOldIndikatorKinerja $l ChildOldIndikatorKinerja
     * @return $this|\CoreBundle\Model\OldEperformance\OldIndikatorKinerja The current object (for fluent API support)
     */
    public function addOldIndikatorKinerjaRelatedById(ChildOldIndikatorKinerja $l)
    {
        if ($this->collOldIndikatorKinerjasRelatedById === null) {
            $this->initOldIndikatorKinerjasRelatedById();
            $this->collOldIndikatorKinerjasRelatedByIdPartial = true;
        }

        if (!$this->collOldIndikatorKinerjasRelatedById->contains($l)) {
            $this->doAddOldIndikatorKinerjaRelatedById($l);

            if ($this->oldIndikatorKinerjasRelatedByIdScheduledForDeletion and $this->oldIndikatorKinerjasRelatedByIdScheduledForDeletion->contains($l)) {
                $this->oldIndikatorKinerjasRelatedByIdScheduledForDeletion->remove($this->oldIndikatorKinerjasRelatedByIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildOldIndikatorKinerja $oldIndikatorKinerjaRelatedById The ChildOldIndikatorKinerja object to add.
     */
    protected function doAddOldIndikatorKinerjaRelatedById(ChildOldIndikatorKinerja $oldIndikatorKinerjaRelatedById)
    {
        $this->collOldIndikatorKinerjasRelatedById[]= $oldIndikatorKinerjaRelatedById;
        $oldIndikatorKinerjaRelatedById->setOldIndikatorKinerja($this);
    }

    /**
     * @param  ChildOldIndikatorKinerja $oldIndikatorKinerjaRelatedById The ChildOldIndikatorKinerja object to remove.
     * @return $this|ChildOldIndikatorKinerja The current object (for fluent API support)
     */
    public function removeOldIndikatorKinerjaRelatedById(ChildOldIndikatorKinerja $oldIndikatorKinerjaRelatedById)
    {
        if ($this->getOldIndikatorKinerjasRelatedById()->contains($oldIndikatorKinerjaRelatedById)) {
            $pos = $this->collOldIndikatorKinerjasRelatedById->search($oldIndikatorKinerjaRelatedById);
            $this->collOldIndikatorKinerjasRelatedById->remove($pos);
            if (null === $this->oldIndikatorKinerjasRelatedByIdScheduledForDeletion) {
                $this->oldIndikatorKinerjasRelatedByIdScheduledForDeletion = clone $this->collOldIndikatorKinerjasRelatedById;
                $this->oldIndikatorKinerjasRelatedByIdScheduledForDeletion->clear();
            }
            $this->oldIndikatorKinerjasRelatedByIdScheduledForDeletion[]= $oldIndikatorKinerjaRelatedById;
            $oldIndikatorKinerjaRelatedById->setOldIndikatorKinerja(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this OldIndikatorKinerja is new, it will return
     * an empty collection; or if this OldIndikatorKinerja has previously
     * been saved, it will retrieve related OldIndikatorKinerjasRelatedById from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in OldIndikatorKinerja.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOldIndikatorKinerja[] List of ChildOldIndikatorKinerja objects
     */
    public function getOldIndikatorKinerjasRelatedByIdJoinOldPegawai(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOldIndikatorKinerjaQuery::create(null, $criteria);
        $query->joinWith('OldPegawai', $joinBehavior);

        return $this->getOldIndikatorKinerjasRelatedById($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this OldIndikatorKinerja is new, it will return
     * an empty collection; or if this OldIndikatorKinerja has previously
     * been saved, it will retrieve related OldIndikatorKinerjasRelatedById from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in OldIndikatorKinerja.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOldIndikatorKinerja[] List of ChildOldIndikatorKinerja objects
     */
    public function getOldIndikatorKinerjasRelatedByIdJoinOldMasterSkpd(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOldIndikatorKinerjaQuery::create(null, $criteria);
        $query->joinWith('OldMasterSkpd', $joinBehavior);

        return $this->getOldIndikatorKinerjasRelatedById($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aOldPegawai) {
            $this->aOldPegawai->removeOldIndikatorKinerja($this);
        }
        if (null !== $this->aOldMasterSkpd) {
            $this->aOldMasterSkpd->removeOldIndikatorKinerja($this);
        }
        if (null !== $this->aOldIndikatorKinerja) {
            $this->aOldIndikatorKinerja->removeOldIndikatorKinerjaRelatedById($this);
        }
        $this->id = null;
        $this->pegawai_id = null;
        $this->nama = null;
        $this->deskripsi = null;
        $this->status = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->deleted_at = null;
        $this->skpd_id = null;
        $this->target = null;
        $this->satuan = null;
        $this->target_kemarin = null;
        $this->cara_perhitungan = null;
        $this->capaian_akhir = null;
        $this->jenis = null;
        $this->indikator_id_atasan = null;
        $this->is_tambahan = null;
        $this->is_tahun_lalu = null;
        $this->is_tarik_ebudgeting = null;
        $this->is_perhitungan_terbalik = null;
        $this->is_realisasi_angka_prediksi = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collOldIndikatorKinerjasRelatedById) {
                foreach ($this->collOldIndikatorKinerjasRelatedById as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collOldIndikatorKinerjasRelatedById = null;
        $this->aOldPegawai = null;
        $this->aOldMasterSkpd = null;
        $this->aOldIndikatorKinerja = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(OldIndikatorKinerjaTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}

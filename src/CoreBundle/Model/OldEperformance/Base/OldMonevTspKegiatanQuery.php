<?php

namespace CoreBundle\Model\OldEperformance\Base;

use \Exception;
use \PDO;
use CoreBundle\Model\OldEperformance\OldMonevTspKegiatan as ChildOldMonevTspKegiatan;
use CoreBundle\Model\OldEperformance\OldMonevTspKegiatanQuery as ChildOldMonevTspKegiatanQuery;
use CoreBundle\Model\OldEperformance\Map\OldMonevTspKegiatanTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'eperformance.monev_tsp_kegiatan' table.
 *
 *
 *
 * @method     ChildOldMonevTspKegiatanQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildOldMonevTspKegiatanQuery orderByKegiatanId($order = Criteria::ASC) Order by the kegiatan_id column
 * @method     ChildOldMonevTspKegiatanQuery orderByKegiatanKode($order = Criteria::ASC) Order by the kegiatan_kode column
 * @method     ChildOldMonevTspKegiatanQuery orderByKegiatanNama($order = Criteria::ASC) Order by the kegiatan_nama column
 * @method     ChildOldMonevTspKegiatanQuery orderByUnitId($order = Criteria::ASC) Order by the unit_id column
 * @method     ChildOldMonevTspKegiatanQuery orderByUnitKode($order = Criteria::ASC) Order by the unit_kode column
 * @method     ChildOldMonevTspKegiatanQuery orderByUnitNama($order = Criteria::ASC) Order by the unit_nama column
 * @method     ChildOldMonevTspKegiatanQuery orderByTahun($order = Criteria::ASC) Order by the tahun column
 * @method     ChildOldMonevTspKegiatanQuery orderByTarget($order = Criteria::ASC) Order by the target column
 * @method     ChildOldMonevTspKegiatanQuery orderByRealisasi($order = Criteria::ASC) Order by the realisasi column
 * @method     ChildOldMonevTspKegiatanQuery orderBySatuan($order = Criteria::ASC) Order by the satuan column
 * @method     ChildOldMonevTspKegiatanQuery orderByCapaian($order = Criteria::ASC) Order by the capaian column
 * @method     ChildOldMonevTspKegiatanQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildOldMonevTspKegiatanQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildOldMonevTspKegiatanQuery groupById() Group by the id column
 * @method     ChildOldMonevTspKegiatanQuery groupByKegiatanId() Group by the kegiatan_id column
 * @method     ChildOldMonevTspKegiatanQuery groupByKegiatanKode() Group by the kegiatan_kode column
 * @method     ChildOldMonevTspKegiatanQuery groupByKegiatanNama() Group by the kegiatan_nama column
 * @method     ChildOldMonevTspKegiatanQuery groupByUnitId() Group by the unit_id column
 * @method     ChildOldMonevTspKegiatanQuery groupByUnitKode() Group by the unit_kode column
 * @method     ChildOldMonevTspKegiatanQuery groupByUnitNama() Group by the unit_nama column
 * @method     ChildOldMonevTspKegiatanQuery groupByTahun() Group by the tahun column
 * @method     ChildOldMonevTspKegiatanQuery groupByTarget() Group by the target column
 * @method     ChildOldMonevTspKegiatanQuery groupByRealisasi() Group by the realisasi column
 * @method     ChildOldMonevTspKegiatanQuery groupBySatuan() Group by the satuan column
 * @method     ChildOldMonevTspKegiatanQuery groupByCapaian() Group by the capaian column
 * @method     ChildOldMonevTspKegiatanQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildOldMonevTspKegiatanQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildOldMonevTspKegiatanQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildOldMonevTspKegiatanQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildOldMonevTspKegiatanQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildOldMonevTspKegiatanQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildOldMonevTspKegiatanQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildOldMonevTspKegiatanQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildOldMonevTspKegiatanQuery leftJoinOldMasterSkpd($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldMasterSkpd relation
 * @method     ChildOldMonevTspKegiatanQuery rightJoinOldMasterSkpd($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldMasterSkpd relation
 * @method     ChildOldMonevTspKegiatanQuery innerJoinOldMasterSkpd($relationAlias = null) Adds a INNER JOIN clause to the query using the OldMasterSkpd relation
 *
 * @method     ChildOldMonevTspKegiatanQuery joinWithOldMasterSkpd($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldMasterSkpd relation
 *
 * @method     ChildOldMonevTspKegiatanQuery leftJoinWithOldMasterSkpd() Adds a LEFT JOIN clause and with to the query using the OldMasterSkpd relation
 * @method     ChildOldMonevTspKegiatanQuery rightJoinWithOldMasterSkpd() Adds a RIGHT JOIN clause and with to the query using the OldMasterSkpd relation
 * @method     ChildOldMonevTspKegiatanQuery innerJoinWithOldMasterSkpd() Adds a INNER JOIN clause and with to the query using the OldMasterSkpd relation
 *
 * @method     ChildOldMonevTspKegiatanQuery leftJoinOldKegiatanPegawai($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldKegiatanPegawai relation
 * @method     ChildOldMonevTspKegiatanQuery rightJoinOldKegiatanPegawai($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldKegiatanPegawai relation
 * @method     ChildOldMonevTspKegiatanQuery innerJoinOldKegiatanPegawai($relationAlias = null) Adds a INNER JOIN clause to the query using the OldKegiatanPegawai relation
 *
 * @method     ChildOldMonevTspKegiatanQuery joinWithOldKegiatanPegawai($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldKegiatanPegawai relation
 *
 * @method     ChildOldMonevTspKegiatanQuery leftJoinWithOldKegiatanPegawai() Adds a LEFT JOIN clause and with to the query using the OldKegiatanPegawai relation
 * @method     ChildOldMonevTspKegiatanQuery rightJoinWithOldKegiatanPegawai() Adds a RIGHT JOIN clause and with to the query using the OldKegiatanPegawai relation
 * @method     ChildOldMonevTspKegiatanQuery innerJoinWithOldKegiatanPegawai() Adds a INNER JOIN clause and with to the query using the OldKegiatanPegawai relation
 *
 * @method     \CoreBundle\Model\OldEperformance\OldMasterSkpdQuery|\CoreBundle\Model\OldEperformance\OldKegiatanPegawaiQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildOldMonevTspKegiatan findOne(ConnectionInterface $con = null) Return the first ChildOldMonevTspKegiatan matching the query
 * @method     ChildOldMonevTspKegiatan findOneOrCreate(ConnectionInterface $con = null) Return the first ChildOldMonevTspKegiatan matching the query, or a new ChildOldMonevTspKegiatan object populated from the query conditions when no match is found
 *
 * @method     ChildOldMonevTspKegiatan findOneById(int $id) Return the first ChildOldMonevTspKegiatan filtered by the id column
 * @method     ChildOldMonevTspKegiatan findOneByKegiatanId(int $kegiatan_id) Return the first ChildOldMonevTspKegiatan filtered by the kegiatan_id column
 * @method     ChildOldMonevTspKegiatan findOneByKegiatanKode(string $kegiatan_kode) Return the first ChildOldMonevTspKegiatan filtered by the kegiatan_kode column
 * @method     ChildOldMonevTspKegiatan findOneByKegiatanNama(string $kegiatan_nama) Return the first ChildOldMonevTspKegiatan filtered by the kegiatan_nama column
 * @method     ChildOldMonevTspKegiatan findOneByUnitId(int $unit_id) Return the first ChildOldMonevTspKegiatan filtered by the unit_id column
 * @method     ChildOldMonevTspKegiatan findOneByUnitKode(string $unit_kode) Return the first ChildOldMonevTspKegiatan filtered by the unit_kode column
 * @method     ChildOldMonevTspKegiatan findOneByUnitNama(string $unit_nama) Return the first ChildOldMonevTspKegiatan filtered by the unit_nama column
 * @method     ChildOldMonevTspKegiatan findOneByTahun(int $tahun) Return the first ChildOldMonevTspKegiatan filtered by the tahun column
 * @method     ChildOldMonevTspKegiatan findOneByTarget(string $target) Return the first ChildOldMonevTspKegiatan filtered by the target column
 * @method     ChildOldMonevTspKegiatan findOneByRealisasi(string $realisasi) Return the first ChildOldMonevTspKegiatan filtered by the realisasi column
 * @method     ChildOldMonevTspKegiatan findOneBySatuan(string $satuan) Return the first ChildOldMonevTspKegiatan filtered by the satuan column
 * @method     ChildOldMonevTspKegiatan findOneByCapaian(string $capaian) Return the first ChildOldMonevTspKegiatan filtered by the capaian column
 * @method     ChildOldMonevTspKegiatan findOneByCreatedAt(string $created_at) Return the first ChildOldMonevTspKegiatan filtered by the created_at column
 * @method     ChildOldMonevTspKegiatan findOneByUpdatedAt(string $updated_at) Return the first ChildOldMonevTspKegiatan filtered by the updated_at column *

 * @method     ChildOldMonevTspKegiatan requirePk($key, ConnectionInterface $con = null) Return the ChildOldMonevTspKegiatan by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTspKegiatan requireOne(ConnectionInterface $con = null) Return the first ChildOldMonevTspKegiatan matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOldMonevTspKegiatan requireOneById(int $id) Return the first ChildOldMonevTspKegiatan filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTspKegiatan requireOneByKegiatanId(int $kegiatan_id) Return the first ChildOldMonevTspKegiatan filtered by the kegiatan_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTspKegiatan requireOneByKegiatanKode(string $kegiatan_kode) Return the first ChildOldMonevTspKegiatan filtered by the kegiatan_kode column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTspKegiatan requireOneByKegiatanNama(string $kegiatan_nama) Return the first ChildOldMonevTspKegiatan filtered by the kegiatan_nama column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTspKegiatan requireOneByUnitId(int $unit_id) Return the first ChildOldMonevTspKegiatan filtered by the unit_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTspKegiatan requireOneByUnitKode(string $unit_kode) Return the first ChildOldMonevTspKegiatan filtered by the unit_kode column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTspKegiatan requireOneByUnitNama(string $unit_nama) Return the first ChildOldMonevTspKegiatan filtered by the unit_nama column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTspKegiatan requireOneByTahun(int $tahun) Return the first ChildOldMonevTspKegiatan filtered by the tahun column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTspKegiatan requireOneByTarget(string $target) Return the first ChildOldMonevTspKegiatan filtered by the target column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTspKegiatan requireOneByRealisasi(string $realisasi) Return the first ChildOldMonevTspKegiatan filtered by the realisasi column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTspKegiatan requireOneBySatuan(string $satuan) Return the first ChildOldMonevTspKegiatan filtered by the satuan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTspKegiatan requireOneByCapaian(string $capaian) Return the first ChildOldMonevTspKegiatan filtered by the capaian column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTspKegiatan requireOneByCreatedAt(string $created_at) Return the first ChildOldMonevTspKegiatan filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldMonevTspKegiatan requireOneByUpdatedAt(string $updated_at) Return the first ChildOldMonevTspKegiatan filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOldMonevTspKegiatan[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildOldMonevTspKegiatan objects based on current ModelCriteria
 * @method     ChildOldMonevTspKegiatan[]|ObjectCollection findById(int $id) Return ChildOldMonevTspKegiatan objects filtered by the id column
 * @method     ChildOldMonevTspKegiatan[]|ObjectCollection findByKegiatanId(int $kegiatan_id) Return ChildOldMonevTspKegiatan objects filtered by the kegiatan_id column
 * @method     ChildOldMonevTspKegiatan[]|ObjectCollection findByKegiatanKode(string $kegiatan_kode) Return ChildOldMonevTspKegiatan objects filtered by the kegiatan_kode column
 * @method     ChildOldMonevTspKegiatan[]|ObjectCollection findByKegiatanNama(string $kegiatan_nama) Return ChildOldMonevTspKegiatan objects filtered by the kegiatan_nama column
 * @method     ChildOldMonevTspKegiatan[]|ObjectCollection findByUnitId(int $unit_id) Return ChildOldMonevTspKegiatan objects filtered by the unit_id column
 * @method     ChildOldMonevTspKegiatan[]|ObjectCollection findByUnitKode(string $unit_kode) Return ChildOldMonevTspKegiatan objects filtered by the unit_kode column
 * @method     ChildOldMonevTspKegiatan[]|ObjectCollection findByUnitNama(string $unit_nama) Return ChildOldMonevTspKegiatan objects filtered by the unit_nama column
 * @method     ChildOldMonevTspKegiatan[]|ObjectCollection findByTahun(int $tahun) Return ChildOldMonevTspKegiatan objects filtered by the tahun column
 * @method     ChildOldMonevTspKegiatan[]|ObjectCollection findByTarget(string $target) Return ChildOldMonevTspKegiatan objects filtered by the target column
 * @method     ChildOldMonevTspKegiatan[]|ObjectCollection findByRealisasi(string $realisasi) Return ChildOldMonevTspKegiatan objects filtered by the realisasi column
 * @method     ChildOldMonevTspKegiatan[]|ObjectCollection findBySatuan(string $satuan) Return ChildOldMonevTspKegiatan objects filtered by the satuan column
 * @method     ChildOldMonevTspKegiatan[]|ObjectCollection findByCapaian(string $capaian) Return ChildOldMonevTspKegiatan objects filtered by the capaian column
 * @method     ChildOldMonevTspKegiatan[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildOldMonevTspKegiatan objects filtered by the created_at column
 * @method     ChildOldMonevTspKegiatan[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildOldMonevTspKegiatan objects filtered by the updated_at column
 * @method     ChildOldMonevTspKegiatan[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class OldMonevTspKegiatanQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \CoreBundle\Model\OldEperformance\Base\OldMonevTspKegiatanQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'old_eperformance', $modelName = '\\CoreBundle\\Model\\OldEperformance\\OldMonevTspKegiatan', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildOldMonevTspKegiatanQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildOldMonevTspKegiatanQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildOldMonevTspKegiatanQuery) {
            return $criteria;
        }
        $query = new ChildOldMonevTspKegiatanQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildOldMonevTspKegiatan|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(OldMonevTspKegiatanTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = OldMonevTspKegiatanTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldMonevTspKegiatan A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, kegiatan_id, kegiatan_kode, kegiatan_nama, unit_id, unit_kode, unit_nama, tahun, target, realisasi, satuan, capaian, created_at, updated_at FROM eperformance.monev_tsp_kegiatan WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildOldMonevTspKegiatan $obj */
            $obj = new ChildOldMonevTspKegiatan();
            $obj->hydrate($row);
            OldMonevTspKegiatanTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildOldMonevTspKegiatan|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the kegiatan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByKegiatanId(1234); // WHERE kegiatan_id = 1234
     * $query->filterByKegiatanId(array(12, 34)); // WHERE kegiatan_id IN (12, 34)
     * $query->filterByKegiatanId(array('min' => 12)); // WHERE kegiatan_id > 12
     * </code>
     *
     * @param     mixed $kegiatanId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function filterByKegiatanId($kegiatanId = null, $comparison = null)
    {
        if (is_array($kegiatanId)) {
            $useMinMax = false;
            if (isset($kegiatanId['min'])) {
                $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_KEGIATAN_ID, $kegiatanId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($kegiatanId['max'])) {
                $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_KEGIATAN_ID, $kegiatanId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_KEGIATAN_ID, $kegiatanId, $comparison);
    }

    /**
     * Filter the query on the kegiatan_kode column
     *
     * Example usage:
     * <code>
     * $query->filterByKegiatanKode('fooValue');   // WHERE kegiatan_kode = 'fooValue'
     * $query->filterByKegiatanKode('%fooValue%', Criteria::LIKE); // WHERE kegiatan_kode LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kegiatanKode The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function filterByKegiatanKode($kegiatanKode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kegiatanKode)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_KEGIATAN_KODE, $kegiatanKode, $comparison);
    }

    /**
     * Filter the query on the kegiatan_nama column
     *
     * Example usage:
     * <code>
     * $query->filterByKegiatanNama('fooValue');   // WHERE kegiatan_nama = 'fooValue'
     * $query->filterByKegiatanNama('%fooValue%', Criteria::LIKE); // WHERE kegiatan_nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kegiatanNama The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function filterByKegiatanNama($kegiatanNama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kegiatanNama)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_KEGIATAN_NAMA, $kegiatanNama, $comparison);
    }

    /**
     * Filter the query on the unit_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitId(1234); // WHERE unit_id = 1234
     * $query->filterByUnitId(array(12, 34)); // WHERE unit_id IN (12, 34)
     * $query->filterByUnitId(array('min' => 12)); // WHERE unit_id > 12
     * </code>
     *
     * @see       filterByOldMasterSkpd()
     *
     * @param     mixed $unitId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function filterByUnitId($unitId = null, $comparison = null)
    {
        if (is_array($unitId)) {
            $useMinMax = false;
            if (isset($unitId['min'])) {
                $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_UNIT_ID, $unitId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($unitId['max'])) {
                $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_UNIT_ID, $unitId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_UNIT_ID, $unitId, $comparison);
    }

    /**
     * Filter the query on the unit_kode column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitKode('fooValue');   // WHERE unit_kode = 'fooValue'
     * $query->filterByUnitKode('%fooValue%', Criteria::LIKE); // WHERE unit_kode LIKE '%fooValue%'
     * </code>
     *
     * @param     string $unitKode The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function filterByUnitKode($unitKode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($unitKode)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_UNIT_KODE, $unitKode, $comparison);
    }

    /**
     * Filter the query on the unit_nama column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitNama('fooValue');   // WHERE unit_nama = 'fooValue'
     * $query->filterByUnitNama('%fooValue%', Criteria::LIKE); // WHERE unit_nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $unitNama The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function filterByUnitNama($unitNama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($unitNama)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_UNIT_NAMA, $unitNama, $comparison);
    }

    /**
     * Filter the query on the tahun column
     *
     * Example usage:
     * <code>
     * $query->filterByTahun(1234); // WHERE tahun = 1234
     * $query->filterByTahun(array(12, 34)); // WHERE tahun IN (12, 34)
     * $query->filterByTahun(array('min' => 12)); // WHERE tahun > 12
     * </code>
     *
     * @param     mixed $tahun The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function filterByTahun($tahun = null, $comparison = null)
    {
        if (is_array($tahun)) {
            $useMinMax = false;
            if (isset($tahun['min'])) {
                $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_TAHUN, $tahun['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tahun['max'])) {
                $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_TAHUN, $tahun['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_TAHUN, $tahun, $comparison);
    }

    /**
     * Filter the query on the target column
     *
     * Example usage:
     * <code>
     * $query->filterByTarget(1234); // WHERE target = 1234
     * $query->filterByTarget(array(12, 34)); // WHERE target IN (12, 34)
     * $query->filterByTarget(array('min' => 12)); // WHERE target > 12
     * </code>
     *
     * @param     mixed $target The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function filterByTarget($target = null, $comparison = null)
    {
        if (is_array($target)) {
            $useMinMax = false;
            if (isset($target['min'])) {
                $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_TARGET, $target['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($target['max'])) {
                $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_TARGET, $target['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_TARGET, $target, $comparison);
    }

    /**
     * Filter the query on the realisasi column
     *
     * Example usage:
     * <code>
     * $query->filterByRealisasi(1234); // WHERE realisasi = 1234
     * $query->filterByRealisasi(array(12, 34)); // WHERE realisasi IN (12, 34)
     * $query->filterByRealisasi(array('min' => 12)); // WHERE realisasi > 12
     * </code>
     *
     * @param     mixed $realisasi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function filterByRealisasi($realisasi = null, $comparison = null)
    {
        if (is_array($realisasi)) {
            $useMinMax = false;
            if (isset($realisasi['min'])) {
                $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_REALISASI, $realisasi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($realisasi['max'])) {
                $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_REALISASI, $realisasi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_REALISASI, $realisasi, $comparison);
    }

    /**
     * Filter the query on the satuan column
     *
     * Example usage:
     * <code>
     * $query->filterBySatuan('fooValue');   // WHERE satuan = 'fooValue'
     * $query->filterBySatuan('%fooValue%', Criteria::LIKE); // WHERE satuan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $satuan The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function filterBySatuan($satuan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($satuan)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_SATUAN, $satuan, $comparison);
    }

    /**
     * Filter the query on the capaian column
     *
     * Example usage:
     * <code>
     * $query->filterByCapaian(1234); // WHERE capaian = 1234
     * $query->filterByCapaian(array(12, 34)); // WHERE capaian IN (12, 34)
     * $query->filterByCapaian(array('min' => 12)); // WHERE capaian > 12
     * </code>
     *
     * @param     mixed $capaian The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function filterByCapaian($capaian = null, $comparison = null)
    {
        if (is_array($capaian)) {
            $useMinMax = false;
            if (isset($capaian['min'])) {
                $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_CAPAIAN, $capaian['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($capaian['max'])) {
                $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_CAPAIAN, $capaian['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_CAPAIAN, $capaian, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldMasterSkpd object
     *
     * @param \CoreBundle\Model\OldEperformance\OldMasterSkpd|ObjectCollection $oldMasterSkpd The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function filterByOldMasterSkpd($oldMasterSkpd, $comparison = null)
    {
        if ($oldMasterSkpd instanceof \CoreBundle\Model\OldEperformance\OldMasterSkpd) {
            return $this
                ->addUsingAlias(OldMonevTspKegiatanTableMap::COL_UNIT_ID, $oldMasterSkpd->getSkpdId(), $comparison);
        } elseif ($oldMasterSkpd instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OldMonevTspKegiatanTableMap::COL_UNIT_ID, $oldMasterSkpd->toKeyValue('PrimaryKey', 'SkpdId'), $comparison);
        } else {
            throw new PropelException('filterByOldMasterSkpd() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldMasterSkpd or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldMasterSkpd relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function joinOldMasterSkpd($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldMasterSkpd');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldMasterSkpd');
        }

        return $this;
    }

    /**
     * Use the OldMasterSkpd relation OldMasterSkpd object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldMasterSkpdQuery A secondary query class using the current class as primary query
     */
    public function useOldMasterSkpdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinOldMasterSkpd($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldMasterSkpd', '\CoreBundle\Model\OldEperformance\OldMasterSkpdQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldKegiatanPegawai object
     *
     * @param \CoreBundle\Model\OldEperformance\OldKegiatanPegawai|ObjectCollection $oldKegiatanPegawai the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function filterByOldKegiatanPegawai($oldKegiatanPegawai, $comparison = null)
    {
        if ($oldKegiatanPegawai instanceof \CoreBundle\Model\OldEperformance\OldKegiatanPegawai) {
            return $this
                ->addUsingAlias(OldMonevTspKegiatanTableMap::COL_ID, $oldKegiatanPegawai->getKegiatanId(), $comparison);
        } elseif ($oldKegiatanPegawai instanceof ObjectCollection) {
            return $this
                ->useOldKegiatanPegawaiQuery()
                ->filterByPrimaryKeys($oldKegiatanPegawai->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOldKegiatanPegawai() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldKegiatanPegawai or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldKegiatanPegawai relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function joinOldKegiatanPegawai($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldKegiatanPegawai');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldKegiatanPegawai');
        }

        return $this;
    }

    /**
     * Use the OldKegiatanPegawai relation OldKegiatanPegawai object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldKegiatanPegawaiQuery A secondary query class using the current class as primary query
     */
    public function useOldKegiatanPegawaiQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinOldKegiatanPegawai($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldKegiatanPegawai', '\CoreBundle\Model\OldEperformance\OldKegiatanPegawaiQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildOldMonevTspKegiatan $oldMonevTspKegiatan Object to remove from the list of results
     *
     * @return $this|ChildOldMonevTspKegiatanQuery The current query, for fluid interface
     */
    public function prune($oldMonevTspKegiatan = null)
    {
        if ($oldMonevTspKegiatan) {
            $this->addUsingAlias(OldMonevTspKegiatanTableMap::COL_ID, $oldMonevTspKegiatan->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the eperformance.monev_tsp_kegiatan table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldMonevTspKegiatanTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            OldMonevTspKegiatanTableMap::clearInstancePool();
            OldMonevTspKegiatanTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldMonevTspKegiatanTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(OldMonevTspKegiatanTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            OldMonevTspKegiatanTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            OldMonevTspKegiatanTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 7200)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(OldMonevTspKegiatanTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(OldMonevTspKegiatanTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }


        return $con->getDataFetcher($stmt);
    }

} // OldMonevTspKegiatanQuery

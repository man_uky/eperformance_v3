<?php

namespace CoreBundle\Model\OldEperformance\Base;

use \Exception;
use \PDO;
use CoreBundle\Model\OldEperformance\OldPegawai as ChildOldPegawai;
use CoreBundle\Model\OldEperformance\OldPegawaiQuery as ChildOldPegawaiQuery;
use CoreBundle\Model\OldEperformance\Map\OldPegawaiTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'eperformance.pegawai' table.
 *
 *
 *
 * @method     ChildOldPegawaiQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildOldPegawaiQuery orderByNip($order = Criteria::ASC) Order by the nip column
 * @method     ChildOldPegawaiQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method     ChildOldPegawaiQuery orderByGolongan($order = Criteria::ASC) Order by the golongan column
 * @method     ChildOldPegawaiQuery orderByTmt($order = Criteria::ASC) Order by the tmt column
 * @method     ChildOldPegawaiQuery orderByPendidikanTerakhir($order = Criteria::ASC) Order by the pendidikan_terakhir column
 * @method     ChildOldPegawaiQuery orderBySkpdId($order = Criteria::ASC) Order by the skpd_id column
 * @method     ChildOldPegawaiQuery orderByAtasanId($order = Criteria::ASC) Order by the atasan_id column
 * @method     ChildOldPegawaiQuery orderByIsAtasan($order = Criteria::ASC) Order by the is_atasan column
 * @method     ChildOldPegawaiQuery orderByLevelStruktural($order = Criteria::ASC) Order by the level_struktural column
 * @method     ChildOldPegawaiQuery orderByLevelPenilaian($order = Criteria::ASC) Order by the level_penilaian column
 * @method     ChildOldPegawaiQuery orderByPassword($order = Criteria::ASC) Order by the password column
 * @method     ChildOldPegawaiQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildOldPegawaiQuery orderByUsernameEproject($order = Criteria::ASC) Order by the username_eproject column
 * @method     ChildOldPegawaiQuery orderByTglMasuk($order = Criteria::ASC) Order by the tgl_masuk column
 * @method     ChildOldPegawaiQuery orderByTglKeluar($order = Criteria::ASC) Order by the tgl_keluar column
 * @method     ChildOldPegawaiQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildOldPegawaiQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildOldPegawaiQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildOldPegawaiQuery orderByIsOut($order = Criteria::ASC) Order by the is_out column
 * @method     ChildOldPegawaiQuery orderByIsKaUptd($order = Criteria::ASC) Order by the is_ka_uptd column
 * @method     ChildOldPegawaiQuery orderByIsStruktural($order = Criteria::ASC) Order by the is_struktural column
 * @method     ChildOldPegawaiQuery orderByIsKunciAktifitas($order = Criteria::ASC) Order by the is_kunci_aktifitas column
 * @method     ChildOldPegawaiQuery orderByIsOnlySkp($order = Criteria::ASC) Order by the is_only_skp column
 * @method     ChildOldPegawaiQuery orderByJabatanStrukturalId($order = Criteria::ASC) Order by the jabatan_struktural_id column
 *
 * @method     ChildOldPegawaiQuery groupById() Group by the id column
 * @method     ChildOldPegawaiQuery groupByNip() Group by the nip column
 * @method     ChildOldPegawaiQuery groupByNama() Group by the nama column
 * @method     ChildOldPegawaiQuery groupByGolongan() Group by the golongan column
 * @method     ChildOldPegawaiQuery groupByTmt() Group by the tmt column
 * @method     ChildOldPegawaiQuery groupByPendidikanTerakhir() Group by the pendidikan_terakhir column
 * @method     ChildOldPegawaiQuery groupBySkpdId() Group by the skpd_id column
 * @method     ChildOldPegawaiQuery groupByAtasanId() Group by the atasan_id column
 * @method     ChildOldPegawaiQuery groupByIsAtasan() Group by the is_atasan column
 * @method     ChildOldPegawaiQuery groupByLevelStruktural() Group by the level_struktural column
 * @method     ChildOldPegawaiQuery groupByLevelPenilaian() Group by the level_penilaian column
 * @method     ChildOldPegawaiQuery groupByPassword() Group by the password column
 * @method     ChildOldPegawaiQuery groupByUserId() Group by the user_id column
 * @method     ChildOldPegawaiQuery groupByUsernameEproject() Group by the username_eproject column
 * @method     ChildOldPegawaiQuery groupByTglMasuk() Group by the tgl_masuk column
 * @method     ChildOldPegawaiQuery groupByTglKeluar() Group by the tgl_keluar column
 * @method     ChildOldPegawaiQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildOldPegawaiQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildOldPegawaiQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildOldPegawaiQuery groupByIsOut() Group by the is_out column
 * @method     ChildOldPegawaiQuery groupByIsKaUptd() Group by the is_ka_uptd column
 * @method     ChildOldPegawaiQuery groupByIsStruktural() Group by the is_struktural column
 * @method     ChildOldPegawaiQuery groupByIsKunciAktifitas() Group by the is_kunci_aktifitas column
 * @method     ChildOldPegawaiQuery groupByIsOnlySkp() Group by the is_only_skp column
 * @method     ChildOldPegawaiQuery groupByJabatanStrukturalId() Group by the jabatan_struktural_id column
 *
 * @method     ChildOldPegawaiQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildOldPegawaiQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildOldPegawaiQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildOldPegawaiQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildOldPegawaiQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildOldPegawaiQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildOldPegawaiQuery leftJoinOldMasterSkpd($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldMasterSkpd relation
 * @method     ChildOldPegawaiQuery rightJoinOldMasterSkpd($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldMasterSkpd relation
 * @method     ChildOldPegawaiQuery innerJoinOldMasterSkpd($relationAlias = null) Adds a INNER JOIN clause to the query using the OldMasterSkpd relation
 *
 * @method     ChildOldPegawaiQuery joinWithOldMasterSkpd($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldMasterSkpd relation
 *
 * @method     ChildOldPegawaiQuery leftJoinWithOldMasterSkpd() Adds a LEFT JOIN clause and with to the query using the OldMasterSkpd relation
 * @method     ChildOldPegawaiQuery rightJoinWithOldMasterSkpd() Adds a RIGHT JOIN clause and with to the query using the OldMasterSkpd relation
 * @method     ChildOldPegawaiQuery innerJoinWithOldMasterSkpd() Adds a INNER JOIN clause and with to the query using the OldMasterSkpd relation
 *
 * @method     ChildOldPegawaiQuery leftJoinOldPegawai($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldPegawai relation
 * @method     ChildOldPegawaiQuery rightJoinOldPegawai($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldPegawai relation
 * @method     ChildOldPegawaiQuery innerJoinOldPegawai($relationAlias = null) Adds a INNER JOIN clause to the query using the OldPegawai relation
 *
 * @method     ChildOldPegawaiQuery joinWithOldPegawai($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldPegawai relation
 *
 * @method     ChildOldPegawaiQuery leftJoinWithOldPegawai() Adds a LEFT JOIN clause and with to the query using the OldPegawai relation
 * @method     ChildOldPegawaiQuery rightJoinWithOldPegawai() Adds a RIGHT JOIN clause and with to the query using the OldPegawai relation
 * @method     ChildOldPegawaiQuery innerJoinWithOldPegawai() Adds a INNER JOIN clause and with to the query using the OldPegawai relation
 *
 * @method     ChildOldPegawaiQuery leftJoinOldJabatanStruktural($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldJabatanStruktural relation
 * @method     ChildOldPegawaiQuery rightJoinOldJabatanStruktural($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldJabatanStruktural relation
 * @method     ChildOldPegawaiQuery innerJoinOldJabatanStruktural($relationAlias = null) Adds a INNER JOIN clause to the query using the OldJabatanStruktural relation
 *
 * @method     ChildOldPegawaiQuery joinWithOldJabatanStruktural($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldJabatanStruktural relation
 *
 * @method     ChildOldPegawaiQuery leftJoinWithOldJabatanStruktural() Adds a LEFT JOIN clause and with to the query using the OldJabatanStruktural relation
 * @method     ChildOldPegawaiQuery rightJoinWithOldJabatanStruktural() Adds a RIGHT JOIN clause and with to the query using the OldJabatanStruktural relation
 * @method     ChildOldPegawaiQuery innerJoinWithOldJabatanStruktural() Adds a INNER JOIN clause and with to the query using the OldJabatanStruktural relation
 *
 * @method     ChildOldPegawaiQuery leftJoinOldPerilakuHasil($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldPerilakuHasil relation
 * @method     ChildOldPegawaiQuery rightJoinOldPerilakuHasil($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldPerilakuHasil relation
 * @method     ChildOldPegawaiQuery innerJoinOldPerilakuHasil($relationAlias = null) Adds a INNER JOIN clause to the query using the OldPerilakuHasil relation
 *
 * @method     ChildOldPegawaiQuery joinWithOldPerilakuHasil($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldPerilakuHasil relation
 *
 * @method     ChildOldPegawaiQuery leftJoinWithOldPerilakuHasil() Adds a LEFT JOIN clause and with to the query using the OldPerilakuHasil relation
 * @method     ChildOldPegawaiQuery rightJoinWithOldPerilakuHasil() Adds a RIGHT JOIN clause and with to the query using the OldPerilakuHasil relation
 * @method     ChildOldPegawaiQuery innerJoinWithOldPerilakuHasil() Adds a INNER JOIN clause and with to the query using the OldPerilakuHasil relation
 *
 * @method     ChildOldPegawaiQuery leftJoinOldPegawaiRelatedById($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldPegawaiRelatedById relation
 * @method     ChildOldPegawaiQuery rightJoinOldPegawaiRelatedById($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldPegawaiRelatedById relation
 * @method     ChildOldPegawaiQuery innerJoinOldPegawaiRelatedById($relationAlias = null) Adds a INNER JOIN clause to the query using the OldPegawaiRelatedById relation
 *
 * @method     ChildOldPegawaiQuery joinWithOldPegawaiRelatedById($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldPegawaiRelatedById relation
 *
 * @method     ChildOldPegawaiQuery leftJoinWithOldPegawaiRelatedById() Adds a LEFT JOIN clause and with to the query using the OldPegawaiRelatedById relation
 * @method     ChildOldPegawaiQuery rightJoinWithOldPegawaiRelatedById() Adds a RIGHT JOIN clause and with to the query using the OldPegawaiRelatedById relation
 * @method     ChildOldPegawaiQuery innerJoinWithOldPegawaiRelatedById() Adds a INNER JOIN clause and with to the query using the OldPegawaiRelatedById relation
 *
 * @method     ChildOldPegawaiQuery leftJoinOldIndikatorKinerja($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldIndikatorKinerja relation
 * @method     ChildOldPegawaiQuery rightJoinOldIndikatorKinerja($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldIndikatorKinerja relation
 * @method     ChildOldPegawaiQuery innerJoinOldIndikatorKinerja($relationAlias = null) Adds a INNER JOIN clause to the query using the OldIndikatorKinerja relation
 *
 * @method     ChildOldPegawaiQuery joinWithOldIndikatorKinerja($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldIndikatorKinerja relation
 *
 * @method     ChildOldPegawaiQuery leftJoinWithOldIndikatorKinerja() Adds a LEFT JOIN clause and with to the query using the OldIndikatorKinerja relation
 * @method     ChildOldPegawaiQuery rightJoinWithOldIndikatorKinerja() Adds a RIGHT JOIN clause and with to the query using the OldIndikatorKinerja relation
 * @method     ChildOldPegawaiQuery innerJoinWithOldIndikatorKinerja() Adds a INNER JOIN clause and with to the query using the OldIndikatorKinerja relation
 *
 * @method     ChildOldPegawaiQuery leftJoinOldKegiatanPegawai($relationAlias = null) Adds a LEFT JOIN clause to the query using the OldKegiatanPegawai relation
 * @method     ChildOldPegawaiQuery rightJoinOldKegiatanPegawai($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OldKegiatanPegawai relation
 * @method     ChildOldPegawaiQuery innerJoinOldKegiatanPegawai($relationAlias = null) Adds a INNER JOIN clause to the query using the OldKegiatanPegawai relation
 *
 * @method     ChildOldPegawaiQuery joinWithOldKegiatanPegawai($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OldKegiatanPegawai relation
 *
 * @method     ChildOldPegawaiQuery leftJoinWithOldKegiatanPegawai() Adds a LEFT JOIN clause and with to the query using the OldKegiatanPegawai relation
 * @method     ChildOldPegawaiQuery rightJoinWithOldKegiatanPegawai() Adds a RIGHT JOIN clause and with to the query using the OldKegiatanPegawai relation
 * @method     ChildOldPegawaiQuery innerJoinWithOldKegiatanPegawai() Adds a INNER JOIN clause and with to the query using the OldKegiatanPegawai relation
 *
 * @method     \CoreBundle\Model\OldEperformance\OldMasterSkpdQuery|\CoreBundle\Model\OldEperformance\OldPegawaiQuery|\CoreBundle\Model\OldEperformance\OldJabatanStrukturalQuery|\CoreBundle\Model\OldEperformance\OldPerilakuHasilQuery|\CoreBundle\Model\OldEperformance\OldIndikatorKinerjaQuery|\CoreBundle\Model\OldEperformance\OldKegiatanPegawaiQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildOldPegawai findOne(ConnectionInterface $con = null) Return the first ChildOldPegawai matching the query
 * @method     ChildOldPegawai findOneOrCreate(ConnectionInterface $con = null) Return the first ChildOldPegawai matching the query, or a new ChildOldPegawai object populated from the query conditions when no match is found
 *
 * @method     ChildOldPegawai findOneById(int $id) Return the first ChildOldPegawai filtered by the id column
 * @method     ChildOldPegawai findOneByNip(string $nip) Return the first ChildOldPegawai filtered by the nip column
 * @method     ChildOldPegawai findOneByNama(string $nama) Return the first ChildOldPegawai filtered by the nama column
 * @method     ChildOldPegawai findOneByGolongan(string $golongan) Return the first ChildOldPegawai filtered by the golongan column
 * @method     ChildOldPegawai findOneByTmt(string $tmt) Return the first ChildOldPegawai filtered by the tmt column
 * @method     ChildOldPegawai findOneByPendidikanTerakhir(string $pendidikan_terakhir) Return the first ChildOldPegawai filtered by the pendidikan_terakhir column
 * @method     ChildOldPegawai findOneBySkpdId(int $skpd_id) Return the first ChildOldPegawai filtered by the skpd_id column
 * @method     ChildOldPegawai findOneByAtasanId(int $atasan_id) Return the first ChildOldPegawai filtered by the atasan_id column
 * @method     ChildOldPegawai findOneByIsAtasan(boolean $is_atasan) Return the first ChildOldPegawai filtered by the is_atasan column
 * @method     ChildOldPegawai findOneByLevelStruktural(int $level_struktural) Return the first ChildOldPegawai filtered by the level_struktural column
 * @method     ChildOldPegawai findOneByLevelPenilaian(int $level_penilaian) Return the first ChildOldPegawai filtered by the level_penilaian column
 * @method     ChildOldPegawai findOneByPassword(string $password) Return the first ChildOldPegawai filtered by the password column
 * @method     ChildOldPegawai findOneByUserId(int $user_id) Return the first ChildOldPegawai filtered by the user_id column
 * @method     ChildOldPegawai findOneByUsernameEproject(string $username_eproject) Return the first ChildOldPegawai filtered by the username_eproject column
 * @method     ChildOldPegawai findOneByTglMasuk(string $tgl_masuk) Return the first ChildOldPegawai filtered by the tgl_masuk column
 * @method     ChildOldPegawai findOneByTglKeluar(string $tgl_keluar) Return the first ChildOldPegawai filtered by the tgl_keluar column
 * @method     ChildOldPegawai findOneByCreatedAt(string $created_at) Return the first ChildOldPegawai filtered by the created_at column
 * @method     ChildOldPegawai findOneByUpdatedAt(string $updated_at) Return the first ChildOldPegawai filtered by the updated_at column
 * @method     ChildOldPegawai findOneByDeletedAt(string $deleted_at) Return the first ChildOldPegawai filtered by the deleted_at column
 * @method     ChildOldPegawai findOneByIsOut(boolean $is_out) Return the first ChildOldPegawai filtered by the is_out column
 * @method     ChildOldPegawai findOneByIsKaUptd(boolean $is_ka_uptd) Return the first ChildOldPegawai filtered by the is_ka_uptd column
 * @method     ChildOldPegawai findOneByIsStruktural(boolean $is_struktural) Return the first ChildOldPegawai filtered by the is_struktural column
 * @method     ChildOldPegawai findOneByIsKunciAktifitas(boolean $is_kunci_aktifitas) Return the first ChildOldPegawai filtered by the is_kunci_aktifitas column
 * @method     ChildOldPegawai findOneByIsOnlySkp(boolean $is_only_skp) Return the first ChildOldPegawai filtered by the is_only_skp column
 * @method     ChildOldPegawai findOneByJabatanStrukturalId(int $jabatan_struktural_id) Return the first ChildOldPegawai filtered by the jabatan_struktural_id column *

 * @method     ChildOldPegawai requirePk($key, ConnectionInterface $con = null) Return the ChildOldPegawai by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOne(ConnectionInterface $con = null) Return the first ChildOldPegawai matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOldPegawai requireOneById(int $id) Return the first ChildOldPegawai filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByNip(string $nip) Return the first ChildOldPegawai filtered by the nip column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByNama(string $nama) Return the first ChildOldPegawai filtered by the nama column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByGolongan(string $golongan) Return the first ChildOldPegawai filtered by the golongan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByTmt(string $tmt) Return the first ChildOldPegawai filtered by the tmt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByPendidikanTerakhir(string $pendidikan_terakhir) Return the first ChildOldPegawai filtered by the pendidikan_terakhir column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneBySkpdId(int $skpd_id) Return the first ChildOldPegawai filtered by the skpd_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByAtasanId(int $atasan_id) Return the first ChildOldPegawai filtered by the atasan_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByIsAtasan(boolean $is_atasan) Return the first ChildOldPegawai filtered by the is_atasan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByLevelStruktural(int $level_struktural) Return the first ChildOldPegawai filtered by the level_struktural column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByLevelPenilaian(int $level_penilaian) Return the first ChildOldPegawai filtered by the level_penilaian column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByPassword(string $password) Return the first ChildOldPegawai filtered by the password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByUserId(int $user_id) Return the first ChildOldPegawai filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByUsernameEproject(string $username_eproject) Return the first ChildOldPegawai filtered by the username_eproject column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByTglMasuk(string $tgl_masuk) Return the first ChildOldPegawai filtered by the tgl_masuk column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByTglKeluar(string $tgl_keluar) Return the first ChildOldPegawai filtered by the tgl_keluar column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByCreatedAt(string $created_at) Return the first ChildOldPegawai filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByUpdatedAt(string $updated_at) Return the first ChildOldPegawai filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByDeletedAt(string $deleted_at) Return the first ChildOldPegawai filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByIsOut(boolean $is_out) Return the first ChildOldPegawai filtered by the is_out column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByIsKaUptd(boolean $is_ka_uptd) Return the first ChildOldPegawai filtered by the is_ka_uptd column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByIsStruktural(boolean $is_struktural) Return the first ChildOldPegawai filtered by the is_struktural column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByIsKunciAktifitas(boolean $is_kunci_aktifitas) Return the first ChildOldPegawai filtered by the is_kunci_aktifitas column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByIsOnlySkp(boolean $is_only_skp) Return the first ChildOldPegawai filtered by the is_only_skp column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOldPegawai requireOneByJabatanStrukturalId(int $jabatan_struktural_id) Return the first ChildOldPegawai filtered by the jabatan_struktural_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOldPegawai[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildOldPegawai objects based on current ModelCriteria
 * @method     ChildOldPegawai[]|ObjectCollection findById(int $id) Return ChildOldPegawai objects filtered by the id column
 * @method     ChildOldPegawai[]|ObjectCollection findByNip(string $nip) Return ChildOldPegawai objects filtered by the nip column
 * @method     ChildOldPegawai[]|ObjectCollection findByNama(string $nama) Return ChildOldPegawai objects filtered by the nama column
 * @method     ChildOldPegawai[]|ObjectCollection findByGolongan(string $golongan) Return ChildOldPegawai objects filtered by the golongan column
 * @method     ChildOldPegawai[]|ObjectCollection findByTmt(string $tmt) Return ChildOldPegawai objects filtered by the tmt column
 * @method     ChildOldPegawai[]|ObjectCollection findByPendidikanTerakhir(string $pendidikan_terakhir) Return ChildOldPegawai objects filtered by the pendidikan_terakhir column
 * @method     ChildOldPegawai[]|ObjectCollection findBySkpdId(int $skpd_id) Return ChildOldPegawai objects filtered by the skpd_id column
 * @method     ChildOldPegawai[]|ObjectCollection findByAtasanId(int $atasan_id) Return ChildOldPegawai objects filtered by the atasan_id column
 * @method     ChildOldPegawai[]|ObjectCollection findByIsAtasan(boolean $is_atasan) Return ChildOldPegawai objects filtered by the is_atasan column
 * @method     ChildOldPegawai[]|ObjectCollection findByLevelStruktural(int $level_struktural) Return ChildOldPegawai objects filtered by the level_struktural column
 * @method     ChildOldPegawai[]|ObjectCollection findByLevelPenilaian(int $level_penilaian) Return ChildOldPegawai objects filtered by the level_penilaian column
 * @method     ChildOldPegawai[]|ObjectCollection findByPassword(string $password) Return ChildOldPegawai objects filtered by the password column
 * @method     ChildOldPegawai[]|ObjectCollection findByUserId(int $user_id) Return ChildOldPegawai objects filtered by the user_id column
 * @method     ChildOldPegawai[]|ObjectCollection findByUsernameEproject(string $username_eproject) Return ChildOldPegawai objects filtered by the username_eproject column
 * @method     ChildOldPegawai[]|ObjectCollection findByTglMasuk(string $tgl_masuk) Return ChildOldPegawai objects filtered by the tgl_masuk column
 * @method     ChildOldPegawai[]|ObjectCollection findByTglKeluar(string $tgl_keluar) Return ChildOldPegawai objects filtered by the tgl_keluar column
 * @method     ChildOldPegawai[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildOldPegawai objects filtered by the created_at column
 * @method     ChildOldPegawai[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildOldPegawai objects filtered by the updated_at column
 * @method     ChildOldPegawai[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildOldPegawai objects filtered by the deleted_at column
 * @method     ChildOldPegawai[]|ObjectCollection findByIsOut(boolean $is_out) Return ChildOldPegawai objects filtered by the is_out column
 * @method     ChildOldPegawai[]|ObjectCollection findByIsKaUptd(boolean $is_ka_uptd) Return ChildOldPegawai objects filtered by the is_ka_uptd column
 * @method     ChildOldPegawai[]|ObjectCollection findByIsStruktural(boolean $is_struktural) Return ChildOldPegawai objects filtered by the is_struktural column
 * @method     ChildOldPegawai[]|ObjectCollection findByIsKunciAktifitas(boolean $is_kunci_aktifitas) Return ChildOldPegawai objects filtered by the is_kunci_aktifitas column
 * @method     ChildOldPegawai[]|ObjectCollection findByIsOnlySkp(boolean $is_only_skp) Return ChildOldPegawai objects filtered by the is_only_skp column
 * @method     ChildOldPegawai[]|ObjectCollection findByJabatanStrukturalId(int $jabatan_struktural_id) Return ChildOldPegawai objects filtered by the jabatan_struktural_id column
 * @method     ChildOldPegawai[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class OldPegawaiQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \CoreBundle\Model\OldEperformance\Base\OldPegawaiQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'old_eperformance', $modelName = '\\CoreBundle\\Model\\OldEperformance\\OldPegawai', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildOldPegawaiQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildOldPegawaiQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildOldPegawaiQuery) {
            return $criteria;
        }
        $query = new ChildOldPegawaiQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildOldPegawai|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(OldPegawaiTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = OldPegawaiTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldPegawai A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, nip, nama, golongan, tmt, pendidikan_terakhir, skpd_id, atasan_id, is_atasan, level_struktural, level_penilaian, password, user_id, username_eproject, tgl_masuk, tgl_keluar, created_at, updated_at, deleted_at, is_out, is_ka_uptd, is_struktural, is_kunci_aktifitas, is_only_skp, jabatan_struktural_id FROM eperformance.pegawai WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildOldPegawai $obj */
            $obj = new ChildOldPegawai();
            $obj->hydrate($row);
            OldPegawaiTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildOldPegawai|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(OldPegawaiTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(OldPegawaiTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the nip column
     *
     * Example usage:
     * <code>
     * $query->filterByNip('fooValue');   // WHERE nip = 'fooValue'
     * $query->filterByNip('%fooValue%', Criteria::LIKE); // WHERE nip LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nip The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByNip($nip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nip)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_NIP, $nip, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%', Criteria::LIKE); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the golongan column
     *
     * Example usage:
     * <code>
     * $query->filterByGolongan('fooValue');   // WHERE golongan = 'fooValue'
     * $query->filterByGolongan('%fooValue%', Criteria::LIKE); // WHERE golongan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $golongan The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByGolongan($golongan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($golongan)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_GOLONGAN, $golongan, $comparison);
    }

    /**
     * Filter the query on the tmt column
     *
     * Example usage:
     * <code>
     * $query->filterByTmt('2011-03-14'); // WHERE tmt = '2011-03-14'
     * $query->filterByTmt('now'); // WHERE tmt = '2011-03-14'
     * $query->filterByTmt(array('max' => 'yesterday')); // WHERE tmt > '2011-03-13'
     * </code>
     *
     * @param     mixed $tmt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByTmt($tmt = null, $comparison = null)
    {
        if (is_array($tmt)) {
            $useMinMax = false;
            if (isset($tmt['min'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_TMT, $tmt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tmt['max'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_TMT, $tmt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_TMT, $tmt, $comparison);
    }

    /**
     * Filter the query on the pendidikan_terakhir column
     *
     * Example usage:
     * <code>
     * $query->filterByPendidikanTerakhir('fooValue');   // WHERE pendidikan_terakhir = 'fooValue'
     * $query->filterByPendidikanTerakhir('%fooValue%', Criteria::LIKE); // WHERE pendidikan_terakhir LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pendidikanTerakhir The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByPendidikanTerakhir($pendidikanTerakhir = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pendidikanTerakhir)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_PENDIDIKAN_TERAKHIR, $pendidikanTerakhir, $comparison);
    }

    /**
     * Filter the query on the skpd_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySkpdId(1234); // WHERE skpd_id = 1234
     * $query->filterBySkpdId(array(12, 34)); // WHERE skpd_id IN (12, 34)
     * $query->filterBySkpdId(array('min' => 12)); // WHERE skpd_id > 12
     * </code>
     *
     * @see       filterByOldMasterSkpd()
     *
     * @param     mixed $skpdId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterBySkpdId($skpdId = null, $comparison = null)
    {
        if (is_array($skpdId)) {
            $useMinMax = false;
            if (isset($skpdId['min'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_SKPD_ID, $skpdId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($skpdId['max'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_SKPD_ID, $skpdId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_SKPD_ID, $skpdId, $comparison);
    }

    /**
     * Filter the query on the atasan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAtasanId(1234); // WHERE atasan_id = 1234
     * $query->filterByAtasanId(array(12, 34)); // WHERE atasan_id IN (12, 34)
     * $query->filterByAtasanId(array('min' => 12)); // WHERE atasan_id > 12
     * </code>
     *
     * @see       filterByOldPegawai()
     *
     * @param     mixed $atasanId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByAtasanId($atasanId = null, $comparison = null)
    {
        if (is_array($atasanId)) {
            $useMinMax = false;
            if (isset($atasanId['min'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_ATASAN_ID, $atasanId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($atasanId['max'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_ATASAN_ID, $atasanId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_ATASAN_ID, $atasanId, $comparison);
    }

    /**
     * Filter the query on the is_atasan column
     *
     * Example usage:
     * <code>
     * $query->filterByIsAtasan(true); // WHERE is_atasan = true
     * $query->filterByIsAtasan('yes'); // WHERE is_atasan = true
     * </code>
     *
     * @param     boolean|string $isAtasan The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByIsAtasan($isAtasan = null, $comparison = null)
    {
        if (is_string($isAtasan)) {
            $isAtasan = in_array(strtolower($isAtasan), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_IS_ATASAN, $isAtasan, $comparison);
    }

    /**
     * Filter the query on the level_struktural column
     *
     * Example usage:
     * <code>
     * $query->filterByLevelStruktural(1234); // WHERE level_struktural = 1234
     * $query->filterByLevelStruktural(array(12, 34)); // WHERE level_struktural IN (12, 34)
     * $query->filterByLevelStruktural(array('min' => 12)); // WHERE level_struktural > 12
     * </code>
     *
     * @param     mixed $levelStruktural The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByLevelStruktural($levelStruktural = null, $comparison = null)
    {
        if (is_array($levelStruktural)) {
            $useMinMax = false;
            if (isset($levelStruktural['min'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_LEVEL_STRUKTURAL, $levelStruktural['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($levelStruktural['max'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_LEVEL_STRUKTURAL, $levelStruktural['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_LEVEL_STRUKTURAL, $levelStruktural, $comparison);
    }

    /**
     * Filter the query on the level_penilaian column
     *
     * Example usage:
     * <code>
     * $query->filterByLevelPenilaian(1234); // WHERE level_penilaian = 1234
     * $query->filterByLevelPenilaian(array(12, 34)); // WHERE level_penilaian IN (12, 34)
     * $query->filterByLevelPenilaian(array('min' => 12)); // WHERE level_penilaian > 12
     * </code>
     *
     * @param     mixed $levelPenilaian The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByLevelPenilaian($levelPenilaian = null, $comparison = null)
    {
        if (is_array($levelPenilaian)) {
            $useMinMax = false;
            if (isset($levelPenilaian['min'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_LEVEL_PENILAIAN, $levelPenilaian['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($levelPenilaian['max'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_LEVEL_PENILAIAN, $levelPenilaian['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_LEVEL_PENILAIAN, $levelPenilaian, $comparison);
    }

    /**
     * Filter the query on the password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE password = 'fooValue'
     * $query->filterByPassword('%fooValue%', Criteria::LIKE); // WHERE password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the username_eproject column
     *
     * Example usage:
     * <code>
     * $query->filterByUsernameEproject('fooValue');   // WHERE username_eproject = 'fooValue'
     * $query->filterByUsernameEproject('%fooValue%', Criteria::LIKE); // WHERE username_eproject LIKE '%fooValue%'
     * </code>
     *
     * @param     string $usernameEproject The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByUsernameEproject($usernameEproject = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($usernameEproject)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_USERNAME_EPROJECT, $usernameEproject, $comparison);
    }

    /**
     * Filter the query on the tgl_masuk column
     *
     * Example usage:
     * <code>
     * $query->filterByTglMasuk('2011-03-14'); // WHERE tgl_masuk = '2011-03-14'
     * $query->filterByTglMasuk('now'); // WHERE tgl_masuk = '2011-03-14'
     * $query->filterByTglMasuk(array('max' => 'yesterday')); // WHERE tgl_masuk > '2011-03-13'
     * </code>
     *
     * @param     mixed $tglMasuk The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByTglMasuk($tglMasuk = null, $comparison = null)
    {
        if (is_array($tglMasuk)) {
            $useMinMax = false;
            if (isset($tglMasuk['min'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_TGL_MASUK, $tglMasuk['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tglMasuk['max'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_TGL_MASUK, $tglMasuk['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_TGL_MASUK, $tglMasuk, $comparison);
    }

    /**
     * Filter the query on the tgl_keluar column
     *
     * Example usage:
     * <code>
     * $query->filterByTglKeluar('2011-03-14'); // WHERE tgl_keluar = '2011-03-14'
     * $query->filterByTglKeluar('now'); // WHERE tgl_keluar = '2011-03-14'
     * $query->filterByTglKeluar(array('max' => 'yesterday')); // WHERE tgl_keluar > '2011-03-13'
     * </code>
     *
     * @param     mixed $tglKeluar The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByTglKeluar($tglKeluar = null, $comparison = null)
    {
        if (is_array($tglKeluar)) {
            $useMinMax = false;
            if (isset($tglKeluar['min'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_TGL_KELUAR, $tglKeluar['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tglKeluar['max'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_TGL_KELUAR, $tglKeluar['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_TGL_KELUAR, $tglKeluar, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the is_out column
     *
     * Example usage:
     * <code>
     * $query->filterByIsOut(true); // WHERE is_out = true
     * $query->filterByIsOut('yes'); // WHERE is_out = true
     * </code>
     *
     * @param     boolean|string $isOut The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByIsOut($isOut = null, $comparison = null)
    {
        if (is_string($isOut)) {
            $isOut = in_array(strtolower($isOut), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_IS_OUT, $isOut, $comparison);
    }

    /**
     * Filter the query on the is_ka_uptd column
     *
     * Example usage:
     * <code>
     * $query->filterByIsKaUptd(true); // WHERE is_ka_uptd = true
     * $query->filterByIsKaUptd('yes'); // WHERE is_ka_uptd = true
     * </code>
     *
     * @param     boolean|string $isKaUptd The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByIsKaUptd($isKaUptd = null, $comparison = null)
    {
        if (is_string($isKaUptd)) {
            $isKaUptd = in_array(strtolower($isKaUptd), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_IS_KA_UPTD, $isKaUptd, $comparison);
    }

    /**
     * Filter the query on the is_struktural column
     *
     * Example usage:
     * <code>
     * $query->filterByIsStruktural(true); // WHERE is_struktural = true
     * $query->filterByIsStruktural('yes'); // WHERE is_struktural = true
     * </code>
     *
     * @param     boolean|string $isStruktural The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByIsStruktural($isStruktural = null, $comparison = null)
    {
        if (is_string($isStruktural)) {
            $isStruktural = in_array(strtolower($isStruktural), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_IS_STRUKTURAL, $isStruktural, $comparison);
    }

    /**
     * Filter the query on the is_kunci_aktifitas column
     *
     * Example usage:
     * <code>
     * $query->filterByIsKunciAktifitas(true); // WHERE is_kunci_aktifitas = true
     * $query->filterByIsKunciAktifitas('yes'); // WHERE is_kunci_aktifitas = true
     * </code>
     *
     * @param     boolean|string $isKunciAktifitas The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByIsKunciAktifitas($isKunciAktifitas = null, $comparison = null)
    {
        if (is_string($isKunciAktifitas)) {
            $isKunciAktifitas = in_array(strtolower($isKunciAktifitas), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_IS_KUNCI_AKTIFITAS, $isKunciAktifitas, $comparison);
    }

    /**
     * Filter the query on the is_only_skp column
     *
     * Example usage:
     * <code>
     * $query->filterByIsOnlySkp(true); // WHERE is_only_skp = true
     * $query->filterByIsOnlySkp('yes'); // WHERE is_only_skp = true
     * </code>
     *
     * @param     boolean|string $isOnlySkp The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByIsOnlySkp($isOnlySkp = null, $comparison = null)
    {
        if (is_string($isOnlySkp)) {
            $isOnlySkp = in_array(strtolower($isOnlySkp), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_IS_ONLY_SKP, $isOnlySkp, $comparison);
    }

    /**
     * Filter the query on the jabatan_struktural_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJabatanStrukturalId(1234); // WHERE jabatan_struktural_id = 1234
     * $query->filterByJabatanStrukturalId(array(12, 34)); // WHERE jabatan_struktural_id IN (12, 34)
     * $query->filterByJabatanStrukturalId(array('min' => 12)); // WHERE jabatan_struktural_id > 12
     * </code>
     *
     * @see       filterByOldJabatanStruktural()
     *
     * @param     mixed $jabatanStrukturalId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByJabatanStrukturalId($jabatanStrukturalId = null, $comparison = null)
    {
        if (is_array($jabatanStrukturalId)) {
            $useMinMax = false;
            if (isset($jabatanStrukturalId['min'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_JABATAN_STRUKTURAL_ID, $jabatanStrukturalId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jabatanStrukturalId['max'])) {
                $this->addUsingAlias(OldPegawaiTableMap::COL_JABATAN_STRUKTURAL_ID, $jabatanStrukturalId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OldPegawaiTableMap::COL_JABATAN_STRUKTURAL_ID, $jabatanStrukturalId, $comparison);
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldMasterSkpd object
     *
     * @param \CoreBundle\Model\OldEperformance\OldMasterSkpd|ObjectCollection $oldMasterSkpd The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByOldMasterSkpd($oldMasterSkpd, $comparison = null)
    {
        if ($oldMasterSkpd instanceof \CoreBundle\Model\OldEperformance\OldMasterSkpd) {
            return $this
                ->addUsingAlias(OldPegawaiTableMap::COL_SKPD_ID, $oldMasterSkpd->getSkpdId(), $comparison);
        } elseif ($oldMasterSkpd instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OldPegawaiTableMap::COL_SKPD_ID, $oldMasterSkpd->toKeyValue('PrimaryKey', 'SkpdId'), $comparison);
        } else {
            throw new PropelException('filterByOldMasterSkpd() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldMasterSkpd or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldMasterSkpd relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function joinOldMasterSkpd($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldMasterSkpd');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldMasterSkpd');
        }

        return $this;
    }

    /**
     * Use the OldMasterSkpd relation OldMasterSkpd object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldMasterSkpdQuery A secondary query class using the current class as primary query
     */
    public function useOldMasterSkpdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldMasterSkpd($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldMasterSkpd', '\CoreBundle\Model\OldEperformance\OldMasterSkpdQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldPegawai object
     *
     * @param \CoreBundle\Model\OldEperformance\OldPegawai|ObjectCollection $oldPegawai The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByOldPegawai($oldPegawai, $comparison = null)
    {
        if ($oldPegawai instanceof \CoreBundle\Model\OldEperformance\OldPegawai) {
            return $this
                ->addUsingAlias(OldPegawaiTableMap::COL_ATASAN_ID, $oldPegawai->getId(), $comparison);
        } elseif ($oldPegawai instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OldPegawaiTableMap::COL_ATASAN_ID, $oldPegawai->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByOldPegawai() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldPegawai or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldPegawai relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function joinOldPegawai($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldPegawai');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldPegawai');
        }

        return $this;
    }

    /**
     * Use the OldPegawai relation OldPegawai object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldPegawaiQuery A secondary query class using the current class as primary query
     */
    public function useOldPegawaiQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldPegawai($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldPegawai', '\CoreBundle\Model\OldEperformance\OldPegawaiQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldJabatanStruktural object
     *
     * @param \CoreBundle\Model\OldEperformance\OldJabatanStruktural|ObjectCollection $oldJabatanStruktural The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByOldJabatanStruktural($oldJabatanStruktural, $comparison = null)
    {
        if ($oldJabatanStruktural instanceof \CoreBundle\Model\OldEperformance\OldJabatanStruktural) {
            return $this
                ->addUsingAlias(OldPegawaiTableMap::COL_JABATAN_STRUKTURAL_ID, $oldJabatanStruktural->getId(), $comparison);
        } elseif ($oldJabatanStruktural instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OldPegawaiTableMap::COL_JABATAN_STRUKTURAL_ID, $oldJabatanStruktural->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByOldJabatanStruktural() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldJabatanStruktural or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldJabatanStruktural relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function joinOldJabatanStruktural($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldJabatanStruktural');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldJabatanStruktural');
        }

        return $this;
    }

    /**
     * Use the OldJabatanStruktural relation OldJabatanStruktural object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldJabatanStrukturalQuery A secondary query class using the current class as primary query
     */
    public function useOldJabatanStrukturalQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldJabatanStruktural($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldJabatanStruktural', '\CoreBundle\Model\OldEperformance\OldJabatanStrukturalQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldPerilakuHasil object
     *
     * @param \CoreBundle\Model\OldEperformance\OldPerilakuHasil|ObjectCollection $oldPerilakuHasil the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByOldPerilakuHasil($oldPerilakuHasil, $comparison = null)
    {
        if ($oldPerilakuHasil instanceof \CoreBundle\Model\OldEperformance\OldPerilakuHasil) {
            return $this
                ->addUsingAlias(OldPegawaiTableMap::COL_ID, $oldPerilakuHasil->getPegawaiId(), $comparison);
        } elseif ($oldPerilakuHasil instanceof ObjectCollection) {
            return $this
                ->useOldPerilakuHasilQuery()
                ->filterByPrimaryKeys($oldPerilakuHasil->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOldPerilakuHasil() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldPerilakuHasil or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldPerilakuHasil relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function joinOldPerilakuHasil($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldPerilakuHasil');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldPerilakuHasil');
        }

        return $this;
    }

    /**
     * Use the OldPerilakuHasil relation OldPerilakuHasil object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldPerilakuHasilQuery A secondary query class using the current class as primary query
     */
    public function useOldPerilakuHasilQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldPerilakuHasil($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldPerilakuHasil', '\CoreBundle\Model\OldEperformance\OldPerilakuHasilQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldPegawai object
     *
     * @param \CoreBundle\Model\OldEperformance\OldPegawai|ObjectCollection $oldPegawai the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByOldPegawaiRelatedById($oldPegawai, $comparison = null)
    {
        if ($oldPegawai instanceof \CoreBundle\Model\OldEperformance\OldPegawai) {
            return $this
                ->addUsingAlias(OldPegawaiTableMap::COL_ID, $oldPegawai->getAtasanId(), $comparison);
        } elseif ($oldPegawai instanceof ObjectCollection) {
            return $this
                ->useOldPegawaiRelatedByIdQuery()
                ->filterByPrimaryKeys($oldPegawai->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOldPegawaiRelatedById() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldPegawai or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldPegawaiRelatedById relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function joinOldPegawaiRelatedById($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldPegawaiRelatedById');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldPegawaiRelatedById');
        }

        return $this;
    }

    /**
     * Use the OldPegawaiRelatedById relation OldPegawai object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldPegawaiQuery A secondary query class using the current class as primary query
     */
    public function useOldPegawaiRelatedByIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldPegawaiRelatedById($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldPegawaiRelatedById', '\CoreBundle\Model\OldEperformance\OldPegawaiQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldIndikatorKinerja object
     *
     * @param \CoreBundle\Model\OldEperformance\OldIndikatorKinerja|ObjectCollection $oldIndikatorKinerja the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByOldIndikatorKinerja($oldIndikatorKinerja, $comparison = null)
    {
        if ($oldIndikatorKinerja instanceof \CoreBundle\Model\OldEperformance\OldIndikatorKinerja) {
            return $this
                ->addUsingAlias(OldPegawaiTableMap::COL_ID, $oldIndikatorKinerja->getPegawaiId(), $comparison);
        } elseif ($oldIndikatorKinerja instanceof ObjectCollection) {
            return $this
                ->useOldIndikatorKinerjaQuery()
                ->filterByPrimaryKeys($oldIndikatorKinerja->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOldIndikatorKinerja() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldIndikatorKinerja or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldIndikatorKinerja relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function joinOldIndikatorKinerja($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldIndikatorKinerja');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldIndikatorKinerja');
        }

        return $this;
    }

    /**
     * Use the OldIndikatorKinerja relation OldIndikatorKinerja object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldIndikatorKinerjaQuery A secondary query class using the current class as primary query
     */
    public function useOldIndikatorKinerjaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOldIndikatorKinerja($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldIndikatorKinerja', '\CoreBundle\Model\OldEperformance\OldIndikatorKinerjaQuery');
    }

    /**
     * Filter the query by a related \CoreBundle\Model\OldEperformance\OldKegiatanPegawai object
     *
     * @param \CoreBundle\Model\OldEperformance\OldKegiatanPegawai|ObjectCollection $oldKegiatanPegawai the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function filterByOldKegiatanPegawai($oldKegiatanPegawai, $comparison = null)
    {
        if ($oldKegiatanPegawai instanceof \CoreBundle\Model\OldEperformance\OldKegiatanPegawai) {
            return $this
                ->addUsingAlias(OldPegawaiTableMap::COL_ID, $oldKegiatanPegawai->getPegawaiId(), $comparison);
        } elseif ($oldKegiatanPegawai instanceof ObjectCollection) {
            return $this
                ->useOldKegiatanPegawaiQuery()
                ->filterByPrimaryKeys($oldKegiatanPegawai->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOldKegiatanPegawai() only accepts arguments of type \CoreBundle\Model\OldEperformance\OldKegiatanPegawai or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OldKegiatanPegawai relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function joinOldKegiatanPegawai($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OldKegiatanPegawai');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OldKegiatanPegawai');
        }

        return $this;
    }

    /**
     * Use the OldKegiatanPegawai relation OldKegiatanPegawai object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CoreBundle\Model\OldEperformance\OldKegiatanPegawaiQuery A secondary query class using the current class as primary query
     */
    public function useOldKegiatanPegawaiQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinOldKegiatanPegawai($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OldKegiatanPegawai', '\CoreBundle\Model\OldEperformance\OldKegiatanPegawaiQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildOldPegawai $oldPegawai Object to remove from the list of results
     *
     * @return $this|ChildOldPegawaiQuery The current query, for fluid interface
     */
    public function prune($oldPegawai = null)
    {
        if ($oldPegawai) {
            $this->addUsingAlias(OldPegawaiTableMap::COL_ID, $oldPegawai->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the eperformance.pegawai table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldPegawaiTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            OldPegawaiTableMap::clearInstancePool();
            OldPegawaiTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OldPegawaiTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(OldPegawaiTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            OldPegawaiTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            OldPegawaiTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 7200)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(OldPegawaiTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(OldPegawaiTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }


        return $con->getDataFetcher($stmt);
    }

} // OldPegawaiQuery

<?php

namespace CoreBundle\Model\OldEperformance;

use CoreBundle\Model\OldEperformance\Base\OldIndikatorKinerjaQuery as BaseOldIndikatorKinerjaQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'eperformance.indikator_kinerja' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class OldIndikatorKinerjaQuery extends BaseOldIndikatorKinerjaQuery
{

}

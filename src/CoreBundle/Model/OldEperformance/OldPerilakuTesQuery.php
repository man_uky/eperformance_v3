<?php

namespace CoreBundle\Model\OldEperformance;

use CoreBundle\Model\OldEperformance\Base\OldPerilakuTesQuery as BaseOldPerilakuTesQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'eperformance.skp_perilaku_kerja_tes' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class OldPerilakuTesQuery extends BaseOldPerilakuTesQuery
{

}

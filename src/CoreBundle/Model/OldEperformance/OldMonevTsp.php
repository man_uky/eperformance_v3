<?php

namespace CoreBundle\Model\OldEperformance;

use CoreBundle\Model\OldEperformance\Base\OldMonevTsp as BaseOldMonevTsp;

/**
 * Skeleton subclass for representing a row from the 'eperformance.monev_tsp' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class OldMonevTsp extends BaseOldMonevTsp
{

}

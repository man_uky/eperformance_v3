<?php

namespace CoreBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use CoreBundle\Model\Eperformance\PenggunaMasterQuery;
use CoreBundle\Model\Eperformance\PegawaiMasterQuery;
use CoreBundle\Model\Eperformance\PenggunaMaster;
use Propel\Runtime\ActiveQuery\Criteria;

class SetPasswordCommand extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "app/console")
            ->setName('app:set-password')

            // the short description shown while running "php app/console list"
            ->setDescription('Set Password Command.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows you to Set Password")
                
            // configure an argument
            //->addArgument('unit_name', InputArgument::REQUIRED, 'The name of entity.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        /*
        $pengguna = new PenggunaMaster();
        $pengguna->setUsername('api-tesperilaku');
        $pengguna->setPassword(password_hash('1', PASSWORD_BCRYPT));
        $pengguna->setNama('B.T. Basuki');
        $pengguna->setIsAktif(true);
        $pengguna->setCredentialId(9);
        $pengguna->save();
         * 
         */
        
        $pengguna = PenggunaMasterQuery::create()->findPk(3);
        $pengguna->setPassword(password_hash('C0WGlZiiog6Exym5C', PASSWORD_BCRYPT));
        $pengguna->save();
    }
}


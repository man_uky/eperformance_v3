<?php

namespace CoreBundle\Controller;

use CoreBundle\Util\SessionFilter;
use Eperformance\LogBundle\Form\Type\FilterType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) 
    {
        $authorizationChecker = $this->get('security.authorization_checker');
        
        if ($authorizationChecker->isGranted('ROLE_USER')) {
            if ($this->getUser()->hasRoles('ROLE_API_PEGAWAI_VIEW')) {
                return $this->redirect($this->generateUrl('api_pegawai_view'));
            } elseif ($this->getUser()->hasRoles('ROLE_API_INDIKATOR_VIEW')) {
                return $this->redirect($this->generateUrl('api_indikator_view'));
            } elseif ($this->getUser()->hasRoles('ROLE_API_TESPERILAKU_VIEW')) {
                return $this->redirect($this->generateUrl('api_tesperilaku_view'));
            }

            $sessionFilter = new SessionFilter($this->get('session'), 'filters');
            if ($request->query->has('_reset')) {
                $sessionFilter->set([]);
            }
            
            $formFilter = $this->createForm(FilterType::class, $sessionFilter->get([]));
            $formFilter->handleRequest($request);  
            if ($formFilter->isSubmitted() && $formFilter->isValid()) {
                $sessionFilter->set($formFilter->getData());
            }

            return $this->render('default/index.html.twig', [
                'formFilter' => $formFilter->createView()
            ]);
        } else {
            $authenticationUtils = $this->get('security.authentication_utils');

            // get the login error if there is one
            $error = $authenticationUtils->getLastAuthenticationError();

            // last username entered by the user
            $lastUsername = $authenticationUtils->getLastUsername();

            return $this->render('security/login.html.twig', [
                // last username entered by the user
                'last_username' => $lastUsername,
                'error' => $error,
            ]);
        }
    }
    
    /**
     * @Route("/login-check", name="login_check")
     */
    public function loginCheckAction()
    {
        // this controller will not be executed,
        // as the route is handled by the Security system
    }
    
    /**
     * @Route("/logout", name="logout_route")
     */
    public function logoutAction()
    {
        throw new \RuntimeException('You must activate the logout in your security firewall configuration.');
    }
}
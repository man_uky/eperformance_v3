<?php

namespace CoreBundle\Controller;

use CoreBundle\Model\OldEperformance\OldPegawaiQuery;
use CoreBundle\Model\OldEperformance\OldIndikatorKinerjaQuery;
use CoreBundle\Model\OldEperformance\OldPerilakuHasilQuery;
use CoreBundle\Model\OldEperformance\OldMonevTspQuery;
use CoreBundle\Model\OldEperformance\OldMonevTspKegiatanQuery;

use CoreBundle\Util\Log;
use Propel\Runtime\Propel;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class ApiController extends FOSRestController
{
    /*
    http://sideways.bp.surabaya.go.id/api/indikator/view/197805212010012001
    User :bkd-api-indikator
    Pass :COWGlZiiog6Exym5C

    http://sideways.bp.surabaya.go.id/api/tesperilaku/view/197805212010012001
    user : bkd-api-tesperilaku
    Pass :COWGlZiiog6Exym5C

    http://sideways.bp.surabaya.go.id/api/pegawai/view/0800
    user : bkd-api-pegawai
    pass : COWGlZiiog6Exym5C
    */


    /**
     * @Rest\Get("/api/pegawai/view/{param}", name="api_pegawai_view")
     * @Security("has_role('ROLE_API_PEGAWAI_VIEW')")
     */
    public function pegawaiAction(Request $request, $param = null)
    {
        $pegawai = [];
        $data = 'Invalid Url.';

        if (null !== $param) {
            $param = substr($request->get('param'), 0, 18);
            $length = strlen($param);
            $tahun = $this->getOldEperformanceTahun();
           
            if ($length == 4) {
                $pegawais = OldPegawaiQuery::create()
                    ->filterByDeletedAt(null, Criteria::ISNULL)
                    ->useOldMasterSkpdQuery()
                        ->filterBySkpdKode("%".$param."%", Criteria::ILIKE)
                    ->endUse()
                    ->orderByLevelStruktural()
                    ->orderByNama()
                    ->find();
            } elseif ($length == 18) {
                $pegawais = OldPegawaiQuery::create()
                    ->filterByDeletedAt(null, Criteria::ISNULL)
                    ->filterByNip("%".$param."%", Criteria::ILIKE)
                    ->find();
            }

            foreach ($pegawais as $p) {
                /*@var $p OldPegawai*/
                $jabatan = "Staf";
                if (null !== $p->getJabatanStrukturalId() && 0 != $p->getJabatanStrukturalId()) {
                    $jabatan = $p->getOldJabatanStruktural()->getNama();
                }

                $atasanNama = null;
                $atasanNip = null;
                if (@$p->getOldPegawai()) {
                    $atasanNama = $p->getOldPegawai()->getNama();
                    $atasanNip = $p->getOldPegawai()->getNip();
                }

                $pegawai[] = [
                    'tahun' => $tahun,
                    'skpd_kode' => $p->getOldMasterSkpd()->getSkpdKode(),
                    'skpd_nama' => $p->getOldMasterSkpd()->getSkpdNama(),
                    'nama' => $p->getNama(),
                    'nip' => $p->getNip(),
                    'jabatan' => $jabatan,
                    'atasan_nama' => $atasanNama,
                    'atasan_nip' => $atasanNip,
                ];
            }

            if ($pegawai) {
                $data = ['pegawai' => $pegawai];
            }
        }
        
        $view = $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
        
        new Log('api_pegawai_view', $this->getUser()->getUsername(), $this->getUser()->getCredentialId(), sprintf('api/pegawai/view/%s', $param), 'api_pegawai_view', ' ', $request);
        
        return $view;
    }
    
    /**
     * @Rest\Get("/api/tesperilaku/view/{param}", name="api_tesperilaku_view", requirements={"param": "\d+"})
     * @Security("has_role('ROLE_API_TESPERILAKU_VIEW')")
     */
    public function tesPerilakuAction(Request $request, $param = null)
    {
        $pegawai = [];
        $data = "Invalid Url";

        if (null !== $param) {
            $param = substr($request->get('param'), 0, 18);
            $tahun = $this->getOldEperformanceTahun();
            $triwulans  = [1, 2, 3, 4];
            $length = strlen($param);
            
            $pegawais = OldPegawaiQuery::create()
                ->filterByDeletedAt(null, Criteria::ISNULL)
                ->_if($length == 4)
                    ->joinOldMasterSkpd()
                    ->useOldMasterSkpdQuery()
                        ->filterBySkpdKode($param)
                    ->endUse()
                ->_else()
                    ->filterByNip("%".$param."%", Criteria::ILIKE)
                ->_endif()
                ->orderByLevelStruktural()
                ->orderByNama()
                ->find();

            foreach ($pegawais as $p) {
                /*@var $p OldPegawai*/
                foreach ($triwulans as $triwulan) {
                    $perilakus = OldPerilakuHasilQuery::create()
                        ->filterByPegawaiId($p->getId())
                        ->useOldPerilakuTesQuery()
                            ->filterBySkpdId($p->getSkpdId())
                            ->filterByTahun($tahun)
                            ->filterByTriwulan($triwulan)
                        ->endUse()
                        ->useOldPerilakuAspekQuery()
                            ->filterById([1,2,3,4,5,6])
                            ->orderById()
                        ->endUse()
                        ->find()
                    ;

                    $arrHasilPerilaku['Triwulan '.$triwulan] = [
                        'triwulan' => $triwulan,
                        'formulasi_spk' => '(80%xrsp) + (20%xrsrk)'
                    ];
                    foreach ($perilakus as $pk) {
                        /*@var $pk OldPerilakuHasil*/

                        $rsp = $pk->getRsp() > 0 ? (($pk->getRsp() * 55/7) + 40) : 0;
                        $rsrk = $pk->getRsrk() > 0 ? (($pk->getRsrk() * 55/7) + 40) : 0;
                        
                        $spk = (float)$pk->getSpk();
                        //$spk = (float)$this->hitungSpk($pk->getRsp(), $pk->getRsrk());

                        $arrHasilPerilaku['Triwulan '.$triwulan][] = [
                            'aspek' => $pk->getOldPerilakuAspek()->getNama(), 
                            'rsp' => $rsp, 
                            'rsrk' => $rsrk, 
                            'spk' => $spk, 
                        ];
                    }
                }
                
                $pegawai[] = [
                    'tahun' => $tahun,
                    'skpd_kode' => $p->getOldMasterSkpd()->getSkpdKode(),
                    'skpd_nama' => $p->getOldMasterSkpd()->getSkpdNama(),
                    'pegawai_nama' => $p->getNama(),
                    'pegawai_nip' => $p->getNip(),
                    'perilakus' => count($arrHasilPerilaku) > 0 ? $arrHasilPerilaku : null
                ];
            }
            
            if ($pegawai) {
                $data = ['pegawai' => $pegawai];
            }
        }
        
        $view = $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
        
        new Log('api_tesperilaku_view', $this->getUser()->getUsername(), $this->getUser()->getCredentialId(), sprintf('api/tesperilaku/view/%s', $param), 'api_tesperilaku_view', ' ', $request);
        
        return $view;
    }
    
    private function hitungSpk($rsp, $rsrk)
    {
        $spk = 40;

        if ($rsp == 0 && $rsrk == 0) {
            $spk = 0;
        } else {
            if ($rsp > 0 && $rsrk > 0) {
                $spk = ((($rsp * 0.8) + ($rsrk * 0.2)) * 55/7) + 40;
            } else if ($rsp > 0) {
                $spk = ($rsp * 55/7) + 40;
            } else if ($rsrk > 0) {
                $spk = ($rsrk * 55/7) + 40;
            }
        }

        return $spk;
    }

    /**
     * @Rest\Get("/api/indikator/view/{param}", name="api_indikator_view", requirements={"param": "\d+"})
     * @Security("has_role('ROLE_API_INDIKATOR_VIEW')")
     */
    public function indikatorAction(Request $request, $param = null)
    {
        $pegawai = [];
        $data = "Invalid Url";

        if (null !== $param) {
            $param = substr($request->get('param'), 0, 18);
            $tahun = $this->getOldEperformanceTahun();
            $length = strlen($param);
            
            $pegawais = OldPegawaiQuery::create()
                ->filterByDeletedAt(null, Criteria::ISNULL)
                ->_if($length == 4)
                    ->joinOldMasterSkpd()
                    ->useOldMasterSkpdQuery()
                        ->filterBySkpdKode($param)
                    ->endUse()
                ->_else()
                    ->filterByNip("%".$param."%", Criteria::ILIKE)
                ->_endif()
                ->orderByLevelStruktural()
                ->orderByNama()
                ->find();

            foreach ($pegawais as $p) {
                /*@var $p OldPegawai*/

                if (in_array($p->getLevelPenilaian(), array(1,2))) {
                    $indikators = OldMonevTspQuery::create()->getData($p->getSkpdId(), $p->getId());
                } else {
                    $indikators = OldMonevTspKegiatanQuery::create()->getData(12, $p->getSkpdId(), $p->getId());
                }

                $arrIndikator = [];
                foreach ($indikators as $ik) {
                    $arrIndikator[] = [
                        'id' => $ik->getId(),
                        'nama' => $ik->getIndikatorNama(),
                        'formulasi' => $ik instanceof \CoreBundle\Model\OldEperformance\OldMonevTsp ? $ik->getIndikatorFormulasi() : '',
                        'target' => $ik->getIndikatorTarget(),
                        'realisasi' => $ik->getIndikatorRealisasi(),
                        'satuan' => $ik->getIndikatorSatuan(),
                        'capaian' => $ik->getIndikatorCapaian(),
                        'jenis' => $ik instanceof \CoreBundle\Model\OldEperformance\OldMonevTsp ? $ik->getIndikatorLevel() : '4',
                    ];
                }

                $pegawai[] = [
                    'tahun' => $tahun,
                    'skpd_kode' => $p->getOldMasterSkpd()->getSkpdKode(),
                    'skpd_nama' => $p->getOldMasterSkpd()->getSkpdNama(),
                    'pegawai_nama' => $p->getNama(),
                    'pegawai_nip' => $p->getNip(),
                    'indikators' => count($arrIndikator) > 0 ? $arrIndikator : null
                ];
            }
            
            if ($pegawai) {
                $data = ['pegawai' => $pegawai];
            }
        }
        
        $view = $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
        
        new Log('api_indikator_view', $this->getUser()->getUsername(), $this->getUser()->getCredentialId(), sprintf('api/indikator/view/%s', $param), 'api_indikator_view', ' ', $request);
        
        return $view;
    }
    
    private function getOldEperformanceTahun()
    {
        $mgr = Propel::getConnectionManager('old_eperformance');
        $dsn = $mgr->getConfiguration()['dsn'];
        preg_match('/dbname=([^;]+)/', $dsn, $matches);
        
        $string = substr($matches[1], -4);
        $tahun = $string;
        
        $string_expload = explode('_', $string);
        if (count($string) > 0) {
            $tahun = $string_expload[0];
        }
        
        return $tahun;
    }
}

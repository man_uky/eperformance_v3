<?php

namespace CoreBundle\Util;

use CoreBundle\Model\Eperformance\UserLog;
use Symfony\Component\HttpFoundation\Request;

class Log
{
    private $action;
    private $username;
    private $credentialId;
    private $parameter;
    private $keterangan;
    private $macAddress;
    
    public function __construct($action, $username, $credentialId, $parameter, $keterangan, $macAddress, Request $request) 
    {
        $this->action = $action;
        $this->username = $username;
        $this->credentialId = $credentialId;
        $this->parameter = $parameter;
        $this->keterangan = $keterangan;
        $this->macAddress = $macAddress;
        
        $this->setLog($request);
    }
    
    private function setLog(Request $request)
    {
        $log = new UserLog();    
        $log->setAksi($this->action);
        $log->setUsername($this->username);
        $log->setCredentialId($this->credentialId);
        $log->setIpAddress($request->getClientIp());
        $log->setMacAddress($this->macAddress);
        $log->setParameter($this->parameter);
        $log->setKeterangan($this->keterangan);
        $log->setCreatedAt(time());
        $log->save();
        
        if ($this->action == 'login') {
            $log->setParentId($log->getId());
            $request->getSession()->set('loginId', $log->getId());
        } else {
            $log->setParentId($request->getSession()->get('loginId'));
        }
        
        $log->save();
        
        $this->cleanUp();
    }
    
    private function cleanUp()
    {
        unset($this->action);
        unset($this->username);
        unset($this->credentialId);
        unset($this->ipAddress);
        unset($this->parameter);
        unset($this->keterangan);
        unset($this->macAddress);
    }
}


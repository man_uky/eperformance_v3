<?php

namespace CoreBundle\Util;

use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Map\TableMap;
use Doctrine\Common\Inflector\Inflector;

class QueryFilterBuilder
{
    public function build(ModelCriteria $query, array $filters)
    {
        $tableMap = $query->getTableMap();

        foreach ($filters as $key => $value) {
            $hasVirtualFilter = false;
            $virtualFilter = sprintf('filterBy%s', Inflector::camelize($key));
            if (!$tableMap->hasColumn($key)) {
                if (!method_exists($query, $virtualFilter)) {
                    continue;
                }
                $hasVirtualFilter = true;
            }
            
            if (null !== $value && '' !== $value) {
                if ($hasVirtualFilter) {
                    $query->$virtualFilter($value);
                } else {
                    $comparison = Criteria::EQUAL;
                    if ($tableMap->getColumn($key)->isText()) {
                        $query->setIgnoreCase(true);
                        $value = '%'.$value.'%';
                        $comparison = Criteria::LIKE;
                    }

                    $column = call_user_func(array($tableMap, 'translateFieldName'), $key, TableMap::TYPE_FIELDNAME, TableMap::TYPE_PHPNAME);
                    $query->filterBy($column, $value, $comparison);
                }                
            }
        }
    }
}
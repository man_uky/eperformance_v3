<?php

namespace CoreBundle\Util;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SessionFilter
{
    /**
     *
     * @var SessionInterface 
     */
    private $session;
    
    /**
     *
     * @var string 
     */
    private $name;
    
    public function __construct(SessionInterface $session, $name) 
    {
        $this->session = $session;
        $this->name = $name;
    }
   
    /**
     * 
     * @param mixed $value
     */
    public function set($value)
    {
        $this->session->set($this->name, $value);
    }
    
    /**
     * 
     * @param mixed $default
     * @return mixed
     */
    public function get($default = null)
    {
        return $this->session->get($this->name, $default);
    }
}
<?php

namespace CoreBundle\Security\User;

use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;

class UserProxy implements AdvancedUserInterface, EquatableInterface, \Serializable
{
    private $id;
    private $username;
    private $password;
    private $salt;
    private $roles;
    private $credentialId;
    private $enabled;
    private $user;
    private $userClass;
    
    public function __construct(ActiveRecordInterface $userAplikasi)
    {
        $this->user = $userAplikasi;
        $this->copyPropertyValuesFromUser($userAplikasi);
    }

    public function __toString()
    {
        return (string) $this->user;
    }
    
    public function getCurrentUser()
    {
        return $this->user;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getRoles()
    {
        return $this->roles;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function getUsername()
    {
        return $this->username;
    }
    
    public function getCredentialId()
    {
        return $this->credentialId;
    }

    public function eraseCredentials()
    {
    }

    public function isEqualTo(UserInterface $user) 
    {
        if (!$user instanceof UserProxy) {
            return false;
        }

        if ($this->id !== $user->getId()) {
            return false;
        }
        
        if ($this->password !== $user->getPassword()) {
            return false;
        }
        
        if ($this->salt !== $user->getSalt()) {
            return false;
        }

        if ($this->username !== $user->getUsername()) {
            return false;
        }

        if ($this->userClass !== $user->getUserClass()) {
            return false;
        }
        
        return true;
    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->salt,
            $this->enabled,
            $this->userClass
        ));
    }
    
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->username,
            $this->password,
            $this->salt,
            $this->enabled,
            $this->userClass
        ) = unserialize($serialized);
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled() 
    {
        return $this->enabled;
    }
    
    public function getUserClass()
    {
        return $this->userClass;
    }
    
    public function __call($name, $params)
    {
        if (null !== $this->user) {
            $user = $this->user;

            return \call_user_func_array(array($user, $name), $params);
        }

        throw new \PropelException('Call to undefined method: ' . $name);
    }
    
    protected function copyPropertyValuesFromUser($userAplikasi)
    {
        $this->id = $userAplikasi->getPrimaryKey();
        $this->username = $userAplikasi->getUsername();
        $this->password = $userAplikasi->getPassword();
        $this->salt = $userAplikasi->getSalt();
        $this->roles = $userAplikasi->getRoles();
        $this->credentialId = $userAplikasi->getCredentialId();
        $this->enabled = $userAplikasi->isEnabled();
        $this->userClass = get_class($userAplikasi);
        
    }
    
    public function hasRoles($roles, $useAnd = true)
    {
        if (!is_array($roles)) {
            return in_array($roles, $this->roles);
        }

        // now we assume that $roles is an array
        $test = false;

        foreach ($roles as $role) {
            // recursively check the role with a switched AND/OR mode
            $test = $this->isGranted($role, $useAnd ? false : true);

            if ($useAnd) {
                $test = $test ? false : true;
            }

            if ($test) { // either passed one in OR mode or failed one in AND mode
                break; // the matter is settled
            }
        }

        if ($useAnd) { // in AND mode we succeed if $test is false
            $test = $test ? false : true;
        }

        return $test;
    }
}
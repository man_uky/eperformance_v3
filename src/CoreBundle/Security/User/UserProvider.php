<?php

namespace CoreBundle\Security\User;

use CoreBundle\Model\Eperformance\PegawaiQuery;
use CoreBundle\Model\Eperformance\PenggunaMasterQuery;
use Propel\Runtime\ActiveQuery\PropelQuery;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class UserProvider implements UserProviderInterface
{
    public function loadUserByUsername($username) 
    {
        $user = PegawaiQuery::create()->setIgnoreCase(true)->filterByUsername($username)->findOne();
        if (!$user) {
            $user = PenggunaMasterQuery::create()->setIgnoreCase(true)->filterByUsername($username)->findOne();
        }

        if (null === $user) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }
        
        return new UserProxy($user);
    }

    public function refreshUser(UserInterface $user) 
    {
        if (!$user instanceof UserProxy) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return new UserProxy(PropelQuery::from($user->getUserClass())->findPk($user->getId()));
    }

    public function supportsClass($class) 
    {
        return $class === 'CoreBundle\Security\User\UserProxy';
    }
}
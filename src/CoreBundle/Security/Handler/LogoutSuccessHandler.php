<?php

namespace CoreBundle\Security\Handler;

use CoreBundle\Util\Log;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class LogoutSuccessHandler implements LogoutSuccessHandlerInterface
{
    protected $httpUtils;
    protected $targetUrl;
    protected $tokenStorage;
    
    /**
     * @param HttpUtils      $httpUtils
     * @param TokenStorage   $tokenStorage
     * @param string         $targetUrl
     */
    public function __construct(HttpUtils $httpUtils, TokenStorage $tokenStorage, $targetUrl = '/')
    {
        $this->httpUtils = $httpUtils;
        $this->targetUrl = $targetUrl;
        $this->tokenStorage = $tokenStorage;
    }
    
    public function onLogoutSuccess(Request $request) 
    {
        /*$user = $this->get('security.token_storage')->getToken()->getUser();*/
        $user = $this->tokenStorage->getToken()->getUser();
        
        new Log('logout', $user->getUsername(), $user->getCredentialId(), '/logout', 'logout', ' ', $request);
        
        $request->getSession()->clear();
        
        return $this->httpUtils->createRedirectResponse($request, $this->targetUrl);
    }
}
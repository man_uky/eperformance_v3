<?php

namespace CoreBundle\Security\Handler;

use CoreBundle\Util\Log;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;

class AuthenticationSuccessHandler extends DefaultAuthenticationSuccessHandler
{
    public function onAuthenticationSuccess(Request $request, TokenInterface $token) 
    {
        $user = $token->getUser();
        
        new Log('login', $user->getUsername(), $user->getCredentialId(), '/login', 'login', ' ', $request);
        
        return parent::onAuthenticationSuccess($request, $token);
    }
}
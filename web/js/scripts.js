var App = function () {
    var handleStyler = function () {
        var scrollHeight = 'auto';
        jQuery('#theme-change').click(function () {
            if ($(this).attr("opened") && !$(this).attr("opening") && !$(this).attr("closing")) {
                $(this).removeAttr("opened");
                $(this).attr("closing", "1");

                $("#theme-change").css("overflow", "hidden").animate({
                    width: '20px',
                    //height: '22px',
                    'padding-top': '3px'
                }, {
                    complete: function () {
                        $(this).removeAttr("closing");
                        $("#theme-change .settings").hide();
                    }
                });
            } else if (!$(this).attr("closing") && !$(this).attr("opening")) {
                $(this).attr("opening", "1");
                $("#theme-change").css("overflow", "visible").animate({
                    width: '520px',
                    height: scrollHeight,
                    'padding-top': '3px'
                }, {
                    complete: function () {
                        $(this).removeAttr("opening");
                        $(this).attr("opened", 1);
                    }
                });
                $("#theme-change .settings").show();
                
            }
        });


    }
    
    var handleMainMenu = function () {
        jQuery('#sidebar .has-sub > a').click(function () {
            var last = jQuery('.has-sub.open', $('#sidebar'));
            last.removeClass("open");
            jQuery('.arrow', last).removeClass("open");
            jQuery('.sub', last).slideUp(200);
            var sub = jQuery(this).next();
            if (sub.is(":visible")) {
                jQuery('.arrow', jQuery(this)).removeClass("open");
                jQuery(this).parent().removeClass("open");
                sub.slideUp(200);
            } else {
                jQuery('.arrow', jQuery(this)).addClass("open");
                jQuery(this).parent().addClass("open");
                sub.slideDown(200);
            }
        });
    };

    var handleWidgetTools = function () {
        jQuery('.widget .tools .icon-remove').click(function () {
            jQuery(this).parents(".widget").parent().remove();
        });

        jQuery('.widget .tools .icon-chevron-down, .widget .tools .icon-chevron-up').click(function () {
            var el = jQuery(this).parents(".widget").children(".widget-body");
            if (jQuery(this).hasClass("icon-chevron-down")) {
                jQuery(this).removeClass("icon-chevron-down").addClass("icon-chevron-up");
                el.slideUp(200);
            } else {
                jQuery(this).removeClass("icon-chevron-up").addClass("icon-chevron-down");
                el.slideDown(200);
            }
        });
    };

    var handleFixInputPlaceholderForIE = function () {
        //fix html5 placeholder attribute for ie7 & ie8
        if (jQuery.browser.msie && jQuery.browser.version.substr(0, 1) <= 9) { // ie7&ie8
            jQuery('input[placeholder], textarea[placeholder]').each(function () {

                var input = jQuery(this);

                jQuery(input).val(input.attr('placeholder'));

                jQuery(input).focus(function () {
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                    }
                });

                jQuery(input).blur(function () {
                    if (input.val() == '' || input.val() == input.attr('placeholder')) {
                        input.val(input.attr('placeholder'));
                    }
                });
            });
        }
    };

    var handleDeviceWidth = function () {
        function fixWidth(e) {
            var winHeight = $(window).height();
            var winWidth = $(window).width();
            //alert(winWidth);
            //for tablet and small desktops
            if (winWidth < 1125 && winWidth > 767) {
                $(".responsive").each(function () {
                    var forTablet = $(this).attr('data-tablet');
                    var forDesktop = $(this).attr('data-desktop');
                    if (forTablet) {
                        $(this).removeClass(forDesktop);
                        $(this).addClass(forTablet);
                    }

                });
            } else {
                $(".responsive").each(function () {
                    var forTablet = $(this).attr('data-tablet');
                    var forDesktop = $(this).attr('data-desktop');
                    if (forTablet) {
                        $(this).removeClass(forTablet);
                        $(this).addClass(forDesktop);
                    }
                });
            }
        }

        fixWidth();

        running = false;
        jQuery(window).resize(function () {
            if (running == false) {
                running = true;
                setTimeout(function () {
                    // fix layout width
                    fixWidth();
                    
                    // fix event form chosen dropdowns
                    $('#event_priority_chzn').width($('#event_title').width() + 15);
                    $('#event_priority_chzn .chzn-drop').width($('#event_title').width() + 13);

                    $(".chzn-select").val('').trigger("liszt:updated");
                    
                    //finish
                    running = false;
                }, 200); // wait for 200ms on resize event           
            }
        });
    };

    var handleTooltip = function () {
        jQuery('.tooltips').tooltip();
    };

    var handlePopover = function () {
        jQuery('.popovers').popover();
    };

    var handleWysihtml5 = function () {
        if (!jQuery().wysihtml5) {
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5();
        }
    };

    var handleToggleButtons = function () {
        if (!jQuery().toggleButtons) {
            return;
        }
        $('.basic-toggle-button').toggleButtons();
        $('.text-toggle-button').toggleButtons({
            width: 200,
            label: {
                enabled: "Lorem Ipsum",
                disabled: "Dolor Sit"
            }
        });
        $('.danger-toggle-button').toggleButtons({
            style: {
                // Accepted values ["primary", "danger", "info", "success", "warning"] or nothing
                enabled: "danger",
                disabled: "info"
            }
        });
        $('.info-toggle-button').toggleButtons({
            style: {
                enabled: "info",
                disabled: ""
            }
        });
        $('.success-toggle-button').toggleButtons({
            style: {
                enabled: "success",
                disabled: "danger"
            }
        });
        $('.warning-toggle-button').toggleButtons({
            style: {
                enabled: "warning",
                disabled: "success"
            }
        });

        $('.height-toggle-button').toggleButtons({
            height: 100,
            font: {
                'line-height': '100px',
                'font-size': '20px',
                'font-style': 'italic'
            }
        });

        $('.not-animated-toggle-button').toggleButtons({
            animated: false
        });

        $('.transition-value-toggle-button').toggleButtons({
            transitionspeed: 1 // default value: 0.05
        });

    };

    var handleDateTimePickers = function () {

        if (jQuery().daterangepicker) {
            $('.date-range').daterangepicker();
        }

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                format: 'dd/mm/yyyy'
            });
        }
    };

    var handleAccordions = function () {
        $(".accordion").collapse().height('auto');
    };

    var handleScrollers = function () {
        if (!jQuery().slimScroll) {
            return;
        }

        $('.scroller').each(function () {
            $(this).slimScroll({
                //start: $('.blah:eq(1)'),
                height: $(this).attr("data-height"),
                alwaysVisible: ($(this).attr("data-always-visible") == "1" ? true : false),
                railVisible: ($(this).attr("data-rail-visible") == "1" ? true : false),
                disableFadeOut: true
            });
        });
    };

    var handleGoTop = function () {
        /* set variables locally for increased performance */
        jQuery('#footer .go-top').click(function () {
            App.scrollTo();
        });

    };

    // this is optional to use if you want animated show/hide. But plot charts can make the animation slow.
    var handleSidebarTogglerAnimated = function () {

        $('.sidebar-toggler').click(function () {
            if ($('#sidebar > ul').is(":visible") === true) {
                $('#main-content').animate({
                    'margin-left': '25px'
                });
                $('#sidebar').animate({
                    'margin-left': '-220px'
                }, {
                    complete: function () {
                        $('#sidebar > ul').hide();
                        $("#container").addClass("sidebar-closed");
                    }
                });
            } else {
                $('#main-content').animate({
                    'margin-left': '245px'
                });
                $('#sidebar > ul').show();
                $('#sidebar').animate({
                    'margin-left': '0'
                }, {
                    complete: function () {
                        $("#container").removeClass("sidebar-closed");
                    }
                });
            }
        });
    };

    // by default used simple show/hide without animation due to the issue with handleSidebarTogglerAnimated.
    var handleSidebarToggler = function () {

        $('.sidebar-toggler').click(function () {
            if ($('#sidebar > ul').is(":visible") === true) {
//                $('#main-content').css({
//                    'margin-left': '25px'
//                });
//                $('#sidebar').css({
//                    'margin-left': '-190px'
//                });
//                $('#sidebar > ul').hide();
//                $("#container").addClass("sidebar-closed");

                $('#main-content').animate({
                    'margin-left': '25px'
                });
                $('#sidebar').animate({
                    'margin-left': '-190px'
                }, {
                    complete: function () {
                        $('#sidebar > ul').hide();
                        $("#container").addClass("sidebar-closed");
                    }
                });
            } else {
//                $('#main-content').css({
//                    'margin-left': '215px'
//                });
//                $('#sidebar > ul').show();
//                $('#sidebar').css({
//                    'margin-left': '0'
//                });
//                $("#container").removeClass("sidebar-closed");

                $('#main-content').animate({
                    'margin-left': '215px'
                });
                $('#sidebar > ul').show();
                $('#sidebar').animate({
                    'margin-left': '0'
                }, {
                    complete: function () {
                        $("#container").removeClass("sidebar-closed");
                    }
                });
            }
        });
    };

    var handleChoosenSelect = function () {
        if (!jQuery().chosen) {
            return;
        }
        $(".chosen-select").chosen();
        $(".chosen-with-diselect").chosen({
            allow_single_deselect: true
        });
    }
    
    var handleTables = function() {
        jQuery('body').on('change', 'table.table .group-checkable', function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                } else {
                    $(this).attr("checked", false);
                }
            });
            //jQuery.uniform.update(set);
        });

        jQuery('.dataTables_wrapper .dataTables_filter input').addClass("input-mini"); // modify table search input
        jQuery('.dataTables_wrapper .dataTables_length select').addClass("input-mini"); // modify table per page dropdown

    };
    
    var handleChecked = function() {
        $('body').on('change', '.checkboxes', function() {
            if (this.checked) {
                $(this).closest('tr').addClass('success');
            } else {
                $(this).closest('tr').removeClass('success');
            }
        });
    };
    
    var handleDeleteLink = function() {
        $('body').on('click', 'tr a.delete-link', function(e) {
            e.preventDefault();
            var result = confirm('Apakah anda yakin mau menghapus data ini?');
            if (!result) {
                return;
            }
            var url = $(this).data('href');
            var parent = $(this).closest('tr');
            $.ajax({
                type: 'DELETE',
                url: url,
                beforeSend: function() {
                    parent.animate({'backgroundColor':'#fb6c6c'},300);
                },
                success: function() {
                    parent.slideUp(300,function() {
                        parent.remove();
                    });
                }
            });
        });
    };
    return {

        //main function to initiate template pages
        init: function () {
            /*
            if (jQuery.browser.msie && jQuery.browser.version.substr(0, 1) == 8) {
                $('.visible-ie8').show();
            }
            */
            //========================================================================
            var matched, browser;
            jQuery.uaMatch = function( ua ) {
                ua = ua.toLowerCase();

                var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
                    /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
                    /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
                    /(msie)[\s?]([\w.]+)/.exec( ua ) ||       
                    /(trident)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
                    ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
                    [];

                return {
                    browser: match[ 1 ] || "",
                    version: match[ 2 ] || "0"
                };
            };

            matched = jQuery.uaMatch( navigator.userAgent );
            //IE 11+ fix (Trident) 
            matched.browser = matched.browser == 'trident' ? 'msie' : matched.browser;
            browser = {};

            if ( matched.browser ) {
                browser[ matched.browser ] = true;
                browser.version = matched.version;
            }

            // Chrome is Webkit, but Webkit is also Safari.
            if ( browser.chrome ) {
                browser.webkit = true;
            } else if ( browser.webkit ) {
                browser.safari = true;
            }

            jQuery.browser = browser;
            // log removed - adds an extra dependency
            //log(jQuery.browser)
            //=====================================================================================

            handleDeviceWidth(); // handles proper responsive features of the page
            handleWidgetTools();
            handleScrollers(); // handles slim scrolling contents
            handleTooltip(); // handles bootstrap tooltips
            handlePopover(); // handles bootstrap popovers
            handleToggleButtons(); // handles form toogle buttons
            handleWysihtml5(); //handles WYSIWYG Editor 
            handleDateTimePickers(); //handles form timepickers
            handleMainMenu(); // handles main menu
            handleFixInputPlaceholderForIE(); // fixes/enables html5 placeholder attribute for IE9, IE8
            handleGoTop(); //handles scroll to top functionality in the footer
            handleAccordions();
            handleSidebarToggler();
            handleChoosenSelect();
            handleTables();
            handleStyler();
            handleChecked();
            handleDeleteLink();
        },

        rebuildSelectOptions: function(data, $select, emptyOption) {
            if (emptyOption == undefined) {
                emptyOption = '--- Pilih ---';
            }
            $select.empty().append('<option value="">'+ emptyOption +'</option>');
            $.each(data,function(key, value) {
                $select.append('<option value=' + key + '>' + value + '</option>');
            });
        },
    
        // wrapper function to scroll to an element
        scrollTo: function (el) {
            pos = el ? el.offset().top : 0;
            jQuery('html,body').animate({
                scrollTop: pos
            }, 'slow');
        },

        setupDatatableSearch: function (dataTable, swapRow, wrapperTag) {
            //swap row
            if (swapRow !== undefined && swapRow === true) {
                var $trFirst = $('thead tr:first');
                $trFirst.next().after($trFirst);
            }
            
            if (wrapperTag === undefined) {
                wrapperTag = '.dataTables_wrapper';
            }
            
            $(wrapperTag + ' .editable-clear-x').on('click', function () {
                var $self = $(this);
                var $inp = $self.prev();
                $self.hide();
                $inp.val('').removeClass('clearable');
                dataTable.fnFilter('', $inp.data('index'));
            });

            $(wrapperTag + " select").on('change', function() {
                var search = this.value;
                dataTable.fnFilter(search, $(this).data('index'));
            });
            
            var searchWait = 0;
            var searchWaitInterval;
            $(wrapperTag + ' input.input-search')
            .unbind('keypress keyup')
            .bind('keypress keyup', function(e){
                var item = $(this);
                searchWait = 0;
                if(!searchWaitInterval) searchWaitInterval = setInterval(function(){
                    if(searchWait>=3){
                        clearInterval(searchWaitInterval);
                        searchWaitInterval = '';
                        var searchTerm = item.val();
                        if (searchTerm.length === 0) {
                            item.removeClass('clearable');
                            item.nextAll('.editable-clear-x').hide();
                        } else {
                            if (!item.hasClass('clearable')) {
                                item.addClass('clearable');
                                item.nextAll('.editable-clear-x').show();
                            }
                        }
                        dataTable.fnFilter(searchTerm, item.data('index'));
                        searchWait = 0;
                    }
                    searchWait++;
                },200);

            });
        }
    };
}();
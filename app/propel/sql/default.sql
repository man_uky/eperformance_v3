
BEGIN;

-----------------------------------------------------------------------
-- eperformance.user_log
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "eperformance"."user_log" CASCADE;

CREATE TABLE "eperformance"."user_log"
(
    "id" bigserial NOT NULL,
    "parent_id" INT8 NOT NULL,
    "username" VARCHAR(30) NOT NULL,
    "aksi" VARCHAR(50),
    "parameter" VARCHAR(50),
    "ip_address" VARCHAR(50) NOT NULL,
    "credential_id" INT2 NOT NULL,
    "mac_address" VARCHAR(255) NOT NULL,
    "keterangan" VARCHAR(255),
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "eperformance"."user_log"."parent_id" IS 'diisi user_log_id login, supaya ketahuan log sekian melekat pada log in id X.';

CREATE INDEX "user_log_i_f86ef3" ON "eperformance"."user_log" ("username");

CREATE INDEX "user_log_i_889f6e" ON "eperformance"."user_log" ("keterangan");

CREATE INDEX "user_log_i_d404ac" ON "eperformance"."user_log" ("created_at");

-----------------------------------------------------------------------
-- eperformance.pengguna_master
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "eperformance"."pengguna_master" CASCADE;

CREATE TABLE "eperformance"."pengguna_master"
(
    "id" serial NOT NULL,
    "username" VARCHAR(30) NOT NULL,
    "password" VARCHAR(255) NOT NULL,
    "nip" VARCHAR(18),
    "nama" VARCHAR(30) NOT NULL,
    "is_aktif" BOOLEAN DEFAULT 'f',
    "credential_id" INT2 NOT NULL,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    "deleted_at" TIMESTAMP,
    PRIMARY KEY ("id"),
    CONSTRAINT "pengguna_master_u_f86ef3" UNIQUE ("username")
);

CREATE INDEX "pengguna_master_i_c66ff6" ON "eperformance"."pengguna_master" ("nip");

CREATE INDEX "pengguna_master_i_d37f29" ON "eperformance"."pengguna_master" ("nama");

CREATE INDEX "pengguna_master_i_f37e13" ON "eperformance"."pengguna_master" ("is_aktif");

-----------------------------------------------------------------------
-- eperformance.pegawai_status
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "eperformance"."pegawai_status" CASCADE;

CREATE TABLE "eperformance"."pegawai_status"
(
    "id" serial NOT NULL,
    "nama" VARCHAR(10),
    PRIMARY KEY ("id")
);

-----------------------------------------------------------------------
-- eperformance.credential
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "eperformance"."credential" CASCADE;

CREATE TABLE "eperformance"."credential"
(
    "id" serial NOT NULL,
    "nama" VARCHAR(50),
    "is_aktif" BOOLEAN DEFAULT 'f',
    PRIMARY KEY ("id"),
    CONSTRAINT "credential_u_d37f29" UNIQUE ("nama")
);

-----------------------------------------------------------------------
-- eperformance.jabatan
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "eperformance"."jabatan" CASCADE;

CREATE TABLE "eperformance"."jabatan"
(
    "id" serial NOT NULL,
    "nama" VARCHAR(80) NOT NULL,
    "unit_master_id" INT2 NOT NULL,
    "urut" INT2 NOT NULL,
    "is_staf" BOOLEAN DEFAULT 'f',
    "is_jfu" BOOLEAN DEFAULT 'f',
    "is_jft" BOOLEAN DEFAULT 'f',
    "is_staf_ahli" BOOLEAN DEFAULT 'f',
    "is_asisten" BOOLEAN DEFAULT 'f',
    "is_sekda" BOOLEAN DEFAULT 'f',
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    "deleted_at" TIMESTAMP,
    PRIMARY KEY ("id"),
    CONSTRAINT "jabatan_u_43f547" UNIQUE ("urut")
);

CREATE INDEX "jabatan_i_d37f29" ON "eperformance"."jabatan" ("nama");

CREATE INDEX "jabatan_i_56e646" ON "eperformance"."jabatan" ("unit_master_id");

CREATE INDEX "jabatan_i_43f547" ON "eperformance"."jabatan" ("urut");

CREATE INDEX "jabatan_i_4829fe" ON "eperformance"."jabatan" ("is_staf");

CREATE INDEX "jabatan_i_b7a34c" ON "eperformance"."jabatan" ("is_jfu");

CREATE INDEX "jabatan_i_66195c" ON "eperformance"."jabatan" ("is_jft");

CREATE INDEX "jabatan_i_52ad6f" ON "eperformance"."jabatan" ("is_staf_ahli");

CREATE INDEX "jabatan_i_d77859" ON "eperformance"."jabatan" ("is_asisten");

CREATE INDEX "jabatan_i_5031d0" ON "eperformance"."jabatan" ("is_sekda");

-----------------------------------------------------------------------
-- eperformance.pangkat
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "eperformance"."pangkat" CASCADE;

CREATE TABLE "eperformance"."pangkat"
(
    "id" serial NOT NULL,
    "nama" VARCHAR(50),
    "golongan" VARCHAR(5),
    "ruang" VARCHAR(5),
    PRIMARY KEY ("id"),
    CONSTRAINT "pangkat_u_d37f29" UNIQUE ("nama")
);

CREATE INDEX "pangkat_i_d37f29" ON "eperformance"."pangkat" ("nama");

CREATE INDEX "pangkat_i_c7db2e" ON "eperformance"."pangkat" ("golongan");

CREATE INDEX "pangkat_i_5ae0a1" ON "eperformance"."pangkat" ("ruang");

-----------------------------------------------------------------------
-- eperformance.pegawai
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "eperformance"."pegawai" CASCADE;

CREATE TABLE "eperformance"."pegawai"
(
    "id" serial NOT NULL,
    "username" VARCHAR(15) NOT NULL,
    "password" VARCHAR(255) NOT NULL,
    "kode_rapor" VARCHAR(50) NOT NULL,
    "credential_id" INT2 NOT NULL,
    "pegawai_master_id" INTEGER NOT NULL,
    "unit_master_id" INT2 NOT NULL,
    "atasan_id" INTEGER,
    "pangkat_id" INT2 NOT NULL,
    "jabatan_id" INT2 NOT NULL,
    "pegawai_status_id" INT2 NOT NULL,
    "eselon" INT2 DEFAULT 0,
    "level" INT2 NOT NULL,
    "is_aktif" BOOLEAN DEFAULT 'f',
    "is_trantib" BOOLEAN DEFAULT 'f',
    "is_lapangan" BOOLEAN DEFAULT 'f',
    "tanggal_aktif" DATE,
    "tanggal_nonaktif" DATE,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    "deleted_at" TIMESTAMP,
    PRIMARY KEY ("id"),
    CONSTRAINT "pegawai_u_89d874" UNIQUE ("username","pegawai_master_id","unit_master_id","pangkat_id","jabatan_id")
);

CREATE INDEX "pegawai_i_8213a5" ON "eperformance"."pegawai" ("level");

CREATE INDEX "pegawai_i_01bdb1" ON "eperformance"."pegawai" ("tanggal_aktif");

CREATE INDEX "pegawai_i_0f3006" ON "eperformance"."pegawai" ("tanggal_nonaktif");

-----------------------------------------------------------------------
-- eperformance.pegawai_master
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "eperformance"."pegawai_master" CASCADE;

CREATE TABLE "eperformance"."pegawai_master"
(
    "id" serial NOT NULL,
    "nip" VARCHAR(18) NOT NULL,
    "nama" VARCHAR(30) NOT NULL,
    "unit_master_id" INT2 NOT NULL,
    "alamat" VARCHAR(100) NOT NULL,
    "email" VARCHAR(30) NOT NULL,
    "telp" VARCHAR(15) NOT NULL,
    "is_aktif" BOOLEAN DEFAULT 'f',
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    "deleted_at" TIMESTAMP,
    PRIMARY KEY ("id"),
    CONSTRAINT "pegawai_master_u_c66ff6" UNIQUE ("nip")
);

CREATE INDEX "pegawai_master_i_d37f29" ON "eperformance"."pegawai_master" ("nama");

CREATE INDEX "pegawai_master_i_f37e13" ON "eperformance"."pegawai_master" ("is_aktif");

-----------------------------------------------------------------------
-- eperformance.unit_master
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "eperformance"."unit_master" CASCADE;

CREATE TABLE "eperformance"."unit_master"
(
    "id" serial NOT NULL,
    "kode" VARCHAR(4) NOT NULL,
    "nama" VARCHAR(100) NOT NULL,
    "is_sekretariat_daerah" BOOLEAN DEFAULT 'f' NOT NULL,
    "is_dinas" BOOLEAN DEFAULT 'f' NOT NULL,
    "is_badan" BOOLEAN DEFAULT 'f' NOT NULL,
    "is_rumah_sakit" BOOLEAN DEFAULT 'f' NOT NULL,
    "is_bagian" BOOLEAN DEFAULT 'f' NOT NULL,
    "is_kecamatan" BOOLEAN DEFAULT 'f' NOT NULL,
    "is_kelurahan" BOOLEAN DEFAULT 'f' NOT NULL,
    "parent_id" INT2,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    "deleted_at" TIMESTAMP,
    PRIMARY KEY ("id"),
    CONSTRAINT "unit_master_u_7725d4" UNIQUE ("kode","nama")
);

CREATE INDEX "unit_master_i_e390f3" ON "eperformance"."unit_master" ("is_sekretariat_daerah");

CREATE INDEX "unit_master_i_eea0ee" ON "eperformance"."unit_master" ("is_dinas");

CREATE INDEX "unit_master_i_00c7e1" ON "eperformance"."unit_master" ("is_badan");

CREATE INDEX "unit_master_i_a248f3" ON "eperformance"."unit_master" ("is_rumah_sakit");

CREATE INDEX "unit_master_i_44f2e3" ON "eperformance"."unit_master" ("is_bagian");

CREATE INDEX "unit_master_i_c908c3" ON "eperformance"."unit_master" ("is_kecamatan");

CREATE INDEX "unit_master_i_748dbb" ON "eperformance"."unit_master" ("is_kelurahan");

ALTER TABLE "eperformance"."user_log" ADD CONSTRAINT "user_log_fk_06e207"
    FOREIGN KEY ("credential_id")
    REFERENCES "eperformance"."credential" ("id");

ALTER TABLE "eperformance"."pengguna_master" ADD CONSTRAINT "pengguna_master_fk_06e207"
    FOREIGN KEY ("credential_id")
    REFERENCES "eperformance"."credential" ("id");

ALTER TABLE "eperformance"."jabatan" ADD CONSTRAINT "jabatan_fk_15b2db"
    FOREIGN KEY ("unit_master_id")
    REFERENCES "eperformance"."unit_master" ("id");

ALTER TABLE "eperformance"."pegawai" ADD CONSTRAINT "pegawai_fk_06e207"
    FOREIGN KEY ("credential_id")
    REFERENCES "eperformance"."credential" ("id");

ALTER TABLE "eperformance"."pegawai" ADD CONSTRAINT "pegawai_fk_9d2d33"
    FOREIGN KEY ("pegawai_master_id")
    REFERENCES "eperformance"."pegawai_master" ("id");

ALTER TABLE "eperformance"."pegawai" ADD CONSTRAINT "pegawai_fk_15b2db"
    FOREIGN KEY ("unit_master_id")
    REFERENCES "eperformance"."unit_master" ("id");

ALTER TABLE "eperformance"."pegawai" ADD CONSTRAINT "pegawai_fk_6af918"
    FOREIGN KEY ("atasan_id")
    REFERENCES "eperformance"."pegawai" ("id");

ALTER TABLE "eperformance"."pegawai" ADD CONSTRAINT "pegawai_fk_2e4948"
    FOREIGN KEY ("pangkat_id")
    REFERENCES "eperformance"."pangkat" ("id");

ALTER TABLE "eperformance"."pegawai" ADD CONSTRAINT "pegawai_fk_370212"
    FOREIGN KEY ("jabatan_id")
    REFERENCES "eperformance"."jabatan" ("id");

ALTER TABLE "eperformance"."pegawai" ADD CONSTRAINT "pegawai_fk_64e9ef"
    FOREIGN KEY ("pegawai_status_id")
    REFERENCES "eperformance"."pegawai_status" ("id");

ALTER TABLE "eperformance"."pegawai_master" ADD CONSTRAINT "pegawai_master_fk_15b2db"
    FOREIGN KEY ("unit_master_id")
    REFERENCES "eperformance"."unit_master" ("id");

ALTER TABLE "eperformance"."unit_master" ADD CONSTRAINT "unit_master_fk_f3b8a6"
    FOREIGN KEY ("parent_id")
    REFERENCES "eperformance"."unit_master" ("id");

COMMIT;

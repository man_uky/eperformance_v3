
BEGIN;

-----------------------------------------------------------------------
-- eperformance.skp_perilaku_kerja_hasil
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "eperformance"."skp_perilaku_kerja_hasil" CASCADE;

CREATE TABLE "eperformance"."skp_perilaku_kerja_hasil"
(
    "id" serial NOT NULL,
    "pegawai_id" INTEGER,
    "skp_perilaku_kerja_tes_id" INTEGER,
    "skp_perilaku_kerja_aspek_id" INT2,
    "mean" NUMERIC,
    "standar_deviasi" NUMERIC,
    "icc" NUMERIC,
    "rsp" NUMERIC,
    "rsrk" NUMERIC,
    "spk" NUMERIC,
    PRIMARY KEY ("id")
);

CREATE INDEX "skp_perilaku_kerja_hasil_i_a545cf" ON "eperformance"."skp_perilaku_kerja_hasil" ("pegawai_id","skp_perilaku_kerja_tes_id","skp_perilaku_kerja_aspek_id");

-----------------------------------------------------------------------
-- eperformance.skp_perilaku_kerja_aspek
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "eperformance"."skp_perilaku_kerja_aspek" CASCADE;

CREATE TABLE "eperformance"."skp_perilaku_kerja_aspek"
(
    "id" serial NOT NULL,
    "nama" VARCHAR(50),
    "is_for_staf" BOOLEAN DEFAULT 't',
    "status" INT2,
    "keterangan" TEXT,
    PRIMARY KEY ("id")
);

CREATE INDEX "skp_perilaku_kerja_aspek_i_cbc78b" ON "eperformance"."skp_perilaku_kerja_aspek" ("status","is_for_staf");

-----------------------------------------------------------------------
-- eperformance.skp_perilaku_kerja_tes
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "eperformance"."skp_perilaku_kerja_tes" CASCADE;

CREATE TABLE "eperformance"."skp_perilaku_kerja_tes"
(
    "id" serial NOT NULL,
    "tahun" INT2,
    "triwulan" INT2,
    "bulan" INT2,
    "skpd_id" INT2,
    "status" INT2,
    PRIMARY KEY ("id")
);

CREATE INDEX "skp_perilaku_kerja_tes_i_af460f" ON "eperformance"."skp_perilaku_kerja_tes" ("tahun","skpd_id","triwulan","bulan","status");

-----------------------------------------------------------------------
-- eperformance.pegawai
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "eperformance"."pegawai" CASCADE;

CREATE TABLE "eperformance"."pegawai"
(
    "id" serial NOT NULL,
    "nip" VARCHAR(25),
    "nama" VARCHAR(60),
    "golongan" VARCHAR(50),
    "tmt" DATE,
    "pendidikan_terakhir" VARCHAR(256),
    "skpd_id" INTEGER,
    "atasan_id" INTEGER,
    "is_atasan" BOOLEAN DEFAULT 'f',
    "level_struktural" INT2,
    "password" VARCHAR(36),
    "user_id" INTEGER,
    "username_eproject" VARCHAR(255),
    "tgl_masuk" DATE,
    "tgl_keluar" DATE,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    "deleted_at" TIMESTAMP,
    "is_out" BOOLEAN DEFAULT 'f',
    "is_ka_uptd" BOOLEAN DEFAULT 'f',
    "is_struktural" BOOLEAN DEFAULT 'f',
    "is_kunci_aktifitas" BOOLEAN DEFAULT 't',
    "is_only_skp" BOOLEAN DEFAULT 'f',
    "jabatan_struktural_id" INTEGER DEFAULT ,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "eperformance"."pegawai"."tmt" IS 'terhitung mulai tanggal -> tgl masuk';

COMMENT ON COLUMN "eperformance"."pegawai"."tgl_masuk" IS 'tgl masuk SKPD baru';

COMMENT ON COLUMN "eperformance"."pegawai"."tgl_keluar" IS 'tgl keluar';

COMMENT ON COLUMN "eperformance"."pegawai"."is_struktural" IS 'digunakan untuk pegawai kecamatan';

CREATE INDEX "pegawai_i_827ace" ON "eperformance"."pegawai" ("nip","skpd_id","atasan_id","jabatan_struktural_id","level_struktural","is_atasan","is_only_skp","deleted_at");

-----------------------------------------------------------------------
-- eperformance.indikator_kinerja
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "eperformance"."indikator_kinerja" CASCADE;

CREATE TABLE "eperformance"."indikator_kinerja"
(
    "id" serial NOT NULL,
    "pegawai_id" INTEGER,
    "nama" TEXT NOT NULL,
    "deskripsi" TEXT,
    "status" INT2,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    "deleted_at" TIMESTAMP,
    "skpd_id" INTEGER,
    "target" VARCHAR(15),
    "satuan" VARCHAR(30),
    "target_kemarin" NUMERIC,
    "cara_perhitungan" TEXT,
    "capaian_akhir" VARCHAR(15),
    "jenis" INT2,
    "indikator_id_atasan" INTEGER,
    "is_tambahan" BOOLEAN DEFAULT 'f',
    "is_tahun_lalu" BOOLEAN DEFAULT 'f',
    "is_tarik_ebudgeting" BOOLEAN DEFAULT 'f',
    "is_perhitungan_terbalik" BOOLEAN DEFAULT 'f',
    "is_realisasi_angka_prediksi" BOOLEAN DEFAULT 'f',
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "eperformance"."indikator_kinerja"."status" IS '0: belum disetujui';

COMMENT ON COLUMN "eperformance"."indikator_kinerja"."jenis" IS '1=IKU, 2=IKK, 3=IKT, 4=IKS';

CREATE INDEX "indikator_kinerja_i_893c7e" ON "eperformance"."indikator_kinerja" ("pegawai_id","skpd_id","deleted_at","status","jenis");

-----------------------------------------------------------------------
-- eperformance.master_skpd
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "eperformance"."master_skpd" CASCADE;

CREATE TABLE "eperformance"."master_skpd"
(
    "skpd_id" serial NOT NULL,
    "skpd_kode" VARCHAR(8) NOT NULL,
    "skpd_nama" VARCHAR(128) NOT NULL,
    "deleted_at" TIMESTAMP,
    PRIMARY KEY ("skpd_id")
);

CREATE INDEX "master_skpd_i_bf0032" ON "eperformance"."master_skpd" ("skpd_kode","deleted_at");

-----------------------------------------------------------------------
-- eperformance.jabatan_struktural
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "eperformance"."jabatan_struktural" CASCADE;

CREATE TABLE "eperformance"."jabatan_struktural"
(
    "id" INTEGER NOT NULL,
    "nama" VARCHAR(50),
    "eselon" VARCHAR(5),
    "jenis_indikator_kinerja" INT2,
    "skpd_id" INTEGER,
    "nomer" INT2,
    "is_asisten" BOOLEAN,
    "nomer_urut" INT2,
    PRIMARY KEY ("id")
);

CREATE INDEX "jabatan_struktural_i_357a32" ON "eperformance"."jabatan_struktural" ("skpd_id","nomer_urut");

ALTER TABLE "eperformance"."skp_perilaku_kerja_hasil" ADD CONSTRAINT "skp_perilaku_kerja_hasil_fk_9d9ee4"
    FOREIGN KEY ("pegawai_id")
    REFERENCES "eperformance"."pegawai" ("id");

ALTER TABLE "eperformance"."skp_perilaku_kerja_hasil" ADD CONSTRAINT "skp_perilaku_kerja_hasil_fk_fc16af"
    FOREIGN KEY ("skp_perilaku_kerja_tes_id")
    REFERENCES "eperformance"."skp_perilaku_kerja_tes" ("id");

ALTER TABLE "eperformance"."skp_perilaku_kerja_hasil" ADD CONSTRAINT "skp_perilaku_kerja_hasil_fk_8b79fb"
    FOREIGN KEY ("skp_perilaku_kerja_aspek_id")
    REFERENCES "eperformance"."skp_perilaku_kerja_aspek" ("id");

ALTER TABLE "eperformance"."skp_perilaku_kerja_tes" ADD CONSTRAINT "skp_perilaku_kerja_tes_fk_52cecd"
    FOREIGN KEY ("skpd_id")
    REFERENCES "eperformance"."master_skpd" ("skpd_id");

ALTER TABLE "eperformance"."pegawai" ADD CONSTRAINT "pegawai_fk_52cecd"
    FOREIGN KEY ("skpd_id")
    REFERENCES "eperformance"."master_skpd" ("skpd_id");

ALTER TABLE "eperformance"."pegawai" ADD CONSTRAINT "pegawai_fk_6af918"
    FOREIGN KEY ("atasan_id")
    REFERENCES "eperformance"."pegawai" ("id");

ALTER TABLE "eperformance"."pegawai" ADD CONSTRAINT "pegawai_fk_f79eb3"
    FOREIGN KEY ("jabatan_struktural_id")
    REFERENCES "eperformance"."jabatan_struktural" ("id");

ALTER TABLE "eperformance"."indikator_kinerja" ADD CONSTRAINT "indikator_kinerja_fk_9d9ee4"
    FOREIGN KEY ("pegawai_id")
    REFERENCES "eperformance"."pegawai" ("id");

ALTER TABLE "eperformance"."indikator_kinerja" ADD CONSTRAINT "indikator_kinerja_fk_52cecd"
    FOREIGN KEY ("skpd_id")
    REFERENCES "eperformance"."master_skpd" ("skpd_id");

ALTER TABLE "eperformance"."indikator_kinerja" ADD CONSTRAINT "indikator_kinerja_fk_41ea2f"
    FOREIGN KEY ("indikator_id_atasan")
    REFERENCES "eperformance"."indikator_kinerja" ("id");

ALTER TABLE "eperformance"."jabatan_struktural" ADD CONSTRAINT "jabatan_struktural_fk_52cecd"
    FOREIGN KEY ("skpd_id")
    REFERENCES "eperformance"."master_skpd" ("skpd_id");

COMMIT;
